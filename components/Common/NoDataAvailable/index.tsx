import React from 'react';
import Grid from '@mui/material/Grid';
import Box from '@mui/material/Box';
import StickyNote2SharpIcon from '@mui/icons-material/StickyNote2Sharp';
import Typography from '@mui/material/Typography';

interface INoDataProps {
  actions?: React.ReactNode;
  content: string | React.ReactNode;
  icon?: React.ReactNode;
}

const NoDataAvailable = (props: INoDataProps) => {
  const { actions, content, icon } = props;

  return (
    <Grid container justifyContent="center" sx={{ bgcolor: 'divider', py: 4 }}>
      <Grid item xs={9} sm={5}>
        <Box sx={{ display: 'flex', justifyContent: 'center', my: 2 }}>
          {icon || (
            <StickyNote2SharpIcon
              sx={{
                typography: 'h1',
                color: 'text.disabled',
                textAlign: 'center',
              }}
            />
          )}
        </Box>

        {typeof content === 'string' ? (
          <Typography color="textSecondary" textAlign="center">
            {content}
          </Typography>
        ) : (
          content
        )}

        {actions && <Box sx={{ display: 'flex', justifyContent: 'center', my: 2 }}>{actions}</Box>}
      </Grid>
    </Grid>
  );
};

export default NoDataAvailable;
