import React, { useState } from 'react';
import { useQuery } from '@tanstack/react-query';
import Button from '@mui/lab/LoadingButton';
import Box from '@mui/material/Box';
import Dialog from '../../Common/Dialog';
import TextField from '../../Common/FormInputs/TextField';
import NoDataAvailable from '../../Common/NoDataAvailable';
import UserSelectionList from '../../Users/SelectionList';
import { IUser } from '../../../models/user';
import { getAllDepartments } from '../../../services/departments';
import Autocomplete from '@mui/material/Autocomplete';
import Typography from '@mui/material/Typography';
import { getDepartmentName } from '../../../models/department';
import { findUserDepartmentAssignments } from '../../../services/userDepartmentAssignment';
import { createOrUpdateSSMInitialTrainingSession } from '../../../services/ssmInitialTrainingSessions';
import { useNotificationContext } from '../../../context/notificationContext';

const FETCH_QUERY_KEY = ['departments-all'];
const FETCH_ASSIGNMENT_KEY = ['user-departments-ass'];

interface ISSMInitialTrainingFormProps {
  onClose: () => void;
}

const SSMInitialTrainingForm = (props: ISSMInitialTrainingFormProps) => {
  const [departmentId, setDepartment] = useState<string | null>(null);
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const [inputValue, setInputValue] = useState('');
  const [selectedUsers, setSelectedUsers] = useState<IUser[]>([]);
  const { setError } = useNotificationContext();
  const { onClose } = props;

  const { data: departments } = useQuery(FETCH_QUERY_KEY, getAllDepartments);

  const { data: userDepartmentAssignments } = useQuery(
    [FETCH_ASSIGNMENT_KEY, { departmentId }],
    // @ts-ignore
    () => findUserDepartmentAssignments({ departmentId }),
    {
      enabled: !!departmentId,
    },
  );

  const onSubmit = async () => {
    setIsLoading(true);

    let currentUser = null;
    try {
      for (const user of selectedUsers) {
        currentUser = user.fullName;

        await createOrUpdateSSMInitialTrainingSession({
          // @ts-ignore
          userId: user.userId,
          // @ts-ignore
          userDepartmentAssignmentId: user._id,
        });
      }

      onClose();
    } catch (error) {
      setError(`Eroare la crearea instruirii pentru ${currentUser}`);
    }
  };

  return (
    <>
      <Dialog
        open={true}
        maxWidth="md"
        onClose={onClose}
        title="Adaugă instruire la locul de muncă"
        actions={
          <Button
            onClick={onSubmit}
            loading={isLoading}
            disabled={!selectedUsers?.length}
            variant="contained"
          >
            Adaugă
          </Button>
        }
        content={
          <>
            <Box
              sx={{
                bgcolor: 'background.default',
                p: 3,
              }}
            >
              <Typography gutterBottom>Selectează departamentul</Typography>
              {departments?.length ? (
                <Autocomplete
                  value={departmentId}
                  onChange={(event: any, newValue: string | null) => {
                    setDepartment(newValue);
                  }}
                  inputValue={inputValue}
                  onInputChange={(event, newInputValue) => {
                    setInputValue(newInputValue);
                  }}
                  // @ts-ignore
                  options={departments?.map((o) => o._id)}
                  // @ts-ignore
                  getOptionLabel={(o) => {
                    // @ts-ignore
                    const option = departments.find((op) => op._id === o);
                    // @ts-ignore
                    return getDepartmentName(option || {});
                  }}
                  renderInput={(params) => <TextField {...params} />}
                />
              ) : null}
            </Box>

            <Box sx={{ p: 3 }}>
              <Typography gutterBottom>Selectează persoanele instruite</Typography>
              {userDepartmentAssignments?.results?.length ? (
                <UserSelectionList
                  users={userDepartmentAssignments?.results?.map((assignment) => ({
                    // @ts-ignore
                    ...assignment.userId,
                    _id: assignment._id,
                    // @ts-ignore
                    userId: assignment.userId._id,
                  }))}
                  selectedUsers={selectedUsers}
                  onSelectUser={(user) => setSelectedUsers([...selectedUsers, user])}
                  onRemoveUser={(index) => {
                    const newSelectedUsers = [...selectedUsers];
                    newSelectedUsers.splice(index, 1);

                    setSelectedUsers(newSelectedUsers);
                  }}
                />
              ) : (
                <NoDataAvailable content="Nu s-a găsit niciun utilizator comform căutării" />
              )}
            </Box>
          </>
        }
      />
    </>
  );
};

export default SSMInitialTrainingForm;
