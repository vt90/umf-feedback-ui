import { IBaseModel } from './base';

export interface IDepartment extends IBaseModel {
  code: string;
  name: string;
  parentId: string | null;
  childDepartments?: IDepartment[];
}

export interface IHierarchyDepartment extends IDepartment {
  children?: IHierarchyDepartment[];
}

export interface IHierarchyTreeItem {
  _id: string;
  name: string;
  department: IDepartment;
}

export interface IHierarchy {
  [key: string]: IHierarchyDepartment | IHierarchy;
}

export const getDepartmentName = (department: IDepartment): string => {
  return `${department.code} - ${department.name}`;
};
