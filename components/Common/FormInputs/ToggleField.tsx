import React from 'react';
import { Control, Controller } from 'react-hook-form';
import { ISelectFieldOption } from './SelectField';
import { ToggleButtonGroupProps } from '@mui/material/ToggleButtonGroup';
import Tab from '@mui/material/Tab';
import Tabs from '@mui/material/Tabs';

interface IToggleFieldProps extends Omit<ToggleButtonGroupProps, 'onChange' | 'value'> {
  name: string;
  defaultValue: any;
  control: Control;
  options: ISelectFieldOption[];
  [key: string]: any;
}

const ToggleField = (props: IToggleFieldProps, ref: any) => {
  const { name, defaultValue, control, options, sx, ...rest } = props;

  return (
    <Controller
      name={name}
      control={control}
      defaultValue={defaultValue}
      render={({ field }) => {
        const { value, onChange } = field;

        const handleChange = (_: React.MouseEvent<HTMLElement>, newValue: string) => {
          onChange(newValue);
        };

        return (
          <Tabs
            value={value}
            // @ts-ignore
            onChange={handleChange}
            sx={{
              flexDirection: {
                xs: 'column',
                md: 'row',
              },
              ...sx,
            }}
            {...rest}
          >
            {options.map((option) => {
              return (
                <Tab
                  key={option.value}
                  disabled={!!rest.disabled}
                  // @ts-ignore
                  value={option.value}
                  sx={{ px: 1.5, py: 1.5, minHeight: 0, minWidth: 150 }}
                  label={option.name}
                />
              );
            })}
          </Tabs>
        );
      }}
    />
  );
};

export default React.forwardRef(ToggleField);
