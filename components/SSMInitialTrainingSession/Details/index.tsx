import React from 'react';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import { IFunctionModel } from '../../../models/function';
import { ISSMPackage } from '../../../models/ssmPackage';
import { ISSMPeriodicTrainingSession } from '../../../models/ssmPeriodicTrainingSession';
import SSMPackageList from '../../SSMPackages/List';
import NoDataAvailable from '../../Common/NoDataAvailable';
import SSMTrainingSessionEventList from '../Event/List';

interface ISSMTrainingSessionDetailsProps {
  session: ISSMPeriodicTrainingSession;
  ssmPackages: ISSMPackage[];
  functions: IFunctionModel[];
}

const SSMTrainingSessionDetails = (props: ISSMTrainingSessionDetailsProps) => {
  const { session, ssmPackages, functions } = props;

  return (
    <>
      {session?.ssmPeriodicTrainingId?.shortDescription && (
        <Box sx={{ mb: 3 }}>
          <Typography color="textSecondary">
            {session?.ssmPeriodicTrainingId?.shortDescription}
          </Typography>
        </Box>
      )}

      {ssmPackages?.length ? (
        <Box sx={{ mb: 3 }}>
          <Typography sx={{ mb: 3 }}>
            <strong>Pachete de instruire</strong>
          </Typography>
          <SSMPackageList
            // @ts-ignore
            ssmPackages={ssmPackages}
            // @ts-ignore
            functions={functions}
          />
        </Box>
      ) : (
        <NoDataAvailable content="În momentul de faţă nu există niciun pachet de instruire periodca disponibil pe platformă" />
      )}

      {/* @ts-ignore */}
      <SSMTrainingSessionEventList session={session} />
    </>
  );
};

export default SSMTrainingSessionDetails;
