import React, { useCallback, useMemo } from 'react';
import Card from '@mui/material/Card';
import Typography from '@mui/material/Typography';
import Link from 'next/link';
import Tooltip from '@mui/material/Tooltip';
import isBoolean from 'lodash/isBoolean';
import Status from '../../Common/Status';
import EnhancedTable from '../../Common/Table';
import { IEvaluation } from '../../../models/evaluation';
import moment from 'moment';
import NoDataAvailable from '../../Common/NoDataAvailable';
import Button from '@mui/material/Button';
import Box from '@mui/material/Box';
import YesIcon from '@mui/icons-material/CheckBox';
import NoIcon from '@mui/icons-material/DisabledByDefault';
import WaitingIcon from '@mui/icons-material/HourglassBottom';
import { getSurveyRoleName, SURVEY_ROLE } from '../../../models/survey';

interface IUserUserListProps {
  evaluations: IEvaluation[];
}

const UserEvaluationsList = (props: IUserUserListProps) => {
  const { evaluations } = props;

  const headerRows = useMemo(() => {
    return [
      { id: 'name', label: 'Sesiune', align: 'left', colSpan: 3 },
      { id: 'role', label: 'Tip', align: 'left', colSpan: 2 },
      { id: 'department', label: 'Departament', align: 'left', colSpan: 2 },
      { id: 'result', label: 'Rezultat', align: 'left', colSpan: 1 },
      { id: 'salaryChangePercentage', label: '% Modificare Salariala', align: 'left', colSpan: 1 },
      { id: 'userAgreement', label: 'Opinie evaluat', align: 'center', colSpan: 1 },
      { id: 'contraEvaluated', label: 'Contra-evaluată', align: 'center', colSpan: 1 },
      { id: 'userAcknowledged', label: 'Luată la cunoștință', align: 'center', colSpan: 1 },
      { id: 'actions', label: 'Acțiuni', align: 'right', colSpan: 1 },
    ];
  }, []);

  const renderIcon = useCallback(
    (value: boolean | null | undefined, yesText: string, noText?: string) => {
      const color = isBoolean(value) ? (value ? 'success' : 'error') : 'info';
      const tooltipText = isBoolean(value)
        ? value
          ? yesText
          : noText || 'Respinsă'
        : 'În așteptare';

      const iconProps = {
        sx: {
          color: `${color}.light`,
        },
      };

      return (
        <Tooltip title={tooltipText}>
          <Box>
            {isBoolean(value) ? (
              value ? (
                // @ts-ignore
                <YesIcon {...iconProps} />
              ) : (
                // @ts-ignore
                <NoIcon {...iconProps} />
              )
            ) : (
              // @ts-ignore
              <WaitingIcon {...iconProps} />
            )}
          </Box>
        </Tooltip>
      );
    },
    [],
  );

  const getHeaderRows = useCallback(() => {
    return evaluations.map((evaluation) => {
      const {
        _id,
        createdAt,
        departmentId,
        evaluationSessionId,
        finalGrade,
        userDepartmentAssignmentId,
        contraEvaluated,
        userAcknowledged,
        userAgreement,
        role,
        salaryChangePercentage,
      } = evaluation;

      // @ts-ignore
      const linkUrl = `/evaluation-sessions/${evaluationSessionId?._id}/result/${userDepartmentAssignmentId?._id}?resultId=${_id}`;

      const renderLink = (children: React.ReactNode) => (
        <Link href={linkUrl}>
          <a>{children}</a>
        </Link>
      );

      // @ts-ignore
      const renderName = renderLink(
        <Typography>
          {/* @ts-ignore */}
          {evaluationSessionId?.name}
          <br />
          <Typography color="textSecondary" variant="caption">
            {moment(createdAt).format('YYYY.MM.DD')}
          </Typography>
        </Typography>,
      );

      const renderRole = (role: string) => {
        const color = role === SURVEY_ROLE.EVALUATION ? 'info' : 'success';
        return <Status label={getSurveyRoleName(role)} color={color} sx={{ minWidth: 130 }} />;
      };

      const result = evaluation?.finalResult;

      const shouldShowAction = !userAcknowledged || (!isBoolean(userAgreement) && !contraEvaluated);

      return {
        _id,
        smallDeviceHeader: renderName,
        name: renderName,
        contraEvaluated: renderIcon(contraEvaluated ? true : null, 'Contra-evaluată', 'Nu'),
        userAcknowledged: renderIcon(userAcknowledged, 'Luată la cunoștință'),
        userAgreement: renderIcon(userAgreement, 'Acceptată'),
        role: renderRole(`${role}`),
        department: (
          // @ts-ignore
          <Tooltip title={userDepartmentAssignmentId?.role}>
            <Typography variant="body2" color="textSecondary">
              {/* @ts-ignore */}
              {departmentId?.name}
            </Typography>
          </Tooltip>
        ),
        actions: shouldShowAction
          ? renderLink(
              <Button variant="contained" size="small">
                Vizualizează
              </Button>,
            )
          : '-',
        // @ts-ignore
        result: result ? `${finalGrade.toFixed(2)} - ${result}` : '-',
        salaryChangePercentage: salaryChangePercentage ? `${salaryChangePercentage}%` : '-',
        data: evaluation,
      };
    });
  }, [evaluations]);

  if (!evaluations?.length) {
    return (
      <Card sx={{ p: 3 }}>
        <NoDataAvailable content="În momentul de faţă nu există niciun formular de evaluare disponibil pe platformă" />
      </Card>
    );
  }

  return (
    <>
      <EnhancedTable
        headerRows={headerRows}
        initialOrderByField="name"
        initialOrder="desc"
        orderByFieldCriteriaFunctions={{
          name: (row: any) => row?.data && row.data.createdAt,
          department: (row: any) => row?.data && row.data.departmentId.name,
          result: (row: any) => evaluations?.[row._id]?.finalGrade,
          userAgreement: (row: any) => evaluations?.[row._id]?.userAgreement,
          salaryChangePercentage: (row: any) => evaluations?.[row._id]?.salaryChangePercentage,
          contraEvaluated: (row: any) => evaluations?.[row._id]?.contraEvaluated,
        }}
        PaperComponent={Card}
        paperComponentStyles={{
          px: 3,
          py: 2,
          '& h6': {
            mt: 1,
          },
        }}
        rows={getHeaderRows()}
        smallUntilBreakpoint="md"
      />
    </>
  );
};

export default UserEvaluationsList;
