import React, { useCallback, useMemo } from 'react';
import Typography from '@mui/material/Typography';
import EnhancedTable from '../../../Common/Table';
import Link from 'next/link';
import { IExtendedUserDepartmentAssignment } from '../../../../models/userDepartmentAssignment';
import Tooltip from '@mui/material/Tooltip';
import isBoolean from 'lodash/isBoolean';
import YesIcon from '@mui/icons-material/CheckBox';
import NoIcon from '@mui/icons-material/DisabledByDefault';
import WaitingIcon from '@mui/icons-material/HourglassBottom';
import Box from '@mui/material/Box';
import {
  canEvaluate,
  canSalaryChange,
  IEvaluationSession,
} from '../../../../models/evaluationSession';
import Button from '@mui/material/Button';

interface IWaitingUserListProps {
  evaluations?: any;
  initialOrder?: string;
  initialOrderBy?: string;
  title: string;
  users: IExtendedUserDepartmentAssignment[];
  salaryChangesByEvaluatedUser: any;
  evaluationSession: IEvaluationSession;
}

const EvaluationSessionWaitingUserList = (props: IWaitingUserListProps) => {
  const {
    evaluations,
    initialOrder = 'ASC',
    initialOrderBy = 'department',
    title,
    users,
    evaluationSession,
    salaryChangesByEvaluatedUser,
  } = props;

  const headerRows = useMemo(() => {
    return [
      { id: 'name', label: 'Denumire', align: 'left', colSpan: 3 },
      { id: 'email', label: 'Marcă', align: 'left', colSpan: 1 },
      { id: 'department', label: 'Departament', align: 'left', colSpan: 2 },
      { id: 'result', label: 'Rezultat', align: 'left', colSpan: 1 },
      { id: 'salaryChange', label: '% Modificare Salariala', align: 'left', colSpan: 1 },
      { id: 'userAgreement', label: 'Opinie evaluat', align: 'center', colSpan: 1 },
      { id: 'contraEvaluated', label: 'Contra-evaluată', align: 'center', colSpan: 1 },
      { id: 'userAcknowledged', label: 'Luată la cunoștință', align: 'center', colSpan: 1 },
      { id: 'actions', label: 'Acțiuni', align: 'right', colSpan: 2 },
    ];
  }, []);

  const renderIcon = useCallback(
    (value: boolean | null | undefined, yesText: string, noText?: string) => {
      const color = isBoolean(value) ? (value ? 'success' : 'error') : 'info';
      const tooltipText = isBoolean(value)
        ? value
          ? yesText
          : noText || 'Respinsă'
        : 'În așteptare';

      const iconProps = {
        sx: {
          color: `${color}.light`,
        },
      };

      return (
        <Tooltip title={tooltipText}>
          <Box>
            {isBoolean(value) ? (
              value ? (
                // @ts-ignore
                <YesIcon {...iconProps} />
              ) : (
                // @ts-ignore
                <NoIcon {...iconProps} />
              )
            ) : (
              // @ts-ignore
              <WaitingIcon {...iconProps} />
            )}
          </Box>
        </Tooltip>
      );
    },
    [],
  );

  const getHeaderRows = useCallback(() => {
    return users
      .filter(
        (user) =>
          // @ts-ignore
          canEvaluate(evaluationSession, user) || canSalaryChange(evaluationSession, user),
      )
      .map((user) => {
        const renderLink = (url: string, children: React.ReactNode) => (
          <Link href={url}>
            <a>{children}</a>
          </Link>
        );

        const renderName = <Typography>{user.userId.fullName} </Typography>;

        const {
          allowEvaluations,
          allowSalaryChanges,
          hasMinimumEvaluationValueForSalaryChange,
          isOpen,
        } = evaluationSession;

        return {
          _id: user._id,
          smallDeviceHeader: renderName,
          name: renderName,
          email: user?.userId?.email,
          contraEvaluated: '-',
          userAcknowledged: '-',
          userAgreement: '-',
          department: (
            <Tooltip title={user.role}>
              <Typography variant="body2" color="textSecondary">
                {user.departmentId.name}
              </Typography>
            </Tooltip>
          ),
          role: user.role,
          result: '-',
          salaryChange: '-',
          actions:
            allowEvaluations || allowSalaryChanges ? (
              <>
                {/* @ts-ignore */}
                {allowEvaluations && isOpen && canEvaluate(evaluationSession, user)
                  ? renderLink(
                      `/evaluation-sessions/${evaluationSession._id}/evaluate/${user._id}`,
                      <Button variant="contained" size="small" sx={{ minWidth: '140px', mb: 1 }}>
                        Evaluează
                      </Button>,
                    )
                  : null}

                {allowSalaryChanges &&
                !salaryChangesByEvaluatedUser[user._id] &&
                // @ts-ignore
                canSalaryChange(evaluationSession, user) &&
                !hasMinimumEvaluationValueForSalaryChange &&
                isOpen
                  ? renderLink(
                      `/evaluation-sessions/${evaluationSession._id}/salary-change/${user._id}`,
                      <Button
                        variant="contained"
                        color="secondary"
                        size="small"
                        sx={{ minWidth: '140px' }}
                      >
                        Modificare salariala
                      </Button>,
                    )
                  : null}
              </>
            ) : (
              '-'
            ),
          data: user,
        };
      });
  }, [evaluations, users, renderIcon]);

  return (
    <>
      <EnhancedTable
        headerRows={headerRows}
        initialOrderByField={initialOrderBy}
        initialOrder={initialOrder}
        orderByFieldCriteriaFunctions={{
          name: (row: any) => row?.data && row.data.userId.fullName,
          department: (row: any) => row?.data && row.data.departmentId.name,
          result: (row: any) => evaluations?.[row._id]?.finalGrade,
          userAgreement: (row: any) => evaluations?.[row._id]?.userAgreement,
          contraEvaluatorAgreement: (row: any) => evaluations?.[row._id]?.contraEvaluatorAgreement,
        }}
        PaperComponent={Box}
        paperComponentStyles={{
          border: '0.0625rem solid rgb(222, 226, 230)',
          borderRadius: 2,
          px: 3,
          py: 2,
          '& h6': {
            mt: 1,
          },
        }}
        rows={getHeaderRows()}
        smallUntilBreakpoint="md"
        title={title}
      />
    </>
  );
};

export default EvaluationSessionWaitingUserList;
