import { IBaseModel } from './base';
import { ISurvey } from './survey';
import { isNumber } from 'lodash';
import {
  IUserDepartmentAssignment,
  USER_DEP_ASSIGNMENT_STUDY_LEVELS,
  USER_DEP_ASSIGNMENT_STUDY_TYPE,
  USER_DEP_ASSIGNMENT_TYPE,
} from './userDepartmentAssignment';
import { IFunctionModel } from './function';

export interface IEvaluationSessionConfigurationEntry {
  allowEvaluations: boolean;
  allowSalaryChanges: boolean;
}
export interface IEvaluationSessionConfiguration {
  [key: string]: IEvaluationSessionConfigurationEntry;
}

export interface IEvaluationSession extends IBaseModel {
  name: string;
  description: string;
  type: number;
  isOpen: boolean;
  allowEvaluations: boolean;
  allowSalaryChanges: boolean;
  hasMinimumEvaluationValueForSalaryChange?: boolean;
  minimumEvaluationValueForSalaryChange?: number;
  startDate: Date;
  endDate: Date;
  surveys: ISurvey[];
  privateUsersOnly?: boolean;
  privateUsers?: string[];
  privateUsersOperation?: string;
  sessionConfigurationType: string;
  configurations: IEvaluationSessionConfiguration;
}

export const EVALUATION_SESSION_TYPE = {
  Anual: 0,
  Intermediar: 1,
  Exit: 2,
};

export const shouldShowSalaryChange = (
  evaluationSession?: IEvaluationSession,
  result?: number | string,
): boolean => {
  return !!(
    evaluationSession?.allowSalaryChanges &&
    result &&
    (!evaluationSession?.hasMinimumEvaluationValueForSalaryChange ||
      (evaluationSession?.hasMinimumEvaluationValueForSalaryChange &&
        isNumber(evaluationSession?.minimumEvaluationValueForSalaryChange) &&
        parseFloat(`${result}`) >= evaluationSession.minimumEvaluationValueForSalaryChange))
  );
};

export const canEvaluate = (
  evaluationSession: IEvaluationSession,
  assignment: IUserDepartmentAssignment,
): boolean => {
  // @ts-ignore
  return !!evaluationSession?.configurations?.[
    // @ts-ignore
    assignment?.[evaluationSession?.sessionConfigurationType]
  ]?.allowEvaluations;
};

export const canSalaryChange = (
  evaluationSession: IEvaluationSession,
  assignment: IUserDepartmentAssignment,
): boolean => {
  // @ts-ignore
  return !!evaluationSession?.configurations?.[
    // @ts-ignore
    assignment?.[evaluationSession?.sessionConfigurationType]
    // @ts-ignore
  ]?.allowSalaryChanges;
};

/**
 * TIP	          Funcții	                Nivel studii	                Grad/ treaptă profesională	  Interval	Procent 	Interval	Procent
 * Auxiliar	      Fucții de conducere	    superioare	                  -	                            4,75 - 5,00	30%	    4,51 - 4,74%	15% x
 * Auxiliar	      Funcții de execuție	    superioare	                  I/ IA	                        4,75 - 5,00	25%	    4,51 - 4,74%	15% x
 * Auxiliar	      Funcții de execuție	    superioare	                  II	                          4,75 - 5,00	20%	    4,51 - 4,74%	10% x
 * Auxiliar	      Funcții de execuție	    superioare	                  III	                          4,75 - 5,00	15%	    4,51 - 4,74%	10% x
 * Auxiliar	      Funcții de execuție	    superioare de scurtă durată	  I	                            4,75 - 5,00	20%	    4,51 - 4,74%	10%
 * Auxiliar	      Funcții de execuție	    superioare de scurtă durată	  II	                          4,75 - 5,00	15%	    4,51 - 4,74%	10%
 * Auxiliar	      Funcții de execuție	    superioare de scurtă durată	  III	                          4,75 - 5,00	10%	    4,51 - 4,74%	5%
 * Auxiliar	      Funcții de execuție	    studii medii	                I/ IA	                        4,75 - 5,00	20%	    4,51 - 4,74%	10%
 * Auxiliar	      Funcții de execuție	    studii medii	                II	                          4,75 - 5,00	15%	    4,51 - 4,74%	10%
 * Auxiliar	      Funcții de execuție	    studii medii	                III	                          4,75 - 5,00	10%	    4,51 - 4,74%	5%
 * Administrativ	Funcții de execuție	    studii medii sau generale	    -	                            4,75 - 5,00	20%	    4,51 - 4,74%	10% x
 * */
export const getSalaryChangePercentage = (
  userDepartmentAssignment: IUserDepartmentAssignment,
  functions: IFunctionModel[],
  finalGrade: number,
) => {
  const functionInfo = functions?.find((f) => f._id === userDepartmentAssignment?.jobFunction);

  if (finalGrade > 4.5) {
    if (finalGrade >= 4.75) {
      if (userDepartmentAssignment.type === USER_DEP_ASSIGNMENT_TYPE.ADMIN && functionInfo) {
        if (functionInfo.isLead) {
          if (userDepartmentAssignment.studyType === USER_DEP_ASSIGNMENT_STUDY_TYPE.SUPERIOR) {
            return 30;
          }
        } else {
          if (userDepartmentAssignment.studyType === USER_DEP_ASSIGNMENT_STUDY_TYPE.SUPERIOR) {
            if (
              userDepartmentAssignment.studyLevel === USER_DEP_ASSIGNMENT_STUDY_LEVELS.I ||
              userDepartmentAssignment.studyLevel === USER_DEP_ASSIGNMENT_STUDY_LEVELS.IA
            ) {
              return 25;
            }
            if (userDepartmentAssignment.studyLevel === USER_DEP_ASSIGNMENT_STUDY_LEVELS.II) {
              return 20;
            }
            if (userDepartmentAssignment.studyLevel === USER_DEP_ASSIGNMENT_STUDY_LEVELS.III) {
              return 15;
            }
          }

          if (userDepartmentAssignment.studyType === USER_DEP_ASSIGNMENT_STUDY_TYPE.SUP_SHORT) {
            if (userDepartmentAssignment.studyLevel === USER_DEP_ASSIGNMENT_STUDY_LEVELS.I) {
              return 20;
            }
            if (userDepartmentAssignment.studyLevel === USER_DEP_ASSIGNMENT_STUDY_LEVELS.II) {
              return 15;
            }
            if (userDepartmentAssignment.studyLevel === USER_DEP_ASSIGNMENT_STUDY_LEVELS.III) {
              return 10;
            }
          }

          if (userDepartmentAssignment.studyType === USER_DEP_ASSIGNMENT_STUDY_TYPE.MEDIUM) {
            if (
              userDepartmentAssignment.studyLevel === USER_DEP_ASSIGNMENT_STUDY_LEVELS.I ||
              userDepartmentAssignment.studyLevel === USER_DEP_ASSIGNMENT_STUDY_LEVELS.IA
            ) {
              return 15;
            }
            if (userDepartmentAssignment.studyLevel === USER_DEP_ASSIGNMENT_STUDY_LEVELS.II) {
              return 12;
            }
            if (userDepartmentAssignment.studyLevel === USER_DEP_ASSIGNMENT_STUDY_LEVELS.III) {
              return 10;
            }
          }
        }
      }

      if (
        userDepartmentAssignment.type === USER_DEP_ASSIGNMENT_TYPE.ADMINISTRATIV &&
        functionInfo &&
        !functionInfo?.isLead &&
        userDepartmentAssignment.studyType === USER_DEP_ASSIGNMENT_STUDY_TYPE.GENERAL
      ) {
        return 20;
      }

      if (
        userDepartmentAssignment.type === USER_DEP_ASSIGNMENT_TYPE.SUPERIOR &&
        functionInfo &&
        functionInfo?.isLead
      ) {
        return 30;
      }

      if (
        (userDepartmentAssignment.type === USER_DEP_ASSIGNMENT_TYPE.RESEARCH ||
          userDepartmentAssignment.type === USER_DEP_ASSIGNMENT_TYPE.SUPPORT) &&
        functionInfo &&
        !functionInfo?.isLead
      ) {
        return 25;
      }
    }

    if (userDepartmentAssignment.type === USER_DEP_ASSIGNMENT_TYPE.ADMIN && functionInfo) {
      if (functionInfo.isLead) {
        if (userDepartmentAssignment.studyType === USER_DEP_ASSIGNMENT_STUDY_TYPE.SUPERIOR) {
          return 15;
        }
      } else {
        if (userDepartmentAssignment.studyType === USER_DEP_ASSIGNMENT_STUDY_TYPE.SUPERIOR) {
          if (
            userDepartmentAssignment.studyLevel === USER_DEP_ASSIGNMENT_STUDY_LEVELS.I ||
            userDepartmentAssignment.studyLevel === USER_DEP_ASSIGNMENT_STUDY_LEVELS.IA
          ) {
            return 15;
          }
          if (userDepartmentAssignment.studyLevel === USER_DEP_ASSIGNMENT_STUDY_LEVELS.II) {
            return 10;
          }
          if (userDepartmentAssignment.studyLevel === USER_DEP_ASSIGNMENT_STUDY_LEVELS.III) {
            return 10;
          }
        }

        if (userDepartmentAssignment.studyType === USER_DEP_ASSIGNMENT_STUDY_TYPE.SUP_SHORT) {
          if (userDepartmentAssignment.studyLevel === USER_DEP_ASSIGNMENT_STUDY_LEVELS.I) {
            return 10;
          }
          if (userDepartmentAssignment.studyLevel === USER_DEP_ASSIGNMENT_STUDY_LEVELS.II) {
            return 10;
          }
          if (userDepartmentAssignment.studyLevel === USER_DEP_ASSIGNMENT_STUDY_LEVELS.III) {
            return 5;
          }
        }

        if (userDepartmentAssignment.studyType === USER_DEP_ASSIGNMENT_STUDY_TYPE.MEDIUM) {
          if (
            userDepartmentAssignment.studyLevel === USER_DEP_ASSIGNMENT_STUDY_LEVELS.I ||
            userDepartmentAssignment.studyLevel === USER_DEP_ASSIGNMENT_STUDY_LEVELS.IA
          ) {
            return 10;
          }
          if (userDepartmentAssignment.studyLevel === USER_DEP_ASSIGNMENT_STUDY_LEVELS.II) {
            return 10;
          }
          if (userDepartmentAssignment.studyLevel === USER_DEP_ASSIGNMENT_STUDY_LEVELS.III) {
            return 5;
          }
        }
      }
    }

    if (
      userDepartmentAssignment.type === USER_DEP_ASSIGNMENT_TYPE.ADMINISTRATIV &&
      functionInfo &&
      !functionInfo?.isLead &&
      userDepartmentAssignment.studyType === USER_DEP_ASSIGNMENT_STUDY_TYPE.GENERAL
    ) {
      return 10;
    }

    if (
      userDepartmentAssignment.type === USER_DEP_ASSIGNMENT_TYPE.SUPERIOR &&
      functionInfo &&
      functionInfo?.isLead
    ) {
      return 15;
    }

    if (
      (userDepartmentAssignment.type === USER_DEP_ASSIGNMENT_TYPE.RESEARCH ||
        userDepartmentAssignment.type === USER_DEP_ASSIGNMENT_TYPE.SUPPORT) &&
      functionInfo &&
      !functionInfo?.isLead
    ) {
      return 15;
    }
  }
  return 0;
};
