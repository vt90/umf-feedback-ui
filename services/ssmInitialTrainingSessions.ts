import axios from 'axios';
import { IPaginationResult } from '../models/base';
import { ISSMInitialTrainingSession } from '../models/ssmInitialTrainingSession';
import { IEvaluation } from '../models/evaluation';
import { ISSMPackage } from '../models/ssmPackage';

const createInstance = (baseURL = '/api/ssm-initial-training-session') => {
  const httpInstance = axios.create({
    baseURL,
  });

  httpInstance.interceptors.response.use(
    (response) => response.data,
    (error) => {
      if (error && error.response && error.response.data) {
        return Promise.reject(
          error.response.data.message
            ? `${error.response.data.message || error.response.data.error}`
            : error.response.data?.error || error.response.data,
        );
      } else {
        return Promise.reject({ message: 'Server error' });
      }
    },
  );

  return httpInstance;
};

const httpInstance = createInstance();

const cleanupSSMInitialTrainingSessionModelBody = (
  ssmInitialTraining: ISSMInitialTrainingSession,
): ISSMInitialTrainingSession => {
  // @ts-ignore
  delete ssmInitialTraining._id;
  // @ts-ignore
  delete ssmInitialTraining.id;
  // @ts-ignore
  delete ssmInitialTraining.displayName;
  // @ts-ignore
  delete ssmInitialTraining.createdAt;
  // @ts-ignore
  delete ssmInitialTraining.updatedAt;
  // @ts-ignore
  delete ssmInitialTraining['__v'];

  return ssmInitialTraining;
};

export const createOrUpdateSSMInitialTrainingSession = async (
  ssmInitialTrainingSession: Partial<ISSMInitialTrainingSession>,
): Promise<ISSMInitialTrainingSession> => {
  const isUpdate = !!ssmInitialTrainingSession._id;
  const url = isUpdate ? `/${ssmInitialTrainingSession._id}` : ``;
  const req = isUpdate ? httpInstance.patch : httpInstance.post;

  // @ts-ignore
  return await req(url, cleanupSSMInitialTrainingSessionModelBody(ssmInitialTrainingSession));
};

export const findSSMInitialTrainingSessions = async (
  params: Partial<ISSMInitialTrainingSession>,
): Promise<IPaginationResult<ISSMInitialTrainingSession>> => {
  return await httpInstance.get('', { params });
};

export const getUserInitialTrainingSession = async (
  userId: string,
): Promise<ISSMInitialTrainingSession> => {
  return await httpInstance.get('/my-training-session');
};

export const acknowledgeInitial = async (
  id: string,
  ssmPackages: ISSMPackage[],
  type: string,
): Promise<IEvaluation> => {
  return await httpInstance.post(`/acknowledge/${id}`, { ssmPackages, type });
};

export const contraSignInitial = async (id: string, type: string): Promise<IEvaluation> => {
  return await httpInstance.post(`/contra-sign/${id}`, { type });
};

export const ssmSignInitial = async (id: string, type: string): Promise<IEvaluation> => {
  return await httpInstance.post(`/ssm-sign/${id}`, { type });
};

export const acceptAtWork = async (id: string): Promise<IEvaluation> => {
  return await httpInstance.post(`/accept-at-work/${id}`);
};
