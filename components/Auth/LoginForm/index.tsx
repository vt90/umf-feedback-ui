import React from 'react';
import Button from '@mui/material/Button';
import Box from '@mui/material/Box';
import TextField from '@mui/material/TextField';
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import * as yup from 'yup';

interface ILoginFormProps {
  csrfToken: string;
}

const schema = yup
  .object({
    email: yup.string().required(),
    password: yup.string().required(),
  })
  .required();

const LoginForm = (props: ILoginFormProps) => {
  const { csrfToken } = props;
  const {
    register,
    formState: { errors },
  } = useForm({
    defaultValues: { email: '', password: '' },
    mode: 'onTouched',
    resolver: yupResolver(schema),
    shouldUnregister: true,
  });

  return (
    <form method="post" action="/api/auth/callback/credentials">
      <input name="csrfToken" type="hidden" value={csrfToken} />
      <input name="callbackUrl" type="hidden" value={process.env.NEXT_PUBLIC_BASE_URL} />

      <Box sx={{ mb: 2 }}>
        <TextField
          fullWidth
          error={!!errors.email}
          label="Marca"
          required={true}
          helperText={!!errors.email && 'Email invalid'}
          defaultValue=""
          {...register('email')}
        />
      </Box>
      <Box sx={{ mb: 2 }}>
        <TextField
          fullWidth
          error={!!errors.password}
          label="Parola"
          required={true}
          helperText={!!errors.password && 'Parola invalida'}
          defaultValue=""
          type="password"
          {...register('password')}
        />
      </Box>

      <Box sx={{ display: 'flex', justifyContent: 'flex-end' }}>
        <Button
          variant="contained"
          size="large"
          type="submit"
          disabled={!!Object.keys(errors).length}
        >
          Login
        </Button>
      </Box>
    </form>
  );
};

export default LoginForm;
