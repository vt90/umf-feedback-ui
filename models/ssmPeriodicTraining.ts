import { IBaseModel } from './base';

export interface ISSMPeriodicTraining extends IBaseModel {
  name: string;
  shortDescription: string;
  functions: string[];
  departments: string[];
  excludedDepartments: string[];
  monthsOfYear: string[];
  isActive: boolean;
  specificForDepartments: boolean;
}

export const TRAINING_MONTHS = [
  'IAN',
  'FEB',
  'MAR',
  'APR',
  'MAI',
  'IUN',
  'IUL',
  'AUG',
  'SEP',
  'OCT',
  'NOI',
  'DEC',
];

export const TRAINING_MONTHS_EXTENDED = [
  'Ianuarie',
  'Februarie',
  'Martie',
  'Aprilie',
  'Mai',
  'Iunie',
  'Iulie',
  'August',
  'Septembrie',
  'Octombrie',
  'Noiembrie',
  'Decembrie',
];
