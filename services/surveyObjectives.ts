import axios from 'axios';
import { ISurveyObjective } from '../models/surveyObjective';
import { IPaginationResult } from '../models/base';

const httpInstance = axios.create({
  baseURL: '/api/survey-objectives',
});

httpInstance.interceptors.response.use(
  (response) => response.data,
  (error) => {
    if (error && error.response && error.response.data) {
      return Promise.reject(
        error.response.data.message
          ? `${error.response.data.message || error.response.data.error}`
          : error.response.data?.error || error.response.data,
      );
    } else {
      return Promise.reject({ message: 'Server error' });
    }
  },
);

const cleanupSurveyObjectiveBody = (
  surveyObjective: Partial<ISurveyObjective>,
): Partial<ISurveyObjective> => {
  // @ts-ignore
  delete surveyObjective._id;
  // @ts-ignore
  delete surveyObjective.id;
  // @ts-ignore
  delete surveyObjective['__v'];

  if (surveyObjective?.questions) {
    surveyObjective.questions = surveyObjective.questions.map((question) => {
      if (!question._id) {
        // @ts-ignore
        delete question._id;
      }

      return question;
    });
  }

  return surveyObjective;
};

const cleanupBulkSurveyObjectiveBody = (
  bulkUpdateSurveyObjectives: IBulkUpdateSurveyObjectives,
): IBulkUpdateSurveyObjectives => {
  bulkUpdateSurveyObjectives.surveyObjectives = bulkUpdateSurveyObjectives.surveyObjectives.map(
    (chapter) => {
      const surveyObjective = { ...chapter };
      // @ts-ignore
      delete surveyObjective.id;
      // @ts-ignore
      delete surveyObjective['__v'];

      if (!surveyObjective._id) {
        // @ts-ignore
        delete surveyObjective._id;
      }

      if (surveyObjective?.questions) {
        surveyObjective.questions = surveyObjective.questions.map((question) => {
          if (!question._id) {
            // @ts-ignore
            delete question._id;
          }

          return question;
        });
      }

      return surveyObjective;
    },
  );

  return bulkUpdateSurveyObjectives;
};

export const getSurveyObjectives = async (
  params: Partial<ISurveyObjective>,
): Promise<IPaginationResult<ISurveyObjective>> => {
  return await httpInstance.get('', { params });
};

export const getSurveyObjectiveById = async (id: string): Promise<ISurveyObjective> => {
  return await httpInstance.get(`/${id}`);
};

export const createOrUpdateSurveyObjective = async (
  surveyObjective: Partial<ISurveyObjective>,
): Promise<ISurveyObjective> => {
  const isUpdate = !!surveyObjective._id;
  const url = isUpdate ? `/${surveyObjective._id}` : ``;
  const req = isUpdate ? httpInstance.patch : httpInstance.post;
  return await req(url, cleanupSurveyObjectiveBody(surveyObjective));
};

interface IBulkUpdateSurveyObjectives {
  surveyId: string;
  surveyObjectives: Partial<ISurveyObjective>[];
}

export const bulkCreateOrUpdateSurveyObjectives = async (
  bulkUpdateSurveyObjectives: IBulkUpdateSurveyObjectives,
): Promise<void> => {
  return await httpInstance.post(
    `/bulk-update`,
    cleanupBulkSurveyObjectiveBody(bulkUpdateSurveyObjectives),
  );
};

export const deleteSurveyObjective = async (id: string): Promise<any> => {
  return await httpInstance.delete(`/${id}`);
};
