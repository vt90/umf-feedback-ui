import React, { useCallback, useMemo, useState } from 'react';
import Head from 'next/head';
import { withPageAuthRequired } from '../../guards/authRequired';
import {
  createOrUpdateEvaluationSession,
  deleteEvaluationSession,
  getEvaluationSessions,
} from '../../services/evaluationSessions';
import { useQuery, useMutation, useQueryClient } from '@tanstack/react-query';
import Button from '@mui/material/Button';
import AdminPage from '../../components/AdminPage';
import EvaluationSessionsForm from '../../components/EvaluationSessions/Form';
import EvaluationSessionsList from '../../components/EvaluationSessions/List';
import ListLoadingIndicator from '../../components/Common/ListLoadingIndcator';
import { EVALUATION_SESSION_TYPE, IEvaluationSession } from '../../models/evaluationSession';
import LoadingButton from '@mui/lab/LoadingButton';
import Typography from '@mui/material/Typography';
import Dialog from '../../components/Common/Dialog';
import NoDataAvailable from '../../components/Common/NoDataAvailable';
import AddIcon from '@mui/icons-material/Add';
import { SELECTED_ACTIONS } from '../../lib/constants';
import { useNotificationContext } from '../../context/notificationContext';
import { useAuthContext } from '../../context/authContext';
import { USER_DEP_ASSIGNMENT_TYPE } from '../../models/userDepartmentAssignment';
import { getAllFunctions } from '../../services/functions';

const FETCH_QUERY_KEY = ['evaluationSessions'];

const FETCH_FUNCTIONS_QUERY_KEY = ['depFunctions'];

const isUMF = process.env.NEXT_PUBLIC_IS_UMF === 'true';

const EvaluationSessions = () => {
  const [selectedEvaluationSession, setSelectedEvaluationSession] =
    useState<Partial<IEvaluationSession> | null>(null);
  const [selectedAction, setSelectedAction] = useState<string | null>(null);
  const { setError, setMessage } = useNotificationContext();
  const { user } = useAuthContext();

  const onEvaluationSessionAction = useCallback(
    (action: string) => (evaluationSession: Partial<IEvaluationSession>) => {
      setSelectedEvaluationSession({
        ...evaluationSession,
        // @ts-ignore
        privateUsersInfo: [...(evaluationSession?.privateUsers || [])],
        // @ts-ignore
        privateUsers: evaluationSession?.privateUsers?.map((userId) => ({
          // @ts-ignore
          value: typeof userId === 'string' ? userId : userId._id,
        })),
      });
      setSelectedAction(action);
    },
    [setSelectedEvaluationSession, setSelectedAction],
  );

  const resetSelectedEvaluationSessionState = () => {
    setSelectedEvaluationSession(null);
    setSelectedAction(null);
  };

  const queryClient = useQueryClient();

  const { isLoading, data: evaluationSessionsData } = useQuery(FETCH_QUERY_KEY, () =>
    getEvaluationSessions({}),
  );

  const { data: functions } = useQuery(FETCH_FUNCTIONS_QUERY_KEY, getAllFunctions);

  const { isLoading: isUpdating, mutate: updateEvaluationSession } = useMutation(
    createOrUpdateEvaluationSession,
    {
      onSuccess: async () => {
        await queryClient.invalidateQueries(FETCH_QUERY_KEY);
        resetSelectedEvaluationSessionState();
        setMessage('Sesiune salvata');
      },
      onError: setError,
    },
  );

  const { isLoading: isDeleting, mutate: deleteEvaluationSessionById } = useMutation(
    deleteEvaluationSession,
    {
      onSuccess: async () => {
        await queryClient.invalidateQueries(FETCH_QUERY_KEY);
        resetSelectedEvaluationSessionState();
        setMessage('Sesiune stearsa');
      },
      onError: setError,
    },
  );

  const createButton = useMemo(
    () =>
      // @ts-ignore
      user?.canManageSessions && (
        <Button
          onClick={() => {
            onEvaluationSessionAction(SELECTED_ACTIONS.CREATE_OR_UPDATE)({
              allowEvaluations: true,
              allowSalaryChanges: true,
              hasMinimumEvaluationValueForSalaryChange: true,
              minimumEvaluationValueForSalaryChange: 4.51,
              type: EVALUATION_SESSION_TYPE.Anual,
              privateUsersOnly: false,
              privateUsers: [],
              sessionConfigurationType: 'type',
              configurations: {
                [USER_DEP_ASSIGNMENT_TYPE.ASSIST]: {
                  allowEvaluations: true,
                  allowSalaryChanges: true,
                },
                [USER_DEP_ASSIGNMENT_TYPE.SUPERIOR]: {
                  allowEvaluations: true,
                  allowSalaryChanges: true,
                },
                [USER_DEP_ASSIGNMENT_TYPE.ADMINISTRATIV]: {
                  allowEvaluations: true,
                  allowSalaryChanges: true,
                },
                [USER_DEP_ASSIGNMENT_TYPE.PROF]: {
                  allowEvaluations: true,
                  allowSalaryChanges: true,
                },
                ...(isUMF && {
                  [USER_DEP_ASSIGNMENT_TYPE.ADMIN || 'Auxiliar']: {
                    allowEvaluations: true,
                    allowSalaryChanges: true,
                  },
                  [USER_DEP_ASSIGNMENT_TYPE.RESEARCH || 'Cercetător']: {
                    allowEvaluations: true,
                    allowSalaryChanges: true,
                  },
                }),
              },
            });
          }}
          variant="contained"
          endIcon={<AddIcon />}
        >
          Adaugă Sesiune
        </Button>
      ),
    [user, onEvaluationSessionAction],
  );

  return (
    <>
      <Head>
        <title>Sesiuni de evaluare</title>
      </Head>

      <AdminPage
        title="Sesiuni de evaluare"
        pages={[{ url: '/evaluation-sessions', name: 'Sesiuni de evaluare' }]}
        actions={createButton}
      >
        {evaluationSessionsData?.results?.length || isLoading ? (
          <>
            {isLoading && <ListLoadingIndicator />}

            {evaluationSessionsData?.results && (
              <EvaluationSessionsList
                baseUrl="/evaluation-sessions"
                evaluationSessions={evaluationSessionsData?.results}
                // @ts-ignore
                onSelectEvaluationSession={
                  // @ts-ignore
                  user?.canManageSessions
                    ? onEvaluationSessionAction(SELECTED_ACTIONS.CREATE_OR_UPDATE)
                    : null
                }
                // onDeleteEvaluationSession={onEvaluationSessionAction(SELECTED_ACTIONS.DELETE)}
              />
            )}
          </>
        ) : (
          <NoDataAvailable
            actions={createButton}
            content="În momentul de faţă nu există nicio sesiune de evaluare disponibil pe platformă"
          />
        )}

        <Dialog
          actions={
            <LoadingButton
              variant="contained"
              onClick={() =>
                selectedEvaluationSession &&
                deleteEvaluationSessionById(selectedEvaluationSession._id as string)
              }
              loading={isDeleting}
            >
              Șterge
            </LoadingButton>
          }
          content={
            <>
              <Typography color="textSecondary" textAlign="center">
                Sunteți siguri că doriți să ștergeți sesiunea de evaluare
                <strong>
                  <i>&quot;{selectedEvaluationSession?.name}&quot;</i>
                </strong>
                ?
              </Typography>
            </>
          }
          title="Confirmati ștergerea?"
          onClose={resetSelectedEvaluationSessionState}
          open={!!(selectedEvaluationSession && selectedAction === SELECTED_ACTIONS.DELETE)}
        />

        {selectedEvaluationSession &&
          selectedAction === SELECTED_ACTIONS.CREATE_OR_UPDATE &&
          functions?.results && (
            <EvaluationSessionsForm
              initialValues={selectedEvaluationSession}
              functions={functions.results}
              isLoading={isUpdating}
              open={true}
              onClose={resetSelectedEvaluationSessionState}
              onSubmit={updateEvaluationSession}
            />
          )}
      </AdminPage>
    </>
  );
};

export const getServerSideProps = withPageAuthRequired();

export default EvaluationSessions;
