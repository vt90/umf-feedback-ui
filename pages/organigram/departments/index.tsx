import React, { useMemo, useState } from 'react';
import Head from 'next/head';
import { withPageAuthRequired } from '../../../guards/authRequired';
import { useMutation, useQuery, useQueryClient } from '@tanstack/react-query';
import AdminPage from '../../../components/AdminPage';
import ListLoadingIndicator from '../../../components/Common/ListLoadingIndcator';
import {
  getAllDepartments,
  deleteDepartment,
  createOrUpdateDepartment,
} from '../../../services/departments';
import NoDataAvailable from '../../../components/Common/NoDataAvailable';
import dynamic from 'next/dynamic';
import Button from '@mui/material/Button';
import { IDepartment } from '../../../models/department';
import LoadingButton from '@mui/lab/LoadingButton';
import Typography from '@mui/material/Typography';
import Dialog from '../../../components/Common/Dialog';
import AddIcon from '@mui/icons-material/Add';
import DepartmentForm from '../../../components/Departments/Form';
import DepartmentDetails from '../../../components/Departments/Details';
import OrganigramNavigation from '../../../components/Organigram/Navigation';
import { SELECTED_ACTIONS } from '../../../lib/constants';
import { useNotificationContext } from '../../../context/notificationContext';
import { findUserDepartmentAssignments } from '../../../services/userDepartmentAssignment';
import { USER_DEP_ASSIGNMENT_ROLES } from '../../../models/userDepartmentAssignment';
import { getAllFunctions } from '../../../services/functions';
import Paper from '@mui/material/Paper';
import UserDepartmentAssignmentExportButton from '../../../components/UserDepoartmentAssignments/ExportButton';

// @ts-ignore
const DepartmentsOrganigram = dynamic(() => import('../../../components/Departments/Organigram'), {
  ssr: false,
});

const FETCH_QUERY_KEY = ['departments'];
const FETCH_ADMINS_QUERY_KEY = ['departmentAssignments'];
const FETCH_FUNCTIONS_QUERY_KEY = ['depFunctions'];

const Departments = () => {
  const [selectedDepartment, setSelectedDepartment] = useState<Partial<IDepartment> | null>(null);
  const [selectedAction, setSelectedAction] = useState<string | null>(null);
  const { setError, setMessage } = useNotificationContext();

  const onDepartmentAction = (action: string) => (department: Partial<IDepartment>) => {
    setSelectedDepartment(department);
    setSelectedAction(action);
  };

  const resetSelectedDepartmentState = () => {
    setSelectedDepartment(null);
    setSelectedAction(null);
  };

  const queryClient = useQueryClient();

  const { isLoading, data: departments } = useQuery(FETCH_QUERY_KEY, getAllDepartments);

  const { data: functions } = useQuery(FETCH_FUNCTIONS_QUERY_KEY, getAllFunctions);

  const { isLoading: isAdminsLoading, data: departmentAdmins } = useQuery(
    FETCH_ADMINS_QUERY_KEY,
    () =>
      findUserDepartmentAssignments({
        role: USER_DEP_ASSIGNMENT_ROLES.EVALUATOR,
      }),
  );

  const { isLoading: isDeleting, mutate: deleteDepartmentById } = useMutation(deleteDepartment, {
    onSuccess: async () => {
      await queryClient.invalidateQueries(FETCH_QUERY_KEY);
      resetSelectedDepartmentState();
      setMessage('Departament sters');
    },
    onError: setError,
  });

  const { isLoading: isUpdating, mutate: updateDepartment } = useMutation(
    createOrUpdateDepartment,
    {
      onSuccess: async () => {
        await queryClient.invalidateQueries(FETCH_QUERY_KEY);
        resetSelectedDepartmentState();
        setMessage('Departament salvat');
      },
      onError: setError,
    },
  );

  const departmentAdminsMap = useMemo(() => {
    return departmentAdmins?.results?.reduce((acc, cur) => {
      // @ts-ignore
      acc[cur.departmentId?._id] = true;

      return acc;
    }, {});
  }, [departmentAdmins]);

  if (isLoading || isAdminsLoading) return <ListLoadingIndicator />;

  return (
    <div>
      <Head>
        <title>Departamente</title>
      </Head>

      <AdminPage
        title="Organigrama"
        pages={[{ url: '/departments', name: 'Organigrama' }]}
        actions={
          <>
            <UserDepartmentAssignmentExportButton />
            <Button
              onClick={() => {
                onDepartmentAction(SELECTED_ACTIONS.CREATE_OR_UPDATE)({ parentId: null });
              }}
              variant="contained"
              endIcon={<AddIcon />}
              sx={{ ml: 2 }}
            >
              Adaugă Departament
            </Button>
          </>
        }
      >
        <Paper sx={{ p: 3 }}>
          <OrganigramNavigation />

          {departments?.length || isLoading ? (
            <>
              {departments?.length && (
                <DepartmentsOrganigram
                  departments={departments}
                  departmentAdmins={{ ...departmentAdminsMap }}
                  onAddDepartment={onDepartmentAction(SELECTED_ACTIONS.CREATE_OR_UPDATE)}
                  onRemoveDepartment={onDepartmentAction(SELECTED_ACTIONS.DELETE)}
                  onSelectDepartment={onDepartmentAction(SELECTED_ACTIONS.VIEW_DETAILS)}
                />
              )}
            </>
          ) : (
            <NoDataAvailable content="În momentul de faţă nu există niciun departament disponibil pe platformă" />
          )}
        </Paper>

        <Dialog
          actions={
            <LoadingButton
              variant="contained"
              onClick={() =>
                selectedDepartment && deleteDepartmentById(selectedDepartment._id as string)
              }
              loading={isDeleting}
            >
              Închide
            </LoadingButton>
          }
          content={
            <>
              <Typography color="textSecondary" textAlign="center">
                Sunteți siguri că doriți să închideți departamentul
                <strong>
                  <i>&quot;{selectedDepartment?.name}&quot;</i>
                </strong>
                ?
              </Typography>
            </>
          }
          title="Confirmati închiderea?"
          onClose={resetSelectedDepartmentState}
          open={!!(selectedDepartment && selectedAction === SELECTED_ACTIONS.DELETE)}
        />

        {departments &&
        selectedDepartment &&
        selectedAction === SELECTED_ACTIONS.CREATE_OR_UPDATE ? (
          <DepartmentForm
            departments={departments}
            initialValues={selectedDepartment}
            isLoading={isUpdating}
            open={true}
            onClose={resetSelectedDepartmentState}
            onSubmit={updateDepartment}
          />
        ) : null}

        {departments &&
        functions &&
        selectedDepartment?._id &&
        selectedAction === SELECTED_ACTIONS.VIEW_DETAILS ? (
          <DepartmentDetails
            departmentId={selectedDepartment._id}
            onClose={resetSelectedDepartmentState}
            departments={departments}
            // @ts-ignore
            functions={functions.results}
          />
        ) : null}
      </AdminPage>
    </div>
  );
};

export const getServerSideProps = withPageAuthRequired();

export default Departments;
