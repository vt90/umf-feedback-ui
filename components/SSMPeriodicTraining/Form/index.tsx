import React from 'react';
import * as yup from 'yup';
import { useForm, Controller } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogTitle from '@mui/material/DialogTitle';
import Divider from '@mui/material/Divider';
import Grid from '@mui/material/Grid';
import Typography from '@mui/material/Typography';
import InfoIcon from '@mui/icons-material/Info';
import { ISSMPeriodicTraining } from '../../../models/ssmPeriodicTraining';
import { IFunctionModel } from '../../../models/function';
import TextField from '../../Common/FormInputs/TextField';
import FunctionsSelect from '../../Common/FunctionsSelect';
import YesNoToggle from '../../Common/YesNoToggle';
import MonthsInput from '../MonthsInput';
import DepartmentsSelect from '../../Common/DepartmentsSelect';
import { IDepartment } from '../../../models/department';

interface ISSMPeriodicTrainingsFormProps {
  initialValues: Partial<ISSMPeriodicTraining>;
  departments: IDepartment[];
  functions: IFunctionModel[];
  isLoading: boolean;
  onClose: () => void;
  onDelete: () => void;
  onSubmit: (data: any) => void;
  open: boolean;
}
const schema = yup
  .object({
    name: yup.string().required(),
  })
  .required();

const SSMPeriodicTrainingsForm = (props: ISSMPeriodicTrainingsFormProps) => {
  const { isLoading, departments, functions, initialValues, open, onClose, onDelete, onSubmit } =
    props;
  const {
    register,
    handleSubmit,
    formState: { errors },
    control,
  } = useForm({
    defaultValues: initialValues,
    mode: 'onTouched',
    resolver: yupResolver(schema),
    shouldUnregister: true,
  });

  return (
    <Dialog
      open={open}
      fullWidth={true}
      maxWidth="lg"
      keepMounted={false}
      onClose={onClose}
      // @ts-ignore
      PaperComponent="form"
      onSubmit={handleSubmit(onSubmit)}
    >
      <DialogTitle sx={{ typography: 'h5', textAlign: 'center' }}>
        {initialValues?._id ? 'Modifică' : 'Creează'} instruire periodică
      </DialogTitle>
      <DialogContent>
        <input type="hidden" {...register(`_id`)} />
        <Grid container spacing={3} alignItems="center">
          <Grid item xs={12} md={3}>
            <Typography variant="body2" sx={{ textAlign: { xs: 'left', md: 'right' } }}>
              Instruire activă
            </Typography>
          </Grid>
          <Grid item xs={12} md={8}>
            <Controller
              name="isActive"
              control={control}
              defaultValue={!!initialValues.isActive}
              render={({ field }) => {
                const { value, onChange } = field;

                return (
                  <>
                    <YesNoToggle
                      value={!!value}
                      onValueChange={onChange}
                      disabled={!initialValues?._id}
                    />
                  </>
                );
              }}
            />
          </Grid>
          <Grid item xs={12} md={3}>
            <Typography variant="body2" sx={{ textAlign: { xs: 'left', md: 'right' } }}>
              Denumire instruire periodica*
            </Typography>
          </Grid>
          <Grid item xs={12} md={8}>
            <TextField
              required={true}
              disabled={isLoading}
              errorMessage={!!errors.name && 'Denumire necesară'}
              defaultValue={initialValues.name}
              {...register('name')}
            />
          </Grid>

          <Grid item xs={12} md={3} alignSelf="flex-start">
            <Typography variant="body2" sx={{ textAlign: { xs: 'left', md: 'right' } }}>
              Descriere
            </Typography>
          </Grid>
          <Grid item xs={12} md={8}>
            <TextField
              multiline
              minRows={5}
              disabled={isLoading}
              errorMessage={!!errors.shortDescription && 'Câmp necesar'}
              defaultValue=""
              {...register('shortDescription')}
            />
          </Grid>

          <Grid item xs={12} md={3}>
            <Typography variant="body2" sx={{ textAlign: { xs: 'left', md: 'right' } }}>
              Perioda de instruire
            </Typography>
          </Grid>
          <Grid item xs={12} md={8}>
            <Controller
              name="monthsOfYear"
              control={control}
              defaultValue={initialValues?.monthsOfYear || []}
              render={({ field }) => {
                const { value, onChange } = field;

                // @ts-ignore
                return <MonthsInput value={value} onChange={onChange} />;
              }}
            />
          </Grid>
          <Grid item xs={12} md={3}>
            <Typography variant="body2" sx={{ textAlign: { xs: 'left', md: 'right' } }}>
              Functii asociate
            </Typography>
          </Grid>
          <Grid item xs={12} md={8}>
            <FunctionsSelect name="functions" control={control} functions={functions} />
          </Grid>
          <Grid item xs={12}>
            <Divider />
          </Grid>
          <Grid item xs={12} md={3}>
            <Typography
              variant="body2"
              sx={{ textAlign: { xs: 'left', md: 'right' } }}
              gutterBottom
            >
              Instruire specifică doar anumitor departamente
            </Typography>
            <Typography
              variant="caption"
              component="p"
              color="textSecondary"
              sx={{ textAlign: { xs: 'left', md: 'right' } }}
            >
              <InfoIcon fontSize="small" /> în cazul în care sunt specificate departamente,
              instruirea se va aplica doar acelor departamente
            </Typography>
          </Grid>
          <Grid item xs={12} md={8}>
            <DepartmentsSelect name="departments" control={control} departments={departments} />
          </Grid>
          <Grid item xs={12} md={3}>
            <Typography
              variant="body2"
              sx={{ textAlign: { xs: 'left', md: 'right' } }}
              gutterBottom
            >
              Instruire exclusă anumitor departamente
            </Typography>
            <Typography
              variant="caption"
              component="p"
              color="textSecondary"
              sx={{ textAlign: { xs: 'left', md: 'right' } }}
            >
              <InfoIcon fontSize="small" /> în cazul în care sunt specificate departamente,
              instruirea nu se va aplica persoanelor din acele departamente
            </Typography>
          </Grid>
          <Grid item xs={12} md={8}>
            <DepartmentsSelect
              name="excludedDepartments"
              control={control}
              departments={departments}
            />
          </Grid>
        </Grid>
      </DialogContent>
      <DialogActions sx={{ py: 2, justifyContent: 'center', bgcolor: 'background.default' }}>
        <Button color="inherit" disabled={isLoading} onClick={onClose}>
          Anulare
        </Button>
        {initialValues?._id ? (
          <Button variant="contained" color="secondary" onClick={onDelete}>
            Șterge
          </Button>
        ) : null}
        <Button disabled={isLoading} variant="contained" type="submit">
          {initialValues?._id ? 'Modifică' : 'Crează'}
        </Button>
      </DialogActions>
    </Dialog>
  );
};

export default SSMPeriodicTrainingsForm;
