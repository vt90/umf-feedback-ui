import axios from 'axios';
import { ISurvey } from '../models/survey';
import { IPaginationResult } from '../models/base';

const httpInstance = axios.create({
  baseURL: '/api/surveys',
});

httpInstance.interceptors.response.use(
  (response) => response.data,
  (error) => {
    if (error && error.response && error.response.data) {
      return Promise.reject(
        error.response.data.message
          ? `${error.response.data.message || error.response.data.error}`
          : error.response.data?.error || error.response.data,
      );
    } else {
      return Promise.reject({ message: 'Server error' });
    }
  },
);

const cleanupSurveyBody = (survey: Partial<ISurvey>): Partial<ISurvey> => {
  // @ts-ignore
  delete survey._id;
  // @ts-ignore
  delete survey.id;
  // @ts-ignore
  delete survey['__v'];
  // @ts-ignore
  delete survey.surveyChapters;
  // @ts-ignore
  delete survey.surveyResultIntervals;

  if (survey?.surveyBonifications) {
    survey.surveyBonifications = survey.surveyBonifications.map((bonification) => {
      if (!bonification._id) {
        // @ts-ignore
        delete bonification._id;
      }

      return bonification;
    });
  }


  return survey;
};

export const getSurveys = async (params: Partial<ISurvey>): Promise<IPaginationResult<ISurvey>> => {
  return await httpInstance.get('', { params });
};

export const getSurveyById = async (id: string): Promise<ISurvey> => {
  return await httpInstance.get(`/${id}`);
};

export const createOrUpdateSurvey = async (survey: Partial<ISurvey>): Promise<ISurvey> => {
  const isUpdate = !!survey._id;
  const url = isUpdate ? `/${survey._id}` : ``;
  const req = isUpdate ? httpInstance.patch : httpInstance.post;
  return await req(url, cleanupSurveyBody(survey));
};

export const deleteSurvey = async (id: string): Promise<any> => {
  return await httpInstance.delete(`/${id}`);
};
