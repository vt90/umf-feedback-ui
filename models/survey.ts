import { IBaseModel } from './base';
import { ISurveyChapter } from './surveyChapter';
import { ISurveyObjective } from './surveyObjective';

export interface ISurveyResultInterval {
  name: string;
  minValue: number;
}

export interface ISurveyBonification extends IBaseModel {
  name: string;
  value: number;
}

export interface ISurvey extends IBaseModel {
  name: string;
  type: string;
  role: string;
  calculationFormulaType: number;
  surveyChapters: ISurveyChapter[];
  isClone?: boolean;
  surveyResultIntervals?: ISurveyResultInterval[];
  surveyBonifications?: ISurveyBonification[];
  surveyObjectives?: ISurveyObjective[];
}

export const SURVEY_FORMULA_TYPE = {
  'Medie aritmetică': 0,
  'Medie ponderată': 1,
  Sumă: 2,
};

export const SURVEY_ROLE = {
  EVALUATION: 'EVALUATION',
  SALARY_CHANGE: 'SALARY_CHANGE',
};

export const getSurveyRoleName = (role: string): string => {
  switch (role) {
    case SURVEY_ROLE.SALARY_CHANGE:
      return 'Modificare salariala';
    default:
      return 'Evaluare';
  }
};
