import { IBaseModel } from './base';

export interface IQuestion extends IBaseModel {
  name: string;
  order: number;
}

export interface ISurveyChapter extends IBaseModel {
  surveyId: string;
  name: string;
  order: number;
  weight: number;
  questions?: IQuestion[];
}
