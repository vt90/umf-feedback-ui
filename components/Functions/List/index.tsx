import React, { useCallback, useMemo } from 'react';
import Box from '@mui/material/Box';
import EnhancedTable from '../../Common/Table';
import { IFunctionModel } from '../../../models/function';

interface IFunctionsListProps {
  functions: IFunctionModel[];
}

const FunctionsList = (props: IFunctionsListProps) => {
  const { functions } = props;

  const headerRows = useMemo(() => {
    return [
      { id: 'code', label: 'Cod', align: 'left', colSpan: 1 },
      { id: 'name', label: 'Funcție', align: 'left', colSpan: 5 },
      { id: 'isLead', label: 'Tip', align: 'left', colSpan: 2 },
    ];
  }, []);

  const getRows = useCallback(() => {
    return functions.map((functionInfo) => {
      return {
        _id: functionInfo._id,
        name: functionInfo.name,
        smallDeviceHeader: functionInfo.name,
        code: functionInfo.code,
        isLead: functionInfo.isLead ? 'Funcție de conducere' : 'Funcție de execuție',
        data: functionInfo,
      };
    });
  }, [functions]);

  return (
    <>
      <EnhancedTable
        initialOrderByField="code"
        headerRows={headerRows}
        orderByFieldCriteriaFunctions={{
          code: (row: any) => row?.data && parseInt(row.data.code, 10),
          // functionsStarted: (row: any) => row?.data && getUserStats(row.data).completedAnswering,
          // functionsCompleted: (row: any) => row?.data && getUserStats(row.data).completedAnswering,
          // published: (row: any) => row?.data && !!row.data.published,
        }}
        PaperComponent={Box}
        paperComponentStyles={{
          // px: 3,
          py: 2,
        }}
        rows={getRows()}
        smallUntilBreakpoint="md"
      />
    </>
  );
};

export default FunctionsList;
