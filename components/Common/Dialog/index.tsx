import React from 'react';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import MuiDialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogTitle from '@mui/material/DialogTitle';
import { Breakpoint } from '@mui/system';

interface IDialogProps {
  actions?: React.ReactNode;
  content: string | React.ReactNode;
  title: string;
  maxWidth?: Breakpoint;
  onClose?: () => void;
  open: boolean;
}

const Dialog = (props: IDialogProps) => {
  const { actions, content, title, onClose, open, maxWidth = 'sm' } = props;
  return (
    <MuiDialog open={open} fullWidth={true} maxWidth={maxWidth} onClose={onClose}>
      <DialogTitle sx={{ typography: 'h5', textAlign: 'center' }}>{title}</DialogTitle>

      <DialogContent>
        <Box py={1}>{content}</Box>
      </DialogContent>

      <DialogActions sx={{ py: 2, justifyContent: 'center', bgcolor: 'background.default' }}>
        {onClose && (
          <Button onClick={onClose} color="inherit">
            Anulare
          </Button>
        )}
        {actions}
      </DialogActions>
    </MuiDialog>
  );
};

export default Dialog;
