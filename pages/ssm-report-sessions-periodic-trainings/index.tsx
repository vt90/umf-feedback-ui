import React, { useState } from 'react';
import Head from 'next/head';
import { withPageAuthRequired } from '../../guards/authRequired';
import { findSSMPeriodicTrainingSessions } from '../../services/ssmPeriodicTrainingSessions';
import { useQuery } from '@tanstack/react-query';
import AdminPage from '../../components/AdminPage';
import NoDataAvailable from '../../components/Common/NoDataAvailable';
import { getAllFunctions } from '../../services/functions';
import ListLoadingIndicator from '../../components/Common/ListLoadingIndcator';
import Box from '@mui/material/Box';
import Card from '@mui/material/Card';
import Grid from '@mui/material/Grid';
import moment from 'moment';
import Pagination from '../../components/Common/Pagination';
import SSMTrainingSessionList from '../../components/SSMPeriodicTrainingSession/List';
import useDebounce from '../../hooks/useDebounce';
import { getAllDepartments } from '../../services/departments';
import SSMTrainingSessionFilter from '../../components/SSMPeriodicTrainingSession/Filter';
import HREvaluationsListHeader from '../../components/Evaluation/HRUserEvaluationsList/Header';
const FETCH_QUERY_KEY = ['ssm-periodic-sessions-trainings'];
const FETCH_FUNCTIONS_QUERY_KEY = ['depFunctions'];
const FETCH_DEPARTMENTS_QUERY_KEY = ['departments'];

const SSMPeriodicTrainingSessions = () => {
  const [pageNumber, setPageNumber] = useState(1);
  const [order, setOrder] = useState(-1);
  const [orderBy, setOrderBy] = useState('userAcknowledged');
  const [department, setDepartment] = useState<string | null>('');
  const [email, setEmail] = useState<string | null>('');
  const [sessionDate, setSessionDate] = useState<Date>(moment().startOf('month').toDate());
  const [userAcknowledged, setUserAcknowledged] = useState<string | null>('');
  const [contraEvaluated, setContraEvaluated] = useState<string | null>('');
  const [pageSize] = useState(50);

  const userSearchTerm = useDebounce(`${email}`, 600);

  const fetchParams = {
    limit: pageSize,
    offset: (pageNumber - 1) * pageSize,
    order,
    orderBy,
    sessionDate,
    userAcknowledged,
    contraEvaluated,
    userSearchTerm,
    // @ts-ignore
    ...(department ? { departmentId: department } : {}),
  };

  const { isLoading, data: ssmPeriodicTrainingsSessions } = useQuery(
    [FETCH_QUERY_KEY, fetchParams],
    // @ts-ignore
    () => findSSMPeriodicTrainingSessions(fetchParams),
  );

  const { data: functions } = useQuery(FETCH_FUNCTIONS_QUERY_KEY, getAllFunctions);

  const { data: departments } = useQuery(FETCH_DEPARTMENTS_QUERY_KEY, getAllDepartments);

  return (
    <>
      <Head>
        <title>Rapoarte Instruiri periodice</title>
      </Head>

      <AdminPage
        title="Rapoarte Instruiri periodice"
        pages={[
          { url: '/ssm-report-sessions-periodic-trainings', name: 'Rapoarte Instruiri periodice' },
        ]}
        actions={<></>}
      >
        <Card sx={{ p: 3 }}>
          <Grid container spacing={2} columns={13}>
            <Grid item xs={12} md={3} lg={2}>
              <SSMTrainingSessionFilter
                order={order}
                setOrder={setOrder}
                orderBy={orderBy}
                setOrderBy={setOrderBy}
                department={department}
                departments={departments}
                setDepartment={setDepartment}
                contraEvaluated={contraEvaluated}
                setContraEvaluated={setContraEvaluated}
                userAcknowledged={userAcknowledged}
                setUserAcknowledged={setUserAcknowledged}
                sessionData={sessionDate}
                setSessionData={setSessionDate}
              />
            </Grid>

            <Grid item xs={12} md={10} lg={11}>
              <Box sx={{ mb: 3 }}>
                <HREvaluationsListHeader
                  // @ts-ignore
                  activeView={'s'}
                  order={order}
                  setOrder={setOrder}
                  orderBy={orderBy}
                  setOrderBy={setOrderBy}
                  searchTerm={email}
                  setSearchTerm={setEmail}
                />
              </Box>

              {isLoading || !functions?.results?.length ? (
                <ListLoadingIndicator />
              ) : ssmPeriodicTrainingsSessions?.results?.length ? (
                <>
                  <SSMTrainingSessionList
                    ssmPeriodicTrainingsSessions={ssmPeriodicTrainingsSessions?.results || []}
                  />

                  <Pagination
                    sx={{ mt: 2 }}
                    pageNumber={pageNumber}
                    pageSize={pageSize}
                    totalResults={ssmPeriodicTrainingsSessions?.count || 0}
                    setPageNumber={setPageNumber}
                  />
                </>
              ) : (
                <NoDataAvailable content="În momentul de faţă nu există nicio instruire periodca disponibilă pe platformă" />
              )}
            </Grid>
          </Grid>
        </Card>
      </AdminPage>
    </>
  );
};

export const getServerSideProps = withPageAuthRequired();

export default SSMPeriodicTrainingSessions;
