import { getSSMFileDisplayName, ISSMPackage } from '../../../models/ssmPackage';
import Box from '@mui/material/Box';
import ButtonBase from '@mui/material/ButtonBase';
import Grid from '@mui/material/Grid';
import IconButton from '@mui/material/Button';
import ImageListItemBar from '@mui/material/ImageListItemBar';
import InfoIcon from '@mui/icons-material/Download';
import Avatar from '@mui/material/Avatar';
import EditIcon from '@mui/icons-material/Edit';
import DeleteIcon from '@mui/icons-material/Delete';
import File from '@mui/icons-material/InsertDriveFile';
import Typography from '@mui/material/Typography';
import { isImage } from '../../../lib/constants';
import React from 'react';

interface ISSMPackageList {
  ssmPackages: ISSMPackage[];
  functions: ISSMPackage[];
  onEdit?: (ssmPackage: ISSMPackage) => void;
  onDelete?: (ssmPackage: ISSMPackage) => void;
}

const SSMPackageList = (props: ISSMPackageList) => {
  const { ssmPackages, onEdit, onDelete } = props;

  return (
    <>
      {ssmPackages?.map((ssmPackage) => {
        return (
          <Box
            key={ssmPackage._id}
            sx={{ p: 2, border: '0.0625rem solid rgb(222, 226, 230)', borderRadius: 2, mb: 2 }}
          >
            <Box sx={{ display: 'flex' }}>
              <Typography variant="h6" sx={{ flexGrow: 1 }}>
                <strong>{ssmPackage.name}</strong>
              </Typography>

              {onEdit && (
                <IconButton
                  onClick={() => onEdit(ssmPackage)}
                  sx={{ color: 'text.primary' }}
                  startIcon={<EditIcon fontSize="inherit" />}
                >
                  Modifică
                </IconButton>
              )}

              {onDelete && (
                <IconButton
                  onClick={() => onDelete(ssmPackage)}
                  color="error"
                  startIcon={<DeleteIcon fontSize="inherit" />}
                >
                  Șterge
                </IconButton>
              )}
            </Box>

            <Box sx={{ mt: 2 }}>
              <Grid container spacing={2} columns={20}>
                {ssmPackage?.documents?.map((document) => {
                  const avatarProps = isImage(document)
                    ? { src: document, alt: ssmPackage.name }
                    : {};
                  return (
                    <Grid item xs={10} sm={5} md={4} key={document}>
                      <ButtonBase
                        sx={{ display: 'block' }}
                        component="a"
                        target="_blank"
                        href={document}
                      >
                        <Box sx={{ position: 'relative' }}>
                          <Avatar
                            variant="rounded"
                            sx={{
                              height: {
                                xs: 150,
                                sm: 225,
                              },
                              width: '100%',
                              mr: 2,
                              boxShadow: 'rgba(0, 0, 0, 0.12) 0rem 0.3125rem 0.625rem 0rem',
                              bgcolor: 'action.disabled',
                            }}
                            {...avatarProps}
                          >
                            <File sx={{ fontSize: 80 }} />
                          </Avatar>

                          <ImageListItemBar
                            title={getSSMFileDisplayName(document)}
                            actionIcon={
                              <IconButton sx={{ color: 'rgba(255, 255, 255, 0.54)' }}>
                                <InfoIcon />
                              </IconButton>
                            }
                          />
                        </Box>
                      </ButtonBase>
                    </Grid>
                  );
                })}
              </Grid>

              {/*<List>*/}
              {/*  {ssmPackage?.documents?.map((document, index) => {*/}

              {/*    return (*/}
              {/*      <ListItem*/}
              {/*        // @ts-ignore*/}
              {/*        button*/}
              {/*        key={document}*/}
              {/*        // divider={index < ssmPackage.documents.length - 1}*/}

              {/*      >*/}
              {/*        <ListItemAvatar>*/}
              {/*          */}
              {/*        </ListItemAvatar>*/}
              {/*        <ListItemText primary={getSSMFileDisplayName(document)} />*/}
              {/*      </ListItem>*/}
              {/*    );*/}
              {/*  })}*/}
              {/*</List>*/}
            </Box>
          </Box>
        );
      })}
    </>
  );
};

export default SSMPackageList;
