import React, { useState } from 'react';
import Head from 'next/head';
import { withPageAuthRequired } from '../../guards/authRequired';
import { accept, findMyMedicalCheck } from '../../services/medicalCheck';
import { useMutation, useQuery, useQueryClient } from '@tanstack/react-query';
import Card from '@mui/material/Card';
import Paper from '@mui/material/Paper';
import AdminPage from '../../components/AdminPage';
import { IMedicalCheck, MEDICAL_CHECK_RESULT_TYPES } from '../../models/medicalCheck';
import NoDataAvailable from '../../components/Common/NoDataAvailable';
import { useNotificationContext } from '../../context/notificationContext';
import ListLoadingIndicator from '../../components/Common/ListLoadingIndcator';
import MedicalCheckList from '../../components/MedicalChecks/List';
import Dialog from '../../components/Common/Dialog';
import Button from '@mui/lab/LoadingButton';
import MedicalCheckDetails from '../../components/MedicalChecks/Details';

const FETCH_QUERY_KEY = ['my-medical-checks'];

const MedicalChecks = () => {
  const [selectedMedicalCheck, setSelectedMedicalCheck] = useState<Partial<IMedicalCheck> | null>(
    null,
  );
  const { setError, setMessage } = useNotificationContext();

  const resetSelectedMedicalCheckState = () => {
    setSelectedMedicalCheck(null);
  };

  const queryClient = useQueryClient();

  const { isLoading, data: medicalCheckData } = useQuery(FETCH_QUERY_KEY, () =>
    findMyMedicalCheck({}),
  );

  const { isLoading: isAccepting, mutate: onAccept } = useMutation(accept, {
    onSuccess: async () => {
      await queryClient.invalidateQueries(FETCH_QUERY_KEY);
      resetSelectedMedicalCheckState();
      setMessage('Fișă de aptitudini acceptată');
    },
    onError: setError,
  });

  return (
    <>
      <Head>
        <title>Medicina muncii</title>
      </Head>

      <AdminPage
        title="Medicina muncii"
        pages={[{ url: '/my-medical-checks', name: 'Medicina muncii' }]}
      >
        {/* @ts-ignore */}
        {medicalCheckData?.length || isLoading ? (
          <>
            {isLoading && <ListLoadingIndicator />}

            <Paper sx={{ p: 3 }}>
              <MedicalCheckList
                // @ts-ignore
                medicalChecks={medicalCheckData}
                onSelectMedicalCheck={setSelectedMedicalCheck}
              />
            </Paper>
          </>
        ) : (
          <Card sx={{ p: 3 }}>
            <NoDataAvailable content="În momentul de faţă nu există nicio fișă de aptitudini disponibilă pe platformă" />
          </Card>
        )}
      </AdminPage>

      <Dialog
        // @ts-ignore
        content={<MedicalCheckDetails medicalCheck={selectedMedicalCheck} />}
        actions={
          selectedMedicalCheck &&
          !selectedMedicalCheck.isAccepted &&
          selectedMedicalCheck.result !== MEDICAL_CHECK_RESULT_TYPES.APT ? (
            // @ts-ignore
            <Button variant="contained" color="primary" onClick={onAccept} loading={isAccepting}>
              Rezultat luat la cunostintă
            </Button>
          ) : null
        }
        title="Fișă de aptitudini"
        open={!!selectedMedicalCheck}
        maxWidth="lg"
        onClose={() => setSelectedMedicalCheck(null)}
      />
    </>
  );
};

export const getServerSideProps = withPageAuthRequired();

export default MedicalChecks;
