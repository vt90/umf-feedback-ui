export interface IChangePassword {
  userId?: string;
  oldPassword?: string;
  newPassword?: string;
  newPasswordConfirmation?: string;
}
