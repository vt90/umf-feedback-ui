import axios from 'axios';
import { ISSMPeriodicTraining } from '../models/ssmPeriodicTraining';
import { IPaginationResult } from '../models/base';

const createInstance = (baseURL = '/api/ssm-periodic-training') => {
  const httpInstance = axios.create({
    baseURL,
  });

  httpInstance.interceptors.response.use(
    (response) => response.data,
    (error) => {
      if (error && error.response && error.response.data) {
        return Promise.reject(
          error.response.data.message
            ? `${error.response.data.message || error.response.data.error}`
            : error.response.data?.error || error.response.data,
        );
      } else {
        return Promise.reject({ message: 'Server error' });
      }
    },
  );

  return httpInstance;
};

const httpInstance = createInstance();

const cleanupSSMPeriodicTrainingModelBody = (
  ssmPeriodicTraining: ISSMPeriodicTraining,
): ISSMPeriodicTraining => {
  // @ts-ignore
  delete ssmPeriodicTraining._id;
  // @ts-ignore
  delete ssmPeriodicTraining.id;
  // @ts-ignore
  delete ssmPeriodicTraining.displayName;
  // @ts-ignore
  delete ssmPeriodicTraining.createdAt;
  // @ts-ignore
  delete ssmPeriodicTraining.updatedAt;
  // @ts-ignore
  delete ssmPeriodicTraining['__v'];

  return ssmPeriodicTraining;
};

export const findSSMPeriodicTraining = async (
  params: Partial<ISSMPeriodicTraining>,
): Promise<IPaginationResult<ISSMPeriodicTraining>> => {
  return await httpInstance.get('', { params });
};

export const getAllSSMPeriodicTrainings = async (): Promise<
  IPaginationResult<ISSMPeriodicTraining>
> => {
  return await httpInstance.get('', {});
};

export const createOrUpdateSSMPeriodicTraining = async (
  ssmPeriodicTraining: ISSMPeriodicTraining,
): Promise<ISSMPeriodicTraining> => {
  const isUpdate = !!ssmPeriodicTraining._id;
  const url = isUpdate ? `/${ssmPeriodicTraining._id}` : ``;
  const req = isUpdate ? httpInstance.patch : httpInstance.post;

  return await req(url, cleanupSSMPeriodicTrainingModelBody(ssmPeriodicTraining));
};

export const deleteSSMPeriodicTraining = async (id: string): Promise<any> => {
  return await httpInstance.delete(`/${id}`);
};
