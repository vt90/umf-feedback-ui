import { Controller, Control } from 'react-hook-form';
import Dropzone, { Accept } from 'react-dropzone';
import Avatar from '@mui/material/Avatar';
import ButtonBase from '@mui/material/ButtonBase';
import IconButton from '@mui/material/IconButton';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemAvatar from '@mui/material/ListItemAvatar';
import ListItemText from '@mui/material/ListItemText';
import ListItemSecondaryAction from '@mui/material/ListItemSecondaryAction';
import Typography from '@mui/material/Typography';
import File from '@mui/icons-material/InsertDriveFile';
import Delete from '@mui/icons-material/Delete';
import React from 'react';

interface IFileUploadProps {
  name: string;
  control: Control;
  setValue: (name: string, files: FileList, options: any) => void;
  multiple: boolean;
  accept: Accept;

  [key: string]: any;
}

const FileUploadField = (props: IFileUploadProps, ref: any) => {
  const { control, name, setValue, accept, multiple = false } = props;

  const onFileUploaded = (newValue: FileList) => {
    setValue(name, newValue as unknown as FileList, {
      shouldValidate: true,
    });
  };

  return (
    <>
      <Controller
        control={control}
        name={name}
        // rules={{
        //   required: { value: true, message: 'This field is required' },
        // }}
        render={({ field: { value, onChange, onBlur } }) => {
          return (
            <>
              <Dropzone
                accept={accept}
                multiple={multiple}
                noClick
                onDrop={(acceptedFiles) => {
                  const newValue = acceptedFiles;

                  if (value && multiple) {
                    newValue.unshift(...value);
                  }

                  // @ts-ignore
                  onFileUploaded(newValue);
                }}
              >
                {({ getRootProps, getInputProps, open, isDragActive }) => (
                  // @ts-ignore
                  <ButtonBase
                    {...getRootProps()}
                    onClick={open}
                    component="div"
                    role="presentation"
                    sx={{
                      display: 'flex',
                      borderWidth: 1,
                      borderStyle: 'solid',
                      borderColor: 'divider',
                      borderRadius: 1,
                      padding: 8,
                      backgroundColor: isDragActive ? `#808080` : 'transparent',
                    }}
                  >
                    <div className="text-center">
                      <div className="flex justify-center">
                        <img src="/drag&drop.png" alt="Drag & Drop" />
                      </div>
                      <Typography color="textSecondary" variant="caption" gutterBottom>
                        <strong>Drag & Drop</strong>
                        &nbsp; or &nbsp;
                      </Typography>
                      <Typography color="textSecondary" variant="caption">
                        <strong>Upload</strong>
                        &nbsp; files from your computer
                      </Typography>
                      <input
                        {...getInputProps({
                          onChange,
                          onBlur,
                        })}
                      />
                    </div>
                    {/*<div>*/}
                    {/*  {fieldState.error && <span role="alert">{fieldState.error.message}</span>}*/}
                    {/*</div>*/}
                  </ButtonBase>
                )}
              </Dropzone>
              {value && value.length && Array.isArray(value) ? (
                <List>
                  {/* @ts-ignore */}
                  {value?.map((document, index) => (
                    <ListItem key={document?.name}>
                      <ListItemAvatar>
                        <Avatar>
                          <File />
                        </Avatar>
                      </ListItemAvatar>
                      <ListItemText primary={document?.name} />
                      <ListItemSecondaryAction>
                        <IconButton
                          onClick={() => {
                            const newFiles = [...value];

                            newFiles.splice(index, 1);

                            // @ts-ignore
                            onFileUploaded(newFiles);
                          }}
                        >
                          <Delete />
                        </IconButton>
                      </ListItemSecondaryAction>
                    </ListItem>
                  ))}
                </List>
              ) : null}
            </>
          );
        }}
      />
    </>
  );
};

export default React.forwardRef(FileUploadField);
