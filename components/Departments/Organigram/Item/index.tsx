import React, { useState } from 'react';
import { IDepartment, IHierarchy } from '../../../../models/department';
import Box from '@mui/material/Box';
import ButtonBase from '@mui/material/ButtonBase';
import Collapse from '@mui/material/Collapse';
import IconButton from '@mui/material/IconButton';
import Tooltip from '@mui/material/Tooltip';
import Typography from '@mui/material/Typography';
import AddIcon from '@mui/icons-material/Add';
import EditIcon from '@mui/icons-material/Edit';
import DeleteIcon from '@mui/icons-material/Block';
import UpIcon from '@mui/icons-material/KeyboardArrowUp';
import DownIcon from '@mui/icons-material/KeyboardArrowDown';
import WarningIcon from '@mui/icons-material/Warning';
import { COLORS } from '../../../../lib/theme';
import { USER_DEP_ASSIGNMENT_ROLES } from '../../../../models/userDepartmentAssignment';

interface IDepartmentsOrganigramItemProps {
  department: IHierarchy;
  departmentAdmins: object;
  depth?: number;
  color?: string;
  lineColor?: string;
  onAddDepartment: (department: Partial<IDepartment>) => void;
  onRemoveDepartment: (department: Partial<IDepartment>) => void;
  onSelectDepartment: (department: Partial<IDepartment>) => void;
}

const DEPARTMENT_LEFT_OFFSET = 8;

const DepartmentsOrganigramItem = (props: IDepartmentsOrganigramItemProps) => {
  const [hovered, setHovered] = useState(false);
  const [collapsed, setCollapsed] = useState(false);
  const {
    department,
    departmentAdmins,
    depth = 0,
    color,
    lineColor,
    onAddDepartment,
    onRemoveDepartment,
    onSelectDepartment,
  } = props;
  const hasChildren = department?.children && Object.keys(department.children).length;

  return (
    <>
      <Box
        sx={{
          my: 2,
          pl: depth ? DEPARTMENT_LEFT_OFFSET : 0,
        }}
      >
        <Box onMouseEnter={() => setHovered(true)} onMouseLeave={() => setHovered(false)}>
          <Box sx={{ alignItems: 'center', display: 'flex' }}>
            <ButtonBase
              onClick={() => onSelectDepartment(department)}
              sx={{
                position: 'relative',
                '&:after': () => {
                  const offset = DEPARTMENT_LEFT_OFFSET * 8;

                  if (!depth) return null;

                  return {
                    position: 'absolute',
                    content: '""',
                    height: `50%`,
                    width: offset,
                    borderBottom: 1,
                    borderBottomLeftRadius: 6,
                    // borderBottomColor: depth > 1 ? color : 'divider',
                    borderBottomColor: 'divider',
                    top: 0,
                    left: `${-1 * offset}px`,
                    zIndex: 999,
                  };
                },
              }}
            >
              <Box
                sx={{
                  py: 1,
                  pl: 2,
                  pr: 1,
                }}
              >
                {/*@ts-ignore*/}
                <Typography
                  variant="body2"
                  color={
                    hovered ? 'primary' : department.deactivated ? 'textSecondary' : 'textPrimary'
                  }
                  sx={{
                    alignItems: 'center',
                    display: 'flex',
                    justifyContent: 'space-between',
                    ...(department.deactivated && {
                      textDecoration: 'line-through',
                      fontStyle: 'italic',
                    }),
                  }}
                >
                  {/*@ts-ignore*/}
                  {!departmentAdmins?.[department._id] && (
                    <Tooltip
                      title={`Departamentul nu are asignat niciun ${USER_DEP_ASSIGNMENT_ROLES.EVALUATOR}`}
                    >
                      <WarningIcon fontSize="small" color="warning" sx={{ mr: 1 }} />
                    </Tooltip>
                  )}
                  <Typography variant="body2" component="span">
                    {/*@ts-ignore*/}
                    {department.code}
                  </Typography>
                  &nbsp;- &nbsp;
                  {department.name}
                </Typography>
              </Box>
            </ButtonBase>

            {hasChildren ? (
              <Tooltip title={`${collapsed ? 'Afișează' : 'Ascunde'} sub-departamente`}>
                <IconButton size="small" onClick={() => setCollapsed(!collapsed)} sx={{ ml: 1 }}>
                  {collapsed ? <DownIcon /> : <UpIcon />}
                </IconButton>
              </Tooltip>
            ) : null}
            <Collapse in={hovered}>
              <>
                <Tooltip title="Adaugă sub-departament">
                  <IconButton
                    size="small"
                    onClick={() =>
                      onAddDepartment({
                        parentId: `${department._id}`,
                      })
                    }
                    sx={{ ml: 1 }}
                  >
                    <AddIcon />
                  </IconButton>
                </Tooltip>

                <Tooltip title="Modifică departament">
                  <IconButton
                    color="primary"
                    size="small"
                    onClick={() => onAddDepartment(department)}
                    sx={{ ml: 1 }}
                  >
                    <EditIcon />
                  </IconButton>
                </Tooltip>

                <Tooltip title="Închide departament">
                  <IconButton
                    color="error"
                    size="small"
                    onClick={() => onRemoveDepartment(department)}
                    sx={{ ml: 1 }}
                  >
                    <DeleteIcon />
                  </IconButton>
                </Tooltip>
              </>
            </Collapse>
          </Box>
        </Box>

        {hasChildren ? (
          <Collapse in={!collapsed}>
            <Box
              sx={{
                position: 'relative',
                ml: 2,
                '&:after': () => {
                  const offset = 12;
                  return {
                    position: 'absolute',
                    content: '""',
                    height: `calc(100% - 9px)`,
                    width: '1px',
                    // bgcolor: color,
                    bgcolor: 'divider',
                    top: `${-1 * offset}px`,
                    left: 0,
                  };
                },
              }}
            >
              {Object.values(department.children).map((department, index) => {
                return (
                  <DepartmentsOrganigramItem
                    key={department._id}
                    depth={depth + 1}
                    department={department}
                    lineColor={lineColor}
                    color={depth === 0 ? COLORS[index % COLORS.length] : color}
                    onAddDepartment={onAddDepartment}
                    onRemoveDepartment={onRemoveDepartment}
                    onSelectDepartment={onSelectDepartment}
                    departmentAdmins={departmentAdmins}
                  />
                );
              })}
            </Box>
          </Collapse>
        ) : null}
      </Box>
    </>
  );
};

export default DepartmentsOrganigramItem;
