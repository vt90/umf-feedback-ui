import axios from 'axios';
import { ISurveyChapter } from '../models/surveyChapter';
import { IPaginationResult } from '../models/base';

const httpInstance = axios.create({
  baseURL: '/api/survey-chapters',
});

httpInstance.interceptors.response.use(
  (response) => response.data,
  (error) => {
    if (error && error.response && error.response.data) {
      return Promise.reject(
        error.response.data.message
          ? `${error.response.data.message || error.response.data.error}`
          : error.response.data?.error || error.response.data,
      );
    } else {
      return Promise.reject({ message: 'Server error' });
    }
  },
);

const cleanupSurveyChapterBody = (
  surveyChapter: Partial<ISurveyChapter>,
): Partial<ISurveyChapter> => {
  // @ts-ignore
  delete surveyChapter._id;
  // @ts-ignore
  delete surveyChapter.id;
  // @ts-ignore
  delete surveyChapter['__v'];

  if (surveyChapter?.questions) {
    surveyChapter.questions = surveyChapter.questions.map((question) => {
      if (!question._id) {
        // @ts-ignore
        delete question._id;
      }

      return question;
    });
  }

  return surveyChapter;
};

const cleanupBulkSurveyChapterBody = (
  bulkUpdateSurveyChapters: IBulkUpdateSurveyChapters,
): IBulkUpdateSurveyChapters => {
  bulkUpdateSurveyChapters.surveyChapters = bulkUpdateSurveyChapters.surveyChapters.map(
    (chapter) => {
      const surveyChapter = { ...chapter };
      // @ts-ignore
      delete surveyChapter.id;
      // @ts-ignore
      delete surveyChapter['__v'];

      if (!surveyChapter._id) {
        // @ts-ignore
        delete surveyChapter._id;
      }

      if (surveyChapter?.questions) {
        surveyChapter.questions = surveyChapter.questions.map((question) => {
          if (!question._id) {
            // @ts-ignore
            delete question._id;
          }

          return question;
        });
      }

      return surveyChapter;
    },
  );

  return bulkUpdateSurveyChapters;
};

export const getSurveyChapters = async (
  params: Partial<ISurveyChapter>,
): Promise<IPaginationResult<ISurveyChapter>> => {
  return await httpInstance.get('', { params });
};

export const getSurveyChapterById = async (id: string): Promise<ISurveyChapter> => {
  return await httpInstance.get(`/${id}`);
};

export const createOrUpdateSurveyChapter = async (
  surveyChapter: Partial<ISurveyChapter>,
): Promise<ISurveyChapter> => {
  const isUpdate = !!surveyChapter._id;
  const url = isUpdate ? `/${surveyChapter._id}` : ``;
  const req = isUpdate ? httpInstance.patch : httpInstance.post;
  return await req(url, cleanupSurveyChapterBody(surveyChapter));
};

interface IBulkUpdateSurveyChapters {
  surveyId: string;
  surveyChapters: Partial<ISurveyChapter>[];
}

export const bulkCreateOrUpdateSurveyChapters = async (
  bulkUpdateSurveyChapters: IBulkUpdateSurveyChapters,
): Promise<void> => {
  return await httpInstance.post(
    `/bulk-update`,
    cleanupBulkSurveyChapterBody(bulkUpdateSurveyChapters),
  );
};

export const deleteSurveyChapter = async (id: string): Promise<any> => {
  return await httpInstance.delete(`/${id}`);
};
