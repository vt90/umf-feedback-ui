import React from 'react';
import Box from '@mui/material/Box';
import Grid from '@mui/material/Grid';
import Typography from '@mui/material/Typography';
import { SxProps } from '@mui/material';
import { ISurveyBonification } from '../../../models/survey';
import { ISurvey } from '../../../models/survey';

interface ISurveyBonificationListItemProps {
  onSelect: (field?: string) => void;
  survey: ISurvey;
  surveyBonification: ISurveyBonification;
  surveyBonificationIndex: number;
}

interface ITypographyProps {
  id: string;
  onClick: (field?: string) => void;
  readOnly: boolean;
  sx?: SxProps;
  text: string | number;
}

const RenderTypography = (props: ITypographyProps) => {
  const { id, onClick, readOnly, sx, text } = props;

  return (
    <Typography
      onClick={(ev) => {
        ev.preventDefault();
        ev.stopPropagation();
        !readOnly && onClick(id);
      }}
      sx={{
        py: 1,
        pl: 2,
        border: 1,
        borderRadius: 1,
        borderColor: 'transparent',
        '&:hover': {
          borderColor: readOnly ? 'transparent' : '#3e3e3e',
        },
        ...sx,
      }}
    >
      <strong>{text}</strong>
    </Typography>
  );
};

const SurveyBonificationListItem = (props: ISurveyBonificationListItemProps) => {
  const { onSelect, survey, surveyBonification, surveyBonificationIndex } = props;

  const readOnly = !!survey.isClone;

  return (
    <Box onClick={() => !readOnly && onSelect()}>
      <Grid container spacing={2} alignItems="center">
        <Grid item xs={1}>
          <Typography variant="body2" sx={{ textAlign: { xs: 'left', md: 'right' } }}>
            {surveyBonificationIndex + 1}
          </Typography>
        </Grid>

        <Grid item xs={11}>
          <Box sx={{ display: 'flex' }}>
            <Box sx={{ flexGrow: 1, mr: 2 }}>
              <RenderTypography
                text={surveyBonification.name}
                id={`name`}
                onClick={onSelect}
                sx={{ px: '14px', py: '8.5px', minWidth: 64 }}
                readOnly={readOnly}
              />
            </Box>

            <Box
              sx={{
                bgcolor: 'background.default',
                display: 'flex',
                alignItems: 'center',
                pl: 2,
              }}
            >
              <Typography variant="body2">Punctaj:</Typography>
              <Box sx={{ maxWidth: 64, ml: 1 }}>
                <RenderTypography
                  text={surveyBonification.value}
                  id="value"
                  onClick={onSelect}
                  sx={{ px: '14px', py: '8.5px', minWidth: 64 }}
                  readOnly={readOnly}
                />
              </Box>
            </Box>
          </Box>
        </Grid>
      </Grid>
    </Box>
  );
};

export default SurveyBonificationListItem;
