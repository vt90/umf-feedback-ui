import React, { useCallback, useEffect, useMemo, useState } from 'react';
import Head from 'next/head';
import { useRouter } from 'next/router';
import { withPageAuthRequired } from '../../../../../guards/authRequired';
import { useMutation, useQuery, useQueryClient } from '@tanstack/react-query';
import Box from '@mui/material/Box';
import Button from '@mui/lab/LoadingButton';
import Card from '@mui/material/Card';
import CheckIcon from '@mui/icons-material/Check';
import CloseIcon from '@mui/icons-material/Close';
import RemoveIcon from '@mui/icons-material/Delete';
import EditIcon from '@mui/icons-material/Edit';
import Grid from '@mui/material/Grid';
import LoadingButton from '@mui/lab/LoadingButton';
import Typography from '@mui/material/Typography';
import AdminPage from '../../../../../components/AdminPage';
import ListLoadingIndicator from '../../../../../components/Common/ListLoadingIndcator';
import { getEvaluationSessionById } from '../../../../../services/evaluationSessions';
import { getUserDepartmentAssignmentById } from '../../../../../services/userDepartmentAssignment';
import {
  acknowledge,
  calculateEvaluationResult,
  contraSignEvaluation,
  createOrUpdateEvaluation,
  getEvaluations,
  removeEvaluation,
  setUserAgreement,
} from '../../../../../services/evaluations';
import EvaluationResult from '../../../../../components/Evaluation/Result';
import { USER_DEP_ASSIGNMENT_ROLES } from '../../../../../models/userDepartmentAssignment';
import Dialog from '../../../../../components/Common/Dialog';
import { useNotificationContext } from '../../../../../context/notificationContext';
import { IEvaluation } from '../../../../../models/evaluation';
import isBoolean from 'lodash/isBoolean';
import CommentForm from '../../../../../components/Evaluation/Comment/Form';
import noop from 'lodash/noop';
import Link from 'next/link';
import moment from 'moment';
import Status from '../../../../../components/Common/Status';
import { getSurveyRoleName, SURVEY_ROLE } from '../../../../../models/survey';
import { useAuthContext } from '../../../../../context/authContext';
import { canSalaryChange, shouldShowSalaryChange } from '../../../../../models/evaluationSession';
import SwitchToSalaryChangeDialog from '../../../../../components/EvaluationSessions/SwitchToSalaryChangeDialog';

const Survey = () => {
  const [showSalaryChangeMessage, setShowSalaryChangeMessage] = useState<IEvaluation | null>(null);
  const [userHasSalaryChangeInSession, setUserHasSalaryChangeInSession] = useState(false);
  const [showContraSignConfirmationDialogInfo, setShowContraSignConfirmationDialogInfo] =
    useState(null);
  const [showUserAgreementDialogInfo, setShowUserAgreementDialogInfo] = useState(null);
  const [contraSignAcceptReject, setContraSignAcceptReject] = useState<boolean | null>(null);
  const [userAcceptReject, setUserAcceptReject] = useState<boolean | null>(null);
  const [evaluationResult, setEvalResult] = useState<Partial<IEvaluation> | null>(null);
  const [showAcknowledgeDialog, setShowAcknowledgeDialog] = useState<boolean>(false);

  const [changes, setChanges] = useState({});
  const [bonificationChanges, setBonificationChanges] = useState({});
  const [objectiveChanges, setObjectiveChanges] = useState({});
  const { user } = useAuthContext();
  const router = useRouter();
  const { setError, setMessage } = useNotificationContext();
  const { id, userDepartmentAssignmentId, resultId } = router.query;

  const queryClient = useQueryClient();

  const FETCH_QUERY_KEY = ['evaluation-session', id];

  const FETCH_USER_EVAL_QUERY_KEY = (userId: string) => ['user-evaluations', userId, resultId];

  const FETCH_DEP_QUERY_KEY = [
    'evaluation-session-dep-assignment',
    userDepartmentAssignmentId,
    resultId,
  ];

  const FETCH_EVALUATION_QUERY_KEY = ['evaluation', userDepartmentAssignmentId, resultId];

  const { isLoading, data: evaluationSession } = useQuery(
    FETCH_QUERY_KEY,
    () => getEvaluationSessionById(id as string),
    {
      onError: () => router.push('/404'),
    },
  );

  const { isLoading: isDepartmentAssignmentLoading, data: userDepartmentAssignment } = useQuery(
    FETCH_DEP_QUERY_KEY,
    () => getUserDepartmentAssignmentById(userDepartmentAssignmentId as string),
    {
      onError: () => router.push('/404'),
    },
  );

  const { isLoading: isEvaluationLoading, data: evaluationResultData } = useQuery(
    FETCH_EVALUATION_QUERY_KEY,
    () =>
      getEvaluations({
        userDepartmentAssignmentId: userDepartmentAssignmentId as string,
        evaluationSessionId: id as string,
      }).then((data) => {
        if (!data?.results?.length) return null;

        let evaluationResult = data?.results[0];
        let userHasSalaryChangeInSession = false;

        if (resultId) {
          let found = null;
          data?.results.forEach((result) => {
            if (result._id === resultId) {
              found = result;
            }
            if (result.role === SURVEY_ROLE.SALARY_CHANGE) {
              userHasSalaryChangeInSession = true;
            }
          });

          if (found) evaluationResult = found;

          setUserHasSalaryChangeInSession(userHasSalaryChangeInSession);
        }

        return evaluationResult;
      }),
    {
      onError: () => router.push('/404'),
    },
  );

  const { data: userEvaluations } = useQuery(
    // @ts-ignore
    FETCH_USER_EVAL_QUERY_KEY(userDepartmentAssignment?.userId?._id),
    () =>
      getEvaluations({ userId: userDepartmentAssignment?.userId?._id }).then((data) => {
        return {
          ...data,
          // @ts-ignore
          results: data.results
            // ?.filter((result) => result._id !== evaluationResultData._id)
            ?.sort((a, b) => {
              return -1 * moment(a.createdAt).diff(moment(b.createdAt));
            }),
        };
      }),
    {
      enabled: !!(userDepartmentAssignment && evaluationResultData),
    },
  );

  const { isLoading: isContraSigning, mutate: contraSignEvaluationMutation } = useMutation(
    contraSignEvaluation,
    {
      onSuccess: async () => {
        await queryClient.invalidateQueries(FETCH_EVALUATION_QUERY_KEY);
        setShowContraSignConfirmationDialogInfo(null);
        setContraSignAcceptReject(null);
        setMessage('Formular contra-semnat');
      },
      onError: setError,
    },
  );

  const { isLoading: isUserAgreementLoading, mutate: userAgreementMutation } = useMutation(
    setUserAgreement,
    {
      onSuccess: async () => {
        await queryClient.invalidateQueries(FETCH_EVALUATION_QUERY_KEY);
        setShowUserAgreementDialogInfo(null);
        setUserAcceptReject(null);
        setMessage('Observatie salvata');
      },
      onError: setError,
    },
  );

  const { isLoading: isEvaluationDeleting, mutate: onDeleteEvaluationMutation } = useMutation(
    removeEvaluation,
    {
      onSuccess: async () => {
        await queryClient.invalidateQueries(FETCH_EVALUATION_QUERY_KEY);
        setMessage('Evaluare stearsa');
        await router.push('/reports');
      },
      onError: setError,
    },
  );

  const { isLoading: isEvaluationUpdating, mutate: updateEvaluation } = useMutation(
    createOrUpdateEvaluation,
    {
      onSuccess: async (evaluation) => {
        await queryClient.invalidateQueries(FETCH_EVALUATION_QUERY_KEY);
        setChanges({});
        setBonificationChanges({});
        setObjectiveChanges({});
        setMessage('Evaluare salvată cu success');

        if (
          !userHasSalaryChangeInSession &&
          evaluation?.role !== SURVEY_ROLE.SALARY_CHANGE &&
          // @ts-ignore
          canSalaryChange(evaluationSession, userDepartmentAssignment) &&
          shouldShowSalaryChange(evaluationSession, evaluationResult?.finalGrade)
        ) {
          setShowSalaryChangeMessage(evaluation);
        } else {
          setEvalResult(null);
        }
      },
      onError: setError,
    },
  );

  const { isLoading: isCalculating, mutate: calculate } = useMutation(calculateEvaluationResult, {
    onSuccess: async (data) => {
      setEvalResult(data);
    },
    onError: setError,
  });

  const { isLoading: isAcknowledging, mutate: acknowledgeEvaluation } = useMutation(acknowledge, {
    onSuccess: async () => {
      await queryClient.invalidateQueries(FETCH_EVALUATION_QUERY_KEY);
      setShowAcknowledgeDialog(false);
    },
    onError: setError,
  });

  const surveyForm = useMemo(() => {
    return (
      // @ts-ignore
      evaluationResultData &&
      evaluationSession &&
      evaluationSession.surveys &&
      userDepartmentAssignment &&
      evaluationSession.surveys.find((survey) => {
        // @ts-ignore
        return (
          survey.type === userDepartmentAssignment.type &&
          // @ts-ignore
          survey.role === evaluationResultData.role
        );
      })
    );
  }, [evaluationSession, userDepartmentAssignment, evaluationResultData]);

  const evaluation: Partial<IEvaluation> = useMemo(() => {
    // @ts-ignore
    let evalQuestionResults = [];
    // @ts-ignore
    let evalObjectiveResults = [];
    // @ts-ignore
    let evalBonifications = [];
    const comment = [];

    // @ts-ignore
    if (evaluationResultData?.questionResults) {
      const surveyQuestions = surveyForm?.surveyChapters?.length
        ? (() => {
            // @ts-ignore
            return surveyForm.surveyChapters.reduce((acc, cur) => {
              // @ts-ignore
              return [...acc, ...cur.questions];
            }, []);
          })()
        : [];

      evalQuestionResults = evaluationResultData?.questionResults?.map((question) => {
        const questionReplica = { ...question };
        // @ts-ignore
        const questionDetails = surveyQuestions?.find((q) => q._id === question.questionId);
        // @ts-ignore
        if (changes[question.questionId]) {
          comment.push({
            // @ts-ignore
            newValue: changes[question.questionId],
            previousValue: question.result,
            text: questionDetails?.name,
          });

          // @ts-ignore
          questionReplica.result = changes[question.questionId];
        }

        return questionReplica;
      });
    }

    // @ts-ignore
    if (evaluationResultData?.objectiveResult) {
      const surveyObjectives = surveyForm?.surveyObjectives?.length
        ? (() => {
            // @ts-ignore
            return surveyForm.surveyObjectives.reduce((acc, cur) => {
              // @ts-ignore
              return [...acc, ...cur.questions];
            }, []);
          })()
        : [];

      evalObjectiveResults = evaluationResultData?.objectiveResult?.map((objective) => {
        const objectiveReplica = { ...objective };
        // @ts-ignore
        const objectiveDetails = surveyObjectives?.find((o) => o._id === objective.objectiveId);
        // @ts-ignore
        if (objectiveChanges[objective.objectiveId]) {
          comment.push({
            // @ts-ignore
            newValue: objectiveChanges[objective.objectiveId],
            previousValue: objective.result,
            text: objectiveDetails?.name,
          });

          // @ts-ignore
          objectiveReplica.result = objectiveChanges[objective.objectiveId];
        }

        return objectiveReplica;
      });
    }

    // @ts-ignore
    if (evaluationResultData?.bonifications) {
      evalBonifications = evaluationResultData?.bonifications;
    }

    for (const bonificationId of Object.keys(bonificationChanges)) {
      const bonificationDetails = surveyForm?.surveyBonifications?.find(
        (bonification) => bonification._id === bonificationId,
      );

      const commentItem = {
        // @ts-ignore
        newValue: !!bonificationChanges[bonificationId],
        previousValue: false,
        text: bonificationDetails?.name,
      };

      // @ts-ignore
      const isExistingIndex = evalBonifications.findIndex(
        (b) => b.bonificationId === bonificationId,
      );

      if (isExistingIndex === -1) {
        evalBonifications.push({
          bonificationId,
          // @ts-ignore
          result: bonificationChanges[bonificationId],
        });
      } else {
        // @ts-ignore
        comment.previousValue = !!evalBonifications[isExistingIndex].result;
        // @ts-ignore
        evalBonifications[isExistingIndex].result = bonificationChanges[bonificationId];
      }

      comment.push(commentItem);
    }

    return evaluationResultData
      ? {
          // @ts-ignore
          ...evaluationResultData,
          // @ts-ignore
          questionResults: evalQuestionResults,
          // @ts-ignore
          objectiveResult: evalObjectiveResults,
          // @ts-ignore
          bonifications: evalBonifications,
          // @ts-ignore
          comment: JSON.stringify(comment),
        }
      : {};
  }, [evaluationResultData, changes, bonificationChanges, objectiveChanges, surveyForm]);

  const onDeleteEvaluation = useCallback(async () => {
    if (evaluation?._id) {
      await onDeleteEvaluationMutation(evaluation._id);
    }
  }, [evaluation, onDeleteEvaluationMutation]);

  const evaluationSettings = useMemo(() => {
    const departmentParentId = userDepartmentAssignment?.departmentId?.parentId;
    const isManagerToEvaluate =
      userDepartmentAssignment?.role === USER_DEP_ASSIGNMENT_ROLES.EVALUATOR;

    const whereToCheckForManager = isManagerToEvaluate
      ? // @ts-ignore
        user?.departmentsInManagement?.reduce((acc, cur) => {
          return [
            ...acc,
            // @ts-ignore
            ...(cur?.directChildDepartments?.length
              ? // @ts-ignore
                cur.directChildDepartments.map((dep) => ({ ...dep, departmentId: dep._id }))
              : []),
          ];
        }, [])
      : // @ts-ignore
        user?.userDepartmentAssignments?.filter((department) => {
          return !department.deactivated && department.role === USER_DEP_ASSIGNMENT_ROLES.EVALUATOR;
        });

    // @ts-ignore
    const isEvaluator = evaluation && user && evaluation?.evaluatorId?._id === user?._id;

    // @ts-ignore
    const isTargetUser = evaluation && user && evaluation?.userId?._id === user?._id;
    const isManager =
      departmentParentId &&
      user &&
      // @ts-ignore
      whereToCheckForManager?.find((department) => department.departmentId === departmentParentId);

    return {
      canAgree:
        isTargetUser && !evaluation?.contraEvaluated && !isBoolean(evaluation?.userAgreement),
      canComment: !!(isTargetUser || isManager) && !evaluation?.contraEvaluated,
      canContraSign: !!isManager && !evaluation?.contraEvaluated,
      canEdit:
        // @ts-ignore
        ((user && user.canEditAllReviews) ||
          (isManager && !evaluation?.contraEvaluated) ||
          (isEvaluator && !evaluation?.contraEvaluated && !evaluation.userAgreement)) &&
        evaluationSession?.isOpen,
      isEvaluator,
      isManager,
      isTargetUser,
    };
  }, [user, evaluation, userDepartmentAssignment]);

  const { canAgree, canComment, canContraSign, canEdit, isManager, isTargetUser } =
    evaluationSettings;

  useEffect(() => {
    if (isTargetUser && evaluation.contraEvaluated && !evaluation.userAcknowledged) {
      setShowAcknowledgeDialog(true);
    }
  }, [isTargetUser, evaluation]);

  const focusComment = (value: boolean) => {
    setTimeout(() => {
      !value && document.getElementById('commentForm')?.scrollIntoView({ behavior: 'smooth' });
      document.getElementById('commentFormSubmit')?.click();
    }, 200);
  };

  const onContraSign = (value: boolean) => {
    setContraSignAcceptReject(value);
    focusComment(value);
  };

  const onUserSign = (value: boolean) => {
    setUserAcceptReject(value);
    focusComment(value);
  };

  // @ts-ignore
  const onCommentSubmit = ({ comment }) => {
    const agreement = isManager
      ? { contraEvaluatorAgreement: !!contraSignAcceptReject }
      : { userAgreement: !!userAcceptReject };

    const action = isManager
      ? setShowContraSignConfirmationDialogInfo
      : setShowUserAgreementDialogInfo;

    action({
      ...evaluation,
      // @ts-ignore
      ...agreement,
      // @ts-ignore
      comment,
    });
  };

  if (isLoading || isDepartmentAssignmentLoading || isEvaluationLoading)
    return <ListLoadingIndicator />;

  if (!(evaluationSession && userDepartmentAssignment && surveyForm && evaluationResultData))
    return null;

  return (
    <>
      <Head>
        <title>{evaluationSession.name}</title>
      </Head>

      <AdminPage
        title={evaluationSession.name}
        pages={[
          { url: '/evaluation-sessions', name: 'Sesiuni de evaluare' },
          { url: `/evaluation-sessions/${id}`, name: evaluationSession.name },
          {
            url: `/evaluation-sessions/${id}/evaluate/${userDepartmentAssignmentId}`,
            name: userDepartmentAssignment?.userId?.fullName,
          },
        ]}
        actions={
          <>
            {Object.keys(changes).length ||
            Object.keys(bonificationChanges).length ||
            Object.keys(objectiveChanges).length ? (
              <Box sx={{ mr: 1 }}>
                <Button
                  onClick={() => calculate(evaluation)}
                  variant="contained"
                  color="inherit"
                  endIcon={<EditIcon />}
                  loading={isCalculating}
                >
                  Modifică
                </Button>
              </Box>
            ) : null}
            {canContraSign ? (
              <Button
                key="accept"
                onClick={() => onContraSign(true)}
                variant="contained"
                disabled={
                  !!(
                    Object.keys(changes).length ||
                    Object.keys(objectiveChanges).length ||
                    Object.keys(bonificationChanges).length
                  )
                }
                endIcon={<CheckIcon />}
              >
                Contra-semnează
              </Button>
            ) : null}

            {canAgree
              ? [
                  <Button
                    key="reject"
                    onClick={() => onUserSign(false)}
                    variant="contained"
                    color="error"
                    endIcon={<CloseIcon />}
                    sx={{ mr: 1 }}
                  >
                    Nu sunt de acord
                  </Button>,
                  <Button
                    key="accept"
                    onClick={() => onUserSign(true)}
                    variant="contained"
                    endIcon={<CheckIcon />}
                  >
                    Sunt de acord
                  </Button>,
                ]
              : null}

            {
              //@ts-ignore
              user.isPlatformAdmin ? (
                <Button
                  key="delete"
                  onClick={onDeleteEvaluation}
                  variant="contained"
                  color="error"
                  endIcon={<RemoveIcon />}
                  loading={isEvaluationDeleting}
                  sx={{ mr: 1 }}
                >
                  Sterge Evaluarea
                </Button>
              ) : null
            }
          </>
        }
      >
        <Card sx={{ p: 3 }}>
          <Grid container spacing={2}>
            {/*@ts-ignore */}
            <Grid item xs={12} lg={userEvaluations?.results?.length > 1 ? 8 : 12}>
              <Box sx={{ mt: -1 }}>
                <EvaluationResult
                  // @ts-ignore
                  canEdit={canEdit}
                  // @ts-ignore
                  evaluation={evaluation}
                  hasChanges={
                    !!(
                      Object.keys(changes).length ||
                      Object.keys(bonificationChanges).length ||
                      Object.keys(objectiveChanges).length
                    )
                  }
                  onChangeEvaluationResult={(questionId, value) =>
                    setChanges({
                      ...changes,
                      [questionId]: value,
                    })
                  }
                  onChangeBonifications={(bonificationId, value) =>
                    setBonificationChanges({
                      ...bonificationChanges,
                      [bonificationId]: value,
                    })
                  }
                  onChangeObjectiveResult={(objectiveId, value) => {
                    console.log('objectiveId: ', objectiveId, value);
                    setObjectiveChanges({
                      ...objectiveChanges,
                      [objectiveId]: value,
                    });
                  }}
                  survey={surveyForm}
                  userDepartmentAssignment={userDepartmentAssignment}
                />
              </Box>
            </Grid>

            {userEvaluations?.results?.length ? (
              <Grid item xs={12} lg={4}>
                <Box
                  sx={{
                    borderLeftWidth: 1,
                    borderLeftColor: 'divider',
                    borderLeftStyle: 'solid',
                    px: 2,
                    py: 1.5,
                  }}
                >
                  <Typography variant="body1">
                    <strong>Evaluări precedente</strong>
                  </Typography>
                  {userEvaluations?.results?.map((evaluation) => {
                    const {
                      _id,
                      createdAt,
                      departmentId,
                      evaluationSessionId,
                      finalGrade,
                      finalResult,
                      userDepartmentAssignmentId,
                      role,
                    } = evaluation;

                    // @ts-ignore
                    const linkUrl = `/evaluation-sessions/${evaluationSessionId?._id}/result/${userDepartmentAssignmentId?._id}?resultId=${_id}`;

                    const renderLink = (children: React.ReactNode) => (
                      <Link href={linkUrl}>
                        <a>{children}</a>
                      </Link>
                    );

                    const color = role === SURVEY_ROLE.EVALUATION ? 'info' : 'success';

                    return (
                      <Box
                        sx={{
                          px: 4,
                          py: 3,
                          mt: 2,
                          mb: 4,
                          border: '0.0625rem solid rgb(222, 226, 230)',
                          borderRadius: 2,
                        }}
                        key={_id}
                      >
                        {/*@ts-ignore*/}
                        {renderLink(<Typography>{evaluationSessionId?.name}</Typography>)}
                        <Status label={getSurveyRoleName(`${role}`)} color={color} sx={{ mb: 3 }} />

                        <Grid container spacing={1}>
                          {[
                            // @ts-ignore
                            { name: 'Departament', value: departmentId.name },
                            // @ts-ignore
                            { name: 'Funcția', value: userDepartmentAssignmentId?.role },
                            // @ts-ignore
                            { name: 'Tip', value: userDepartmentAssignmentId?.type },
                            // @ts-ignore
                            {
                              name: 'Data evaluarii',
                              value: moment(createdAt).format('DD.MM.YYYY'),
                            },
                            // @ts-ignore
                            {
                              name: 'Rezultat',
                              value: (
                                <strong>
                                  {finalGrade?.toFixed(2)} - {finalResult}
                                </strong>
                              ),
                            },
                          ].map((item) => [
                            <Grid key={item.name} item xs={6} md={3}>
                              <Typography variant="body2">
                                <strong>{item.name}</strong>
                              </Typography>
                            </Grid>,
                            <Grid key={`${item.name}-value`} item xs={6} md={9}>
                              <Typography variant="body2" sx={{ textAlign: 'right' }}>
                                {item.value}
                              </Typography>
                            </Grid>,
                          ])}
                        </Grid>
                      </Box>
                    );
                  })}
                </Box>
              </Grid>
            ) : null}

            {canComment && (
              <Grid item xs={12}>
                <CommentForm
                  isLoading={isContraSigning}
                  shouldValidate={contraSignAcceptReject === false || userAcceptReject === false}
                  onSubmit={onCommentSubmit}
                />
              </Grid>
            )}
          </Grid>
        </Card>
      </AdminPage>

      <Dialog
        actions={
          <LoadingButton
            variant="contained"
            onClick={() =>
              showContraSignConfirmationDialogInfo &&
              contraSignEvaluationMutation(showContraSignConfirmationDialogInfo)
            }
            loading={isContraSigning}
          >
            Contra-semnează
          </LoadingButton>
        }
        content={
          <>
            <Typography color="textSecondary" textAlign="center">
              Sunteți siguri că doriți să {/*@ts-ignore*/}
              {showContraSignConfirmationDialogInfo?.contraEvaluatorAgreement
                ? 'aprobați'
                : 'respingeți'}{' '}
              formularul de evaluare?
            </Typography>
          </>
        }
        title="Confirmati contra-semnarea?"
        onClose={() => {
          setShowContraSignConfirmationDialogInfo(null);
          setContraSignAcceptReject(null);
        }}
        open={!!showContraSignConfirmationDialogInfo}
      />

      <Dialog
        actions={
          <LoadingButton
            variant="contained"
            onClick={() =>
              showUserAgreementDialogInfo && userAgreementMutation(showUserAgreementDialogInfo)
            }
            loading={isUserAgreementLoading}
          >
            {/* @ts-ignore */}
            {showUserAgreementDialogInfo?.userAgreement ? 'Acceptă' : 'Respinge'}
          </LoadingButton>
        }
        content={
          <>
            <Typography color="textSecondary" textAlign="center">
              Sunteți siguri că doriți să {/*@ts-ignore*/}
              {showUserAgreementDialogInfo?.userAgreement ? 'acceptați' : 'respingeți'} formularul
              de evaluare?
            </Typography>
          </>
        }
        title="Confirmați"
        onClose={() => {
          setShowUserAgreementDialogInfo(null);
          setUserAcceptReject(null);
        }}
        open={!!showUserAgreementDialogInfo}
      />

      <Dialog
        actions={
          <LoadingButton
            variant="contained"
            onClick={() => updateEvaluation(evaluation)}
            loading={isEvaluationUpdating}
          >
            Confirmă
          </LoadingButton>
        }
        content={
          <>
            {evaluation && (
              <>
                <EvaluationResult
                  // @ts-ignore
                  evaluation={{
                    ...evaluation,
                    ...evaluationResult,
                  }}
                  survey={surveyForm}
                  userDepartmentAssignment={userDepartmentAssignment}
                />
              </>
            )}
          </>
        }
        title="Confirmati modificarea?"
        onClose={() => setEvalResult(null)}
        open={!!evaluationResult}
        maxWidth="lg"
      />

      <Dialog
        actions={
          <LoadingButton
            variant="contained"
            // @ts-ignore
            onClick={() => acknowledgeEvaluation(evaluation._id)}
            loading={isAcknowledging}
          >
            Vizualizează
          </LoadingButton>
        }
        content={
          <>
            <Typography color="textSecondary" textAlign="center">
              Prin apăsarea butonului de vizualizare veți confirma ca ați luat la cunoștința
              evaluarea și punctajul final al evaluării!
            </Typography>
          </>
        }
        title="Luare la cunoștință"
        onClose={noop}
        open={showAcknowledgeDialog}
        maxWidth="sm"
      />

      <SwitchToSalaryChangeDialog
        evalutation={showSalaryChangeMessage}
        onClose={() => {
          setShowSalaryChangeMessage(null);
          setEvalResult(null);
        }}
        open={!!showSalaryChangeMessage}
      />
    </>
  );
};

export const getServerSideProps = withPageAuthRequired();

export default Survey;
