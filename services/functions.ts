import axios from 'axios';
import { IFunctionModel } from '../models/function';
import { IPaginationResult } from '../models/base';

const httpInstance = axios.create({
  baseURL: '/api/functions',
});

httpInstance.interceptors.response.use(
  (response) => response.data,
  (error) => {
    if (error && error.response && error.response.data) {
      return Promise.reject(
        error.response.data.message
          ? `${error.response.data.message || error.response.data.error}`
          : error.response.data?.error || error.response.data,
      );
    } else {
      return Promise.reject({ message: 'Server error' });
    }
  },
);

export const getAllFunctions = async (): Promise<IPaginationResult<IFunctionModel>> => {
  return await httpInstance.get('', {});
};
