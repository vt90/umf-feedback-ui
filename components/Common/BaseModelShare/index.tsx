import React, { useMemo, useState } from 'react';
import { CopyToClipboard } from 'react-copy-to-clipboard';
import noop from 'lodash/noop';
import ContentCopyIcon from '@mui/icons-material/ContentCopy';
import SendIcon from '@mui/icons-material/Send';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';
import Snackbar from '@mui/material/Snackbar';
import { IBaseModel } from '../../../models/base';
import Dialog from '../Dialog';

const BASE_URL = process.env.NEXT_PUBLIC_BASE_URL;

interface IShareProps {
  model: IBaseModel;
  endpoint: string;
  params?: string;
}

const BaseModelShare = (props: IShareProps) => {
  const [open, setOpen] = useState(false);
  const [copied, setCopied] = useState(false);
  const { endpoint, model, params } = props;
  const URL = useMemo(
    () => `${BASE_URL}/${endpoint}/${model._id}${params ? `?${params}` : ''}`,
    [model, endpoint, params],
  );

  return (
    <>
      <Box>
        <Button
          color="info"
          endIcon={<SendIcon />}
          variant="contained"
          onClick={() => setOpen(true)}
        >
          Distribuie
        </Button>
      </Box>

      <Dialog
        actions={
          <CopyToClipboard text={URL} onCopy={() => setCopied(true)}>
            <Button endIcon={<ContentCopyIcon />} variant="contained" onClick={() => setOpen(true)}>
              Copiază
            </Button>
          </CopyToClipboard>
        }
        content={
          <>
            <TextField
              value={URL}
              onChange={noop}
              size="small"
              variant="outlined"
              fullWidth
              sx={{
                '& legend': { display: 'none' },
                '& fieldset': { top: 0 },
              }}
            />
          </>
        }
        title="Distribuie"
        onClose={() => setOpen(false)}
        open={open}
      />

      <Snackbar
        anchorOrigin={{ vertical: 'bottom', horizontal: 'center' }}
        autoHideDuration={3000}
        message="Link copiat"
        onClose={() => setCopied(false)}
        open={copied}
      />
    </>
  );
};

export default BaseModelShare;
