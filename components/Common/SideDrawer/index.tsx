import React from 'react';
import Box from '@mui/material/Box';
import Divider from '@mui/material/Divider';
import Drawer from '@mui/material/Drawer';
import Typography from '@mui/material/Typography';

interface ISideDrawerProps {
  actions?: React.ReactNode;
  children: React.ReactNode;
  onClose: () => void;
  open: boolean;
  title?: string;
}

const SideDrawer = (props: ISideDrawerProps) => {
  const { actions, children, onClose, open, title } = props;

  return (
    <Drawer anchor="right" open={open} onClose={onClose}>
      <Box
        sx={{
          width: '80vw',
          maxWidth: '670px',
          position: 'relative',
        }}
      >
        <Box p={2}>
          <Typography variant="h5">{title}</Typography>
        </Box>
        <Divider />

        {children}

        {actions ? (
          <Box sx={{ position: 'sticky', bottom: 0, width: '100%' }}>
            <Divider />

            <Box py={1} px={2}>
              {actions}
            </Box>
          </Box>
        ) : null}
      </Box>
    </Drawer>
  );
};

export default SideDrawer;
