import React from 'react';
import MuiTextField, { StandardTextFieldProps } from '@mui/material/TextField';

export interface ITextFieldProps extends StandardTextFieldProps {
  errorMessage?: string | boolean;
  [key: string]: any;
}

const TextField = (props: ITextFieldProps, ref: any) => {
  const { errorMessage, sx, ...rest } = props;
  return (
    <MuiTextField
      ref={ref}
      fullWidth
      error={!!errorMessage}
      helperText={errorMessage}
      sx={{
        '& legend': { display: 'none' },
        '& fieldset': { top: 0 },
        ...sx,
      }}
      {...rest}
    />
  );
};

export default React.forwardRef(TextField);
