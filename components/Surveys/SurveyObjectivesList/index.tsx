import React, { useCallback, useEffect, useState } from 'react';
import { useMutation } from '@tanstack/react-query';
import Button from '@mui/material/Button';
import AddIcon from '@mui/icons-material/Add';
import Box from '@mui/material/Box';
import LoadingButton from '@mui/lab/LoadingButton';
import Typography from '@mui/material/Typography';
import SurveyObjectiveForm from '../SurveyObjectivesForm';
import SurveyObjectiveListItem from './SurveyObjectiveListItem';
import Dialog from '../../Common/Dialog';
import NoDataAvailable from '../../Common/NoDataAvailable';
import { ISurvey } from '../../../models/survey';
import { ISurveyObjective } from '../../../models/surveyObjective';
import { deleteSurveyObjective } from '../../../services/surveyObjectives';

interface ISurveyObjectivesListProps {
  isLoading: boolean;
  onSubmit: (data: any) => void;
  survey: ISurvey;
}

const SurveyObjectivesList = (props: ISurveyObjectivesListProps) => {
  const { isLoading, onSubmit, survey } = props;
  const surveyId = survey._id;

  const [createFormValues, setCreateFormValues] = useState<Partial<ISurveyObjective> | null>(null);
  const [removeSurveyObjectiveInfo, setRemoveSurveyObjectiveInfo] =
    useState<Partial<ISurveyObjective> | null>(null);
  const [formFocus, setFormFocus] = useState<string | undefined>(undefined);

  const resetStates = useCallback(() => {
    setRemoveSurveyObjectiveInfo(null);
    setCreateFormValues(null);
    setFormFocus(undefined);
  }, []);

  const { isLoading: isSurveyObjectiveDeleting, mutate: deleteSurveyObjectiveById } = useMutation(
    deleteSurveyObjective,
    {
      onSuccess: () => {
        // @ts-ignore
        removeSurveyObjectiveInfo && onRemoveSurveyObjective(removeSurveyObjectiveInfo);
        resetStates();
      },
    },
  );

  useEffect(() => {
    resetStates();
  }, [resetStates, survey]);

  if (!survey) return null;

  const createButton = !createFormValues && !survey.isClone && (
    <Button
      onClick={() =>
        setCreateFormValues({
          // @ts-ignore
          name: `Obiective în perioada evaluată`,
          order: 1,
          surveyId,
          weight: 1,
          questions: [
            { _id: '', name: 'Obiectiv 1', order: 1, timePercentage: 100, donePercentage: 100 },
          ],
        })
      }
      variant="contained"
      size="large"
      endIcon={<AddIcon />}
    >
      Adaugă obiective&nbsp;&nbsp;&nbsp;&nbsp;
    </Button>
  );

  return (
    <>
      {survey.surveyObjectives?.length || createFormValues ? (
        <>
          {survey.surveyObjectives?.map((surveyObjective) => {
            const isInEditMode = createFormValues && surveyObjective._id === createFormValues._id;
            return (
              <Box key={surveyObjective._id} sx={{ mb: 2 }}>
                {isInEditMode ? (
                  <SurveyObjectiveForm
                    survey={survey}
                    initialValues={createFormValues}
                    initialFocus={formFocus}
                    isLoading={isLoading}
                    onSubmit={onSubmit}
                    onCancel={() => setCreateFormValues(null)}
                  />
                ) : (
                  <>
                    <SurveyObjectiveListItem
                      onSelect={(id) => {
                        id && setFormFocus(id);
                        setCreateFormValues({ ...surveyObjective });
                      }}
                      survey={survey}
                      surveyObjective={surveyObjective}
                    />
                  </>
                )}
              </Box>
            );
          })}

          {createFormValues && !createFormValues._id && (
            <SurveyObjectiveForm
              survey={survey}
              initialValues={createFormValues}
              isLoading={isLoading}
              onSubmit={onSubmit}
              onCancel={() => setCreateFormValues(null)}
            />
          )}
        </>
      ) : (
        <NoDataAvailable
          content="În momentul de faţă nu există niciun obiectiv de evaluare definit"
          actions={createButton}
        />
      )}

      <Dialog
        actions={
          <LoadingButton
            variant="contained"
            onClick={() => {
              if (removeSurveyObjectiveInfo?._id) {
                deleteSurveyObjectiveById(removeSurveyObjectiveInfo._id);
              }
            }}
            loading={isSurveyObjectiveDeleting}
          >
            Șterge
          </LoadingButton>
        }
        content={
          <>
            <Typography color="textSecondary" textAlign="center">
              Sunteți siguri că doriți să ștergeți obiectivul de evaluare
              <strong>
                <i>&quot;{removeSurveyObjectiveInfo?.name}&quot;</i>
              </strong>
              ?
            </Typography>
          </>
        }
        title="Confirmati ștergerea?"
        onClose={() => setRemoveSurveyObjectiveInfo(null)}
        open={!!removeSurveyObjectiveInfo}
      />
    </>
  );
};

export default SurveyObjectivesList;
