import React from 'react';
import Grid from '@mui/material/Grid';
import Typography from '@mui/material/Typography';
import Select from '@mui/material/Select';
import MenuItem from '@mui/material/MenuItem';
import Box from '@mui/material/Box';
import InputAdornment from '@mui/material/InputAdornment';
import SearchIcon from '@mui/icons-material/Search';
import DownloadIcon from '@mui/icons-material/Download';
import TextField from '../../Common/FormInputs/TextField';
import OrderToggle from '../../Common/OrderToggle';
import LoadingButton from '@mui/lab/LoadingButton';

const ORDER_BY = {
  createdAt: {
    name: 'Data evaluării',
    value: 'createdAt',
  },
  finalGrade: {
    name: 'Rezultat',
    value: 'finalGrade',
  },
  contraEvaluated: {
    name: 'Contra-evaluată',
    value: 'contraEvaluated',
  },
  userAcknowledged: {
    name: 'Luată la cunoștinț',
    value: 'userAcknowledged',
  },
  userAgreement: {
    name: 'Acceptată de evaluat',
    value: 'userAgreement',
  },
  contraEvaluatorAgreement: {
    name: 'Aprobată',
    value: 'contraEvaluatorAgreement',
  },
};

interface HRListHeaderProps {
  activeView: string | null;
  order: number;
  setOrder: (value: number) => void;
  orderBy: string | null;
  setOrderBy: (orderBy: any) => void;
  searchTerm: string | null;
  setSearchTerm: (searchTerm: string | null) => void;
  onExport?: () => void;
  isExporting?: boolean;
  onDownload?: () => void;
  isDownloading?: boolean;
}

const HREvaluationsListHeader = (props: HRListHeaderProps) => {
  const {
    activeView,
    order,
    setOrder,
    orderBy,
    setOrderBy,
    searchTerm,
    setSearchTerm,
    onExport,
    isExporting,
    onDownload,
    isDownloading,
  } = props;

  return (
    <>
      <Grid container spacing={2} alignItems="center">
        <Grid item xs={12} lg={4}>
          <TextField
            value={searchTerm}
            onChange={(ev: { target: { value: string | null } }) => setSearchTerm(ev.target.value)}
            placeholder="Nume sau marca"
            InputProps={{
              startAdornment: (
                <InputAdornment position="start">
                  <SearchIcon />
                </InputAdornment>
              ),
            }}
          />
        </Grid>

        <Grid item xs={12} lg={8}>
          <Box sx={{ display: 'flex', alignItems: 'center' }}>
            {activeView === 'reports' ? (
              <>
                <Typography variant="body2" color="textSecondary">
                  Ordonează după
                </Typography>

                <Select
                  value={orderBy}
                  onChange={(ev) => setOrderBy(ev.target.value)}
                  size="small"
                  sx={{
                    boxShadow: 'none',
                    '.MuiOutlinedInput-notchedOutline': {
                      border: 0,
                      outline: 'none',
                    },
                  }}
                >
                  {Object.values(ORDER_BY).map((criteria) => (
                    <MenuItem key={criteria.value} value={criteria.value}>
                      {criteria.name}
                    </MenuItem>
                  ))}
                </Select>

                <Box>
                  <OrderToggle order={order} setOrder={setOrder} />
                </Box>
              </>
            ) : null}

            <Box sx={{ flexGrow: 1 }}></Box>

            <Box>
              {onExport ? (
                <Box>
                  <LoadingButton
                    endIcon={<DownloadIcon />}
                    variant="contained"
                    onClick={onExport}
                    loading={isExporting}
                    color="success"
                    fullWidth
                  >
                    Export Excel
                  </LoadingButton>
                </Box>
              ) : null}

              {onDownload ? (
                <Box>
                  <LoadingButton
                    endIcon={<DownloadIcon />}
                    variant="contained"
                    onClick={onDownload}
                    loading={isDownloading}
                    sx={{ mt: 1 }}
                  >
                    Descarcă Evaluări
                  </LoadingButton>
                </Box>
              ) : null}
            </Box>
          </Box>
        </Grid>
      </Grid>
    </>
  );
};

export default HREvaluationsListHeader;
