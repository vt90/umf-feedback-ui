import React, { useMemo } from 'react';
import Grid from '@mui/material/Grid';
import Typography from '@mui/material/Typography';
import { IExtendedUserDepartmentAssignment } from '../../../models/userDepartmentAssignment';
import { ISurvey, SURVEY_ROLE } from '../../../models/survey';
import Box from '@mui/material/Box';
import orderBy from 'lodash/orderBy';
import { EVALUATION_ACTIVITY_TYPES, IEvaluation } from '../../../models/evaluation';
import Status from '../../Common/Status';
import moment from 'moment/moment';
import { useMutation } from '@tanstack/react-query';
import { exportEvaluations } from '../../../services/evaluations';
import LoadingButton from '@mui/lab/LoadingButton';
import DownloadIcon from '@mui/icons-material/Download';

interface IEvaluationHeaderProps {
  evaluation?: IEvaluation;
  hasChanges?: boolean;
  survey: ISurvey;
  userDepartmentAssignment: IExtendedUserDepartmentAssignment;
}

const isUMF = process.env.NEXT_PUBLIC_IS_UMF === 'true';

const EvaluationHeader = (props: IEvaluationHeaderProps) => {
  const { evaluation, hasChanges, userDepartmentAssignment, survey } = props;
  // @ts-ignore
  const { isLoading: isExporting, mutate: onExportEvaluation } = useMutation(exportEvaluations);

  const exportParams = {
    // @ts-ignore
    evaluationSessionId: evaluation?.evaluationSessionId?._id,
    // @ts-ignore
    userSearchTerm: evaluation?.userId?.email,
  };

  const onDownload = async () => {
    if (evaluation?._id) {
      await onExportEvaluation(exportParams);
    }
  };

  const headerItems = useMemo(
    () => [
      { name: 'Dl./D-na', value: <strong>{userDepartmentAssignment?.userId?.fullName}</strong> },
      { name: 'Marca', value: userDepartmentAssignment?.userId?.email },
      { name: 'Departament', value: userDepartmentAssignment?.departmentId?.name },
      { name: 'Funcția', value: userDepartmentAssignment?.role },
      { name: 'Tip', value: userDepartmentAssignment?.type },
      // @ts-ignore
      ...(evaluation?.evaluatorId?.fullName
        ? // @ts-ignore
          [{ name: 'Evaluator', value: evaluation?.evaluatorId?.fullName }]
        : []), // @ts-ignore
      ...(evaluation?.contraEvaluatorId?.fullName
        ? // @ts-ignore
          [{ name: 'Contra-evaluator', value: evaluation?.contraEvaluatorId?.fullName }]
        : []),
    ],
    [evaluation, userDepartmentAssignment],
  );

  return (
    <Grid container spacing={2} alignItems="flex-start">
      {evaluation?.finalGrade
        ? [
            <Grid key="grade" item xs={12} sm={5} md={4}>
              <Box
                sx={{ px: 3, py: 2, border: '0.0625rem solid rgb(222, 226, 230)', borderRadius: 2 }}
              >
                <Box
                  sx={{ display: 'flex', justifyContent: 'space-between', alignItems: 'center' }}
                >
                  <Typography
                    color="textSecondary"
                    sx={{
                      ...(hasChanges && {
                        textDecoration: 'line-through',
                        fontStyle: 'italic',
                      }),
                    }}
                  >
                    <strong>Rezultat final</strong>
                  </Typography>

                  <Typography
                    variant="h6"
                    sx={{
                      ...(hasChanges && {
                        textDecoration: 'line-through',
                        fontStyle: 'italic',
                      }),
                    }}
                  >
                    <strong>
                      {evaluation.finalResult ? `${evaluation.finalResult} - ` : ''}
                      {evaluation.finalGrade}
                    </strong>
                  </Typography>
                </Box>

                {survey.role === SURVEY_ROLE.SALARY_CHANGE && isUMF && (
                  <Box
                    sx={{ display: 'flex', justifyContent: 'space-between', alignItems: 'center' }}
                  >
                    <Typography
                      color="textSecondary"
                      sx={{
                        ...(hasChanges && {
                          textDecoration: 'line-through',
                          fontStyle: 'italic',
                        }),
                      }}
                    >
                      <strong>Procentaj modificare</strong>
                    </Typography>

                    <Typography
                      variant="h6"
                      gutterBottom
                      sx={{
                        ...(hasChanges && {
                          textDecoration: 'line-through',
                          fontStyle: 'italic',
                        }),
                      }}
                    >
                      <strong>{evaluation.salaryChangePercentage}%</strong>
                    </Typography>
                  </Box>
                )}

                {survey.role !== SURVEY_ROLE.SALARY_CHANGE &&
                survey?.surveyResultIntervals?.length ? (
                  <Box sx={{ mt: 1, mb: 2 }}>
                    {(() => {
                      const orderedIntervals = orderBy(
                        survey?.surveyResultIntervals,
                        ['minValue'],
                        ['desc'],
                      );

                      return orderedIntervals.map((interval, index) => {
                        return (
                          <Box key={interval.name} sx={{ display: 'flex', alignItems: 'center' }}>
                            <Box sx={{ flexGrow: 1, mr: 2 }}>
                              <Typography
                                variant="caption"
                                color="textSecondary"
                                sx={{ fontWeight: 300 }}
                              >
                                {interval.name}
                              </Typography>
                            </Box>

                            <Typography
                              variant="caption"
                              color="textSecondary"
                              sx={{ fontWeight: 300 }}
                            >
                              {index ? '' : '≥ '}
                              {interval.minValue.toFixed(2)}
                              {index
                                ? ` - ${(orderedIntervals[index - 1]?.minValue - 0.01).toFixed(2)}`
                                : ''}
                            </Typography>
                          </Box>
                        );
                      });
                    })()}
                  </Box>
                ) : null}

                {evaluation?.events
                  ?.filter(
                    (event) =>
                      event.type === EVALUATION_ACTIVITY_TYPES.USER_APPROVAL ||
                      event.type === EVALUATION_ACTIVITY_TYPES.USER_ACKNOWLEDGEMENT ||
                      event.type === EVALUATION_ACTIVITY_TYPES.CONTRA_SIGNED,
                  )
                  .map((event) => {
                    let eventStatus;
                    let eventStatusColor = 'success';

                    switch (event.type) {
                      case EVALUATION_ACTIVITY_TYPES.CONTRA_SIGNED:
                        eventStatus = 'Evaluare contra-semnată';
                        break;
                      case EVALUATION_ACTIVITY_TYPES.USER_ACKNOWLEDGEMENT:
                        eventStatus = 'Evaluare luată la cunostinta';
                        break;
                      case EVALUATION_ACTIVITY_TYPES.USER_APPROVAL:
                        // @ts-ignore
                        const value = JSON.parse(event.value);
                        eventStatus = `Evaluare ${value ? 'acceptată' : 'respinsă'}`;
                        if (!value) {
                          eventStatusColor = 'error';
                        }
                        break;
                    }

                    return (
                      <Box
                        key={event._id}
                        sx={{ mt: 1, display: 'flex', justifyContent: 'space-between' }}
                      >
                        <Status
                          // @ts-ignore
                          label={eventStatus}
                          // @ts-ignore
                          color={eventStatusColor}
                          sx={{ minWidth: 170 }}
                        />

                        <Typography
                          variant="caption"
                          color="textSecondary"
                          sx={{ fontWeight: 300 }}
                        >
                          {moment(event.createdAt).format('YYYY.MM.DD')}
                        </Typography>
                      </Box>
                    );
                  })}
              </Box>
            </Grid>,
            <Grid key="offset" item xs={12} sm={2}></Grid>,
          ]
        : null}

      <Grid item xs={12} sm={5} md={6} container spacing={0.5} sx={{ mt: 1 }}>
        {headerItems.map((item, index) => [
          <Grid key={item.name} item xs={6} md={3}>
            <Typography>
              <strong>{item.name}</strong>
            </Typography>
          </Grid>,
          <Grid key={`${item.name}-value`} item xs={6} md={9}>
            <Typography variant={`body${index ? '2' : '1'}`} sx={{ textAlign: 'right' }}>
              {item.value}
            </Typography>
          </Grid>,
        ])}

        {evaluation?._id ? (
          <>
            <Grid item xs={6} md={3} />
            <Grid item xs={6} md={9}>
              <Box sx={{ display: 'flex', justifyContent: 'flex-end' }}>
                <LoadingButton
                  endIcon={<DownloadIcon />}
                  variant="contained"
                  onClick={onDownload}
                  loading={isExporting}
                  sx={{ mt: 1 }}
                >
                  Descarcă Evaluare
                </LoadingButton>
              </Box>
            </Grid>
          </>
        ) : null}
      </Grid>
    </Grid>
  );
};

export default EvaluationHeader;
