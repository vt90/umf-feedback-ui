import React, { useEffect, useState } from 'react';
import Head from 'next/head';
import dynamic from 'next/dynamic';
import { withPageAuthRequired } from '../../guards/authRequired';
import {
  createOrUpdateSSMPackage,
  deleteSSMPackage,
  findSSMPackage,
} from '../../services/ssmPackages';
import { useQuery, useMutation, useQueryClient } from '@tanstack/react-query';
import Button from '@mui/material/Button';
import Paper from '@mui/material/Paper';
import AdminPage from '../../components/AdminPage';
import { ISSMPackage, SSM_PACKAGE_TYPE } from '../../models/ssmPackage';
import LoadingButton from '@mui/lab/LoadingButton';
import Typography from '@mui/material/Typography';
import Dialog from '../../components/Common/Dialog';
import NoDataAvailable from '../../components/Common/NoDataAvailable';
import AddIcon from '@mui/icons-material/Add';
import { SELECTED_ACTIONS } from '../../lib/constants';
import { useNotificationContext } from '../../context/notificationContext';
import { getAllFunctions } from '../../services/functions';
import SSMPackagesForm from '../../components/SSMPackages/Form';
import ListLoadingIndicator from '../../components/Common/ListLoadingIndcator';
import SSMFunctionAllocationMatrix from '../../components/SSM/FunctionAllocationMatrix';
import { useRouter } from 'next/router';
import Tab from '@mui/material/Tab';
import Tabs from '@mui/material/Tabs';
import Card from '@mui/material/Card';
import { getAllDepartments } from '../../services/departments';
// @ts-ignore
const SSMPackageList = dynamic(() => import('../../components/SSMPackages/List'), {
  ssr: false,
});

const FETCH_QUERY_KEY = ['ssm-packages'];
const FETCH_FUNCTIONS_QUERY_KEY = ['depFunctions'];
const FETCH_DEPARTMENTS_QUERY_KEY = ['departments'];

const VIEWS = ['list', 'function-allocations'];
const VIEW_NAMES = ['Lista pachete', 'Alocări funcţii'];

const SSMPackages = () => {
  const [selectedSSMPackage, setSelectedSSMPackage] = useState<Partial<ISSMPackage> | null>(null);
  const [selectedAction, setSelectedAction] = useState<string | null>(null);
  const { setError, setMessage } = useNotificationContext();
  const router = useRouter();

  const { activeView } = router.query;

  const onViewChange = (view: string) => {
    router.push(`${router.route}?activeView=${view}`);
  };

  useEffect(() => {
    // @ts-ignore
    if (activeView && VIEWS.indexOf(activeView)) {
      return;
    } else {
      onViewChange('list');
    }
  }, [activeView]);

  const onSSMPackageAction = (action: string) => (survey: Partial<ISSMPackage>) => {
    setSelectedSSMPackage(survey);
    setSelectedAction(action);
  };

  const resetSelectedSSMPackageState = () => {
    setSelectedSSMPackage(null);
    setSelectedAction(null);
  };

  const queryClient = useQueryClient();

  const { isLoading, data: ssmPackagesData } = useQuery(FETCH_QUERY_KEY, () => findSSMPackage({}));

  const { data: functions } = useQuery(FETCH_FUNCTIONS_QUERY_KEY, getAllFunctions);

  const { data: departments } = useQuery(FETCH_DEPARTMENTS_QUERY_KEY, getAllDepartments);

  const { isLoading: isUpdating, mutate: updateSSMPackage } = useMutation(
    createOrUpdateSSMPackage,
    {
      onSuccess: async () => {
        await queryClient.invalidateQueries(FETCH_QUERY_KEY);
        resetSelectedSSMPackageState();
        setMessage('Pachet salvat');
      },
      onError: setError,
    },
  );

  const { isLoading: isDeleting, mutate: deleteSSMPackageById } = useMutation(deleteSSMPackage, {
    onSuccess: async () => {
      await queryClient.invalidateQueries(FETCH_QUERY_KEY);
      resetSelectedSSMPackageState();
      setMessage('Pachet șters');
    },
    onError: setError,
  });

  const onFunctionAllocationChange = (
    ssmPackageInfo: ISSMPackage,
    newFunctionsAllocationValue: string[],
  ) => {
    updateSSMPackage({ ...ssmPackageInfo, functions: newFunctionsAllocationValue });
  };

  return (
    <>
      <Head>
        <title>Documentație SSM</title>
      </Head>

      <AdminPage
        title="Pachete de instruire"
        pages={[{ url: '/ssm-packages', name: 'Documentație SSM' }]}
        actions={
          <Button
            onClick={() => {
              onSSMPackageAction(SELECTED_ACTIONS.CREATE_OR_UPDATE)({
                type: SSM_PACKAGE_TYPE.PERIODIC,
                name: '',
                functions: [],
                departments: [],
                excludedDepartments: [],
              });
            }}
            variant="contained"
            endIcon={<AddIcon />}
          >
            Adaugă Pachet de Instruire
          </Button>
        }
      >
        {ssmPackagesData?.results?.length || isLoading ? (
          <>
            {isLoading && <ListLoadingIndicator />}

            {functions?.results?.length && (
              <Paper sx={{ p: 3 }}>
                <Tabs
                  value={activeView}
                  onChange={(ev, newValue) => onViewChange(newValue)}
                  sx={{ mb: 2 }}
                >
                  {VIEWS.map((view, index) => (
                    <Tab label={VIEW_NAMES[index]} value={view} key={view} />
                  ))}
                </Tabs>
                {activeView === VIEWS[0] && (
                  <SSMPackageList
                    // @ts-ignore
                    ssmPackages={ssmPackagesData?.results}
                    // @ts-ignore
                    functions={functions?.results}
                    onEdit={onSSMPackageAction(SELECTED_ACTIONS.CREATE_OR_UPDATE)}
                    onDelete={onSSMPackageAction(SELECTED_ACTIONS.DELETE)}
                  />
                )}
                {activeView === VIEWS[1] && (
                  <SSMFunctionAllocationMatrix
                    functions={functions?.results || []}
                    columns={ssmPackagesData?.results || []}
                    // @ts-ignore
                    onCheckboxCLick={onFunctionAllocationChange}
                    onColumnClick={(ssmPackage) =>
                      onSSMPackageAction(SELECTED_ACTIONS.CREATE_OR_UPDATE)(ssmPackage)
                    }
                  />
                )}
              </Paper>
            )}
          </>
        ) : (
          <Card sx={{ p: 3 }}>
            <NoDataAvailable content="În momentul de faţă nu există niciun pachet de instruire disponibil pe platformă" />
          </Card>
        )}

        <Dialog
          actions={
            <LoadingButton
              variant="contained"
              onClick={() =>
                selectedSSMPackage && deleteSSMPackageById(selectedSSMPackage._id as string)
              }
              loading={isDeleting}
            >
              Șterge
            </LoadingButton>
          }
          content={
            <>
              <Typography color="textSecondary" textAlign="center">
                Sunteți siguri că doriți să ștergeți pachetul de instruire
                <strong>
                  <i>&quot;{selectedSSMPackage?.name}&quot;</i>
                </strong>
                ?
              </Typography>
            </>
          }
          title="Confirmaţi ștergerea?"
          onClose={resetSelectedSSMPackageState}
          open={!!(selectedSSMPackage && selectedAction === SELECTED_ACTIONS.DELETE)}
        />

        {functions &&
          departments &&
          selectedSSMPackage &&
          selectedAction === SELECTED_ACTIONS.CREATE_OR_UPDATE && (
            <SSMPackagesForm
              initialValues={selectedSSMPackage}
              isLoading={isUpdating}
              // @ts-ignore
              functions={functions?.results}
              // @ts-ignore
              departments={departments}
              open={true}
              onClose={resetSelectedSSMPackageState}
              onDelete={() =>
                selectedSSMPackage?._id &&
                onSSMPackageAction(SELECTED_ACTIONS.DELETE)(selectedSSMPackage)
              }
              onSubmit={updateSSMPackage}
            />
          )}
      </AdminPage>
    </>
  );
};

export const getServerSideProps = withPageAuthRequired();

export default SSMPackages;
