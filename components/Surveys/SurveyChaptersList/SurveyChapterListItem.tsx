import React, { Fragment } from 'react';
import Box from '@mui/material/Box';
import Grid from '@mui/material/Grid';
import Typography from '@mui/material/Typography';
import { SxProps } from '@mui/material';
import { ISurveyChapter } from '../../../models/surveyChapter';
import { ISurvey, SURVEY_FORMULA_TYPE } from '../../../models/survey';

interface ISurveyChapterListItemProps {
  onSelect: (field?: string) => void;
  survey: ISurvey;
  surveyChapter: ISurveyChapter;
  surveyChapterIndex: number;
}

interface ITypographyProps {
  id: string;
  onClick: (field?: string) => void;
  readOnly: boolean;
  sx?: SxProps;
  text: string | number;
}

const RenderTypography = (props: ITypographyProps) => {
  const { id, onClick, readOnly, sx, text } = props;

  return (
    <Typography
      onClick={(ev) => {
        ev.preventDefault();
        ev.stopPropagation();
        !readOnly && onClick(id);
      }}
      sx={{
        py: 1,
        pl: 2,
        border: 1,
        borderRadius: 1,
        borderColor: 'transparent',
        '&:hover': {
          borderColor: readOnly ? 'transparent' : '#3e3e3e',
        },
        ...sx,
      }}
    >
      <strong>{text}</strong>
    </Typography>
  );
};

const SurveyChapterListItem = (props: ISurveyChapterListItemProps) => {
  const { onSelect, survey, surveyChapter, surveyChapterIndex } = props;

  const showWeights = survey?.calculationFormulaType === SURVEY_FORMULA_TYPE['Medie ponderată'];

  const readOnly = !!survey.isClone;

  return (
    <>
      <Box>
        <Box
          sx={{
            border: '0.0625rem solid rgb(222, 226, 230)',
            borderRadius: 2,
          }}
          onClick={() => !readOnly && onSelect()}
        >
          <Box sx={{ p: 3 }}>
            <Grid container spacing={2} alignItems="center">
              <Grid item xs={12}>
                <Box sx={{ display: 'flex' }}>
                  <Box sx={{ flexGrow: 1, mr: 2 }}>
                    <RenderTypography
                      text={surveyChapter.name}
                      id="name"
                      onClick={onSelect}
                      readOnly={readOnly}
                    />
                  </Box>

                  {showWeights && (
                    <Box
                      sx={{
                        bgcolor: 'background.default',
                        display: 'flex',
                        alignItems: 'center',
                        pl: 2,
                      }}
                    >
                      <Typography variant="body2">Pondere:</Typography>
                      <Box sx={{ maxWidth: 64, ml: 1 }}>
                        <RenderTypography
                          text={surveyChapter.weight}
                          id="weight"
                          onClick={onSelect}
                          sx={{ px: '14px', py: '8.5px', minWidth: 64 }}
                          readOnly={readOnly}
                        />
                      </Box>
                    </Box>
                  )}
                </Box>
              </Grid>

              {surveyChapter.questions?.map((question, surveyChapterQuestionIndex) => (
                <Fragment key={question._id}>
                  <Grid item xs={1}>
                    <Typography variant="body2" sx={{ textAlign: { xs: 'left', md: 'right' } }}>
                      {surveyChapterIndex + 1}.{surveyChapterQuestionIndex + 1}
                    </Typography>
                  </Grid>

                  <Grid item xs={11}>
                    <RenderTypography
                      text={question.name}
                      id={`questions.${surveyChapterQuestionIndex}.name`}
                      onClick={onSelect}
                      sx={{ px: '14px', py: '8.5px', minWidth: 64 }}
                      readOnly={readOnly}
                    />
                  </Grid>
                </Fragment>
              ))}
            </Grid>
          </Box>
        </Box>
      </Box>
    </>
  );
};

export default SurveyChapterListItem;
