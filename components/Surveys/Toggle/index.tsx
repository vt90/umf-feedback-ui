import React from 'react';
import { ISelectFieldOption } from '../../Common/FormInputs/SelectField';
import Tabs from '@mui/material/Tabs';
import Tab from '@mui/material/Tab';

interface ISurveyToggleProps {
  value: string | number | undefined;
  onChange: (value: string | number) => void;
  options: ISelectFieldOption[];
  size: 'small' | 'medium' | 'large';
  disabled?: boolean;
}

export const RenderSurveyToggle = (props: ISurveyToggleProps) => {
  const { value, onChange, options, disabled } = props;

  const handleChange = (_: React.MouseEvent<HTMLElement>, newValue: string) => {
    onChange(newValue);
  };

  return (
    <Tabs
      disabled={disabled}
      value={value}
      // @ts-ignore
      onChange={handleChange}
      sx={{
        // border: '0.0625rem solid rgb(222, 226, 230)',
        // borderRadius: 2,
        bgcolor: 'action.hover',
        flexDirection: {
          xs: 'column',
          md: 'row',
        },
      }}
    >
      {options.map((option) => {
        return (
          <Tab
            key={option.value}
            // @ts-ignore
            value={option.value}
            sx={{ px: 1.5, py: 1.5, minHeight: 0, minWidth: 150 }}
            label={option.name}
          />
        );
      })}
    </Tabs>
  );
};

export default RenderSurveyToggle;
