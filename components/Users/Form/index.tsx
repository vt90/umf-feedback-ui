import React, { useState } from 'react';
import Button from '@mui/material/Button';
import Grid from '@mui/material/Grid';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogTitle from '@mui/material/DialogTitle';
import LinearProgress from '@mui/material/LinearProgress';
import Typography from '@mui/material/Typography';
import InfoIcon from '@mui/icons-material/Info';
import { Controller, useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import * as yup from 'yup';
import UserDepartmentAssignmentFieldArray from './userDepartmentAssignmentsFieldArray';
import TextField from '../../Common/FormInputs/TextField';
import YesNoToggle from '../../Common/YesNoToggle';
import { IDepartment } from '../../../models/department';
import { IFunctionModel } from '../../../models/function';
import { IUser } from '../../../models/user';
import { IUserDepartmentAssignment } from '../../../models/userDepartmentAssignment';
import { useMutation } from '@tanstack/react-query';
import { resetPassword } from '../../../services/users';
import { useNotificationContext } from '../../../context/notificationContext';
import ForgotPasswordForm from '../../Auth/ChangePasswordForm';

interface IUserFormProps {
  departments: IDepartment[];
  functions: IFunctionModel[];
  initialValues: Partial<IUser>;
  isLoading: boolean;
  onClose: () => void;
  onSubmit: (data: any) => void;
  onRemoveAssignment: (data: IUserDepartmentAssignment) => void;
  open: boolean;
  changeAssignmentOnly?: boolean;
  isCreatingSSM?: boolean;
  onSsmInit?: () => void;
}

const userDepAssSchema = {
  departmentId: yup.string().required(),
  role: yup.string().required(),
  type: yup.string().required(),
};

const isUMF = process.env.NEXT_PUBLIC_IS_UMF === 'true';
const hasSSM = process.env.NEXT_PUBLIC_HAS_SSM === 'true';

const schema = (props: IUserFormProps) =>
  yup
    .object({
      email: yup.string().required(),
      fullName: yup.string().required(),
      ...(!props?.initialValues?._id && {
        password: yup.string().required(),
      }),
      ...(props.departments?.length && {
        userDepartmentAssignments: yup
          .array()
          // .min(1)
          .of(yup.object().shape(userDepAssSchema)),
      }),
    })
    .required();

const UserForm = (props: IUserFormProps) => {
  const { setError, setMessage } = useNotificationContext();
  const {
    changeAssignmentOnly,
    departments,
    functions,
    initialValues,
    isLoading,
    onClose,
    onSubmit,
    onRemoveAssignment,
    isCreatingSSM,
    onSsmInit,
    open,
  } = props;
  const [changePassword, setChangePassword] = useState<boolean>(false);
  const {
    control,
    formState: { errors },
    handleSubmit,
    register,
    getValues,
    setValue,
  } = useForm({
    defaultValues: initialValues,
    mode: 'onTouched',
    resolver: yupResolver(schema(props)),
    shouldUnregister: true,
  });

  const { isLoading: isPasswordChanging, mutate: changePasswordMutation } = useMutation(
    resetPassword,
    {
      onSuccess: async () => {
        setChangePassword(false);
        setMessage('Parolă modificată');
      },
      onError: setError,
    },
  );

  return (
    <>
      <Dialog
        open={open}
        keepMounted={false}
        onClose={onClose}
        // @ts-ignore
        PaperComponent="form"
        maxWidth="lg"
        fullWidth
        onSubmit={handleSubmit(onSubmit)}
      >
        <DialogTitle sx={{ typography: 'h5', textAlign: 'center' }}>
          {initialValues?._id ? 'Modifică' : 'Creează'} utilizator
        </DialogTitle>
        <DialogContent sx={{ my: 3 }}>
          {/* @ts-ignore */}
          <input type="hidden" {...register(`_id`)} />
          <Grid container spacing={3} alignItems="center">
            <Grid item xs={12} md={3}>
              <Typography variant="body2" sx={{ textAlign: { xs: 'left', md: 'right' } }}>
                Nume Complet*
              </Typography>
            </Grid>
            <Grid item xs={12} md={8}>
              <TextField
                required={true}
                disabled={isLoading || changeAssignmentOnly}
                defaultValue={initialValues.fullName}
                errorMessage={!!errors.fullName && 'Numele este necesar'}
                {...register('fullName')}
              />
            </Grid>
            {initialValues?._id ? null : (
              <>
                <Grid item xs={12} md={3}>
                  <Typography variant="body2" sx={{ textAlign: { xs: 'left', md: 'right' } }}>
                    Parola*
                  </Typography>
                </Grid>
                <Grid item xs={12} md={8}>
                  <TextField
                    required={true}
                    disabled={isLoading || changeAssignmentOnly}
                    defaultValue={initialValues.password}
                    errorMessage={!!errors.password && 'Parola este necesar'}
                    {...register('password')}
                  />
                </Grid>
              </>
            )}
            <Grid item xs={12} md={3}>
              <Typography variant="body2" sx={{ textAlign: { xs: 'left', md: 'right' } }}>
                Marca*
              </Typography>
            </Grid>
            <Grid item xs={12} md={8}>
              <TextField
                required={true}
                disabled={isLoading || changeAssignmentOnly}
                defaultValue={initialValues.email}
                errorMessage={!!errors.email && 'Email necesar'}
                {...register('email')}
              />
            </Grid>

            <Grid item xs={12} md={3}>
              <Typography variant="body2" sx={{ textAlign: { xs: 'left', md: 'right' } }}>
                Permite managementul sesiunilor de evaluare
              </Typography>
            </Grid>
            <Grid item xs={12} md={3}>
              <Controller
                name="canManageSessions"
                control={control}
                defaultValue={initialValues.canManageSessions}
                render={({ field }) => {
                  const { value, onChange } = field;

                  return (
                    <>
                      <YesNoToggle
                        value={!!value}
                        onValueChange={onChange}
                        disabled={changeAssignmentOnly}
                      />
                    </>
                  );
                }}
              />
            </Grid>

            <Grid item xs={12} md={3}>
              <Typography variant="body2" sx={{ textAlign: { xs: 'left', md: 'right' } }}>
                Access full
              </Typography>
              <Typography
                color="textSecondary"
                variant="caption"
                sx={{ display: 'block', textAlign: { xs: 'left', md: 'right' } }}
              >
                <InfoIcon fontSize="small" /> Permite modificarea tuturor evaluărilor
              </Typography>
            </Grid>
            <Grid item xs={12} md={3}>
              <Controller
                name="canEditAllReviews"
                control={control}
                defaultValue={initialValues.canEditAllReviews}
                render={({ field }) => {
                  const { value, onChange } = field;

                  return (
                    <>
                      <YesNoToggle
                        value={!!value}
                        onValueChange={onChange}
                        disabled={changeAssignmentOnly}
                      />
                    </>
                  );
                }}
              />
            </Grid>

            <Grid item xs={12} md={3}>
              <Typography variant="body2" sx={{ textAlign: { xs: 'left', md: 'right' } }}>
                Permite managementul utilizatorilor
              </Typography>
            </Grid>
            <Grid item xs={12} md={3}>
              <Controller
                name="canManageUsers"
                control={control}
                defaultValue={initialValues.canManageUsers}
                render={({ field }) => {
                  const { value, onChange } = field;

                  return (
                    <>
                      <YesNoToggle
                        value={!!value}
                        onValueChange={onChange}
                        disabled={changeAssignmentOnly}
                      />
                    </>
                  );
                }}
              />
            </Grid>

            <Grid item xs={12} md={3}>
              <Typography variant="body2" sx={{ textAlign: { xs: 'left', md: 'right' } }}>
                Access full &quot;read-only&ldquo;
              </Typography>
              <Typography
                color="textSecondary"
                variant="caption"
                sx={{ display: 'block', textAlign: { xs: 'left', md: 'right' } }}
              >
                <InfoIcon fontSize="small" /> HR Manager
              </Typography>
            </Grid>
            <Grid item xs={12} md={3}>
              <Controller
                name="canViewAllReviews"
                control={control}
                defaultValue={initialValues.canViewAllReviews}
                render={({ field }) => {
                  const { value, onChange } = field;

                  return (
                    <>
                      <YesNoToggle
                        value={!!value}
                        onValueChange={onChange}
                        disabled={changeAssignmentOnly}
                      />
                    </>
                  );
                }}
              />
            </Grid>

            <Grid item xs={12} md={3}>
              <Typography variant="body2" sx={{ textAlign: { xs: 'left', md: 'right' } }}>
                Permite managementul departamentelor
              </Typography>
            </Grid>
            <Grid item xs={12} md={3}>
              <Controller
                name="canManageDepartments"
                control={control}
                defaultValue={initialValues.canManageDepartments}
                render={({ field }) => {
                  const { value, onChange } = field;

                  return (
                    <>
                      <YesNoToggle
                        value={!!value}
                        onValueChange={onChange}
                        disabled={changeAssignmentOnly}
                      />
                    </>
                  );
                }}
              />
            </Grid>
            {/*<Grid item xs={12} md={3}>*/}
            {/*  <Typography variant="body2" sx={{ textAlign: { xs: 'left', md: 'right' } }}>*/}
            {/*    Include in sesiunea de modificari salariale*/}
            {/*  </Typography>*/}
            {/*</Grid>*/}
            {/*<Grid item xs={12} md={3}>*/}
            {/*  <Controller*/}
            {/*    name="allowSalaryChange"*/}
            {/*    control={control}*/}
            {/*    defaultValue={initialValues.allowSalaryChange}*/}
            {/*    render={({ field }) => {*/}
            {/*      const { value, onChange } = field;*/}

            {/*      return (*/}
            {/*        <>*/}
            {/*          <YesNoToggle*/}
            {/*            value={!!value}*/}
            {/*            onValueChange={onChange}*/}
            {/*            disabled={changeAssignmentOnly}*/}
            {/*          />*/}
            {/*        </>*/}
            {/*      );*/}
            {/*    }}*/}
            {/*  />*/}
            {/*</Grid>*/}

            {isUMF ? (
              <>
                <Grid item xs={12} md={3}>
                  <Typography variant="body2" sx={{ textAlign: { xs: 'left', md: 'right' } }}>
                    Permite managementul SSM
                  </Typography>
                </Grid>
                <Grid item xs={12} md={3}>
                  <Controller
                    name="isSSM"
                    control={control}
                    defaultValue={initialValues.isSSM}
                    render={({ field }) => {
                      const { value, onChange } = field;

                      return (
                        <>
                          <YesNoToggle
                            value={!!value}
                            onValueChange={onChange}
                            disabled={changeAssignmentOnly}
                          />
                        </>
                      );
                    }}
                  />
                </Grid>
                <Grid item xs={12} md={3}>
                  <Typography variant="body2" sx={{ textAlign: { xs: 'left', md: 'right' } }}>
                    Rol extern
                  </Typography>
                </Grid>
                <Grid item xs={12} md={3}>
                  <Controller
                    name="isExternal"
                    control={control}
                    defaultValue={initialValues.isExternal}
                    render={({ field }) => {
                      const { value, onChange } = field;

                      return (
                        <>
                          <YesNoToggle
                            value={!!value}
                            onValueChange={onChange}
                            disabled={changeAssignmentOnly}
                          />
                        </>
                      );
                    }}
                  />
                </Grid>
              </>
            ) : null}

            {initialValues.deactivated && [
              <Grid key="label-deactivated" item xs={12} md={3}>
                <Typography variant="body2" sx={{ textAlign: { xs: 'left', md: 'right' } }}>
                  Utilizator deactivat
                </Typography>
              </Grid>,
              <Grid key="toggle-deactivatd" item xs={12} md={3}>
                <Controller
                  name="deactivated"
                  control={control}
                  defaultValue={initialValues.deactivated}
                  render={({ field }) => {
                    const { value, onChange } = field;

                    return (
                      <>
                        <YesNoToggle
                          value={!!value}
                          onValueChange={onChange}
                          disabled={changeAssignmentOnly}
                        />
                      </>
                    );
                  }}
                />
              </Grid>,
            ]}

            <Grid item xs={12}>
              <Typography variant="h6" sx={{ my: 2, textAlign: 'center' }}>
                Alocarea în departamente
              </Typography>
            </Grid>

            <UserDepartmentAssignmentFieldArray
              changeAssignmentOnly={changeAssignmentOnly}
              departments={departments}
              functions={functions}
              control={control}
              errors={errors}
              getValues={getValues}
              register={register}
              setValue={setValue}
              onRemoveAssignment={onRemoveAssignment}
            />
          </Grid>
        </DialogContent>

        {isLoading && <LinearProgress />}
        <DialogActions sx={{ py: 2, justifyContent: 'center', bgcolor: 'background.default' }}>
          <Button color="inherit" onClick={onClose} disabled={isLoading}>
            Anulare
          </Button>
          {initialValues?._id ? (
            <Button
              onClick={() => setChangePassword(true)}
              variant="contained"
              color="secondary"
              disabled={isLoading}
            >
              Modifică parola
            </Button>
          ) : null}
          {initialValues?._id && !initialValues.deactivated && hasSSM && onSsmInit ? (
            <Button
              onClick={() => onSsmInit()}
              variant="contained"
              color="warning"
              disabled={isCreatingSSM}
            >
              Initiaza instruire la angajare
            </Button>
          ) : null}
          {changeAssignmentOnly ? null : (
            <Button
              onClick={() => {
                // @ts-ignore
                document.getElementById('userDepartmentAssignmentAddButton')?.click();
              }}
              variant="contained"
              color="inherit"
              disabled={isLoading}
            >
              Adaugă alocare
            </Button>
          )}
          <Button variant="contained" type="submit" disabled={isLoading}>
            {initialValues?._id ? 'Modifică' : 'Creează'}
          </Button>
        </DialogActions>
      </Dialog>

      {changePassword && (
        <ForgotPasswordForm
          isLoading={isPasswordChanging}
          skipPasswordCheck
          onClose={() => setChangePassword(false)}
          // @ts-ignore
          initialValues={{ userId: initialValues?._id }}
          // @ts-ignore
          onSubmit={changePasswordMutation}
        />
      )}
    </>
  );
};

export default UserForm;
