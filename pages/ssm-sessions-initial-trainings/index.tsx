import React, { useState } from 'react';
import Head from 'next/head';
import { withPageAuthRequired } from '../../guards/authRequired';
import {
  contraSignInitial,
  ssmSignInitial,
  acceptAtWork,
  findSSMInitialTrainingSessions,
} from '../../services/ssmInitialTrainingSessions';
import { useMutation, useQuery, useQueryClient } from '@tanstack/react-query';
import AdminPage from '../../components/AdminPage';
import NoDataAvailable from '../../components/Common/NoDataAvailable';
import { getAllFunctions } from '../../services/functions';
import ListLoadingIndicator from '../../components/Common/ListLoadingIndcator';
import Box from '@mui/material/Box';
import Card from '@mui/material/Card';
import Grid from '@mui/material/Grid';
import Typography from '@mui/material/Typography';
import moment from 'moment';
import Pagination from '../../components/Common/Pagination';
import { getUserSubUsers } from '../../services/userDepartmentAssignment';
import { useRouter } from 'next/router';
import { ISSMInitialTrainingSession } from '../../models/ssmInitialTrainingSession';
import { useAuthContext } from '../../context/authContext';
import SSMTrainingSessionDetails from '../../components/SSMPeriodicTrainingSession/Details';
import Button from '@mui/lab/LoadingButton';
import Dialog from '../../components/Common/Dialog';
import { findSSMPackage } from '../../services/ssmPackages';
import { useNotificationContext } from '../../context/notificationContext';
import useDebounce from '../../hooks/useDebounce';
import HREvaluationsListHeader from '../../components/Evaluation/HRUserEvaluationsList/Header';
import { SSM_PACKAGE_TYPE } from '../../models/ssmPackage';
import SSMInitialTrainingSessionList from '../../components/SSMInitialTrainingSession/List';
import AddIcon from '@mui/icons-material/Add';
import SSMInitialTrainingForm from '../../components/SSMInitialTrainingSession/Form';
const FETCH_QUERY_KEY = ['ssm-initial-sessions-trainings'];
const FETCH_PACKAGES_QUERY_KEY = ['training-ssm-packages'];
const FETCH_FUNCTIONS_QUERY_KEY = ['depFunctions'];

const SSMInitialTrainingSessions = () => {
  const [pageNumber, setPageNumber] = useState(1);
  const [showAddForm, setShowAddForm] = useState(false);
  const [order, setOrder] = useState(-1);
  const [orderBy, setOrderBy] = useState('userAcknowledgedGeneral');
  const [email, setEmail] = useState<string | null>('');
  const [sessionDate, setSessionDate] = useState<Date>(moment().startOf('month').toDate());
  const [userAcknowledged, setUserAcknowledged] = useState<string | null>('');
  const [contraEvaluated, setContraEvaluated] = useState<string | null>('');
  const [selectedSession, setSelectedSession] = useState<ISSMInitialTrainingSession | null>(null);
  const [pageSize] = useState(50);
  const { setError, setMessage } = useNotificationContext();
  const router = useRouter();
  const { user } = useAuthContext();
  const queryClient = useQueryClient();

  const userSearchTerm = useDebounce(`${email}`, 600);

  const { data: subUsers } = useQuery(['sub-users'], getUserSubUsers, {
    onError: () => router.push('/404'),
  });

  const fetchParams = {
    limit: pageSize,
    offset: (pageNumber - 1) * pageSize,
    order,
    orderBy,
    sessionDate,
    userAcknowledged,
    contraEvaluated,
    userSearchTerm,
    // @ts-ignore
    ...(user?.isSSM
      ? {}
      : {
          // @ts-ignore
          userDepartmentAssignmentIds: subUsers?.results?.map((assingment) => assingment._id),
        }),
  };

  const { isLoading, data: ssmInitialTrainingsSessions } = useQuery(
    [FETCH_QUERY_KEY, fetchParams],
    // @ts-ignore
    () => findSSMInitialTrainingSessions(fetchParams),
    {
      // @ts-ignore
      enabled: user?.isSSM || !!subUsers?.count,
    },
  );

  const { data: functions } = useQuery(FETCH_FUNCTIONS_QUERY_KEY, getAllFunctions);

  const { data: ssmPackages } = useQuery(
    [FETCH_PACKAGES_QUERY_KEY, selectedSession?.userDepartmentAssignmentId?.jobFunction],
    () =>
      findSSMPackage({
        // @ts-ignore
        functions: [selectedSession?.userDepartmentAssignmentId?.jobFunction],
        // @ts-ignore
        userDepartments: [selectedSession?.userDepartmentAssignmentId?.departmentId?._id],
        type: SSM_PACKAGE_TYPE.AT_HIRE,
      }),
    {
      enabled: !!selectedSession,
    },
  );

  const { isLoading: isContraSigning, mutate: onContraSign } = useMutation(
    // @ts-ignore
    (type) => contraSignInitial(selectedSession._id, type),
    {
      onSuccess: async () => {
        await queryClient.invalidateQueries([FETCH_QUERY_KEY, fetchParams]);
        setSelectedSession(null);
        setMessage('Instruire semnată');
      },
      onError: setError,
    },
  );

  const { isLoading: isSSMSigning, mutate: onSSMSign } = useMutation(
    // @ts-ignore
    (type) => ssmSignInitial(selectedSession._id, type),
    {
      onSuccess: async () => {
        await queryClient.invalidateQueries([FETCH_QUERY_KEY, fetchParams]);
        setSelectedSession(null);
        setMessage('Instruire verificată');
      },
      onError: setError,
    },
  );

  const { isLoading: isAccepting, mutate: onAcceptAtWork } = useMutation(
    // @ts-ignore
    () => acceptAtWork(selectedSession._id),
    {
      onSuccess: async () => {
        await queryClient.invalidateQueries([FETCH_QUERY_KEY, fetchParams]);
        setSelectedSession(null);
        setMessage('Angajat admis la lucru');
      },
      onError: setError,
    },
  );

  const onCloseForm = async () => {
    await queryClient.invalidateQueries([FETCH_QUERY_KEY, fetchParams]);
    setShowAddForm(false);
  };

  return (
    <>
      <Head>
        <title>Instruiri la angajare</title>
      </Head>

      <AdminPage
        title="Instruiri la angajare"
        pages={[{ url: '/ssm-sessions-initial-trainings', name: 'Instruiri la angajare' }]}
        actions={
          <>
            <Button onClick={() => setShowAddForm(true)} variant="contained" endIcon={<AddIcon />}>
              Adaugă instruit
            </Button>
          </>
        }
      >
        <Card sx={{ p: 3 }}>
          <Grid container spacing={2} columns={13}>
            <Grid item xs={12} md={3} lg={2}>
              {/*<SSMTrainingSessionFilter*/}
              {/*  order={order}*/}
              {/*  setOrder={setOrder}*/}
              {/*  orderBy={orderBy}*/}
              {/*  setOrderBy={setOrderBy}*/}
              {/*  contraEvaluated={contraEvaluated}*/}
              {/*  setContraEvaluated={setContraEvaluated}*/}
              {/*  userAcknowledged={userAcknowledged}*/}
              {/*  setUserAcknowledged={setUserAcknowledged}*/}
              {/*  sessionData={sessionDate}*/}
              {/*  setSessionData={setSessionDate}*/}
              {/*/>*/}
            </Grid>

            {/*<Grid item xs={12} md={10} lg={11}>*/}
            <Grid item xs={13}>
              <Box sx={{ mb: 3 }}>
                <HREvaluationsListHeader
                  // @ts-ignore
                  activeView={'s'}
                  order={order}
                  setOrder={setOrder}
                  orderBy={orderBy}
                  setOrderBy={setOrderBy}
                  searchTerm={email}
                  setSearchTerm={setEmail}
                />
              </Box>

              {isLoading || !functions?.results?.length ? (
                <ListLoadingIndicator />
              ) : ssmInitialTrainingsSessions?.results?.length ? (
                <>
                  <SSMInitialTrainingSessionList
                    ssmInitialTrainingsSessions={
                      ssmInitialTrainingsSessions?.results?.reduce((acc, cur) => {
                        // @ts-ignore
                        acc.push({
                          ...cur,
                          ssmPeriodicTrainingId: {
                            shortDescription: (
                              <Typography variant="h5" color="textPrimary">
                                Instruire introductiv generală
                              </Typography>
                            ),
                          },
                          // @ts-ignore
                          userAcknowledged: cur.userAcknowledgedGeneral,
                          // @ts-ignore
                          contraEvaluated: cur.contraEvaluatedGeneral,
                          // @ts-ignore
                          ssmEvaluated: cur.ssmEvaluatedGeneral,
                          action_type_flag: 'General',
                        });
                        // @ts-ignore
                        acc.push({
                          ...cur,
                          ssmPeriodicTrainingId: {
                            shortDescription: (
                              <Typography variant="h5" color="textPrimary">
                                Instruire la locul de muncă
                              </Typography>
                            ),
                          },
                          // @ts-ignore
                          userAcknowledged: cur.userAcknowledgedWork,
                          // @ts-ignore
                          contraEvaluated: cur.contraEvaluatedWork,
                          // @ts-ignore
                          ssmEvaluated: cur.ssmEvaluatedWork,
                          action_type_flag: 'Work',
                          _id: `${cur._id}**`,
                        });
                        // @ts-ignore
                        acc.push({
                          ...cur,
                          ssmPeriodicTrainingId: {
                            shortDescription: (
                              <Typography variant="h5" color="textPrimary">
                                Admitere la lucru
                              </Typography>
                            ),
                          },
                          // @ts-ignore
                          action_type_flag: 'Accept_At_Work',
                          _id: `${cur._id}##`,
                        });
                        return acc;
                      }, []) || []
                    }
                    onSelectSession={(session) => {
                      setSelectedSession({
                        ...session,
                        _id: session._id.replace('**', '').replace('##', ''),
                      });
                    }}
                  />

                  <Pagination
                    sx={{ mt: 2 }}
                    pageNumber={pageNumber}
                    pageSize={pageSize}
                    totalResults={ssmInitialTrainingsSessions?.count || 0}
                    setPageNumber={setPageNumber}
                  />
                </>
              ) : (
                <NoDataAvailable content="În momentul de faţă nu există nicio instruire la angajare disponibilă pe platformă" />
              )}
            </Grid>
          </Grid>
        </Card>
      </AdminPage>

      <Dialog
        content={
          <>
            <SSMTrainingSessionDetails
              isInitial={true}
              // @ts-ignore
              functions={functions?.results}
              // @ts-ignore
              session={selectedSession}
              // @ts-ignore
              ssmPackages={
                selectedSession?.ssmPackages?.length
                  ? selectedSession?.ssmPackages
                  : ssmPackages?.results
              }
            />
          </>
        }
        actions={
          <>
            {
              // @ts-ignore
              selectedSession?.action_type_flag === 'Accept_At_Work' ? (
                <>
                  <Button
                    variant="contained"
                    color="primary"
                    disabled={
                      !(
                        (
                          selectedSession?.contraEvaluatedWork &&
                          selectedSession?.contraEvaluatedGeneral
                        ) // &&
                        // selectedSession?.ssmEvaluatedGeneral &&
                        // selectedSession?.ssmEvaluatedWork
                      )
                    }
                    onClick={() => onAcceptAtWork()}
                    loading={isSSMSigning}
                  >
                    Admis la lucru
                  </Button>
                </>
              ) : null
            }
            {
              // @ts-ignore
              selectedSession?.action_type_flag === 'General' ? (
                <>
                  {
                    // @ts-ignore
                    !selectedSession?.contraEvaluatedGeneral && (
                      // @ts-ignore
                      <Button
                        variant="contained"
                        color="success"
                        // @ts-ignore
                        onClick={() => onContraSign('General')}
                        loading={isContraSigning}
                      >
                        Semnează
                      </Button>
                    )
                  }
                  {
                    // @ts-ignore
                    user?.isSSM && !selectedSession?.ssmEvaluatedGeneral && (
                      // @ts-ignore
                      <Button
                        variant="contained"
                        color="primary"
                        disabled={!selectedSession?.contraEvaluatedGeneral}
                        // @ts-ignore
                        onClick={() => onSSMSign('General')}
                        loading={isSSMSigning}
                      >
                        Validează
                      </Button>
                    )
                  }
                </>
              ) : null
            }
            {
              // @ts-ignore
              selectedSession?.action_type_flag === 'Work' ? (
                <>
                  {
                    // @ts-ignore
                    !selectedSession?.contraEvaluatedWork && (
                      // @ts-ignore
                      <Button
                        variant="contained"
                        color="success"
                        // @ts-ignore
                        onClick={() => onContraSign('Work')}
                        loading={isContraSigning}
                      >
                        Semnează
                      </Button>
                    )
                  }
                  {
                    // @ts-ignore
                    user?.isSSM && !selectedSession?.ssmEvaluatedWork && (
                      // @ts-ignore
                      <Button
                        variant="contained"
                        color="primary"
                        disabled={!selectedSession?.contraEvaluatedWork}
                        // @ts-ignore
                        onClick={() => onSSMSign('Work')}
                        loading={isSSMSigning}
                      >
                        Validează
                      </Button>
                    )
                  }
                </>
              ) : null
            }
          </>
        }
        // @ts-ignore
        title={selectedSession?.ssmInitialTrainingId?.name}
        open={!!selectedSession}
        maxWidth="lg"
        onClose={() => setSelectedSession(null)}
      />

      {showAddForm ? <SSMInitialTrainingForm onClose={() => onCloseForm()} /> : null}
    </>
  );
};

export const getServerSideProps = withPageAuthRequired();

export default SSMInitialTrainingSessions;
