import React, { useEffect } from 'react';
import { useForm } from 'react-hook-form';
import * as yup from 'yup';
import { yupResolver } from '@hookform/resolvers/yup';
import Button from '@mui/material/Button';
import Card from '@mui/material/Card';
import Box from '@mui/material/Box';
import Grid from '@mui/material/Grid';
import Typography from '@mui/material/Typography';
import Collapse from '@mui/material/Collapse';
import AddIcon from '@mui/icons-material/Add';
import CloseIcon from '@mui/icons-material/Close';
import DeleteIcon from '@mui/icons-material/Delete';
import SaveIcon from '@mui/icons-material/CheckCircle';
import SurveyChapterQuestionsField from './SurveyChapterQuestions';
import TextField, { ITextFieldProps } from '../../Common/FormInputs/TextField';
import { ISurvey, SURVEY_FORMULA_TYPE } from '../../../models/survey';
import { ISurveyChapter } from '../../../models/surveyChapter';

interface ISurveyChapterFormProps {
  initialFocus?: string;
  initialValues: Partial<ISurveyChapter>;
  isLoading: boolean;
  onCancel?: () => void;
  onRemoveSurveyChapter?: (surveyChapter: ISurveyChapter) => void;
  onSubmit: (data: any) => void;
  survey: ISurvey;
  surveyChapterIndex: number;
}

const questionSchema = {
  name: yup.string().required(),
};

const schema = (props: ISurveyChapterFormProps) => {
  const showWeights =
    props.survey?.calculationFormulaType === SURVEY_FORMULA_TYPE['Medie ponderată'];

  return yup
    .object({
      name: yup.string().required(),
      questions: yup.array().of(yup.object().shape(questionSchema)),
      ...(showWeights && {
        weight: yup.string().required(),
      }),
    })
    .required();
};

const SurveyChapterForm = (props: ISurveyChapterFormProps) => {
  const {
    initialFocus,
    initialValues,
    isLoading,
    onSubmit,
    onCancel,
    onRemoveSurveyChapter,
    survey,
    surveyChapterIndex,
  } = props;

  const isExistingSurveyChapter = initialValues._id;
  const showWeights = survey?.calculationFormulaType === SURVEY_FORMULA_TYPE['Medie ponderată'];

  const {
    register,
    handleSubmit,
    formState: { errors },
    control,
    setFocus,
  } = useForm({
    defaultValues: initialValues,
    mode: 'onTouched',
    resolver: yupResolver(schema(props)),
    shouldUnregister: true,
  });

  useEffect(() => {
    // @ts-ignore
    initialFocus && setFocus(initialFocus);
  }, [setFocus, initialFocus]);

  const textFieldProps: Partial<ITextFieldProps> = {
    required: true,
    disabled: isLoading,
    size: 'small',
    sx: {
      '& legend': { display: 'none' },
      '& fieldset': {
        top: 0,
        borderColor: 'transparent',
      },
    },
    inputProps: {
      sx: {
        typography: 'body1',
        fontWeight: 'bold',
      },
    },
  };

  return (
    <>
      <form onSubmit={handleSubmit(onSubmit)}>
        <Card elevation={8}>
          <input type="hidden" {...register(`_id`)} />
          <input type="hidden" {...register(`surveyId`)} />
          <Box sx={{ p: 3 }}>
            <Grid container spacing={2} alignItems="center">
              <Grid item xs={12}>
                <Box sx={{ display: 'flex' }}>
                  <Box sx={{ flexGrow: 1, mr: 2 }}>
                    <TextField
                      multiline
                      errorMessage={!!errors?.name && 'Criteriu necesar'}
                      defaultValue={initialValues.name}
                      {...register(`name`)}
                      {...textFieldProps}
                    />
                  </Box>

                  {showWeights && (
                    <Box
                      sx={{
                        bgcolor: 'background.default',
                        display: 'flex',
                        alignItems: 'center',
                        pl: 2,
                      }}
                    >
                      <Typography variant="body2">Pondere:</Typography>
                      <Box sx={{ maxWidth: 64, ml: 1 }}>
                        <TextField
                          errorMessage={!!errors?.weight && 'Ponderea necesară'}
                          defaultValue={initialValues.weight}
                          {...register(`weight`)}
                          {...textFieldProps}
                        />
                      </Box>
                    </Box>
                  )}
                </Box>
              </Grid>

              <SurveyChapterQuestionsField
                control={control}
                errors={errors}
                isLoading={isLoading}
                register={register}
                surveyChapterIndex={surveyChapterIndex}
              />

              <Grid item xs={12}>
                <Collapse in={true}>
                  <Grid container spacing={2} width="100%">
                    <Grid item xs={1}></Grid>
                    <Grid item xs={11}>
                      <Button
                        onClick={() => document.getElementById(`addQuestionButton`)?.click()}
                        variant="contained"
                        endIcon={<AddIcon />}
                        sx={{ ml: 1.5 }}
                        size="small"
                      >
                        Adaugă competență
                      </Button>
                    </Grid>
                  </Grid>
                </Collapse>
              </Grid>
            </Grid>
          </Box>
        </Card>

        <Box sx={{ py: 1, display: 'flex', justifyContent: 'flex-end' }}>
          <Box>
            <Button
              // @ts-ignore
              onClick={onCancel}
              color="inherit"
              endIcon={<CloseIcon fontSize="small" />}
              size="small"
            >
              Anulează
            </Button>
          </Box>

          {isExistingSurveyChapter && onRemoveSurveyChapter && (
            <Box sx={{ ml: 1 }}>
              <Button
                // @ts-ignore
                onClick={() => onRemoveSurveyChapter(initialValues)}
                variant="contained"
                color="error"
                endIcon={<DeleteIcon fontSize="small" />}
                size="small"
              >
                Șterge criteriu
              </Button>
            </Box>
          )}

          <Box sx={{ ml: 1 }}>
            <Button
              type="submit"
              variant="contained"
              endIcon={<SaveIcon fontSize="small" />}
              size="small"
            >
              Salvează
            </Button>
          </Box>
        </Box>
        <Button id="surveyChapterFormSubmit" type="submit" sx={{ display: 'none' }} />
      </form>
    </>
  );
};

export default SurveyChapterForm;
