import React, { useCallback, useMemo } from 'react';
import Card from '@mui/material/Card';
import Typography from '@mui/material/Typography';
import Tooltip from '@mui/material/Tooltip';
import EnhancedTable from '../../Common/Table';
import moment from 'moment';
import NoDataAvailable from '../../Common/NoDataAvailable';

import { IExtendedUserDepartmentAssignment } from '../../../models/userDepartmentAssignment';

interface IUserUserListProps {
  unEvaluatedList: IExtendedUserDepartmentAssignment[];
}

const HRUnevaluatedList = (props: IUserUserListProps) => {
  const { unEvaluatedList } = props;

  const headerRows = useMemo(() => {
    return [
      { id: 'name', label: 'Membru', align: 'left', colSpan: 2 },
      { id: 'email', label: 'Marca', align: 'left', colSpan: 1 },
      { id: 'department', label: 'Departament', align: 'left', colSpan: 3 },
    ];
  }, []);

  const getHeaderRows = useCallback(() => {
    return unEvaluatedList.map((userDepAssignment) => {
      const { _id, createdAt, departmentId, userId, role } = userDepAssignment;

      // @ts-ignore
      const renderName = (
        <Typography>
          {/* @ts-ignore */}
          {userId.fullName}
        </Typography>
      );

      return {
        _id,
        smallDeviceHeader: renderName,
        name: renderName,
        // @ts-ignore
        email: userId?.email,
        department: (
          // @ts-ignore
          <Tooltip title={role}>
            <Typography variant="body2" color="textSecondary">
              {/* @ts-ignore */}
              {departmentId?.name}
            </Typography>
          </Tooltip>
        ),
        createdAt: moment(createdAt).format('YYYY.MM.DD'),
        data: userDepAssignment,
      };
    });
  }, [unEvaluatedList]);

  if (!unEvaluatedList?.length) {
    return (
      <NoDataAvailable content="În momentul de faţă nu există date disponibile pe platformă" />
    );
  }

  return (
    <>
      <EnhancedTable
        headerRows={headerRows}
        initialOrderByField="department"
        initialOrder="asc"
        orderByFieldCriteriaFunctions={{
          createdAt: (row: any) => row?.data?.createdAt,
          department: (row: any) => {
            return row?.data?.departmentId?.name;
          },
        }}
        PaperComponent={Card}
        paperComponentStyles={{
          px: 3,
          py: 2,
          '& h6': {
            mt: 1,
          },
        }}
        rows={getHeaderRows()}
        smallUntilBreakpoint="md"
      />
    </>
  );
};

export default HRUnevaluatedList;
