import * as React from 'react';
import Typography from '@mui/material/Typography';
import Breadcrumbs from '@mui/material/Breadcrumbs';
import MuiLink from '@mui/material/Link';
import HomeIcon from '@mui/icons-material/Home';
import Link from 'next/link';

export interface BreadcrumbInfo {
  name: string;
  url: string;
}

interface INavBreadcrumbs {
  pages: BreadcrumbInfo[];
}

const COLOR = 'grey.200';

export default function NavBreadcrumbs(props: INavBreadcrumbs) {
  const { pages } = props;

  return (
    <Breadcrumbs
      sx={{
        '*': {
          color: COLOR,
        },
      }}
    >
      <MuiLink
        underline="hover"
        sx={{ display: 'flex', alignItems: 'center' }}
        color="inherit"
        href="/"
        component={Link}
      >
        <a>
          <HomeIcon sx={{ color: COLOR, mr: 0.5 }} fontSize="inherit" />
        </a>
      </MuiLink>
      {pages.map((page) => {
        const { name, url } = page;

        return (
          <MuiLink
            underline="hover"
            sx={{ display: 'flex', alignItems: 'center' }}
            component={Link}
            href={url}
            key={name}
          >
            <a>
              <Typography sx={{ color: COLOR, fontWeight: '300' }}>{name}</Typography>
            </a>
          </MuiLink>
        );
      })}
    </Breadcrumbs>
  );
}
