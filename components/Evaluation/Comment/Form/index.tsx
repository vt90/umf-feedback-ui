import React from 'react';
import * as yup from 'yup';
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import TextField from '../../../Common/FormInputs/TextField';
import Typography from '@mui/material/Typography';
import Button from '@mui/material/Button';

interface ICommentFormProps {
  isLoading: boolean;
  shouldValidate: boolean;
  onSubmit: (data: any) => void;
}
const schema = (props: ICommentFormProps) =>
  yup
    .object({
      ...(props.shouldValidate && { comment: yup.string().required() }),
    })
    .required();

const CommentForm = (props: ICommentFormProps) => {
  const { isLoading, onSubmit } = props;
  const {
    formState: { errors },
    handleSubmit,
    register,
  } = useForm({
    defaultValues: { comment: '' },
    mode: 'onTouched',
    resolver: yupResolver(schema(props)),
    shouldUnregister: true,
  });

  return (
    <form id="commentForm" onSubmit={handleSubmit(onSubmit)}>
      <Typography gutterBottom>
        <strong>Adaugă observație</strong>
      </Typography>

      <TextField
        multiline
        minRows={5}
        disabled={isLoading}
        errorMessage={!!errors.comment && 'Câmp necesar'}
        defaultValue=""
        {...register('comment')}
      />
      <Button id="commentFormSubmit" sx={{ display: 'none' }} type="submit"></Button>
    </form>
  );
};

export default CommentForm;
