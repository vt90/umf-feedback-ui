import { IBaseModel } from './base';
import { IUser } from './user';
import { IDepartment } from './department';
import { IFunctionModel } from './function';

const isUMF = process.env.NEXT_PUBLIC_IS_UMF === 'true';

export const USER_DEP_ASSIGNMENT_ROLES = {
  EVALUATOR: 'Director departament (evaluator)',
  EVALUATEE: 'Membru departament (persoană evaluată)',
};

export const USER_DEP_ASSIGNMENT_TYPE = {
  SUPERIOR: isUMF ? 'Rol de conducere' : 'Achizitii',
  PROF: isUMF ? 'Profesor/Conferențiar' : 'Expert',
  ASSIST: isUMF ? 'Șef lucrări/Asistent' : 'Financiar',
  ADMINISTRATIV: isUMF ? 'Administrativ' : 'HR',
  ...(isUMF && {
    ADMIN: 'Auxiliar',
    RESEARCH: 'Cercetător',
    SUPPORT: 'Personal Suport Cercetare - Administrativ',
  }),
};

export const USER_DEP_ASSIGNMENT_STUDY_TYPE = {
  SUPERIOR: 'Studii Superioare',
  SUP_SHORT: 'Studii Superioare de Scurtă Durată',
  MEDIUM: 'Studii Medii',
  GENERAL: 'Studii Medii sau Generale',
};

export const USER_DEP_ASSIGNMENT_STUDY_LEVELS = {
  IA: 'IA',
  I: 'I',
  II: 'II',
  III: 'III',
};

export interface IUserDepartmentAssignment extends IBaseModel {
  userId: string;
  departmentId: string;
  role: string;
  type: string;
  jobFunction: string;
  studyType: string;
  studyLevel: string;
}

export interface IExtendedUserDepartmentAssignment extends IBaseModel {
  userId: IUser;
  departmentId: IDepartment;
  role: string;
  type: string;
  jobFunction?: string;
  jobFunctionInfo?: IFunctionModel;
  studyType?: string;
  studyLevel?: string;
  isContraEvaluation?: boolean;
  jobPlace?: string;
  personalCategory?: string;
  excludeFromEvaluations?: boolean;
}
