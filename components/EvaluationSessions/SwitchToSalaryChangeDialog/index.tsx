import Dialog from '../../Common/Dialog';
import LoadingButton from '@mui/lab/LoadingButton';
import React from 'react';
import { IEvaluation } from '../../../models/evaluation';
import Typography from '@mui/material/Typography';
import { useRouter } from 'next/router';
import { refuseSalaryChange } from '../../../services/evaluations';

const SwitchToSalaryChangeDialog = ({
  open,
  evalutation,
  onClose,
}: {
  evalutation?: IEvaluation | null;
  onClose: () => void;
  open: boolean;
}) => {
  const router = useRouter();

  const handleClose = async () => {
    await refuseSalaryChange(`${evalutation?._id}`);
    onClose();
  };

  const onAccept = async () => {
    await router.push({
      pathname: '/evaluation-sessions/[id]/salary-change/[userDepartmentAssignmentId]',
      query: {
        id: `${evalutation?.evaluationSessionId}`,
        userDepartmentAssignmentId: `${evalutation?.userDepartmentAssignmentId}`,
      },
    });
  };

  return (
    <Dialog
      actions={
        <>
          <LoadingButton onClick={handleClose} loading={false}>
            Nu
          </LoadingButton>
          <LoadingButton variant="contained" onClick={onAccept} loading={false}>
            Da
          </LoadingButton>
        </>
      }
      content={
        <>
          <Typography color="textSecondary" textAlign="center" gutterBottom>
            Rezultatul evaluării îndeplinește condiția pentru a beneficia de salar suplimentar.
          </Typography>

          <Typography color="textSecondary" textAlign="center">
            Doriți să completați formularul pentru salar suplimentar?
          </Typography>
        </>
      }
      title="Notificare modificare salarială"
      open={open}
      maxWidth="sm"
    />
  );
};

export default SwitchToSalaryChangeDialog;
