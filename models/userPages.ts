import { USER_DEP_ASSIGNMENT_ROLES } from './userDepartmentAssignment';

export interface IMenuPage {
  name: string;
  url: string;
  isAvailable: (params?: any) => boolean;
}

export const USER_PAGES: IMenuPage[] = [
  {
    name: 'Evaluări primite',
    url: '/my-evaluations',
    isAvailable: () => true,
  },
  {
    name: 'Rapoarte',
    url: '/reports',
    isAvailable: (user) => !!user?.canViewAllReviews || !!user?.canEditAllReviews,
  },
  {
    name: 'Formulare',
    url: '/surveys',
    isAvailable: (user) => !!user?.canManageSessions,
  },
  {
    name: 'Sesiuni de evaluare',
    url: '/evaluation-sessions',
    isAvailable: (user) =>
      !!user?.canManageSessions ||
      user?.userDepartmentAssignments?.find(
        // @ts-ignore
        (depAss) => depAss.role === USER_DEP_ASSIGNMENT_ROLES.EVALUATOR,
      ),
  },
  {
    name: 'Pachete de instruire',
    url: '/ssm-packages',
    isAvailable: (user) => !!user?.isSSM,
  },
  {
    name: 'Organigrama',
    url: '/organigram',
    isAvailable: (user) => !!(user?.canManageUsers || user?.canManageDepartments),
  },
];
