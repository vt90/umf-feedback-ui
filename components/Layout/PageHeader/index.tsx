import React from 'react';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import NavBreadcrumbs, { BreadcrumbInfo } from '../Breadcrumbs';
import UserMenu from '../UserMenu';

interface IPageHeaderProps {
  pages: BreadcrumbInfo[];
  title: string | React.ReactNode;
}

const PageHeader = (props: IPageHeaderProps) => {
  const { pages, title } = props;

  return (
    <Box component="header" sx={{ display: 'flex', alignItems: 'center' }}>
      <Box sx={{ flexGrow: 1 }}>
        <NavBreadcrumbs pages={pages} />

        {typeof title === 'string' ? (
          <Typography sx={{ color: 'white' }} variant="h5" component="h1">
            <strong>{title}</strong>
          </Typography>
        ) : (
          title
        )}
      </Box>

      <Box>
        <UserMenu />
      </Box>
    </Box>
  );
};

export default PageHeader;
