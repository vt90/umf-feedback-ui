import React, { useCallback, useMemo } from 'react';
import EditIcon from '@mui/icons-material/Edit';
import DeleteIcon from '@mui/icons-material/Block';
import IconButton from '@mui/material/IconButton';
import Box from '@mui/material/Box';
import Tooltip from '@mui/material/Tooltip';
import Typography from '@mui/material/Typography';
import EnhancedTable from '../../Common/Table';
import { IUser } from '../../../models/user';
import { IDepartment } from '../../../models/department';

interface IUsersListProps {
  users: IUser[];
  departments: IDepartment[];
  onSelectUser?: (org: IUser) => void;
  onDeleteUser?: (org: IUser) => void;
  PaperComponent?: React.FunctionComponent;
  showType?: boolean;
}

const UsersList = (props: IUsersListProps) => {
  const { users, departments, onSelectUser, onDeleteUser, PaperComponent = Box, showType } = props;

  const headerRows = useMemo(() => {
    return [
      { id: 'fullName', label: 'Nume', align: 'left', colSpan: 3, disableSorting: true },
      { id: 'email', label: 'Marca', align: 'left', colSpan: 1, disableSorting: true },
      { id: 'departments', label: 'Departament', align: 'left', colSpan: 2, disableSorting: true },
      ...(showType
        ? [{ id: 'type', label: 'Tip', align: 'left', colSpan: 1, disableSorting: true }]
        : []),
      { id: 'actions', label: 'Acțiuni', align: 'right', disableSorting: true },
    ];
  }, [showType]);

  const departmentMap = useMemo(
    () => departments && departments.reduce((acc, cur) => ({ ...acc, [cur._id]: cur }), {}),
    [departments],
  );

  const getHeaderRows = useCallback(() => {
    return users.map((user) => {
      const renderName = (
        <Typography
          color={user.deactivated ? 'textSecondary' : 'textPrimary'}
          onClick={() => onSelectUser && onSelectUser(user)}
          sx={{
            ...(onSelectUser && {
              cursor: 'pointer',
            }),
            ...(user.deactivated && {
              fontStyle: 'italic',
            }),
          }}
        >
          {user.fullName}
        </Typography>
      );

      return {
        _id: user._id,
        fullName: renderName,
        smallDeviceHeader: renderName,
        email: user.email,
        departments: (
          <>
            {departmentMap &&
              user?.userDepartmentAssignments
                ?.filter((assignment) => !assignment.deactivated)
                ?.map((assignment) => {
                  return (
                    <Tooltip key={assignment._id} title={assignment.role}>
                      <Typography
                        variant="body2"
                        color="textSecondary"
                        sx={{ display: 'flex' }}
                        gutterBottom
                      >
                        <Typography component="span" variant="body2" sx={{ minWidth: 44 }}>
                          {/* @ts-ignore */}
                          {departmentMap?.[assignment?.departmentId]?.code}&nbsp;-&nbsp;
                        </Typography>

                        <Typography component="span" variant="body2">
                          {/* @ts-ignore */}
                          {departmentMap?.[assignment?.departmentId]?.name}
                        </Typography>
                      </Typography>
                    </Tooltip>
                  );
                })}
          </>
        ),
        type: (
          <>
            {departmentMap &&
              user?.userDepartmentAssignments
                ?.filter((assignment) => !assignment.deactivated)
                ?.map((assignment) => {
                  return (
                    <Typography key={assignment._id} variant="body2" color="textSecondary">
                      {assignment.type}
                    </Typography>
                  );
                })}
          </>
        ),
        actions: (
          <>
            {onSelectUser && (
              <IconButton onClick={() => onSelectUser(user)} size="small" color="info">
                <EditIcon fontSize="inherit" />
              </IconButton>
            )}

            {onDeleteUser && (
              <IconButton onClick={() => onDeleteUser(user)} size="small" color="error">
                <DeleteIcon fontSize="inherit" />
              </IconButton>
            )}
          </>
        ),
        data: user,
      };
    });
  }, [departmentMap, onDeleteUser, onSelectUser, users]);

  return (
    <>
      <EnhancedTable
        initialOrderByField="fullName"
        headerRows={headerRows}
        orderByFieldCriteriaFunctions={{
          fullName: (row: any) => row?.data && row.data.fullName,
          // questionsCount: (row: any) => row?.data && row.data.questions?.length,
          // usersStarted: (row: any) => row?.data && getUserStats(row.data).completedAnswering,
          // usersCompleted: (row: any) => row?.data && getUserStats(row.data).completedAnswering,
          // published: (row: any) => row?.data && !!row.data.published,
        }}
        PaperComponent={PaperComponent}
        paperComponentStyles={{
          // px: 3,
          py: 2,
        }}
        rows={getHeaderRows()}
        smallUntilBreakpoint="md"
      />
    </>
  );
};

export default UsersList;
