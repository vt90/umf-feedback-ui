import React from 'react';
import Link from 'next/link';
import ButtonBase from '@mui/material/ButtonBase';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import { useAuthContext } from '../../../context/authContext';
import { useRouter } from 'next/router';
import ListItemText from '@mui/material/ListItemText';
import Card from '@mui/material/Card';
import { USER_DEP_ASSIGNMENT_ROLES } from '../../../models/userDepartmentAssignment';
import { IUser } from '../../../models/user';
import AccountBox from '@mui/icons-material/AccountBox';
import AssessmentIcon from '@mui/icons-material/Assessment';
import AssignmentIcon from '@mui/icons-material/Assignment';
import ArticleIcon from '@mui/icons-material/Article';
import AccountTreeIcon from '@mui/icons-material/AccountTree';
import BarChartIcon from '@mui/icons-material/BarChart';
import BadgeIcon from '@mui/icons-material/Badge';
import BeenhereIcon from '@mui/icons-material/Beenhere';
import CalendarMonthIcon from '@mui/icons-material/CalendarMonth';
import HealthAndSafetyIcon from '@mui/icons-material/HealthAndSafety';
import History from '@mui/icons-material/History';
import PlaylistAddCheckCircleIcon from '@mui/icons-material/PlaylistAddCheckCircle';
import TodayIcon from '@mui/icons-material/Today';

const hasEvaluations = process.env.NEXT_PUBLIC_HAS_EVALUATIONS === 'true';
const hasSSM = process.env.NEXT_PUBLIC_HAS_SSM === 'true';

const PAGES = {
  public: {
    isAvailable: (user: IUser) => !!user,
    pages: [
      {
        name: 'Evaluări primite',
        url: '/my-evaluations',
        isAvailable: () => hasEvaluations,
        icon: <AccountBox sx={{ color: '#3a416f' }} />,
      },
      {
        name: 'Medicina muncii',
        url: '/my-medical-checks',
        isAvailable: () => hasSSM,
        icon: <HealthAndSafetyIcon sx={{ color: '#f53939' }} />,
      },
      {
        name: 'Instruirile mele SSM',
        url: '/my-ssm',
        isAvailable: () => hasSSM,
        icon: <History sx={{ color: '#FFA177' }} />,
      },
    ],
  },
  evaluations: {
    name: 'Evaluări',
    isAvailable: (user: IUser) =>
      hasEvaluations &&
      (!!user?.canManageSessions ||
        !!user?.canViewAllReviews ||
        !!user?.canEditAllReviews ||
        user?.userDepartmentAssignments?.find(
          // @ts-ignore
          (depAss) => depAss.role === USER_DEP_ASSIGNMENT_ROLES.EVALUATOR,
        )),
    pages: [
      {
        name: 'Sesiuni de evaluare',
        url: '/evaluation-sessions',
        isAvailable: (user: IUser) =>
          hasEvaluations &&
          (!!user?.canManageSessions ||
            user?.userDepartmentAssignments?.find(
              // @ts-ignore
              (depAss) => depAss.role === USER_DEP_ASSIGNMENT_ROLES.EVALUATOR,
            )),
        icon: <CalendarMonthIcon sx={{ color: '#e81e63' }} />,
      },
      {
        name: 'Formulare standard',
        url: '/surveys',
        isAvailable: (user: IUser) => hasEvaluations && !!user?.canManageSessions,
        icon: <ArticleIcon sx={{ color: '#A3A1FB' }} />,
      },
      {
        name: 'Rapoarte',
        url: '/reports',
        isAvailable: (user: IUser) =>
          (hasEvaluations && !!user?.canViewAllReviews) || !!user?.canEditAllReviews,
        icon: <AssessmentIcon sx={{ color: '#5EE2A0' }} />,
      },
    ],
  },
  ssm: {
    name: 'Instruire SSM și Medicina Muncii',
    isAvailable: (user: IUser) =>
      hasSSM &&
      (!!user?.isSSM ||
        !!user?.isExternal ||
        !!user?.canViewAllReviews ||
        !!user?.canEditAllReviews ||
        user?.userDepartmentAssignments?.find(
          // @ts-ignore
          (depAss) => depAss.role === USER_DEP_ASSIGNMENT_ROLES.EVALUATOR,
        )),
    pages: [
      {
        name: 'Fișe de aptitudini',
        url: '/medical-checks',
        isAvailable: (user: IUser) =>
          hasSSM &&
          (!!user?.isSSM ||
            !!user?.isExternal ||
            !!user?.canViewAllReviews ||
            !!user?.canEditAllReviews),
        icon: <PlaylistAddCheckCircleIcon sx={{ color: '#ff7d40' }} />,
      },
      {
        name: 'Instruiri la angajare',
        url: '/ssm-sessions-initial-trainings',
        isAvailable: (user: IUser) =>
          hasSSM &&
          (!!user?.isSSM ||
            user?.userDepartmentAssignments?.find(
              // @ts-ignore
              (depAss) => depAss.role === USER_DEP_ASSIGNMENT_ROLES.EVALUATOR,
            )),
        icon: <BadgeIcon sx={{ color: '#8b42f5' }} />,
      },
      {
        name: 'Instruiri periodice',
        url: '/ssm-sessions-periodic-trainings',
        isAvailable: (user: IUser) =>
          hasSSM &&
          (!!user?.isSSM ||
            user?.userDepartmentAssignments?.find(
              // @ts-ignore
              (depAss) => depAss.role === USER_DEP_ASSIGNMENT_ROLES.EVALUATOR,
            )),
        icon: <TodayIcon sx={{ color: '#FEC163' }} />,
      },
      {
        name: 'Rapoarte',
        url: '/ssm-report-sessions-periodic-trainings',
        isAvailable: (user: IUser) =>
          (hasSSM && !!user?.canViewAllReviews) || !!user?.canEditAllReviews,
        icon: <BarChartIcon sx={{ color: '#11cdef' }} />,
      },
    ],
  },
  admin: {
    name: 'Administrativ',
    isAvailable: (user: IUser) =>
      !!(user?.canManageUsers || user?.canManageDepartments || user?.isSSM),
    pages: [
      {
        name: 'Documentație SSM',
        url: '/ssm-packages',
        isAvailable: (user: IUser) => hasSSM && !!user?.isSSM,
        icon: <AssignmentIcon sx={{ color: '#2196f3' }} />,
      },
      {
        name: 'Instruiri Periodice SSM',
        url: '/ssm-periodic-trainings',
        isAvailable: (user: IUser) => hasSSM && !!user?.isSSM,
        icon: <BeenhereIcon sx={{ color: '#fb940f' }} />,
      },
      {
        name: 'Organigramă',
        url: '/organigram',
        isAvailable: (user: IUser) => !!(user?.canManageUsers || user?.canManageDepartments),
        icon: <AccountTreeIcon sx={{ color: 'rgb(103, 116, 142)' }} />,
      },
    ],
  },
};

const logo = process.env.NEXT_PUBLIC_LOGO || '/logo-umf.png';

const Sidenav = () => {
  const { user } = useAuthContext();
  const router = useRouter();

  const isActivePage = (menuItemUrl: string) => {
    return router.pathname.includes(menuItemUrl);
  };

  return (
    <Card component="aside" sx={{ py: 3, position: { xs: 'relative', md: 'sticky' }, top: 30 }}>
      <Box sx={{ display: 'flex', alignItems: 'center', px: 3 }}>
        <Link href="/">
          <a>
            <img src={logo} style={{ height: 48, width: 'auto' }} alt="UMF" />
          </a>
        </Link>

        {/* @ts-ignore */}
        <ListItemText sx={{ ml: 2 }} primary={user?.fullName} secondary={user?.email} />
      </Box>

      {Object.entries(PAGES)
        // @ts-ignore
        .filter(([_, value]) => !!value.isAvailable(user))
        .map(([key, value], index) => {
          // @ts-ignore
          const { name, pages } = value;
          return (
            <Box key={key} sx={{ mb: index < Object.keys(PAGES).length - 1 ? 2 : 1, mt: 1 }}>
              {name && (
                <Box sx={{ px: 2.5, mb: 1 }}>
                  <Typography variant="body2" sx={{ color: 'text.disabled' }}>
                    {name}
                  </Typography>
                </Box>
              )}
              {pages
                // @ts-ignore
                .filter((page) => page.isAvailable(user))
                .map((page) => {
                  const { url, name, icon } = page;
                  const isActive = isActivePage(url);
                  return (
                    <ButtonBase
                      key={url}
                      sx={{
                        mx: 1,
                        width: 'calc(100% - 20px)',
                        justifyContent: 'flex-start',
                        ...(isActive && {
                          bgcolor: 'rgba(17, 205, 239, 0.1)',
                          borderRadius: 2,
                        }),
                      }}
                    >
                      <Link href={url}>
                        <a
                          style={{
                            width: '100%',
                            textAlign: 'left',
                            display: 'flex',
                            alignItems: 'center',
                            padding: '8px 12px ',
                          }}
                        >
                          {icon && <Box sx={{ display: 'inline-block', mr: 1 }}>{icon}</Box>}
                          <Typography
                            component="span"
                            sx={{ color: isActive ? 'primary.main' : 'text.primary' }}
                          >
                            {name}
                          </Typography>
                        </a>
                      </Link>
                    </ButtonBase>
                  );
                })}
            </Box>
          );
        })}
    </Card>
  );
};

export default Sidenav;
