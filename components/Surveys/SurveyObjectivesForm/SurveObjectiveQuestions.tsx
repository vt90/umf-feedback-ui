import React, { Fragment } from 'react';
import { Control, useFieldArray } from 'react-hook-form';
import get from 'lodash/get';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import Grid from '@mui/material/Grid';
import IconButton from '@mui/material/IconButton';
import Typography from '@mui/material/Typography';
import DeleteIcon from '@mui/icons-material/Delete';
import TextField from '../../Common/FormInputs/TextField';

interface ISurveyObjectiveQuestionsFieldProps {
  control: Control;
  errors: any;
  isLoading: boolean;
  register: any;
}

const SurveyObjectiveQuestionsField = (props: ISurveyObjectiveQuestionsFieldProps) => {
  const { control, errors, isLoading, register } = props;

  const {
    fields: surveyObjectivesQuestions,
    append: appendSurveyObjectiveQuestions,
    remove: removeSurveyObjectiveQuestions,
  } = useFieldArray({
    control,
    name: `questions`,
  });

  return (
    <>
      {surveyObjectivesQuestions.map((surveyObjectiveQuestion, surveyObjectiveQuestionIndex) => {
        const key = `questions.${surveyObjectiveQuestionIndex}`;
        const chapterQuestionErrors = get(errors, key);

        return (
          <Fragment key={key}>
            <Grid item xs={1}>
              <input type="hidden" {...register(`${key}._id`)} />
              <Button
                id={`addQuestionButton`}
                onClick={() =>
                  appendSurveyObjectiveQuestions(
                    {
                      name: `Obiectivul ${surveyObjectivesQuestions.length + 1}`,
                      order: surveyObjectivesQuestions.length + 1,
                      timePercentage: 100,
                      donePercentage: 100,
                    },
                    {
                      shouldFocus: true,
                      focusName: `questions.${surveyObjectivesQuestions.length}.name`,
                    },
                  )
                }
                sx={{ display: 'none' }}
              />
              <Typography variant="body2" sx={{ textAlign: { xs: 'left', md: 'right' } }}>
                {surveyObjectiveQuestionIndex + 1}
              </Typography>
            </Grid>

            <Grid item xs={11}>
              <Box sx={{ display: 'flex', alignItems: 'center' }}>
                <Box sx={{ mr: 1, flexGrow: 1 }}>
                  <TextField
                    multiline
                    required
                    disabled={isLoading}
                    errorMessage={!!chapterQuestionErrors?.name && 'Obiectivul este necesar'}
                    {...register(`${key}.name`)}
                    size="small"
                    sx={{
                      '& legend': { display: 'none' },
                      '& fieldset': {
                        top: 0,
                        borderColor: 'transparent',
                      },
                    }}
                    inputProps={{
                      sx: {
                        typography: 'body1',
                        fontWeight: 'bold',
                      },
                    }}
                  />
                </Box>

                <Box
                  sx={{
                    display: 'flex',
                    alignItems: 'center',
                    pl: 2,
                  }}
                >
                  <Typography variant="body2">% din timp:</Typography>
                  <Box sx={{ maxWidth: 64, ml: 1, mr: 1 }}>
                    <TextField
                      errorMessage={!!errors?.weight && 'Ponderea necesară'}
                      {...register(`${key}.timePercentage`)}
                    />
                  </Box>
                </Box>

                <Box
                  sx={{
                    bgcolor: 'background.default',
                    display: 'flex',
                    alignItems: 'center',
                    pl: 2,
                  }}
                >
                  <Typography variant="body2">% realizat:</Typography>
                  <Box sx={{ maxWidth: 64, ml: 1 }}>
                    <TextField
                      errorMessage={!!errors?.weight && 'Ponderea necesară'}
                      {...register(`${key}.donePercentage`)}
                    />
                  </Box>
                </Box>

                <IconButton
                  onClick={() => removeSurveyObjectiveQuestions(surveyObjectiveQuestionIndex)}
                  color="error"
                >
                  <DeleteIcon />
                </IconButton>
              </Box>
            </Grid>
          </Fragment>
        );
      })}
    </>
  );
};

export default SurveyObjectiveQuestionsField;
