import React, { useCallback, useMemo } from 'react';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import Tooltip from '@mui/material/Tooltip';
import EnhancedTable from '../../Common/Table';
import NoDataAvailable from '../../Common/NoDataAvailable';
import YesIcon from '@mui/icons-material/CheckBox';
import WaitingIcon from '@mui/icons-material/HourglassBottom';
import { ISSMPeriodicTrainingSession } from '../../../models/ssmPeriodicTrainingSession';
import Button from '@mui/material/Button';
import { TRAINING_MONTHS } from '../../../models/ssmPeriodicTraining';
import moment from 'moment';
// import Status from '../../Common/Status';

interface IUserUserListProps {
  ssmPeriodicTrainingsSessions: ISSMPeriodicTrainingSession[];
  onSelectSession?: (session: ISSMPeriodicTrainingSession) => void;
}

const SSMTrainingSessionList = (props: IUserUserListProps) => {
  const { ssmPeriodicTrainingsSessions, onSelectSession } = props;

  const headerRows = useMemo(() => {
    const columns = [
      { id: 'name', label: 'Membru Evaluat', align: 'left', colSpan: 2 },
      { id: 'email', label: 'Marca', align: 'left', colSpan: 1 },
      { id: 'department', label: 'Departament', align: 'left', colSpan: 3 },
      { id: 'training', label: 'Instruire', align: 'left', colSpan: 3 },
      {
        id: 'createdAt',
        label: 'Data instruirii',
        align: 'center',
        colSpan: 2,
        disableSorting: true,
      },
      { id: 'userAcknowledged', label: 'Semnătură celui instruit', align: 'center', colSpan: 1 },
      {
        id: 'contraEvaluated',
        label: 'Semnătură celui care a instruit',
        align: 'center',
        colSpan: 1,
      },
      { id: 'ssmEvaluated', label: 'Instruire verificată', align: 'center', colSpan: 1 },
    ];

    if (onSelectSession) {
      columns.push({ id: 'actions', label: 'Acțiuni', align: 'right', colSpan: 1 });
    }

    return columns;
  }, [onSelectSession]);

  const renderIcon = useCallback((value: boolean | null | undefined, yesText: string) => {
    const color = value ? 'success' : 'info';
    const tooltipText = value ? yesText : 'În așteptare';

    const iconProps = {
      sx: {
        color: `${color}.light`,
      },
    };

    return (
      <Tooltip title={tooltipText}>
        <Box>
          {value ? (
            // @ts-ignore
            <YesIcon {...iconProps} />
          ) : (
            // @ts-ignore
            <WaitingIcon {...iconProps} />
          )}
        </Box>
      </Tooltip>
    );
  }, []);

  const getHeaderRows = useCallback(() => {
    return ssmPeriodicTrainingsSessions.map((session) => {
      const {
        _id,
        userDepartmentAssignmentId,
        userId,
        ssmPeriodicTrainingId,
        userAcknowledged,
        contraEvaluated,
        ssmEvaluated,
        sessionDate,
      } = session;

      const created = moment(sessionDate);

      return {
        _id,
        smallDeviceHeader: userId.fullName,
        name: userId.fullName,
        // @ts-ignore
        email: userId?.email,

        // @ts-ignore
        training: ssmPeriodicTrainingId?.name,
        department: (
          // @ts-ignore
          <Tooltip title={userDepartmentAssignmentId?.role}>
            <Typography
              variant="body2"
              color="textSecondary"
              sx={{
                textDecoration: userDepartmentAssignmentId?.deactivated ? 'line-through' : 'none',
              }}
            >
              {/* @ts-ignore */}
              {userDepartmentAssignmentId?.departmentId?.name}
            </Typography>
          </Tooltip>
        ),
        createdAt: TRAINING_MONTHS[created.month()] + ' ' + created.format('YYYY'),
        contraEvaluated: renderIcon(contraEvaluated, 'Instruire semnată de către instruit'),
        userAcknowledged: renderIcon(
          userAcknowledged,
          'Instruire semnată de către persoana care a instruit',
        ),
        ssmEvaluated: renderIcon(ssmEvaluated, 'Instruire verificată'),
        data: session,
        actions: onSelectSession ? (
          <>
            <Button onClick={() => onSelectSession(session)} variant="contained" size="small">
              Vizualizează
            </Button>
          </>
        ) : null,
        sx: userDepartmentAssignmentId?.deactivated
          ? {
              bgcolor: 'action.hover',
            }
          : null,
      };
    });
  }, [ssmPeriodicTrainingsSessions, renderIcon, onSelectSession]);

  if (!ssmPeriodicTrainingsSessions?.length) {
    return (
      <NoDataAvailable content="În momentul de faţă nu există nicio instruire disponibila pe platformă" />
    );
  }

  return (
    <>
      <EnhancedTable
        headerRows={headerRows}
        initialOrderByField="createdAt"
        initialOrder="desc"
        orderByFieldCriteriaFunctions={{
          createdAt: (row: any) => row?.data?.createdAt,
          // department: (row: any) => row?.data?.departmentId?.name,
          contraEvaluated: (row: any) => row?.data?.contraEvaluated,
          userAcknowledged: (row: any) => row?.data?.userAcknowledged,
          userAgreement: (row: any) => row?.data?.userAgreement,
          ssmEvaluated: (row: any) => row?.data?.ssmEvaluated,
          contraEvaluatorAgreement: (row: any) => row?.data?.contraEvaluatorAgreement,
        }}
        PaperComponent={Box}
        paperComponentStyles={{
          px: 3,
          py: 2,
          border: '0.0625rem solid rgb(222, 226, 230)',
          borderRadius: 2,
          '& h6': {
            mt: 1,
          },
        }}
        rows={getHeaderRows()}
        smallUntilBreakpoint="md"
        disableOrder
      />
    </>
  );
};

export default SSMTrainingSessionList;
