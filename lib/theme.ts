import { createTheme, responsiveFontSizes } from '@mui/material/styles';
import createCache from '@emotion/cache';
import { Breakpoint } from '@mui/system';

const isBrowser = typeof document !== 'undefined';

export const COLORS = [
  '#A3A1FB',
  '#e81e63',
  '#5EE2A0',
  '#FEC163',
  '#39c1e8',
  '#FFA177',
  '#f53939',
  '#3a416f',
];

const TEXT_COLORS = {
  primary: 'rgb(52, 71, 103)',
  secondary: 'rgb(103, 116, 142)',
};

const BACKGROUND_COLORS = {
  default: '#f0f2f5',
  paper: '#ffffff',
};

export const VIEWPORT_MAX_WIDTH: Breakpoint = 'xl';

export const theme = responsiveFontSizes(
  createTheme({
    palette: {
      primary: {
        100: '#99CCF3',
        main: '#2196f3',
        dark: '#1e2b48',
        contrastText: '#FFFFFF',
      },
      secondary: {
        main: '#e81e63',
      },
      success: {
        main: 'rgb(45, 206, 137)',
        contrastText: '#FFFFFF',
      },
      warning: {
        main: '#fb940f',
        contrastText: '#FFFFFF',
      },
      error: {
        main: 'rgb(245, 54, 92)',
      },
      text: TEXT_COLORS,
      background: BACKGROUND_COLORS,
    },
    typography: {
      fontFamily: ['Gilroy', 'Poppins', 'Arial', 'sans-serif'].join(','),
      fontSize: 13,
      fontWeightRegular: 500,
    },
    components: {
      MuiButton: {
        styleOverrides: {
          root: {
            textTransform: 'none',
          },
          containedInherit: {
            backgroundColor: '#3e3e3e',
            color: '#FFFFFF',
            '&:hover': {
              backgroundColor: '#1c1c1c',
            },
          },
        },
      },
      MuiDialog: {
        styleOverrides: {
          paper: {
            backgroundColor: '#FFFFFF',
            borderRadius: '4px',
            boxShadow:
              '0px 11px 15px -7px rgba(0,0,0,0.2), 0px 24px 38px 3px rgba(0,0,0,0.14), 0px 9px 46px 8px rgba(0,0,0,0.12)',
            minWidth: '40vh',
          },
        },
      },
      MuiTabs: {
        styleOverrides: {
          root: {
            backgroundColor: BACKGROUND_COLORS.default,
            borderRadius: 4,
            padding: 6,
          },
          indicator: {
            display: 'none',
          },
        },
      },
      MuiTab: {
        styleOverrides: {
          root: {
            '& *': {
              textTransform: 'none',
              fontWeight: 400,
            },
            '&.Mui-selected': {
              backgroundColor: BACKGROUND_COLORS.paper,
              fontWeight: 700,
              color: TEXT_COLORS.primary,
            },
            textTransform: 'none',
            borderRadius: 4,
          },
        },
      },
    },
  }),
);

theme.shadows[1] = '0 4px 6px -1px rgb(0 0 0 / 10%), 0 2px 4px -1px rgb(0 0 0 / 6%)';
theme.shadows[2] = '0 4px 6px -1px rgb(0 0 0 / 10%), 0 2px 4px -1px rgb(0 0 0 / 6%)';

// On the client side, Create a meta tag at the top of the <head> and set it as insertionPoint.
// This assures that MUI styles are loaded first.
// It allows developers to easily override MUI styles with other styling solutions, like CSS modules.
export const createEmotionCache = () => {
  let insertionPoint;

  if (isBrowser) {
    const emotionInsertionPoint = document.querySelector('meta[name="emotion-insertion-point"]');
    insertionPoint = emotionInsertionPoint ?? undefined;
  }

  // @ts-ignore
  return createCache({ key: 'mui-style', insertionPoint });
};
