import React, { useState } from 'react';
import Head from 'next/head';
import { useMutation, useQuery, useQueryClient } from '@tanstack/react-query';
import Button from '@mui/material/Button';
import Dialog from '../../../components/Common/Dialog';
import LoadingButton from '@mui/lab/LoadingButton';
import Typography from '@mui/material/Typography';
import AddIcon from '@mui/icons-material/Add';
import AdminPage from '../../../components/AdminPage';
import ListLoadingIndicator from '../../../components/Common/ListLoadingIndcator';
import NoDataAvailable from '../../../components/Common/NoDataAvailable';
import Pagination from '../../../components/Common/Pagination';
import UserForm from '../../../components/Users/Form';
import UsersList from '../../../components/Users/List';
import UserListFilter from '../../../components/Users/List/Filter';
import OrganigramNavigation from '../../../components/Organigram/Navigation';
import { withPageAuthRequired } from '../../../guards/authRequired';
import { IUser } from '../../../models/user';
import { findUsers, deleteUser, createOrUpdateUser } from '../../../services/users';
import { findDepartments } from '../../../services/departments';
import { SELECTED_ACTIONS } from '../../../lib/constants';
import { useNotificationContext } from '../../../context/notificationContext';
import { getAllFunctions } from '../../../services/functions';
import Paper from '@mui/material/Paper';
import { createOrUpdateSSMInitialTrainingSession } from '../../../services/ssmInitialTrainingSessions';
import UserDepartmentAssignmentExportButton from '../../../components/UserDepoartmentAssignments/ExportButton';

const FETCH_QUERY_KEY = ['users'];
const FETCH_DEPARTMENTS_QUERY_KEY = ['find-departments'];
const FETCH_FUNCTIONS_QUERY_KEY = ['depFunctions'];
const FETCH_SSM_KEY = ['ssm-initial-sessions-trainings'];

const Users = () => {
  const [pageNumber, setPageNumber] = useState(1);
  const [pageSize] = useState(50);
  const [order, setOrder] = useState(1);
  const [orderBy, setOrderBy] = useState('fullName');
  const [searchTerm, setSearchTerm] = useState('');
  const [showDeactivated, setShowDeactivated] = useState(false);
  const [selectedUser, setSelectedUser] = useState<Partial<IUser> | null>(null);
  const [selectedAction, setSelectedAction] = useState<string | null>(null);
  const { setError, setMessage } = useNotificationContext();

  const onUserAction = (action: string) => (user: Partial<IUser>) => {
    setSelectedUser({
      ...user,
      userDepartmentAssignments: user.userDepartmentAssignments?.filter((ass) => !ass.deactivated),
    });
    setSelectedAction(action);
  };

  const resetSelectedUserState = () => {
    setSelectedUser(null);
    setSelectedAction(null);
  };

  const queryClient = useQueryClient();

  const fetchParams = {
    limit: pageSize,
    offset: (pageNumber - 1) * pageSize,
    // @ts-ignore
    order,
    // @ts-ignore
    orderBy,
    // @ts-ignore
    searchTerm,
    // @ts-ignore
    showDeactivated,
  };

  const { isLoading, data } = useQuery([...FETCH_QUERY_KEY, fetchParams], () =>
    findUsers(fetchParams),
  );

  const { data: functions } = useQuery(FETCH_FUNCTIONS_QUERY_KEY, getAllFunctions);

  const { data: departmentsData } = useQuery(FETCH_DEPARTMENTS_QUERY_KEY, () =>
    findDepartments({}),
  );

  const { isLoading: isDeleting, mutate: deleteUserById } = useMutation(deleteUser, {
    onSuccess: async () => {
      await queryClient.invalidateQueries(FETCH_QUERY_KEY);
      resetSelectedUserState();
      setMessage('Utilizator sters');
    },
    onError: setError,
  });

  const { isLoading: isUpdating, mutate: updateUser } = useMutation(createOrUpdateUser, {
    onSuccess: async () => {
      await queryClient.invalidateQueries(FETCH_QUERY_KEY);
      resetSelectedUserState();
      setMessage('Utilizator salvat');
    },
    onError: setError,
  });

  const { isLoading: isCreatingSSM, mutate: initSSM } = useMutation(
    () =>
      createOrUpdateSSMInitialTrainingSession({
        // @ts-ignore
        userId: selectedUser._id,
        // @ts-ignore
        userDepartmentAssignmentId: selectedUser?.userDepartmentAssignments[0]._id,
      }),
    {
      onSuccess: async () => {
        await queryClient.invalidateQueries(FETCH_SSM_KEY);
        resetSelectedUserState();
        setMessage('Instruire creata');
      },
      onError: setError,
    },
  );

  const onRemoveAssignment = async () => await queryClient.invalidateQueries(FETCH_QUERY_KEY);

  const onUserFormSubmit = (user: IUser) => {
    /**
     * Filter out assignments that remained the same
     * */
    const modifiedUserDepartmentAssignments = user?.userDepartmentAssignments?.filter(
      (assignment) => {
        const isChanged = selectedUser?.userDepartmentAssignments?.find((orgAssignment) => {
          let sameValues = true;

          Object.keys(assignment).forEach((key) => {
            // @ts-ignore
            if (assignment[key] !== orgAssignment[key]) {
              sameValues = false;
            }
          });

          return sameValues;
        });

        return !isChanged;
      },
    );

    updateUser({
      ...user,
      userDepartmentAssignments: modifiedUserDepartmentAssignments,
    });
  };

  return (
    <div>
      <Head>
        <title>Utilizatori</title>
      </Head>

      <AdminPage
        title="Utilizatori"
        pages={[{ url: '/users', name: 'Utilizatori' }]}
        actions={
          <>
            <UserDepartmentAssignmentExportButton />
            <Button
              onClick={() => {
                setSelectedUser({});
                setSelectedAction(SELECTED_ACTIONS.CREATE_OR_UPDATE);
              }}
              variant="contained"
              sx={{ ml: 1 }}
              endIcon={<AddIcon />}
            >
              Adaugă Utilizator
            </Button>
          </>
        }
      >
        <Paper sx={{ p: 3 }}>
          <OrganigramNavigation />

          <UserListFilter
            order={order}
            orderBy={orderBy}
            setOrder={setOrder}
            setOrderBy={setOrderBy}
            searchTerm={searchTerm}
            setSearchTerm={(value) => {
              setPageNumber(1);
              setSearchTerm(value);
            }}
            showDeactivated={showDeactivated}
            setShowDeactivated={setShowDeactivated}
          />

          {data?.results?.length || isLoading ? (
            <>
              {isLoading && <ListLoadingIndicator />}

              {data?.results?.length && departmentsData?.results && (
                <>
                  <UsersList
                    users={data?.results}
                    onSelectUser={onUserAction(SELECTED_ACTIONS.CREATE_OR_UPDATE)}
                    onDeleteUser={onUserAction(SELECTED_ACTIONS.DELETE)}
                    departments={departmentsData.results}
                  />

                  <Pagination
                    sx={{ mt: 2 }}
                    pageNumber={pageNumber}
                    pageSize={pageSize}
                    totalResults={data.count}
                    setPageNumber={setPageNumber}
                  />
                </>
              )}
            </>
          ) : (
            <NoDataAvailable content="În momentul de faţă nu există niciun utilizator disponibil pe platformă" />
          )}
        </Paper>

        <Dialog
          actions={
            <LoadingButton
              variant="contained"
              onClick={() => selectedUser && deleteUserById(selectedUser._id as string)}
              loading={isDeleting}
            >
              Dezactivează
            </LoadingButton>
          }
          content={
            <>
              <Typography color="textSecondary" textAlign="center">
                Sunteți siguri că doriți să dezactivați utilizatorul
                <strong>
                  <i>&quot;{selectedUser?.fullName}&quot;</i>
                </strong>
                ?
              </Typography>
            </>
          }
          title="Confirmati dezactivarea utilizatorului?"
          onClose={resetSelectedUserState}
          open={!!(selectedUser && selectedAction === SELECTED_ACTIONS.DELETE)}
        />

        {data?.results &&
        functions?.results &&
        departmentsData?.results &&
        selectedUser &&
        selectedAction === SELECTED_ACTIONS.CREATE_OR_UPDATE ? (
          <UserForm
            departments={departmentsData.results}
            functions={functions?.results}
            initialValues={selectedUser}
            isLoading={isUpdating}
            isCreatingSSM={isCreatingSSM}
            open={true}
            onClose={resetSelectedUserState}
            onSubmit={onUserFormSubmit}
            onRemoveAssignment={onRemoveAssignment}
            onSsmInit={initSSM}
          />
        ) : null}
      </AdminPage>
    </div>
  );
};

export const getServerSideProps = withPageAuthRequired();

export default Users;
