import React from 'react';
import ReactQuill, { ReactQuillProps } from 'react-quill';
import { useIntersectionObserver } from 'react-intersection-observer-hook';
import Card from '@mui/material/Card';
import { SxProps } from '@mui/material';

interface IRichTextProps extends ReactQuillProps {
  sx?: SxProps;
  disableStickyToolbar?: boolean;
}

const RichText = (props: IRichTextProps) => {
  const { defaultValue, onChange, readOnly, sx, disableStickyToolbar } = props;
  const [ref, { entry }] = useIntersectionObserver();
  const isVisible = entry && entry.isIntersecting;

  const modules = {
    toolbar: readOnly
      ? null
      : [
          [{ header: [1, 2, 3, false] }],
          ['bold', 'italic', 'underline', 'strike', 'blockquote'],
          [{ list: 'ordered' }, { list: 'bullet' }, { indent: '-1' }, { indent: '+1' }],
          ['link', 'image'],
        ],
  };

  const formats = [
    'header',
    'bold',
    'italic',
    'underline',
    'strike',
    'blockquote',
    'list',
    'bullet',
    'indent',
    'link',
    'image',
  ];

  return (
    <>
      {/* Dummy div to toggle toolbar position */}
      <div ref={ref} />
      <Card
        className="text-editor"
        sx={{
          bgcolor: 'background.paper',
          '& .ql-toolbar': {
            position: isVisible || disableStickyToolbar ? 'relative' : 'fixed',
            top: 0,
            borderTop: 'none !important',
            borderLeft: 'none !important',
            borderRight: 'none !important',
            borderBottom: isVisible ? 'none !important' : 1,
            px: '24px !important',
            py: '16px !important',
            ...(!isVisible &&
              !disableStickyToolbar && {
                backdropFilter: 'saturate(110%) blur(3px) !important',
                backgroundColor: 'hsla(0,0%,100%,.75)!important',
                width: 'calc(100% - 48px)',
                maxWidth: '1152px',
                zIndex: 10,
                borderBottomColor: 'divider',
              }),
          },
          '& .ql-container': {
            border: 'none !important',
            minHeight: 'calc(100vh - 200px)',
            px: 2,
            pb: 2,
          },
          '& h1': {
            typography: 'h3',
          },
          '& h2': {
            typography: 'h5',
          },
          '& h3': {
            typography: 'h6',
          },
          '& p': {
            typography: 'body1',
            fontWeight: 'light',
          },
          ...sx,
        }}
      >
        <ReactQuill
          theme="snow"
          defaultValue={defaultValue}
          onChange={onChange}
          modules={modules}
          formats={formats}
          readOnly={readOnly}
        />
      </Card>
    </>
  );
};

export default RichText;
