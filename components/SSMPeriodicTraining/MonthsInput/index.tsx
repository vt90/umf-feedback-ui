import React from 'react';
import Box from '@mui/material/Box';
import ToggleButton from '@mui/material/ToggleButton';
import ToggleButtonGroup, { ToggleButtonGroupProps } from '@mui/material/ToggleButtonGroup';
import Typography from '@mui/material/Typography';
import { TRAINING_MONTHS } from '../../../models/ssmPeriodicTraining';

// @ts-ignore
interface IPeriodicTrainingMonthsSelect extends ToggleButtonGroupProps {
  value: string;
  onChange: (newValue: string[]) => void;
  label?: string;
  disabled?: boolean;
  sx?: any;
}

// @ts-ignore
const PeriodicTrainingMonthsSelect = (props: IPeriodicTrainingMonthsSelect, ref) => {
  const { value, onChange, label, disabled, sx } = props;

  const handleChange = (event: any, newValue: string | null) => {
    // @ts-ignore
    !disabled && onChange(newValue || []);
  };

  return (
    <Box sx={{ display: 'flex', alignItems: 'center' }}>
      {label && (
        <Typography variant="body2" sx={{ mr: 1 }}>
          {label}
        </Typography>
      )}

      <ToggleButtonGroup
        ref={ref}
        value={value || []}
        onChange={handleChange}
        sx={{
          bgcolor: disabled ? 'background.paper' : 'background.default',
          borderRadius: '4px',
          padding: '6px',
          ...sx,
        }}
      >
        {TRAINING_MONTHS.map((month) => (
          <ToggleButton
            value={month}
            key={month}
            sx={{
              '& *': {
                textTransform: 'none',
                fontWeight: 100,
                color: disabled ? 'text.secondary' : 'text.primary',
              },
              '&.Mui-selected': {
                bgcolor: 'background.paper',
                fontWeight: 700,
                color: 'text.primary',
                opacity: '1',
              },
              textTransform: 'none',
              borderRadius: '4px',
              border: 'none',

              color: disabled ? 'text.secondary' : 'text.primary',
              opacity: disabled ? '0.4' : '1',
              minWidth: {
                sm: '60px',
                xs: '17%',
              },
            }}
          >
            {month}
          </ToggleButton>
        ))}
      </ToggleButtonGroup>
    </Box>
  );
};

export default React.forwardRef(PeriodicTrainingMonthsSelect);
