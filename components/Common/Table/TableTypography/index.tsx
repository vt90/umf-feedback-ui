import React from 'react';
import Typography from '@mui/material/Typography';
import { SxProps } from '@mui/material';

interface ITableTypographyProps {
  label: string | number;
  sx?: SxProps;
}

const TableTypography = (props: ITableTypographyProps) => {
  const { label, sx } = props;

  return (
    <Typography color="textSecondary" variant="body2" sx={sx}>
      {label}
    </Typography>
  );
};

export default TableTypography;
