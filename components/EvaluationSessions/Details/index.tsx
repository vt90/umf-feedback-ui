import React, { useCallback, useMemo, useState } from 'react';
import Box from '@mui/material/Box';
import { IEvaluationSession } from '../../../models/evaluationSession';
import { IExtendedUserDepartmentAssignment } from '../../../models/userDepartmentAssignment';
import SurveysList from '../../Surveys/List';
import Alert from '@mui/material/Alert';
import moment from 'moment';
import Card from '@mui/material/Card';
import EvaluationSessionUserUserList from './UsersList';
import { IEvaluation } from '../../../models/evaluation';
import Tabs from '@mui/material/Tabs';
import Tab from '@mui/material/Tab';
import Typography from '@mui/material/Typography';
import Button from '@mui/material/Button';
import ButtonBase from '@mui/material/ButtonBase';
import Grid from '@mui/material/Grid';
import { groupBy } from 'lodash';
import { getSurveyRoleName, SURVEY_ROLE } from '../../../models/survey';
import EvaluationSessionWaitingUserList from './WaitingList';
import EvaluationSessionCompletedUserList from './CompletedEvaluationList';
import { IFunctionModel } from '../../../models/function';

const EVALUATION_VIEWS = {
  DASHBOARD: 'Sumar Evaluări',
  SESSION_DETAILS: 'Informații Sesiune',
};

interface IEvaluationSessionDetailsProps {
  currentUserId?: string;
  evaluationSession: IEvaluationSession;
  functions: IFunctionModel[];
  isUsersLoading: boolean;
  users?: IExtendedUserDepartmentAssignment[];

  // evaluations that need evaluating
  evaluatorEvaluations?: IEvaluation[];

  // evaluations that need contra-signing
  evaluatorContraEvaluateEvaluations?: IEvaluation[];
}

const EvaluationSessionDetails = (props: IEvaluationSessionDetailsProps) => {
  const {
    currentUserId,
    evaluationSession,
    evaluatorEvaluations,
    evaluatorContraEvaluateEvaluations,
    functions,
    users,
  } = props;
  const [activeView, setActiveView] = useState(EVALUATION_VIEWS.DASHBOARD);

  const handleChange = (event: React.SyntheticEvent, newValue: string) => {
    setActiveView(newValue);
  };

  const evaluationSessionPrivateUsers = useMemo(() => {
    return evaluationSession.privateUsers?.reduce((acc, cur) => {
      // @ts-ignore
      acc[cur] = true;

      return acc;
    }, {});
  }, [evaluationSession]);

  const filterAvailableUsersInSession = useCallback(
    (users: IExtendedUserDepartmentAssignment[]) => {
      if (!evaluationSession.privateUsersOnly) {
        return users;
      } else {
        return users.filter((user) => {
          if (evaluationSession.privateUsersOperation === 'EXCLUDE') {
            // @ts-ignore
            return !evaluationSessionPrivateUsers[user?.userId?._id];
          }
          // @ts-ignore
          return !!evaluationSessionPrivateUsers[user?.userId?._id];
        });
      }
    },
    [evaluationSession.privateUsersOnly, evaluationSessionPrivateUsers],
  );

  const evaluationSurveysGroup = useMemo(() => {
    return evaluationSession?.surveys?.reduce((acc, survey) => {
      // @ts-ignore
      if (!acc[survey.role]) acc[survey.role] = [];

      // @ts-ignore
      acc[survey.role].push(survey);

      return acc;
    }, {});
  }, [evaluationSession]);

  const renderStat = useCallback((name: string, value: any, image: string, id: string) => {
    return (
      <ButtonBase
        onClick={() => document.getElementById(id)?.scrollIntoView({ behavior: 'smooth' })}
        sx={{
          width: '100%',
          textAlign: 'left',
          mr: 2,
          mb: 4,
        }}
      >
        <Box
          sx={{
            px: 3,
            pt: 3,
            pb: 2,
            width: '100%',
            border: '0.0625rem solid rgb(222, 226, 230)',
            borderRadius: 2,
          }}
        >
          <Box sx={{ display: 'flex' }}>
            <Box sx={{ flexGrow: 1 }}>
              <Typography variant="h3">
                <strong>{value}</strong>
              </Typography>
            </Box>
            <Box>
              <img
                src={`/icons/eval-${image}.png`}
                alt="Statistic"
                style={{
                  height: 40,
                  width: 'auto',
                  opacity: 0.3,
                }}
              />
            </Box>
          </Box>

          <Box>
            <Typography
              variant="caption"
              color="textSecondary"
              sx={{
                display: 'block',
                fontWeight: 'light',
                minHeight: 38,
              }}
            >
              {name}
            </Typography>
          </Box>
        </Box>
      </ButtonBase>
    );
  }, []);

  const renderStatsHeader = useCallback(
    (statItems: { name: string; value: number; icon: string; group: string; id: string }[]) => {
      if (!statItems?.length) return null;

      const groupedItems = groupBy(
        statItems.filter((item) => !!item.value),
        'group',
      );
      return (
        <Grid
          container
          spacing={3}
          alignItems="flex-end"
          columns={{
            xs: 12,
            lg: 14,
            xl: 16,
          }}
          // flexDirection={{ xs: 'column', md: 'row' }}
        >
          {Object.keys(groupedItems).map((key) => {
            const items = groupedItems[key];

            return items.map((item, index) => {
              return (
                <Grid key={key + item.name} item xs={6} md={3}>
                  {index ? null : (
                    <Typography color="textSecondary" gutterBottom>
                      {key}
                    </Typography>
                  )}

                  {renderStat(item.name, item.value, item.icon, item.id)}
                </Grid>
              );
            });
          })}
        </Grid>
      );
    },
    [renderStat],
  );

  const [evaluationsByEvaluatedUser, salaryChangesByEvaluatedUser] = useMemo(() => {
    const evaluationsByEvaluatedUser = {};
    const salaryChangesByEvaluatedUser = {};

    if (evaluatorEvaluations?.length) {
      for (const evaluation of evaluatorEvaluations) {
        if (evaluation.role === SURVEY_ROLE.EVALUATION) {
          // @ts-ignore
          evaluationsByEvaluatedUser[evaluation.userDepartmentAssignmentId?._id] = evaluation;
        }

        if (evaluation.role === SURVEY_ROLE.SALARY_CHANGE) {
          // @ts-ignore
          salaryChangesByEvaluatedUser[evaluation.userDepartmentAssignmentId?._id] = evaluation;
        }
      }
    }

    return [evaluationsByEvaluatedUser, salaryChangesByEvaluatedUser];
  }, [evaluatorEvaluations]);

  const {
    evaluationsNotContraSignedByEvaluatedUser,
    evaluationsNotContraSigned,
    // --------------
    salaryChangesNotContraSignedByEvaluatedUser,
    salaryChangesNotContraSigned,
    // --------------
    contraSignedEvaluations,
    contraSignedSalaryChanges,
  } = useMemo(() => {
    const contraSignedEvaluations: IExtendedUserDepartmentAssignment[] = [];
    const contraSignedSalaryChanges: IExtendedUserDepartmentAssignment[] = [];
    const evaluationsNotContraSigned: IExtendedUserDepartmentAssignment[] = [];
    const salaryChangesNotContraSigned: IExtendedUserDepartmentAssignment[] = [];

    const evaluationsNotContraSignedByEvaluatedUser = {};
    const salaryChangesNotContraSignedByEvaluatedUser = {};

    if (evaluatorContraEvaluateEvaluations?.length) {
      for (const evaluation of evaluatorContraEvaluateEvaluations) {
        if (evaluation.role === SURVEY_ROLE.EVALUATION) {
          if (evaluation.contraEvaluatorId === currentUserId) {
            // @ts-ignore
            contraSignedEvaluations.push({
              // @ts-ignore
              ...evaluation.userDepartmentAssignmentId,
              isContraEvaluation: true,
            });
          } else {
            // @ts-ignore
            evaluationsNotContraSigned.push(evaluation.userDepartmentAssignmentId);
          }

          // @ts-ignore
          evaluationsNotContraSignedByEvaluatedUser[evaluation.userDepartmentAssignmentId?._id] =
            evaluation;
        }
        if (evaluation.role === SURVEY_ROLE.SALARY_CHANGE) {
          if (evaluation.contraEvaluatorId === currentUserId) {
            // @ts-ignore
            contraSignedSalaryChanges.push(evaluation.userDepartmentAssignmentId);
          } else {
            // @ts-ignore
            salaryChangesNotContraSigned.push(evaluation.userDepartmentAssignmentId);
          }

          // @ts-ignore
          salaryChangesNotContraSignedByEvaluatedUser[evaluation.userDepartmentAssignmentId?._id] =
            evaluation;
        }
      }
    }

    return {
      evaluationsNotContraSignedByEvaluatedUser,
      evaluationsNotContraSigned,
      // --------------
      salaryChangesNotContraSignedByEvaluatedUser,
      salaryChangesNotContraSigned,
      // --------------
      contraSignedEvaluations,
      contraSignedSalaryChanges,
    };
  }, [evaluatorContraEvaluateEvaluations, currentUserId]);

  const { remainingUsers, evaluatedUsers, salaryChangedUsers } = useMemo(() => {
    const remainingUsers: IExtendedUserDepartmentAssignment[] = [];
    const evaluatedUsers: IExtendedUserDepartmentAssignment[] = [];
    const salaryChangedUsers: IExtendedUserDepartmentAssignment[] = [];

    if (users && evaluationsByEvaluatedUser && salaryChangesByEvaluatedUser) {
      users.forEach((user) => {
        // @ts-ignore
        if (evaluationsByEvaluatedUser[user._id]) {
          evaluatedUsers.push(user);
        } else {
          remainingUsers.push(user);
        }

        // @ts-ignore
        if (salaryChangesByEvaluatedUser[user._id]) {
          salaryChangedUsers.push(user);
        }
      });
    }

    return {
      remainingUsers: filterAvailableUsersInSession(remainingUsers),
      evaluatedUsers,
      salaryChangedUsers,
    };
  }, [users, evaluationsByEvaluatedUser, salaryChangesByEvaluatedUser]);

  const completedEvaluatedUsers = [
    ...(evaluatedUsers && evaluatedUsers?.length ? evaluatedUsers : []),
    ...(contraSignedEvaluations && contraSignedEvaluations?.length ? contraSignedEvaluations : []),
  ];

  const completedSalaryChangeUsers = [
    ...(salaryChangedUsers && salaryChangedUsers?.length ? salaryChangedUsers : []),
    ...(contraSignedSalaryChanges && contraSignedSalaryChanges?.length
      ? contraSignedSalaryChanges
      : []),
  ];

  const completedEvaluations = {
    ...evaluationsByEvaluatedUser,
    ...evaluationsNotContraSignedByEvaluatedUser,
  };

  return (
    <Card sx={{ p: 3 }}>
      <Box sx={{ mb: 2 }}>
        <Tabs value={activeView} onChange={handleChange}>
          {Object.values(EVALUATION_VIEWS).map((tab: string) => {
            return <Tab key={tab} label={tab} value={tab} />;
          })}
        </Tabs>
      </Box>

      {evaluationSession.endDate && (
        <Card elevation={2} sx={{ mb: 3 }}>
          <Alert severity="warning">
            Termen limită de finalizare a evaluărilor:{' '}
            <strong>{moment(evaluationSession.endDate).format('YYYY.MM.DD')}</strong>
          </Alert>
        </Card>
      )}

      {activeView === EVALUATION_VIEWS.DASHBOARD && (
        <Box>
          {renderStatsHeader([
            {
              id: 'pending-evaluations-list',
              name: 'Evaluări în așteptare',
              value: remainingUsers?.length,
              icon: 'pending',
              group: 'Activități în așteptare',
            },
            {
              id: 'contrasign-evaluations-list',
              name: 'În așteptarea contra-semnării',
              value: evaluationsNotContraSigned?.length,
              icon: 'contrasign',
              group: 'Activități în așteptare',
            },
            {
              id: 'contrasign-salary-change-lis',
              name: 'În așteptarea contra-semnării',
              value: salaryChangesNotContraSigned?.length,
              icon: 'contrasign',
              group: 'Activități în așteptare',
            },
            {
              id: 'completed-evaluations-list',
              name: 'Evaluări completate',
              value: completedEvaluatedUsers?.length,
              icon: 'completed',
              group: 'Activități finalizate',
            },
            {
              id: 'completed-salary-change-list',
              name: 'Modificări salariale completate',
              value: completedSalaryChangeUsers?.length,
              icon: 'completed',
              group: 'Activități finalizate',
            },
            // {
            //   id: 'completed-evaluations-list',
            //   name: 'Evaluări completate',
            //   value: evaluatedUsers?.length,
            //   icon: 'completed',
            //   group: 'Activități finalizate',
            // },
            // {
            //   id: 'done-evaluations-list',
            //   name: 'Evaluări finalizate',
            //   value: contraSignedEvaluations?.length,
            //   icon: 'done',
            //   group: 'Activități finalizate',
            // },
          ])}

          {remainingUsers && remainingUsers?.length ? (
            <Box sx={{ mb: 5 }} id="pending-evaluations-list">
              <EvaluationSessionWaitingUserList
                users={remainingUsers}
                salaryChangesByEvaluatedUser={salaryChangesByEvaluatedUser}
                evaluationSession={evaluationSession}
                title="Evaluări în așteptare"
              />
            </Box>
          ) : null}

          {evaluationsNotContraSigned && evaluationsNotContraSigned?.length ? (
            <Box sx={{ mb: 5 }} id="contrasign-evaluations-list">
              <EvaluationSessionUserUserList
                users={evaluationsNotContraSigned}
                evaluations={evaluationsNotContraSignedByEvaluatedUser}
                baseUrl={`/evaluation-sessions/${evaluationSession._id}/result`}
                title="Evaluări în așteptarea contra-semnării"
                initialOrderBy="userAgreement"
                initialOrder="DESC"
                action={
                  <>
                    <Button variant="contained" size="small" sx={{ minWidth: '140px' }}>
                      Contra-semnează
                    </Button>
                  </>
                }
              />
            </Box>
          ) : null}

          {salaryChangesNotContraSigned && salaryChangesNotContraSigned?.length ? (
            <Box sx={{ mb: 5 }} id="contrasign-salary-change-list">
              <EvaluationSessionUserUserList
                users={salaryChangesNotContraSigned}
                evaluations={salaryChangesNotContraSignedByEvaluatedUser}
                baseUrl={`/evaluation-sessions/${evaluationSession._id}/result`}
                title="Modificări salariale în așteptarea contra-semnării"
                initialOrderBy="userAgreement"
                initialOrder="DESC"
                action={
                  <>
                    <Button variant="contained" size="small" sx={{ minWidth: '140px' }}>
                      Contra-semnează
                    </Button>
                  </>
                }
              />
            </Box>
          ) : null}

          {completedEvaluatedUsers?.length ? (
            <Box sx={{ mb: 5 }} id="completed-evaluations-list">
              <EvaluationSessionCompletedUserList
                users={completedEvaluatedUsers}
                evaluations={completedEvaluations}
                salaryChangesByEvaluatedUser={salaryChangesByEvaluatedUser}
                evaluationSession={evaluationSession}
                baseUrl={`/evaluation-sessions/${evaluationSession._id}/result`}
                title="Evaluări completate"
                initialOrderBy="contraEvaluatorAgreement"
              />
            </Box>
          ) : null}

          {completedSalaryChangeUsers?.length ? (
            <Box sx={{ mb: 5 }} id="completed-salary-change-list">
              <EvaluationSessionUserUserList
                users={completedSalaryChangeUsers}
                evaluations={salaryChangesByEvaluatedUser}
                baseUrl={`/evaluation-sessions/${evaluationSession._id}/result`}
                title="Modificări salarile completate"
                initialOrderBy="contraEvaluatorAgreement"
              />
            </Box>
          ) : null}

          {/*{evaluatedUsers && evaluatedUsers?.length ? (*/}
          {/*  <Box sx={{ mb: 5 }} id="completed-evaluations-list">*/}
          {/*    <EvaluationSessionUserUserList*/}
          {/*      users={evaluatedUsers}*/}
          {/*      evaluations={evaluationsByEvaluatedUser}*/}
          {/*      baseUrl={`/evaluation-sessions/${evaluationSession._id}/result`}*/}
          {/*      title="Evaluări completate"*/}
          {/*      initialOrderBy="contraEvaluatorAgreement"*/}
          {/*    />*/}
          {/*  </Box>*/}
          {/*) : null}*/}

          {/*{contraSignedEvaluations && contraSignedEvaluations?.length ? (*/}
          {/*  <Box sx={{ mb: 5 }} id="done-evaluations-list">*/}
          {/*    <EvaluationSessionUserUserList*/}
          {/*      users={contraSignedEvaluations}*/}
          {/*      evaluations={evaluationsNotContraSignedByEvaluatedUser}*/}
          {/*      baseUrl={`/evaluation-sessions/${evaluationSession._id}/result`}*/}
          {/*      title="Evaluări finalizate"*/}
          {/*      initialOrderBy="contraEvaluatorAgreement"*/}
          {/*    />*/}
          {/*  </Box>*/}
          {/*) : null}*/}
        </Box>
      )}

      {activeView === EVALUATION_VIEWS.SESSION_DETAILS && (
        <Box>
          {/*<RichText*/}
          {/*  defaultValue={evaluationSession.description}*/}
          {/*  readOnly*/}
          {/*  sx={{*/}
          {/*    border: `none !important`,*/}
          {/*    bgcolor: 'transparent',*/}
          {/*    px: 0,*/}
          {/*    boxShadow: 0,*/}
          {/*    '& .ql-container': {*/}
          {/*      border: 'none !important',*/}
          {/*      minHeight: '300px',*/}
          {/*      py: 2,*/}
          {/*    },*/}
          {/*  }}*/}
          {/*/>*/}

          <Box sx={{ mt: 3 }}>
            {evaluationSurveysGroup &&
              Object.keys(evaluationSurveysGroup).map((key) => {
                // @ts-ignore
                const surveys = evaluationSurveysGroup[key];

                return (
                  <Box sx={{ mb: 4 }} key={key}>
                    <Typography variant="subtitle1" sx={{ mb: 2 }}>
                      {getSurveyRoleName(key)}
                    </Typography>
                    <SurveysList baseUrl="/surveys" surveys={surveys} PaperComponent={Card} />
                  </Box>
                );
              })}
          </Box>
        </Box>
      )}
    </Card>
  );
};

export default EvaluationSessionDetails;
