import React, { useMemo } from 'react';
import Head from 'next/head';
import { useRouter } from 'next/router';
import { withPageAuthRequired } from '../../../guards/authRequired';
import { useQuery, useMutation, useQueryClient } from '@tanstack/react-query';
import Box from '@mui/material/Box';
import Card from '@mui/material/Card';
import Grid from '@mui/material/Grid';
import AdminPage from '../../../components/AdminPage';
import ListLoadingIndicator from '../../../components/Common/ListLoadingIndcator';
import TextField from '@mui/material/TextField';
import { useDebouncedCallback } from 'use-debounce';
import RenderSurveyToggle from '../../../components/Surveys/Toggle';
import { USER_DEP_ASSIGNMENT_TYPE } from '../../../models/userDepartmentAssignment';
import Typography from '@mui/material/Typography';
import { ISurveyBonification, SURVEY_FORMULA_TYPE } from '../../../models/survey';
import { createOrUpdateSurvey, getSurveyById } from '../../../services/surveys';
import { createOrUpdateSurveyChapter } from '../../../services/surveyChapters';
import { createOrUpdateSurveyObjective } from '../../../services/surveyObjectives';
import { CircularProgress } from '@mui/material';
import SurveyChaptersList from '../../../components/Surveys/SurveyChaptersList';
import SurveyObjectivesList from '../../../components/Surveys/SurveyObjectivesList';
import SurveyBonificationsList from '../../../components/Surveys/SurveyBonificationsList';
import { useNotificationContext } from '../../../context/notificationContext';

const Survey = () => {
  const router = useRouter();
  const { setError, setMessage } = useNotificationContext();
  const { id } = router.query;

  const queryClient = useQueryClient();

  const FETCH_QUERY_KEY = useMemo(() => ['surveys', id], [id]);

  const { isLoading: isSurveyLoading, data: survey } = useQuery(
    FETCH_QUERY_KEY,
    () => getSurveyById(id as string),
    {
      onError: () => router.push('/404'),
    },
  );

  const { isLoading: isSurveyUpdating, mutate: updateSurvey } = useMutation(createOrUpdateSurvey, {
    onSuccess: async () => {
      setMessage('Formular modificat cu success');
      await queryClient.invalidateQueries(FETCH_QUERY_KEY);
    },
    onError: setError,
  });

  const { isLoading: isChaptersUpdating, mutate: updateSurveyChapters } = useMutation(
    createOrUpdateSurveyChapter,
    {
      onSuccess: async () => {
        await queryClient.invalidateQueries(FETCH_QUERY_KEY);
        setMessage('Criteriu salvat');
      },
      onError: setError,
    },
  );

  const { isLoading: isObjectivesUpdating, mutate: updateSurveyObjectives } = useMutation(
    createOrUpdateSurveyObjective,
    {
      onSuccess: async () => {
        await queryClient.invalidateQueries(FETCH_QUERY_KEY);
        setMessage('Obiectiv salvat');
      },
      onError: setError,
    },
  );

  const onRemoveSurveyChapter = async () => await queryClient.invalidateQueries(FETCH_QUERY_KEY);

  const onSubmitSurveyBonification = (bonification: ISurveyBonification) => {
    const surveyBonifications = [
      ...(survey?.surveyBonifications ? survey.surveyBonifications : []),
    ];
    const isExisting = bonification?._id;

    if (isExisting) {
      const bonificationIndex = surveyBonifications.findIndex((b) => b._id === bonification._id);
      surveyBonifications[bonificationIndex] = bonification;
    } else {
      surveyBonifications.push(bonification);
    }

    updateSurvey({
      _id: survey?._id,
      surveyBonifications,
    });
  };

  const onRemoveSurveyBonification = (bonification: ISurveyBonification) => {
    const surveyBonifications = [
      ...(survey?.surveyBonifications ? survey.surveyBonifications : []),
    ];

    const bonificationIndex = surveyBonifications.findIndex((b) => b._id === bonification._id);

    surveyBonifications.splice(bonificationIndex, 1);

    updateSurvey({
      _id: survey?._id,
      surveyBonifications,
    });
  };

  const debouncedChange = useDebouncedCallback(
    (name, value) =>
      updateSurvey({
        ...survey,
        [name]: value,
      }),
    1000,
  );

  if (isSurveyLoading) return <ListLoadingIndicator />;

  // ToDo implement not found
  if (!survey) return 'Survey unavailable';

  const readOnly = survey.isClone;

  return (
    <>
      <Head>
        <title>{survey.name}</title>
      </Head>

      <AdminPage
        title={
          <TextField
            defaultValue={survey.name}
            onChange={(ev) => debouncedChange('name', ev.target.value)}
            multiline
            fullWidth
            disabled={readOnly}
            sx={{
              '& legend': { display: 'none' },
              '& fieldset': {
                top: 0,
                borderColor: 'transparent !important',
              },
              '& .MuiInputBase-root': { py: 0.5, pl: 0.25 },
            }}
            inputProps={{
              sx: {
                typography: 'h5',
                fontWeight: 'bold',
                color: `#FFFFFF !important`,
              },
            }}
          />
        }
        pages={[
          { url: '/surveys', name: 'Formulare de evaluare' },
          { url: `/surveys/${id}`, name: `${survey.name}` },
        ]}
      >
        <Card sx={{ p: 3 }}>
          <Grid container spacing={3}>
            <Grid item xs={6} md={12}>
              <Box sx={{ display: { md: 'flex' }, alignItems: 'center' }}>
                <Box sx={{ mr: 2, minWidth: 150 }}>
                  <Typography variant="body2">Tip de utilizator</Typography>
                </Box>

                <Box>
                  <RenderSurveyToggle
                    options={Object.values(USER_DEP_ASSIGNMENT_TYPE).map((option) => ({
                      name: option,
                      value: option,
                    }))}
                    value={survey.type}
                    onChange={(newValue) => debouncedChange('type', newValue)}
                    size="small"
                    disabled={isSurveyUpdating || readOnly}
                  />
                </Box>
              </Box>
            </Grid>

            <Grid item xs={6} md={12}>
              <Box sx={{ display: { md: 'flex' }, alignItems: 'center' }}>
                <Box sx={{ mr: 2, minWidth: 150 }}>
                  <Typography variant="body2">Tip de calcul</Typography>
                </Box>

                <Box>
                  <RenderSurveyToggle
                    options={Object.keys(SURVEY_FORMULA_TYPE).map((option) => ({
                      name: option,
                      // @ts-ignore
                      value: SURVEY_FORMULA_TYPE[option],
                    }))}
                    value={survey.calculationFormulaType}
                    onChange={(newValue) => debouncedChange('calculationFormulaType', newValue)}
                    size="small"
                    disabled={isSurveyUpdating || readOnly}
                  />
                </Box>
              </Box>
            </Grid>

            <Grid item xs={12}>
              <Box sx={{ mb: 2, display: 'flex', alignItems: 'center' }}>
                <Typography variant="h6">Obiective</Typography>

                {isObjectivesUpdating && <CircularProgress size={16} sx={{ mb: 0.5, ml: 1 }} />}
              </Box>

              <SurveyObjectivesList
                survey={survey}
                isLoading={isObjectivesUpdating}
                onSubmit={updateSurveyObjectives}
              />

              <Box sx={{ mt: 4, mb: 2, display: 'flex', alignItems: 'center' }}>
                <Typography variant="h6">Criterii de evaluare</Typography>

                {isChaptersUpdating && <CircularProgress size={16} sx={{ mb: 0.5, ml: 1 }} />}
              </Box>

              <SurveyChaptersList
                survey={survey}
                isLoading={isChaptersUpdating}
                onRemoveSurveyChapter={onRemoveSurveyChapter}
                onSubmit={updateSurveyChapters}
              />

              <Box sx={{ mt: 4, mb: 2, display: 'flex', alignItems: 'center' }}>
                <Typography variant="h6">Bonificații (opțional)</Typography>

                {isChaptersUpdating && <CircularProgress size={16} sx={{ mb: 0.5, ml: 1 }} />}
              </Box>

              <SurveyBonificationsList
                survey={survey}
                isLoading={isSurveyUpdating}
                onSubmit={onSubmitSurveyBonification}
                onRemoveSurveyBonification={onRemoveSurveyBonification}
              />
            </Grid>
          </Grid>
        </Card>
      </AdminPage>
    </>
  );
};

export const getServerSideProps = withPageAuthRequired();

export default Survey;
