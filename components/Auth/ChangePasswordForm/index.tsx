import React from 'react';
import Button from '@mui/lab/LoadingButton';
import Box from '@mui/material/Box';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogTitle from '@mui/material/DialogTitle';
import LinearProgress from '@mui/material/LinearProgress';
import TextField from '@mui/material/TextField';
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import * as yup from 'yup';

interface IForgotPasswordFormProps {
  initialValues: { userId: string };
  skipPasswordCheck: boolean;
  isLoading: boolean;
  onClose: () => void;
  onSubmit: () => void;
}

const schema = (props: IForgotPasswordFormProps) =>
  yup
    .object({
      newPassword: yup.string().required(),
      newPasswordConfirmation: yup
        .string()
        .oneOf([yup.ref('newPassword'), null], 'Passwords must match'),
      ...(!props.skipPasswordCheck && {
        oldPassword: yup.string().required(),
      }),
    })
    .required();

const ForgotPasswordForm = (props: IForgotPasswordFormProps) => {
  const { initialValues, isLoading, onClose, onSubmit, skipPasswordCheck } = props;
  const {
    handleSubmit,
    register,
    formState: { errors },
  } = useForm({
    defaultValues: {
      userId: initialValues.userId,
      oldPassword: '',
      newPassword: '',
      newPasswordConfirmation: '',
    },
    mode: 'onTouched',
    resolver: yupResolver(schema(props)),
    shouldUnregister: true,
  });

  return (
    <Dialog
      open={true}
      keepMounted={false}
      onClose={onClose}
      // @ts-ignore
      PaperComponent="form"
      maxWidth="sm"
      fullWidth
      onSubmit={handleSubmit(onSubmit)}
    >
      <DialogTitle sx={{ typography: 'h5', textAlign: 'center' }}>Modifică parola</DialogTitle>
      <DialogContent sx={{ my: 3 }}>
        {/* @ts-ignore */}
        <input type="hidden" {...register(`userId`)} />
        <Box sx={{ mt: 2, mb: 2 }}>
          {skipPasswordCheck ? null : (
            <TextField
              fullWidth
              error={!!errors.oldPassword}
              label="Parola veche"
              required={true}
              helperText={!!errors.oldPassword && 'Parola invalida'}
              defaultValue=""
              type="password"
              {...register('oldPassword')}
            />
          )}
        </Box>

        <Box sx={{ mb: 2 }}>
          <TextField
            fullWidth
            error={!!errors.newPassword}
            label="Parolă nouă"
            required={true}
            helperText={!!errors.newPassword && 'Parola invalida'}
            defaultValue=""
            type="password"
            {...register('newPassword')}
          />
        </Box>

        <Box sx={{ mb: 2 }}>
          <TextField
            fullWidth
            error={!!errors.newPasswordConfirmation}
            label="Confirmare parolă nouă"
            required={true}
            helperText={!!errors.newPasswordConfirmation && 'Parola invalida'}
            defaultValue=""
            type="password"
            {...register('newPasswordConfirmation')}
          />
        </Box>
      </DialogContent>

      {isLoading && <LinearProgress />}
      <DialogActions sx={{ py: 2, justifyContent: 'center', bgcolor: 'background.default' }}>
        <Button color="inherit" onClick={onClose} disabled={isLoading}>
          Anulare
        </Button>

        <Button variant="contained" type="submit" loading={isLoading}>
          Modifică parola
        </Button>
      </DialogActions>
    </Dialog>
  );
};

export default ForgotPasswordForm;
