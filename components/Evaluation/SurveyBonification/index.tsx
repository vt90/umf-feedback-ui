import React from 'react';
import { ISurvey } from '../../../models/survey';
import { IEvaluationResult } from '../../../models/evaluation';
import Box from '@mui/material/Box';
import Divider from '@mui/material/Divider';
import Grid from '@mui/material/Grid';
import ToggleButtonGroup from '@mui/material/ToggleButtonGroup';
import ToggleButton from '@mui/material/ToggleButton';
import Typography from '@mui/material/Typography';
import sum from 'lodash/sum';

interface IEvaluationSurveyBonificationsProps {
  bonifications: IEvaluationResult;
  onChangeBonifications?: (questionId: string, value: number) => void;
  survey: ISurvey;
  showSum?: boolean;
}

const EvaluationSurveyBonifications = (props: IEvaluationSurveyBonificationsProps) => {
  const { bonifications, onChangeBonifications, survey, showSum } = props;

  const gridContainerProps = {
    container: true,
    spacing: 2,
    alignItems: 'flex-start',
  };

  if (!survey?.surveyBonifications?.length) return null;

  return (
    <Box
      sx={{
        p: 3,
        py: 4,
        mb: 5,
        border: '0.0625rem solid rgb(222, 226, 230)',
        borderRadius: 2,
      }}
    >
      <Grid {...gridContainerProps}>
        <Grid item xs={12} md={9} lg={10}>
          <Box sx={{ display: 'flex', mb: 3 }}>
            <Box sx={{ minWidth: 32, mr: 2 }}>
              <Typography sx={{ fontWeight: 900 }}>{survey?.surveyChapters?.length + 1}</Typography>
            </Box>

            <Box>
              <Box>
                <Typography sx={{ fontWeight: 900 }}>Bonificaţii</Typography>
              </Box>
            </Box>
          </Box>
        </Grid>
      </Grid>

      {survey?.surveyBonifications?.map((bonification, bonificationIndex) => {
        const bonificationValue = bonifications[bonification._id];

        return (
          <Grid key={bonification._id} {...gridContainerProps} sx={{ mt: 0.5 }}>
            <Grid item xs={12} md={9} lg={10}>
              <Box sx={{ display: 'flex' }}>
                <Box sx={{ minWidth: 32, mr: 2 }}>
                  <Typography>
                    {survey?.surveyChapters?.length + 1}.{bonificationIndex + 1}
                  </Typography>
                </Box>

                <Box>
                  <Box>
                    <Typography>{bonification.name}</Typography>
                  </Box>
                </Box>
              </Box>
            </Grid>

            <Grid item xs={12} md={3} lg={2}>
              <Box sx={{ display: 'flex', justifyContent: 'flex-end' }}>
                <ToggleButtonGroup
                  value={bonificationValue || 0}
                  disabled={!onChangeBonifications}
                  color="primary"
                  exclusive
                  onChange={(_, value: number) => {
                    onChangeBonifications && onChangeBonifications(bonification._id, value);
                  }}
                >
                  {Array(2)
                    .fill(1)
                    .map((_, index) => {
                      return (
                        <ToggleButton
                          key={index}
                          value={index === 0 ? bonification.value : 0}
                          sx={{
                            px: 1.5,
                            py: 0.75,
                            '&:hover': {
                              color: 'text.primary',
                            },
                            '&.Mui-selected': {
                              color: '#FFFFFF',
                              bgcolor: 'primary.dark',
                              '&:hover': {
                                color: '#FFFFFF',
                                bgcolor: 'primary.dark',
                                opacity: 0.7,
                              },
                            },
                          }}
                        >
                          {index ? 'Nu' : 'Da'}
                        </ToggleButton>
                      );
                    })}
                </ToggleButtonGroup>
              </Box>
            </Grid>

            {/* @ts-ignore */}
            {bonificationIndex < survey.surveyBonifications.length - 1 && (
              <Grid item xs={12}>
                <Divider />
              </Grid>
            )}
          </Grid>
        );
      })}

      {bonifications && showSum && (
        <Grid
          {...gridContainerProps}
          sx={{
            mt: 2,
          }}
        >
          <Grid item xs={12}>
            <Box
              sx={{
                borderTop: 1,
                borderColor: 'text.primary',
              }}
            />
          </Grid>
          <Grid item xs={12} md={9} lg={10}>
            <Typography>Suma bonifcatii</Typography>
          </Grid>
          <Grid item xs={12} md={3} lg={2}>
            <Box
              sx={{
                bgcolor: 'background.default',
                px: 2,
                py: 1,
              }}
            >
              <Typography sx={{ textAlign: 'right', fontWeight: 900 }}>
                {sum(Object.values(bonifications)).toFixed(2)}
              </Typography>
            </Box>
          </Grid>
        </Grid>
      )}
    </Box>
  );
};

export default EvaluationSurveyBonifications;
