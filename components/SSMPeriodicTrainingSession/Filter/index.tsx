import React from 'react';
import Box from '@mui/material/Box';
import Grid from '@mui/material/Grid';
import MenuItem from '@mui/material/MenuItem';
import Select from '@mui/material/Select';
import Typography from '@mui/material/Typography';
import { IDepartment } from '../../../models/department';
import { TRAINING_MONTHS_EXTENDED } from '../../../models/ssmPeriodicTraining';
import moment from 'moment';
// import { USER_DEP_ASSIGNMENT_TYPE } from '../../../models/userDepartmentAssignment';

interface ISSMTrainingSessionFilterProps {
  order: number;
  setOrder: (newOrder: number) => void;
  orderBy: string;
  setOrderBy: (newOrderBy: string) => void;
  department?: string | null;
  departments?: IDepartment[] | undefined;
  setDepartment?: (newOrderBy: string | null) => void;
  contraEvaluated: string | null;
  setContraEvaluated: (newRole: string | null) => void;
  userAcknowledged: string | null;
  setUserAcknowledged: (newRole: string | null) => void;
  sessionData: Date;
  setSessionData: (sessionData: Date) => void;
}

interface SelectOption {
  name: string;
  value: string;
}

interface SelectProps {
  label: string;
  value: string | null;
  disabled?: boolean;
  onChange: (value: string | null) => void;
  options?: SelectOption[];
  hideAll?: boolean;
}
export const RenderSelectField = (props: SelectProps) => {
  const { hideAll, label, value, onChange, options, disabled } = props;

  return (
    <>
      <Typography variant="body2" color="textSecondary">
        {label}
      </Typography>
      <Select
        value={value}
        onChange={(ev) => onChange(ev.target.value)}
        size="small"
        displayEmpty
        disabled={!!disabled}
        sx={{
          boxShadow: 'none',
          maxWidth: '100%',
          '.MuiOutlinedInput-notchedOutline': {
            border: 0,
            outline: 'none',
          },
        }}
      >
        {hideAll ? null : (
          <MenuItem value="">
            <i>Toate</i>
          </MenuItem>
        )}

        {options?.map((option) => (
          <MenuItem key={option.value} value={option.value}>
            {option.name}
          </MenuItem>
        ))}
      </Select>
    </>
  );
};

const SSMTrainingSessionFilter = (props: ISSMTrainingSessionFilterProps) => {
  const {
    departments,
    department,
    setDepartment,
    contraEvaluated,
    setContraEvaluated,
    userAcknowledged,
    setUserAcknowledged,
    sessionData,
    setSessionData,
  } = props;

  const yearOptions = [];
  const currentYear = moment().year();

  for (let year = 2023; year <= currentYear; year++) {
    yearOptions.push(`${year}`);
  }

  return (
    <>
      <Box
        sx={{
          display: 'flex',
          alignItems: 'center',
          flexWrap: 'wrap',
          mb: 2,
          borderColor: 'divider',
          borderRightStyle: 'solid',
          borderRightWidth: 1,
        }}
      >
        <Grid container spacing={2}>
          <Grid item xs={12}>
            <Typography variant="body1">
              <strong>Filtrează</strong>
            </Typography>
          </Grid>
          <Grid item xs={12}>
            <RenderSelectField
              label="Luna instruirii"
              value={`${moment(sessionData).month()}`}
              onChange={(value) => {
                if (value) {
                  const newDate = moment(sessionData);
                  newDate.set('month', parseInt(value));
                  setSessionData(newDate.toDate());
                }
              }}
              options={TRAINING_MONTHS_EXTENDED.map((month, index) => ({
                name: month,
                value: `${index}`,
              }))}
            />
          </Grid>
          <Grid item xs={12}>
            <RenderSelectField
              label="Anul instruirii"
              value={`${moment(sessionData).year()}`}
              onChange={(value) => {
                if (value) {
                  const newDate = moment(sessionData);
                  newDate.set('year', parseInt(value));
                  setSessionData(newDate.toDate());
                }
              }}
              options={yearOptions.map((year) => ({
                name: year,
                value: year,
              }))}
            />
          </Grid>

          <Grid item xs={12}>
            <RenderSelectField
              label="Semnată de angajat"
              value={userAcknowledged}
              onChange={setUserAcknowledged}
              options={[
                { name: 'Da', value: 'true' },
                { name: 'Nu', value: 'false' },
              ]}
            />
          </Grid>

          <Grid item xs={12}>
            <RenderSelectField
              label="Contra-semntată"
              value={contraEvaluated}
              onChange={setContraEvaluated}
              options={[
                { name: 'Da', value: 'true' },
                { name: 'Nu', value: 'false' },
              ]}
            />
          </Grid>

          {departments && setDepartment && (
            <Grid item xs={12}>
              <RenderSelectField
                label="Departament"
                // @ts-ignore
                value={department}
                onChange={setDepartment}
                options={departments?.map((department) => ({
                  value: department._id,
                  name: department.name,
                }))}
              />
            </Grid>
          )}
        </Grid>
      </Box>
    </>
  );
};

export default SSMTrainingSessionFilter;
