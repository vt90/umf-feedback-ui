import * as React from 'react';
import Tabs from '@mui/material/Tabs';
import Tab from '@mui/material/Tab';
import Box from '@mui/material/Box';
import { useRouter } from 'next/router';
import { useCallback } from 'react';

const TABS = [
  { name: 'Departamente', path: '/departments' },
  { name: 'Utilizatori', path: '/users' },
];

const isUMF = process.env.NEXT_PUBLIC_IS_UMF === 'true';

if (isUMF) {
  TABS.push({ name: 'Funcții', path: '/functions' });
}

export default function OrganigramNavigation() {
  const router = useRouter();
  // @ts-ignore
  const pathInfo = router.pathname?.split('/');
  const path = `/${pathInfo[pathInfo.length - 1]}`;

  const handleViewChange = useCallback(
    (activeView: string) =>
      router.push({
        pathname: `/organigram${activeView}`,
      }),
    [router],
  );

  const handleChange = (event: React.SyntheticEvent, newValue: string) => {
    handleViewChange(newValue);
  };

  return (
    <Box sx={{ width: '100%', mb: 4 }}>
      <Tabs value={path} onChange={handleChange}>
        {TABS.map((tab) => {
          return <Tab key={tab.path} label={tab.name} value={tab.path} />;
        })}
      </Tabs>
    </Box>
  );
}
