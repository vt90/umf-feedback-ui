import React, { FormEvent } from 'react';
import { ISurvey } from '../../../models/survey';
import { IEvaluationResult } from '../../../models/evaluation';
import Button from '@mui/material/Button';
import EvaluationSurveyQuestions from '../SurveyQuestions';
import EvaluationSurveyObjectives from '../SurveyObjectives';
import EvaluationSurveyBonifications from '../SurveyBonification';

interface IEvaluationFormProps {
  evaluationResult: IEvaluationResult;
  objectiveResult: IEvaluationResult;
  onChangeEvaluationResult: (questionId: string, value: number) => void;
  onChangeEvaluationObjectiveResult: (objectiveId: string, value: number) => void;
  bonifications: IEvaluationResult;
  onChangeEvaluationBonifications: (bonificationId: string, value: number) => void;
  onSubmit: () => void;
  survey: ISurvey;
}

const EvaluationForm = (props: IEvaluationFormProps) => {
  const {
    bonifications,
    evaluationResult,
    objectiveResult,
    onChangeEvaluationBonifications,
    onChangeEvaluationResult,
    onChangeEvaluationObjectiveResult,
    onSubmit,
    survey,
  } = props;

  const handleSubmit = (ev: FormEvent) => {
    ev.preventDefault();
    ev.stopPropagation();
    onSubmit();
  };

  return (
    <form onSubmit={handleSubmit}>
      <EvaluationSurveyObjectives
        objectiveResult={objectiveResult}
        onChangeEvaluationObjectiveResult={onChangeEvaluationObjectiveResult}
        survey={survey}
      />

      <EvaluationSurveyQuestions
        evaluationResult={evaluationResult}
        onChangeEvaluationResult={onChangeEvaluationResult}
        survey={survey}
      />

      <EvaluationSurveyBonifications
        bonifications={bonifications}
        onChangeBonifications={onChangeEvaluationBonifications}
        survey={survey}
        showSum={false}
      />

      <Button id="evaluationSubmit" type="submit" sx={{ display: 'none' }} />
    </form>
  );
};

export default EvaluationForm;
