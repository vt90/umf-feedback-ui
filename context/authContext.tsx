import React, { createContext, useContext, useEffect, useState } from 'react';
import { useSession } from 'next-auth/react';
import { IUser } from '../models/user';
import { getExtendedUser } from '../services/auth';

const AuthContext = createContext({
  user: undefined,
  accessToken: '',
});

interface IAuthWrapperProps {
  children: React.ReactNode;
}

export function AuthProvider(props: IAuthWrapperProps) {
  const [user, setUser] = useState<IUser | null>(null);
  const { children } = props;
  const { data: session } = useSession();
  // @ts-ignore
  const accessToken: string | undefined = session?.accessToken;

  const fetchUser = async () => {
    const user = await getExtendedUser(`${accessToken}`);
    // @ts-ignore
    setUser(user);
  };
  // @ts-ignore
  useEffect(() => {
    if (session?.user) {
      const userInfo = session.user;

      fetchUser();
      // @ts-ignore
      setUser(userInfo);
    }
    // @ts-ignore
  }, [session?.user]);

  return (
    // @ts-ignore
    <AuthContext.Provider value={{ accessToken, user }}>{children}</AuthContext.Provider>
  );
}

export function useAuthContext() {
  return useContext(AuthContext);
}
