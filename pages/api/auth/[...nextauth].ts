import NextAuth from 'next-auth';
import CredentialsProvider from 'next-auth/providers/credentials';
import { decode } from 'jsonwebtoken';
import { login } from '../../../services/auth';

export const authOptions = {
  pages: {
    signIn: '/sign-in',
  },
  callbacks: {
    // @ts-ignore
    jwt: async (data) => {
      const { token, user: extractedSessionInfo } = data;

      if (extractedSessionInfo) {
        token.user = extractedSessionInfo.userInfo;
        token.accessToken = extractedSessionInfo.accessToken;
      }

      return token;
    },
    // @ts-ignore
    session: async ({ session, token }) => {
      session.user = token.user;
      session.accessToken = token.accessToken;
      return session;
    },
  },
  providers: [
    CredentialsProvider({
      name: 'Credentials',
      credentials: {
        email: { label: 'Email', type: 'text' },
        password: { label: 'Password', type: 'password' },
      },
      // @ts-ignore
      authorize: async (credentials) => {
        try {
          // @ts-ignore
          const { email, password } = credentials;
          const accessToken = await login(email, password);
          const tokenInfo = decode(accessToken);

          return {
            // @ts-ignore
            userInfo: tokenInfo.sub,
            accessToken,
          };
        } catch (error) {
          console.error(error);
          return null;
        }
      },
    }),
  ],
};

// @ts-ignore
export default NextAuth(authOptions);
