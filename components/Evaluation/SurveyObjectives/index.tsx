import React from 'react';
import { ISurvey } from '../../../models/survey';
import { IEvaluationResult } from '../../../models/evaluation';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import Divider from '@mui/material/Divider';
import Grid from '@mui/material/Grid';
import ToggleButtonGroup from '@mui/material/ToggleButtonGroup';
import ToggleButton from '@mui/material/ToggleButton';
import Typography from '@mui/material/Typography';

interface IEvaluationSurveyObjectivesProps {
  showResults?: boolean;
  objectiveResult: IEvaluationResult;
  // surveyChapterResults?: IEvaluationResult;
  onChangeEvaluationObjectiveResult?: (objectiveId: string, value: number) => void;
  survey: ISurvey;
}

const EvaluationSurveyObjectives = (props: IEvaluationSurveyObjectivesProps) => {
  const { showResults, objectiveResult, survey, onChangeEvaluationObjectiveResult } = props;

  let results = 0;

  if (survey.surveyObjectives?.length) {
    const arr = Object.values(objectiveResult);
    results = arr.reduce((acc, current) => acc + current, 0);
    results = results / arr.length;
  }

  const gridContainerProps = {
    container: true,
    spacing: 2,
    alignItems: 'flex-start',
  };

  return (
    <>
      {survey.surveyObjectives?.map((objective) => {
        return (
          <Box
            key={objective._id}
            sx={{
              border: '0.0625rem solid rgb(222, 226, 230)',
              borderRadius: 2,
              p: 3,
              py: 4,
              ...(showResults
                ? {
                    mb: 5,
                  }
                : {
                    mb: 3,
                  }),
            }}
          >
            <Grid {...gridContainerProps}>
              <Grid item xs={12} md={9} lg={10}>
                <Box sx={{ display: 'flex', mb: 3 }}>
                  <Box sx={{ minWidth: 32, mr: 2 }}></Box>

                  <Box>
                    <Box>
                      <Typography sx={{ fontWeight: 900 }}>{objective.name}</Typography>
                    </Box>
                  </Box>
                </Box>
              </Grid>
            </Grid>

            {objective?.questions?.map((question, questionIndex) => {
              const questionValue = objectiveResult && objectiveResult[question._id];

              return (
                <Grid key={question._id} {...gridContainerProps} sx={{ mt: 0.5 }}>
                  <Grid item xs={12} md={9} lg={10}>
                    <Box sx={{ display: 'flex' }}>
                      <Box sx={{ minWidth: 32, mr: 2 }}>
                        <Typography>{questionIndex + 1}</Typography>
                      </Box>

                      <Box>
                        <Box>
                          <Typography>{question.name}</Typography>
                          <Typography color="textSecondary">
                            {question.timePercentage}% (% din timp)
                          </Typography>
                          <Typography color="textSecondary">
                            {question.donePercentage}% (% realizat - pondere)
                          </Typography>
                        </Box>
                      </Box>
                    </Box>
                  </Grid>
                  <Grid item xs={12} md={3} lg={2}>
                    <Box sx={{ display: 'flex', justifyContent: 'flex-end' }}>
                      <ToggleButtonGroup
                        value={questionValue}
                        disabled={!onChangeEvaluationObjectiveResult}
                        color="primary"
                        exclusive
                        onChange={(_, value: number) => {
                          value &&
                            onChangeEvaluationObjectiveResult &&
                            onChangeEvaluationObjectiveResult(question._id, value);
                        }}
                      >
                        {Array(5)
                          .fill(1)
                          .map((_, index) => {
                            return (
                              <ToggleButton
                                key={index}
                                value={index + 1}
                                sx={{
                                  px: 1.5,
                                  py: 0.75,
                                  '&:hover': {
                                    color: 'text.primary',
                                  },
                                  '&.Mui-selected': {
                                    color: '#FFFFFF',
                                    bgcolor: 'primary.dark',
                                    '&:hover': {
                                      color: '#FFFFFF',
                                      bgcolor: 'primary.dark',
                                      opacity: 0.7,
                                    },
                                  },
                                }}
                              >
                                {index + 1}
                              </ToggleButton>
                            );
                          })}
                      </ToggleButtonGroup>
                    </Box>
                  </Grid>

                  {/* @ts-ignore */}
                  {questionIndex < objective.questions.length - 1 && (
                    <Grid item xs={12}>
                      <Divider />
                    </Grid>
                  )}
                </Grid>
              );
            })}

            {showResults && (
              <Grid
                {...gridContainerProps}
                sx={{
                  mt: 2,
                }}
              >
                <Grid item xs={12}>
                  <Box
                    sx={{
                      borderTop: 1,
                      borderColor: 'text.primary',
                    }}
                  />
                </Grid>
                <Grid item xs={12} md={9} lg={10}>
                  <Typography>Nota obiective</Typography>
                </Grid>
                <Grid item xs={12} md={3} lg={2}>
                  <Box
                    sx={{
                      bgcolor: 'background.default',
                      px: 2,
                      py: 1,
                    }}
                  >
                    <Typography sx={{ textAlign: 'right', fontWeight: 900 }}>
                      {results.toFixed(2)}
                    </Typography>
                  </Box>
                </Grid>
              </Grid>
            )}
          </Box>
        );
      })}

      <Button id="evaluationSubmit" type="submit" sx={{ display: 'none' }} />
    </>
  );
};

export default EvaluationSurveyObjectives;
