import React, { useMemo, useState } from 'react';
import { useMutation, useQuery, useQueryClient } from '@tanstack/react-query';
import noop from 'lodash/noop';
import { getDepartmentById } from '../../../services/departments';
import {
  findUserDepartmentAssignments,
  createOrUpdateUserDepartmentAssignment,
  deleteUserDepartmentAssignment,
  changeDepartmentManagers,
} from '../../../services/userDepartmentAssignment';
import Dialog from '../../Common/Dialog';
import { CircularProgress } from '@mui/material';
import UsersList from '../../Users/List';
import { IDepartment } from '../../../models/department';
import { IFunctionModel } from '../../../models/function';
import { IUser } from '../../../models/user';
import { SELECTED_ACTIONS } from '../../../lib/constants';
import LoadingButton from '@mui/lab/LoadingButton';
import Typography from '@mui/material/Typography';
import UserForm from '../../Users/Form';
import DepartmentAddUsersForm from './AddUsersForm';
import { USER_DEP_ASSIGNMENT_ROLES } from '../../../models/userDepartmentAssignment';
import Box from '@mui/material/Box';
import TextField from '@mui/material/TextField';
import MenuItem from '@mui/material/MenuItem';
import orderBy from 'lodash/orderBy';

interface IDepartmentDetailsProps {
  departmentId: string;
  departments: IDepartment[];
  functions: IFunctionModel[];
  onClose: () => void;
}

const DepartmentDetails = (props: IDepartmentDetailsProps) => {
  const { departmentId, departments, functions, onClose } = props;
  const [selectedUser, setSelectedUser] = useState<Partial<IUser> | null>(null);
  const [selectedAction, setSelectedAction] = useState<string | null>(null);
  const FETCH_QUERY_KEY = ['department', { departmentId }];
  const FETCH_DEP_USERS_QUERY_KEY = ['department-users', { departmentId }];

  const onUserAction = (action: string) => (user: Partial<IUser>) => {
    setSelectedUser(user);
    setSelectedAction(action);
  };

  const resetSelectedUserState = () => {
    setSelectedUser(null);
    setSelectedAction(null);
  };

  const onRevalidateDepartmentUsers = async () => {
    await queryClient.invalidateQueries(FETCH_DEP_USERS_QUERY_KEY);
    resetSelectedUserState();
  };

  const queryClient = useQueryClient();

  const { data: department } = useQuery(FETCH_QUERY_KEY, () => getDepartmentById(departmentId), {
    enabled: !!departmentId,
  });

  const { isLoading: isLoadingDepartmentUsers, data: departmentUsers } = useQuery(
    FETCH_DEP_USERS_QUERY_KEY,
    () => findUserDepartmentAssignments({ departmentId }),
    { enabled: !!departmentId },
  );

  const { isLoading: isDeleting, mutate: removeUserAssignment } = useMutation(
    deleteUserDepartmentAssignment,
    {
      onSuccess: onRevalidateDepartmentUsers,
    },
  );

  const { isLoading: isUpdating, mutate: updateUserAssignment } = useMutation(
    createOrUpdateUserDepartmentAssignment,
    {
      onSuccess: onRevalidateDepartmentUsers,
    },
  );

  const { isLoading: isManagersChanging, mutate: changeManagers } = useMutation(
    changeDepartmentManagers,
    {
      onSuccess: onRevalidateDepartmentUsers,
    },
  );

  const departmentManager = useMemo(() => {
    return departmentUsers?.results?.find(
      (depAss) => depAss.role === USER_DEP_ASSIGNMENT_ROLES.EVALUATOR,
    );
  }, [departmentUsers]);

  const onManagerChange = (newManagerAssignmentId: string) => {
    const previousManagerAssignmentId = departmentManager?._id;

    if (newManagerAssignmentId) {
      changeManagers({
        previousManagerAssignmentId: previousManagerAssignmentId || null,
        newManagerAssignmentId,
      });
    }
  };

  return (
    <>
      <Dialog
        title={`${department?.code} - ${department?.name}`}
        onClose={onClose}
        maxWidth="lg"
        open={true}
        content={
          <>
            <DepartmentAddUsersForm
              departmentId={departmentId}
              onUsersAdded={onRevalidateDepartmentUsers}
            />

            {isLoadingDepartmentUsers && <CircularProgress />}

            {departmentUsers?.results?.length ? (
              <>
                {departmentUsers?.results?.length ? (
                  <Box sx={{ maxWidth: { xs: '100%', sm: '50%', md: '33%' }, mb: 4 }}>
                    <Typography sx={{ mt: 2 }} gutterBottom>
                      <strong>{USER_DEP_ASSIGNMENT_ROLES.EVALUATOR}</strong>
                    </Typography>

                    <TextField
                      disabled={isManagersChanging}
                      fullWidth
                      value={departmentManager?._id || null}
                      onChange={(ev) => onManagerChange(ev.target.value)}
                      select
                      SelectProps={{
                        displayEmpty: true,
                      }}
                      sx={{
                        '& legend': { display: 'none' },
                        '& fieldset': { top: 0 },
                      }}
                    >
                      {orderBy(departmentUsers?.results, 'userId.fullName')?.map((depAss) => {
                        return (
                          <MenuItem key={depAss._id} value={depAss._id}>
                            {/* @ts-ignore */}
                            {depAss?.userId?.fullName}
                          </MenuItem>
                        );
                      })}
                    </TextField>
                  </Box>
                ) : null}
                <Typography sx={{ mt: 2 }} gutterBottom>
                  <strong>Membrii departament</strong>
                </Typography>
                <UsersList
                  // @ts-ignore
                  PaperComponent={Box}
                  users={departmentUsers.results?.map((depAss) => ({
                    // @ts-ignore
                    ...depAss.userId,
                    userDepartmentAssignments: [
                      {
                        ...depAss,
                        // @ts-ignore
                        departmentId: depAss.departmentId?._id,
                      },
                    ],
                  }))}
                  departments={departments}
                  onSelectUser={onUserAction(SELECTED_ACTIONS.CREATE_OR_UPDATE)}
                  onDeleteUser={onUserAction(SELECTED_ACTIONS.DELETE)}
                  showType
                />
              </>
            ) : null}
          </>
        }
      />

      <Dialog
        actions={
          <LoadingButton
            variant="contained"
            onClick={() =>
              selectedUser &&
              selectedUser?.userDepartmentAssignments?.[0] &&
              removeUserAssignment(selectedUser?.userDepartmentAssignments[0]?._id as string)
            }
            loading={isDeleting}
          >
            Șterge
          </LoadingButton>
        }
        content={
          <>
            <Typography color="textSecondary" textAlign="center">
              Sunteți siguri că doriți să ștergeți alocarea utilizatorul&nbsp;
              <strong>
                <i>&quot;{selectedUser?.fullName}&quot;</i>
              </strong>
              &nbsp;din&nbsp;
              <strong>
                <i>&quot;{department?.name}&quot;</i>
              </strong>
              ?
            </Typography>
          </>
        }
        title="Confirmati ștergerea?"
        onClose={resetSelectedUserState}
        open={!!(selectedUser && selectedAction === SELECTED_ACTIONS.DELETE)}
      />

      {departments && selectedUser && selectedAction === SELECTED_ACTIONS.CREATE_OR_UPDATE ? (
        <UserForm
          changeAssignmentOnly
          departments={departments}
          functions={functions}
          initialValues={selectedUser}
          isLoading={isUpdating}
          open={true}
          onClose={resetSelectedUserState}
          onSubmit={(user) => {
            user &&
              user?.userDepartmentAssignments?.[0] &&
              updateUserAssignment(user?.userDepartmentAssignments?.[0]);
          }}
          onRemoveAssignment={noop}
        />
      ) : null}
    </>
  );
};

export default DepartmentDetails;
