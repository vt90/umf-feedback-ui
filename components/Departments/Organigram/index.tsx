import React, { useMemo } from 'react';
import { IDepartment } from '../../../models/department';
import { getDepartmentTree } from '../../../services/departments';
import DepartmentsOrganigramItem from './Item';

interface IDepartmentsOrganigramProps {
  departmentAdmins: object;
  departments: IDepartment[];
  onAddDepartment: (department: Partial<IDepartment>) => void;
  onRemoveDepartment: (department: Partial<IDepartment>) => void;
  onSelectDepartment: (department: Partial<IDepartment>) => void;
}

const DepartmentsOrganigram = (props: IDepartmentsOrganigramProps) => {
  const { departments, departmentAdmins, onAddDepartment, onRemoveDepartment, onSelectDepartment } =
    props;

  const departmentTree = useMemo(() => getDepartmentTree(departments), [departments]);

  if (!departmentTree) return null;

  return (
    <>
      {Object.values(departmentTree).map((department) => {
        return (
          <DepartmentsOrganigramItem
            key={department._id}
            color="divider"
            department={department}
            departmentAdmins={departmentAdmins}
            onAddDepartment={onAddDepartment}
            onRemoveDepartment={onRemoveDepartment}
            onSelectDepartment={onSelectDepartment}
          />
        );
      })}
    </>
  );
};

export default DepartmentsOrganigram;
