import { NextApiRequest, NextApiResponse } from 'next';
import axios from 'axios';
import has from 'lodash/has';
import { withApiAuthRequired } from '../../guards/authRequired';
import { unstable_getServerSession } from 'next-auth/next';
import { authOptions } from './auth/[...nextauth]';

export default withApiAuthRequired(async function (req: NextApiRequest, res: NextApiResponse) {
  const session = await unstable_getServerSession(
    // @ts-ignore
    req,
    res,
    authOptions,
  );

  try {
    const reqOptions = {
      ...req,
      method: req.method,
      baseURL: process.env.NEXT_PUBLIC_API_URL,
      url: req.url,
      data: req.body,
      headers: {
        ...req.headers,
        // @ts-ignore
        Authorization: `Bearer ${session?.accessToken}`,
      },
    };

    if (req.url?.includes('/export')) {
      // @ts-ignore
      reqOptions.responseType = 'arraybuffer';
    }
    if (has(req.body, '_id')) delete req.body._id;
    if (has(req.body, '__v')) delete req.body.__v;
    if (has(req.body, 'createdAt')) delete req.body.createdAt;
    if (has(req.body, 'updatedAt')) delete req.body.updatedAt;

    // @ts-ignore
    return axios(reqOptions)
      .then((response) => {
        if (response?.headers?.['content-type']?.includes('application/json')) {
          res.status(response?.status || 200).json(response.data);
        } else if (response?.headers?.['content-type']?.includes('application/zip')) {
          res.setHeader('Content-Type', response?.headers?.['content-type']);
          res.setHeader('Content-Disposition', response?.headers?.['content-disposition']);
          res.status(response?.status || 200).send(response.data);
        }
      })
      .catch((error) => {
        res.status(error.response?.status || 500).json(error?.response?.data);
      });
  } catch ({ error, message }) {
    // if the access token and refresh token is invalid
    if (error === 'invalid_grant') {
      res.status(401).json({
        error: 'not_authenticated',
        description: 'The user does not have an accessToken',
      });
    } else {
      res.status(500).end(message);
    }
    return;
  }
});
