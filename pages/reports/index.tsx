import React, { useCallback, useEffect, useMemo, useState } from 'react';
import * as FileSaver from 'file-saver';
import XLSX from 'sheetjs-style';
import { withPageAuthRequired } from '../../guards/authRequired';
import Head from 'next/head';
import AdminPage from '../../components/AdminPage';
import { useMutation, useQuery } from '@tanstack/react-query';
import { getEvaluations, exportEvaluations } from '../../services/evaluations';
import { getAllDepartments } from '../../services/departments';
import ListLoadingIndicator from '../../components/Common/ListLoadingIndcator';
import HRUserEvaluationsList from '../../components/Evaluation/HRUserEvaluationsList';
import Pagination from '../../components/Common/Pagination';
import EvaluationsFilter from '../../components/Evaluation/Filter';
import {
  getEvaluationSessions,
  getEvaluationSessionUnevaluatedUsers,
} from '../../services/evaluationSessions';
import useDebounce from '../../hooks/useDebounce';
import Grid from '@mui/material/Grid';
import Box from '@mui/material/Box';
import HREvaluationsListHeader from '../../components/Evaluation/HRUserEvaluationsList/Header';
import MenuItem from '@mui/material/MenuItem';
import Select from '@mui/material/Select';
import Typography from '@mui/material/Typography';
import Dialog from '../../components/Common/Dialog';
import { useRouter } from 'next/router';
import HRUnevaluatedList from '../../components/Evaluation/HRUnEvaluatedList';
import moment from 'moment';
import Tab from '@mui/material/Tab';
import Tabs from '@mui/material/Tabs';
import Card from '@mui/material/Card';
import { getSurveyRoleName } from '../../models/survey';
import isBoolean from 'lodash/isBoolean';
import { findUserDepartmentAssignments } from '../../services/userDepartmentAssignment';
import { USER_DEP_ASSIGNMENT_ROLES } from '../../models/userDepartmentAssignment';
import { getAllFunctions } from '../../services/functions';
import { getFunctionName } from '../../models/function';

const FETCH_DEPARTMENTS_QUERY_KEY = ['departments'];
const FETCH_SESSIONS_QUERY_KEY = ['evaluationSessions'];
const FETCH_UN_EVALUATED_QUERY_KEY = ['un-evaluated'];
const FETCH_ADMINS_QUERY_KEY = ['departmentAssignments'];
const FETCH_FUNCTIONS_QUERY_KEY = ['depFunctions'];

const ACTIVE_TABS = {
  reports: 'Raport evaluari',
  unevaluated: 'Personal ne-evaluat',
};

const Evaluations = () => {
  const [pageNumber, setPageNumber] = useState(1);
  const [pageSize] = useState(10000);
  const [order, setOrder] = useState(-1);
  const [orderBy, setOrderBy] = useState('createdAt');
  const [userDepartmentAssignmentType, setUserDepartmentAssignmentType] = useState<string | null>(
    '',
  );
  const [email, setEmail] = useState<string | null>('');
  const [role, setRole] = useState<string | null>('');
  const [minGrade, setMinGrade] = useState<string | null>('');
  const [finalResult, setFinalResult] = useState<string | null>('');
  const [userAgreement, setUserAgreement] = useState<string | null>('');
  const [userAcknowledged, setUserAcknowledged] = useState<string | null>('');
  const [contraEvaluated, setContraEvaluated] = useState<string | null>('');

  const router = useRouter();

  const userSearchTerm = useDebounce(`${email}`, 600);

  const onURLChange = useCallback(
    (key: string, value: string | null) => {
      const newQuery = { ...router.query, [key]: value };
      const queryParams = Object.entries(newQuery).reduce((acc, [key, value]) => {
        if (!!value) {
          return `${acc}&${key}=${value}`;
        } else {
          return acc;
        }
      }, '');
      router.push(`${router.route}?${queryParams}`);
    },
    [router.route, router.query],
  );

  const { activeView, evaluationSessionId, departmentId } = router.query;

  const { data: functions } = useQuery(FETCH_FUNCTIONS_QUERY_KEY, getAllFunctions);

  useEffect(() => {
    // @ts-ignore
    if (!ACTIVE_TABS[activeView]) {
      onURLChange('activeView', Object.keys(ACTIVE_TABS)[0]);
    }
  }, [activeView, onURLChange]);

  const fetchParams = {
    limit: pageSize,
    offset: (pageNumber - 1) * pageSize,
    // @ts-ignore
    order,
    // @ts-ignore
    orderBy,
    departmentId,
    userDepartmentAssignmentType,
    evaluationSessionId,
    role,
    userSearchTerm,
    minGrade,
    finalResult,
    ...(userAgreement && { userAgreement }),
    ...(userAcknowledged && { userAcknowledged }),
    ...(contraEvaluated && { contraEvaluated }),
  };

  const FETCH_QUERY_KEY = ['evaluations-search', fetchParams];

  // @ts-ignore
  const { isLoading, data } = useQuery(FETCH_QUERY_KEY, () => getEvaluations(fetchParams));

  // @ts-ignore
  const { isLoading: isLoadingUnEvaluated, data: unEvalautedUsersData } = useQuery(
    [
      FETCH_UN_EVALUATED_QUERY_KEY,
      {
        departmentId,
        userSearchTerm,
      },
    ],
    () =>
      getEvaluationSessionUnevaluatedUsers(`${evaluationSessionId}`, {
        departmentId,
        userSearchTerm,
      }),
    {
      enabled: !!evaluationSessionId,
    },
  );

  // @ts-ignore
  const { isLoading: isExporting, mutate: onExportEvaluation } = useMutation(exportEvaluations);

  const { isLoading: isDepartmentsLoading, data: departments } = useQuery(
    FETCH_DEPARTMENTS_QUERY_KEY,
    getAllDepartments,
  );

  const { isLoading: isEvaluationSessions, data: evaluationSessions } = useQuery(
    FETCH_SESSIONS_QUERY_KEY,
    () => getEvaluationSessions({}),
  );

  const { isLoading: isAdminsLoading, data: departmentAdmins } = useQuery(
    FETCH_ADMINS_QUERY_KEY,
    () =>
      findUserDepartmentAssignments({
        role: USER_DEP_ASSIGNMENT_ROLES.EVALUATOR,
      }),
  );

  const departmentAdminsInfo = useMemo(() => {
    return departmentAdmins?.results?.reduce((acc, cur) => {
      // @ts-ignore
      acc[cur.departmentId?._id] = cur.userId.fullName;

      return acc;
    }, {});
  }, [departmentAdmins]);

  useEffect(() => {
    if (!evaluationSessionId && evaluationSessions?.results?.length) {
      const lastSession = evaluationSessions?.results[evaluationSessions?.results?.length - 1];
      onURLChange('evaluationSessionId', lastSession?._id);
    }
  }, [evaluationSessions, onURLChange]);

  const onDownlaod = async () => {
    // @ts-ignore
    await onExportEvaluation(fetchParams);
  };

  const onExport = async () => {
    try {
      // @ts-ignore
      const evalSession = evaluationSessions?.results.find(
        (session) => session._id === evaluationSessionId,
      );

      // @ts-ignore
      let rows = [];
      let excelData = [];
      let namePrefix = '';
      if (activeView === 'reports') {
        namePrefix = 'Raport evaluari';

        const renderBoolean = (value: any, yesText: string) => {
          return isBoolean(value) ? (value ? yesText : 'Respinsă') : 'În așteptare';
        };

        // @ts-ignore
        excelData = data?.results.map((data) => {
          const functionInfo =
            // @ts-ignore
            data.userDepartmentAssignmentId?.jobFunction &&
            functions?.results &&
            // @ts-ignore
            functions.results?.find((f) => f._id === data.userDepartmentAssignmentId?.jobFunction);

          return {
            // @ts-ignore
            Membru: data.userId.fullName,
            // @ts-ignore
            Marca: data.userId.email,
            // @ts-ignore
            Departament: data.departmentId.name.replaceAll(',', '‚'),
            // @ts-ignore
            Evaluator: data.evaluatorId?.fullName || '-',
            // @ts-ignore
            Studii: data.userDepartmentAssignmentId?.studyType || '-',
            // @ts-ignore
            'Grad / Treapta': data.userDepartmentAssignmentId?.studyLevel || '-',
            // @ts-ignore
            Funcția: functionInfo ? getFunctionName(functionInfo) : '-',
            // @ts-ignore
            Tip: getSurveyRoleName(data.role),
            'Data evaluări': moment(data.createdAt).format('YYYY.MM.DD'),
            Nota: data?.finalGrade?.toFixed(2),
            Calificativ: data.finalResult,
            'Opinie evaluat': renderBoolean(data.userAgreement, 'Acceptată'),
            'Contra-evaluată': renderBoolean(data.contraEvaluated ? true : null, 'Contra-evaluată'),
            'Luată la cunoștință': renderBoolean(data.userAcknowledged, 'Luată la cunoștință'),
            'Procent Modificare Salarială': data?.salaryChangePercentage
              ? `${data?.salaryChangePercentage}%`
              : '-',
          };
        });

        rows = [
          [
            'Membru',
            'Marca',
            'Departament',
            'Funcția',
            'Studii',
            'Grad / Treaptă',
            'Evaluator',
            'Tip',
            'Data evaluări',
            'Nota',
            'Calificativ',
            'Procent Modificare Salarială',
            'Opinie evaluat',
            'Contra-evaluată',
            'Luată la cunoștință',
          ],
          // @ts-ignore
          ...data?.results.map((data) => [
            // @ts-ignore
            data.userId.fullName,
            // @ts-ignore
            data.userId.email,
            // @ts-ignore
            data.departmentId.name.replaceAll(',', '‚'),
            // @ts-ignore
            departmentAdminsInfo[data.departmentId._id] || '-',
            // @ts-ignore
            getSurveyRoleName(data.role),
            moment(data.createdAt).format('YYYY.MM.DD'),
            data?.finalGrade?.toFixed(2),
            data.finalResult,
            renderBoolean(data.userAgreement, 'Acceptată'),
            renderBoolean(data.contraEvaluated ? true : null, 'Contra-evaluată'),
            renderBoolean(data.userAcknowledged, 'Luată la cunoștință'),
          ]),
        ];
        // @ts-ignore
      } else if (unEvalautedUsersData?.length) {
        namePrefix = 'Personal ne-evaluat';

        // @ts-ignore
        excelData = unEvalautedUsersData.map((data) => ({
          Membru: data.userId.fullName,
          Marca: data.userId.email,
          Departament: data.departmentId.name.replaceAll(',', '‚'),
          // @ts-ignore
          Evaluator: departmentAdminsInfo[data.departmentId._id] || '-',
        }));

        rows = [
          ['Membru', 'Marca', 'Departament', 'Evaluator'],
          // @ts-ignore
          ...unEvalautedUsersData.map((data) => [
            data.userId.fullName,
            data.userId.email,
            data.departmentId.name.replaceAll(',', '‚'),
            // @ts-ignore
            departmentAdminsInfo[data.departmentId._id] || '-',
          ]),
        ];
      }

      console.log('generating XLSX');
      const fileType =
        'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8';
      const ws = XLSX.utils.json_to_sheet(excelData);
      const wb = { Sheets: { data: ws }, SheetNames: ['data'] };
      const excelBuffer = XLSX.write(wb, { bookType: 'xlsx', type: 'array' });
      const blob = new Blob([excelBuffer], { type: fileType });
      // @ts-ignore
      const fileName = `${namePrefix} ${evalSession.name} ${moment().format(
        'YYYY.MM.DD HH:mm',
      )}.xlsx`;
      FileSaver.saveAs(blob, fileName);

      // // @ts-ignore
      // const csvContent = 'data:text/csv;charset=utf-8,' + rows.map((e) => e.join(',')).join('\n');
      //
      // const link = document.createElement('a');
      // link.href = encodeURI(csvContent);
      // // @ts-ignore
      // const fileName = `${namePrefix} ${evalSession.name} ${moment().format(
      //   'YYYY.MM.DD HH:mm',
      // )}.csv`;
      // link.setAttribute('download', fileName);
      // document.body.appendChild(link);
      // link.click();
      // link.remove();
    } catch (e) {
      console.error(e);
    }
  };

  if (isDepartmentsLoading || isEvaluationSessions) return <ListLoadingIndicator />;

  // @ts-ignore
  return (
    <>
      <Head>
        <title>Rapoarte</title>
      </Head>

      <AdminPage title={<></>} pages={[{ url: '/reports', name: 'Rapoarte' }]}>
        <Card sx={{ p: 3, mt: -1 }}>
          <Tabs
            color="primary"
            value={activeView}
            onChange={(_, value) => onURLChange('activeView', value)}
            sx={{
              border: 0,
              mb: 3,
            }}
          >
            {Object.entries(ACTIVE_TABS).map(([key, text]) => (
              <Tab
                key={key}
                value={key}
                label={text}
                sx={{
                  border: 0,
                }}
              />
            ))}
          </Tabs>

          <Grid container spacing={2} columns={13}>
            {evaluationSessionId ? (
              <>
                <Grid item xs={12} md={3} lg={2}>
                  <EvaluationsFilter
                    // @ts-ignore
                    activeView={activeView}
                    order={order}
                    setOrder={setOrder}
                    orderBy={orderBy}
                    setOrderBy={setOrderBy}
                    department={departmentId ? `${departmentId}` : ''}
                    setDepartment={(value) => onURLChange('departmentId', value)}
                    userDepartmentAssignmentType={userDepartmentAssignmentType}
                    setUserDepartmentAssignmentType={setUserDepartmentAssignmentType}
                    departments={departments}
                    evaluationSessionId={`${evaluationSessionId}`}
                    setEvaluationSessionId={(value) => onURLChange('evaluationSessionId', value)}
                    evaluationSessions={evaluationSessions?.results}
                    role={role}
                    setRole={setRole}
                    userAgreement={userAgreement}
                    setUserAgreement={setUserAgreement}
                    contraEvaluated={contraEvaluated}
                    setContraEvaluated={setContraEvaluated}
                    userAcknowledged={userAcknowledged}
                    setUserAcknowledged={setUserAcknowledged}
                    email={email}
                    setEmail={setEmail}
                    minGrade={minGrade}
                    setMinGrade={setMinGrade}
                    finalResult={finalResult}
                    setFinalResult={setFinalResult}
                  />
                </Grid>
                <Grid item xs={12} md={10} lg={11}>
                  <HREvaluationsListHeader
                    // @ts-ignore
                    activeView={activeView}
                    order={order}
                    setOrder={setOrder}
                    orderBy={orderBy}
                    setOrderBy={setOrderBy}
                    searchTerm={email}
                    setSearchTerm={setEmail}
                    onExport={onExport}
                    isExporting={false}
                    isDownloading={isExporting}
                    onDownload={activeView === 'reports' ? onDownlaod : undefined}
                  />

                  <Box sx={{ mt: 3 }}>
                    {isLoading || isLoadingUnEvaluated ? (
                      <ListLoadingIndicator />
                    ) : (
                      <>
                        {activeView === 'reports' ? (
                          <>
                            <HRUserEvaluationsList evaluations={data?.results || []} />

                            <Pagination
                              sx={{ mt: 2 }}
                              pageNumber={pageNumber}
                              pageSize={pageSize}
                              totalResults={data?.count || 0}
                              setPageNumber={setPageNumber}
                            />
                          </>
                        ) : null}

                        {activeView === 'unevaluated' ? (
                          // @ts-ignore
                          <HRUnevaluatedList unEvaluatedList={unEvalautedUsersData} />
                        ) : null}
                      </>
                    )}
                  </Box>
                </Grid>
              </>
            ) : (
              <Dialog
                content={
                  <Typography align="center">
                    Se selectează contextual sesiunea corespunzătoare pe baza cc̆ruia se generează
                    rapoartele de evaluări
                  </Typography>
                }
                actions={
                  <>
                    {evaluationSessions?.results?.length ? (
                      <Box
                        sx={{
                          display: 'flex',
                          alignItems: 'center',
                          justifyContent: 'center',
                        }}
                      >
                        <Typography>Sesiunea de evaluare</Typography>
                        <Select
                          value={evaluationSessionId}
                          onChange={(ev) => onURLChange('evaluationSessionId', ev.target.value)}
                          size="small"
                          displayEmpty
                          sx={{
                            boxShadow: 'none',
                            maxWidth: '100%',
                            '.MuiOutlinedInput-notchedOutline': {
                              border: 0,
                              outline: 'none',
                            },
                          }}
                        >
                          {evaluationSessions?.results.map((evaluation) => (
                            <MenuItem key={evaluation._id} value={evaluation._id}>
                              {evaluation.name}
                            </MenuItem>
                          ))}
                        </Select>
                      </Box>
                    ) : null}
                  </>
                }
                title="Selectati sesiunea de evaluare"
                open={false}
              />
            )}
          </Grid>
        </Card>
      </AdminPage>
    </>
  );
};

export const getServerSideProps = withPageAuthRequired();

export default Evaluations;
