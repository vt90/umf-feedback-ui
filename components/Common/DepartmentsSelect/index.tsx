import * as React from 'react';
import { Control, Controller } from 'react-hook-form';
import Autocomplete from '@mui/material/Autocomplete';
import TextField, { ITextFieldProps } from '../FormInputs/TextField';
import { IDepartment, getDepartmentName } from '../../../models/department';
import { useState } from 'react';

interface IMultipleSelectFieldProps extends ITextFieldProps {
  name: string;
  control: Control;
  departments: IDepartment[];
  [key: string]: any;
}

function DepartmentsSelect(props: IMultipleSelectFieldProps) {
  const { name, control, departments } = props;
  const [inputValue, setInputValue] = useState('');

  return (
    <Controller
      name={name}
      control={control}
      render={({ field }) => {
        const { value, onChange } = field;

        // @ts-ignore
        const handleChange = (event) => {
          const {
            target: { value },
          } = event;
          onChange(
            // On autofill, we get a stringified value.
            typeof value === 'string' ? value.split(',') : value,
          );
        };

        return (
          <div>
            <Autocomplete
              multiple
              value={value}
              // @ts-ignore
              onChange={(event: any, newValue: string | null) => {
                handleChange({ target: { value: newValue } });
              }}
              inputValue={inputValue}
              onInputChange={(event, newInputValue) => {
                setInputValue(newInputValue);
              }}
              options={departments.map((o) => o._id)}
              // @ts-ignore
              getOptionLabel={(o) => {
                // @ts-ignore
                const department = departments.find((op) => op._id === o);
                // @ts-ignore
                return (department && getDepartmentName(department)) || '-';
              }}
              renderInput={(params) => <TextField {...params} />}
            />
          </div>
        );
      }}
    />
  );
}

export default React.forwardRef(DepartmentsSelect);
