import React from 'react';
import Box from '@mui/material/Box';
import Tabs from '@mui/material/Tabs';
import Tab from '@mui/material/Tab';
import { ToggleButtonGroupProps } from '@mui/material/ToggleButtonGroup';
import { AlertColor } from '@mui/material/Alert';
import Typography from '@mui/material/Typography';

interface IYesNoProps extends ToggleButtonGroupProps {
  value: boolean;
  onValueChange: (newValue: boolean) => void;
  label?: string;
  disabled?: boolean;
  yesLabel?: string | React.ReactNode;
  noLabel?: string | React.ReactNode;
  yesColor?: AlertColor;
  noColor?: AlertColor;
}

const YesNoToggle = (props: IYesNoProps) => {
  const { value, onValueChange, label, yesLabel = 'Da', noLabel = 'Nu', disabled, ...rest } = props;

  const handleChange = (event: any, newValue: string | null) => {
    !disabled && onValueChange(newValue === 'yes');
  };

  return (
    <Box sx={{ display: 'flex', alignItems: 'center' }}>
      {label && (
        <Typography variant="body2" sx={{ mr: 1 }}>
          {label}
        </Typography>
      )}
      <Tabs
        value={value ? 'yes' : 'no'}
        // @ts-ignore
        onChange={handleChange}
        disabled={disabled}
        {...rest}
      >
        <Tab value="yes" sx={{ px: 1.5, py: 1.5, minHeight: 0 }} label={yesLabel} />
        <Tab value="no" sx={{ px: 1.5, py: 1.5, minHeight: 0 }} label={noLabel} />
      </Tabs>
    </Box>
  );
};

export default YesNoToggle;
