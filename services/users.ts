import axios from 'axios';
import { IFindUsers, IUser } from '../models/user';
import { IPaginationResult } from '../models/base';
import { createOrUpdateUserDepartmentAssignment } from './userDepartmentAssignment';
import { IChangePassword } from '../models/auth';

const httpInstance = axios.create({
  baseURL: '/api/users',
});

httpInstance.interceptors.response.use(
  (response) => response.data,
  (error) => {
    if (error && error.response && error.response.data) {
      return Promise.reject(
        error.response.data.message
          ? `${error.response.data.message || error.response.data.error}`
          : error.response.data?.error || error.response.data,
      );
    } else {
      return Promise.reject({ message: 'Server error' });
    }
  },
);

const cleanupUserBody = (user: IUser): IUser => {
  // @ts-ignore
  delete user._id;
  // @ts-ignore
  delete user.id;
  // @ts-ignore
  delete user['__v'];

  return user;
};

export const findUsers = async (params: Partial<IFindUsers>): Promise<IPaginationResult<IUser>> => {
  return await httpInstance.get('', { params });
};

export const getUserById = async (id: string): Promise<IUser> => {
  return await httpInstance.get(`/${id}`);
};

export const createOrUpdateUser = async (user: IUser): Promise<IUser> => {
  const isUpdate = !!user._id;
  const url = isUpdate ? `/${user._id}` : ``;
  const req = isUpdate ? httpInstance.patch : httpInstance.post;
  const { userDepartmentAssignments, ...userInfo } = user;

  // @ts-ignore
  return await req(url, cleanupUserBody(userInfo)).then(async (user: IUser) => {
    if (userDepartmentAssignments?.length) {
      for (const assignment of userDepartmentAssignments) {
        assignment.userId = user._id;

        await createOrUpdateUserDepartmentAssignment(assignment);
      }
    }
    return user;
  });
};

export const deleteUser = async (id: string): Promise<any> => {
  return await httpInstance.delete(`/${id}`);
};

export const changePassword = async (data: IChangePassword): Promise<any> => {
  return httpInstance.post('/change-password', data);
};

export const resetPassword = async (data: IChangePassword): Promise<any> => {
  return httpInstance.post('/reset-password', data);
};
