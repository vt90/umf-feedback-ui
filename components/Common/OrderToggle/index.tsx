import React from 'react';
import { SxProps } from '@mui/material';
import IconButton from '@mui/material/IconButton';
import StraightIcon from '@mui/icons-material/Straight';
import { useDebouncedCallback } from 'use-debounce';

interface IOrderToggleProps {
  order: number;
  setOrder: (order: number) => void;
  sx?: SxProps;
}

const OrderToggle = (props: IOrderToggleProps) => {
  const { order, setOrder, sx } = props;

  const toggleOrder = useDebouncedCallback(() => setOrder(order === 1 ? -1 : 1), 250);

  return (
    <IconButton onClick={toggleOrder} sx={sx}>
      <StraightIcon sx={{ color: order === 1 ? 'primary.main' : 'text.secondary' }} />
      <StraightIcon
        sx={{
          marginLeft: -4,
          mt: -0.5,
          transform: 'rotate(180deg)',
          color: order === -1 ? 'primary.main' : 'text.secondary',
        }}
      />
    </IconButton>
  );
};

export default OrderToggle;
