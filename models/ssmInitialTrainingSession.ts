import { IBaseModel } from './base';
import { IUser } from './user';
import { IUserDepartmentAssignment } from './userDepartmentAssignment';
import { IEvaluationEvent } from './evaluation';
import { ISSMPackage } from './ssmPackage';

export interface ISSMInitialTrainingSession extends IBaseModel {
  userId: IUser;
  userDepartmentAssignmentId: IUserDepartmentAssignment;
  userAcknowledgedGeneral: boolean;
  userAcknowledgedWork: boolean;
  contraEvaluatedGeneral: boolean;
  contraEvaluatedWork: boolean;
  ssmEvaluatedGeneral: boolean;
  ssmEvaluatedWork: boolean;
  isAccepted: boolean;
  events: IEvaluationEvent[];
  ssmPackages?: ISSMPackage[];
}

export enum SSM_INITIAL_TRAINING_TYPES {
  USER_ACKNOWLEDGEMENT_GENERAL,
  USER_ACKNOWLEDGEMENT_WORK,
  CONTRA_EVALUATED_GENERAL,
  CONTRA_EVALUATED_WORK,
  SSM_EVALUATED_GENERAL,
  SSM_EVALUATED_WORK,
  ACCEPTED,
}
