import React from 'react';
import { signIn, signOut } from 'next-auth/react';
import Login from '@mui/icons-material/Login';
import Logout from '@mui/icons-material/Logout';
import LockResetIcon from '@mui/icons-material/LockReset';
import Avatar from '@mui/material/Avatar';
import Divider from '@mui/material/Divider';
import IconButton from '@mui/material/IconButton';
import ListItemText from '@mui/material/ListItemText';
import ListItemIcon from '@mui/material/ListItemIcon';
import Menu from '@mui/material/Menu';
import MenuItem from '@mui/material/MenuItem';
import Typography from '@mui/material/Typography';
import { useAuthContext } from '../../../context/authContext';
import ForgotPasswordForm from '../../Auth/ChangePasswordForm';
import { useMutation } from '@tanstack/react-query';
import { changePassword } from '../../../services/users';
import { useNotificationContext } from '../../../context/notificationContext';

const Sidenav = () => {
  const { user } = useAuthContext();
  const { setError, setMessage } = useNotificationContext();
  const [accountAnchorEl, setAccountAnchorEl] = React.useState<null | HTMLElement>(null);
  const [showChangePassword, setShowChangePassword] = React.useState<boolean>(false);

  const accountOpen = Boolean(accountAnchorEl);
  const handleAccountClick = (event: React.MouseEvent<HTMLElement>) => {
    setAccountAnchorEl(event.currentTarget);
  };
  const handleAccountClose = () => {
    setAccountAnchorEl(null);
  };

  const { isLoading: isPasswordChanging, mutate: changePasswordMutation } = useMutation(
    changePassword,
    {
      onSuccess: async () => {
        setShowChangePassword(false);
        setMessage('Parolă modificată');
      },
      onError: setError,
    },
  );

  return (
    <>
      {user ? (
        <>
          <IconButton onClick={handleAccountClick} size="medium">
            <Avatar
              sx={{
                width: 40,
                height: 40,
                bgcolor: 'text.primary',
                fontSize: 12,
                fontWeight: 700,
                color: 'white',
                border: '2px solid #fff',
              }}
            >
              {/* @ts-ignore */}
              {user?.fullName
                ?.split(' ')
                .splice(0, 2)
                .map((name: string) => name.charAt(0).toUpperCase())
                .join('.')}
            </Avatar>
          </IconButton>

          <Menu
            anchorEl={accountAnchorEl}
            open={accountOpen}
            onClose={handleAccountClose}
            onClick={handleAccountClose}
            PaperProps={{
              elevation: 0,
              sx: {
                overflow: 'visible',
                filter: 'drop-shadow(0px 2px 8px rgba(0,0,0,0.32))',
                mt: 1.5,
                '& .MuiAvatar-root': {
                  width: 32,
                  height: 32,
                  ml: -0.5,
                  mr: 1,
                },
                '&:before': {
                  content: '""',
                  display: 'block',
                  position: 'absolute',
                  top: 0,
                  right: 14,
                  width: 10,
                  height: 10,
                  bgcolor: 'background.paper',
                  transform: 'translateY(-50%) rotate(45deg)',
                  zIndex: 0,
                },
              },
            }}
            transformOrigin={{ horizontal: 'right', vertical: 'top' }}
            anchorOrigin={{ horizontal: 'right', vertical: 'bottom' }}
          >
            <MenuItem>
              {/* @ts-ignore */}
              <ListItemText primary={user.fullName} secondary={user.email} />
            </MenuItem>
            <Divider />
            <MenuItem onClick={() => setShowChangePassword(true)}>
              <ListItemIcon>
                <LockResetIcon fontSize="small" />
              </ListItemIcon>
              Modifică parola
            </MenuItem>
            <MenuItem onClick={() => signOut()}>
              <ListItemIcon>
                <Logout fontSize="small" />
              </ListItemIcon>
              Logout
            </MenuItem>
          </Menu>
        </>
      ) : (
        <Typography onClick={() => signIn()}>
          <Login /> Login
        </Typography>
      )}

      {user && showChangePassword && (
        <ForgotPasswordForm
          isLoading={isPasswordChanging}
          onClose={() => setShowChangePassword(false)}
          // @ts-ignore
          initialValues={{ userId: user._id }}
          // @ts-ignore
          onSubmit={changePasswordMutation}
        />
      )}
    </>
  );
};

export default Sidenav;
