import React, { useState } from 'react';
import Head from 'next/head';
import { withPageAuthRequired } from '../../guards/authRequired';
import {
  acknowledge,
  findMySSMPeriodicTrainingSessions,
} from '../../services/ssmPeriodicTrainingSessions';
import {
  acknowledgeInitial,
  getUserInitialTrainingSession,
} from '../../services/ssmInitialTrainingSessions';
import { useQuery, useMutation, useQueryClient } from '@tanstack/react-query';
import AdminPage from '../../components/AdminPage';
import NoDataAvailable from '../../components/Common/NoDataAvailable';
import { getAllFunctions } from '../../services/functions';
import ListLoadingIndicator from '../../components/Common/ListLoadingIndcator';
import Box from '@mui/material/Box';
import Card from '@mui/material/Card';
import Pagination from '../../components/Common/Pagination';
import UserSSMTrainingSessionList from '../../components/SSMPeriodicTrainingSession/UserSessionsList';
import { ISSMPeriodicTrainingSession } from '../../models/ssmPeriodicTrainingSession';
import Dialog from '../../components/Common/Dialog';
import Button from '@mui/lab/LoadingButton';
import { useNotificationContext } from '../../context/notificationContext';
import { findSSMPackage } from '../../services/ssmPackages';
import SSMTrainingSessionDetails from '../../components/SSMPeriodicTrainingSession/Details';
import { SSM_PACKAGE_TYPE } from '../../models/ssmPackage';
const FETCH_INITIAL_QUERY_KEY = ['my-ssm-initial-sessions-trainings'];
const FETCH_QUERY_KEY = ['my-ssm-periodic-sessions-trainings'];
const FETCH_PACKAGES_QUERY_KEY = ['my-ssm-packages'];
const FETCH_FUNCTIONS_QUERY_KEY = ['depFunctions'];

const SSMPeriodicTrainingSessions = () => {
  const [selectedSession, setSelectedSession] = useState<ISSMPeriodicTrainingSession | null>(null);
  const [selectedInitialSessionActionType, setSelectedInitialSessionActionType] = useState<
    string | null
  >(null);

  const [pageNumber, setPageNumber] = useState(1);
  const [pageSize] = useState(50);
  const { setError, setMessage } = useNotificationContext();
  const queryClient = useQueryClient();

  const fetchParams = {
    limit: pageSize,
    offset: (pageNumber - 1) * pageSize,
  };

  const { isLoading, data: ssmPeriodicTrainingsSessions } = useQuery(
    [FETCH_QUERY_KEY, fetchParams],
    // @ts-ignore
    () => findMySSMPeriodicTrainingSessions(fetchParams),
  );

  const { isLoading: isInitialLoading, data: ssmInitialTrainingsSession } = useQuery(
    FETCH_INITIAL_QUERY_KEY,
    // @ts-ignore
    () => getUserInitialTrainingSession(),
  );

  const { data: functions } = useQuery(FETCH_FUNCTIONS_QUERY_KEY, getAllFunctions);

  const { isLoading: ssmPackagesLoading, data: ssmPackages } = useQuery(
    [FETCH_PACKAGES_QUERY_KEY, selectedSession?.userDepartmentAssignmentId?.jobFunction],
    () =>
      findSSMPackage({
        // @ts-ignore
        functions: [selectedSession?.userDepartmentAssignmentId?.jobFunction],
        // @ts-ignore
        userDepartments: [selectedSession?.userDepartmentAssignmentId?.departmentId?._id],
        type: SSM_PACKAGE_TYPE.PERIODIC,
      }),
    {
      enabled: !!selectedSession,
    },
  );

  const { isLoading: initialSsmPackagesLoading, data: initialSsmPackages } = useQuery(
    [FETCH_PACKAGES_QUERY_KEY, selectedInitialSessionActionType],
    () =>
      findSSMPackage({
        // @ts-ignore
        functions: [ssmInitialTrainingsSession?.userDepartmentAssignmentId?.jobFunction],
        // @ts-ignore
        userDepartments: [
          // @ts-ignore
          ssmInitialTrainingsSession?.userDepartmentAssignmentId?.departmentId?._id,
        ],
        type: SSM_PACKAGE_TYPE.AT_HIRE,
      }),
    {
      enabled: !!selectedInitialSessionActionType,
    },
  );

  const { isLoading: isAcknowledging, mutate: acknowledgeSession } = useMutation(
    // @ts-ignore
    () => acknowledge(selectedSession._id, ssmPackages?.results),
    {
      onSuccess: async () => {
        await queryClient.invalidateQueries([FETCH_QUERY_KEY, fetchParams]);
        setSelectedSession(null);
        setMessage('Instruire periodică semnată');
      },
      onError: setError,
    },
  );

  const { isLoading: isAcknowledgingInitial, mutate: acknowledgeInitialSession } = useMutation(
    // @ts-ignore
    () =>
      acknowledgeInitial(
        // @ts-ignore
        ssmInitialTrainingsSession._id,
        // @ts-ignore
        initialSsmPackages?.results,
        selectedInitialSessionActionType,
      ),
    {
      onSuccess: async () => {
        await queryClient.invalidateQueries(FETCH_INITIAL_QUERY_KEY);
        setSelectedInitialSessionActionType(null);
        setMessage('Instruire semnată');
      },
      onError: setError,
    },
  );

  const mockInitialTrainingAsPeriodicList = (): ISSMPeriodicTrainingSession[] => {
    return [
      {
        ...ssmInitialTrainingsSession,
        userAcknowledged: !!ssmInitialTrainingsSession?.userAcknowledgedGeneral,
        contraEvaluated: !!ssmInitialTrainingsSession?.contraEvaluatedGeneral,
        ssmEvaluated: !!ssmInitialTrainingsSession?.ssmEvaluatedGeneral,
        // @ts-ignore
        sessionDate: ssmInitialTrainingsSession?.createdAt,
        // @ts-ignore
        ssmPeriodicTrainingId: {
          name: 'Instruire introductiv generală',
        },
        action_type_flag: 'General',
      },
      {
        ...ssmInitialTrainingsSession,
        userAcknowledged: !!ssmInitialTrainingsSession?.userAcknowledgedWork,
        contraEvaluated: !!ssmInitialTrainingsSession?.contraEvaluatedWork,
        ssmEvaluated: !!ssmInitialTrainingsSession?.ssmEvaluatedWork,
        // @ts-ignore
        sessionDate: ssmInitialTrainingsSession?.createdAt,
        // @ts-ignore
        ssmPeriodicTrainingId: {
          name: 'Instruire la locul de muncă',
        },
        action_type_flag: 'Work',
      },
    ];
  };

  const onSelectInitialSession = ({ action_type_flag }: { action_type_flag: string }) => {
    setSelectedInitialSessionActionType(action_type_flag);
  };

  return (
    <>
      <Head>
        <title>Instruiri SSM</title>
      </Head>

      <AdminPage
        title="Instruiri SSM"
        pages={[{ url: '/ssm', name: 'Instruiri SSM' }]}
        actions={<></>}
      >
        <Card sx={{ p: 3 }}>
          {ssmInitialTrainingsSession || isInitialLoading ? (
            <>
              {ssmInitialTrainingsSession && (
                <Box sx={{ mb: 3 }}>
                  <UserSSMTrainingSessionList
                    ssmPeriodicTrainingsSessions={mockInitialTrainingAsPeriodicList()}
                    // @ts-ignore
                    onSelectSession={onSelectInitialSession}
                    title="Instruire la angajare"
                  />
                </Box>
              )}
            </>
          ) : null}
          {ssmPeriodicTrainingsSessions?.results?.length || isLoading ? (
            <>
              {isLoading && <ListLoadingIndicator />}

              {functions?.results?.length && (
                <>
                  <UserSSMTrainingSessionList
                    ssmPeriodicTrainingsSessions={ssmPeriodicTrainingsSessions?.results || []}
                    onSelectSession={setSelectedSession}
                    title="Instruiri periodice"
                  />

                  <Pagination
                    sx={{ mt: 2 }}
                    pageNumber={pageNumber}
                    pageSize={pageSize}
                    totalResults={ssmPeriodicTrainingsSessions?.count || 0}
                    setPageNumber={setPageNumber}
                  />
                </>
              )}
            </>
          ) : (
            <NoDataAvailable content="În momentul de faţă nu există nicio instruire periodca disponibilă pe platformă" />
          )}
        </Card>
      </AdminPage>

      <Dialog
        content={
          <>
            {ssmPackagesLoading && <ListLoadingIndicator />}

            <SSMTrainingSessionDetails
              // @ts-ignore
              functions={functions?.results}
              // @ts-ignore
              session={selectedSession}
              // @ts-ignore
              ssmPackages={
                selectedSession?.ssmPackages?.length
                  ? selectedSession?.ssmPackages
                  : ssmPackages?.results
              }
            />
          </>
        }
        actions={
          <>
            {!selectedSession?.userAcknowledged && (
              // @ts-ignore
              <Button variant="contained" onClick={acknowledgeSession} loading={isAcknowledging}>
                Semnează
              </Button>
            )}
          </>
        }
        // @ts-ignore
        title={selectedSession?.ssmPeriodicTrainingId?.name}
        open={!!selectedSession}
        maxWidth="lg"
        onClose={() => setSelectedSession(null)}
      />

      <Dialog
        content={
          <>
            {initialSsmPackagesLoading && <ListLoadingIndicator />}

            <SSMTrainingSessionDetails
              isInitial={true}
              // @ts-ignore
              functions={functions?.results}
              // @ts-ignore
              session={ssmInitialTrainingsSession}
              // @ts-ignore
              ssmPackages={
                ssmInitialTrainingsSession?.ssmPackages?.length
                  ? ssmInitialTrainingsSession?.ssmPackages
                  : initialSsmPackages?.results
              }
            />
          </>
        }
        actions={
          <>
            {!ssmInitialTrainingsSession?.userAcknowledgedGeneral &&
              selectedInitialSessionActionType === 'General' && (
                // @ts-ignore
                <Button
                  variant="contained"
                  onClick={() => acknowledgeInitialSession()}
                  loading={isAcknowledgingInitial}
                >
                  Semnează
                </Button>
              )}

            {!ssmInitialTrainingsSession?.userAcknowledgedWork &&
              selectedInitialSessionActionType === 'Work' && (
                // @ts-ignore
                <Button
                  variant="contained"
                  onClick={() => acknowledgeInitialSession()}
                  loading={isAcknowledgingInitial}
                  disabled={!ssmInitialTrainingsSession?.userAcknowledgedGeneral}
                >
                  Semnează
                </Button>
              )}
          </>
        }
        // @ts-ignore
        title={
          selectedInitialSessionActionType === 'Work'
            ? 'Instruire la locul de muncă'
            : 'Instruire introductiv generală'
        }
        open={!!selectedInitialSessionActionType}
        maxWidth="lg"
        onClose={() => setSelectedInitialSessionActionType(null)}
      />
    </>
  );
};

export const getServerSideProps = withPageAuthRequired();

export default SSMPeriodicTrainingSessions;
