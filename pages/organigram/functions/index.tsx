import React from 'react';
import Head from 'next/head';
import { useQuery } from '@tanstack/react-query';
import AdminPage from '../../../components/AdminPage';
import OrganigramNavigation from '../../../components/Organigram/Navigation';
import { withPageAuthRequired } from '../../../guards/authRequired';
import { getAllFunctions } from '../../../services/functions';
import Paper from '@mui/material/Paper';
import FunctionsList from '../../../components/Functions/List';
const FETCH_FUNCTIONS_QUERY_KEY = ['depFunctions'];

const Functions = () => {
  const { data: functions } = useQuery(FETCH_FUNCTIONS_QUERY_KEY, getAllFunctions);

  return (
    <div>
      <Head>
        <title>Funcții</title>
      </Head>

      <AdminPage title="Funcții" pages={[{ url: '/functions', name: 'Funcții' }]}>
        <Paper sx={{ p: 3 }}>
          <OrganigramNavigation />

          {functions?.results && <FunctionsList functions={functions.results} />}
        </Paper>
      </AdminPage>
    </div>
  );
};

export const getServerSideProps = withPageAuthRequired();

export default Functions;
