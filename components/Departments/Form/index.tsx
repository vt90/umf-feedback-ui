import React from 'react';
import * as yup from 'yup';
import { useForm } from 'react-hook-form';
import Button from '@mui/material/Button';
import Grid from '@mui/material/Grid';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogTitle from '@mui/material/DialogTitle';
import LinearProgress from '@mui/material/LinearProgress';
import Typography from '@mui/material/Typography';
import { yupResolver } from '@hookform/resolvers/yup';
import { IDepartment } from '../../../models/department';
import TextField from '../../Common/FormInputs/TextField';
import SelectField from '../../Common/FormInputs/SelectField';

interface IDepartmentFormProps {
  departments: IDepartment[];
  initialValues: Partial<IDepartment>;
  isLoading: boolean;
  onClose: () => void;
  onSubmit: (data: any) => void;
  open: boolean;
}
const schema = yup
  .object({
    name: yup.string().required(),
    code: yup.string().required(),
  })
  .required();

const DepartmentForm = (props: IDepartmentFormProps) => {
  const { departments, initialValues, isLoading, onClose, onSubmit, open } = props;
  const {
    control,
    formState: { errors },
    handleSubmit,
    register,
  } = useForm({
    defaultValues: initialValues,
    mode: 'onTouched',
    resolver: yupResolver(schema),
    shouldUnregister: true,
  });

  return (
    <Dialog
      open={open}
      keepMounted={false}
      onClose={onClose}
      // @ts-ignore
      PaperComponent="form"
      maxWidth="md"
      fullWidth
      onSubmit={handleSubmit(onSubmit)}
    >
      <DialogTitle sx={{ typography: 'h5', textAlign: 'center' }}>
        {initialValues?._id ? 'Modifică' : 'Creează'} departament
      </DialogTitle>
      <DialogContent sx={{ my: 3 }}>
        {/* @ts-ignore */}
        <input type="hidden" {...register(`_id`)} />
        <Grid container spacing={3} alignItems="center">
          <Grid item xs={12} md={3}>
            <Typography variant="body2" sx={{ textAlign: { xs: 'left', md: 'right' } }}>
              Cod*
            </Typography>
          </Grid>
          <Grid item xs={12} md={8}>
            <TextField
              required={true}
              disabled={isLoading}
              errorMessage={!!errors.code && 'Codul este necesar'}
              defaultValue={initialValues.code}
              {...register('code')}
            />
          </Grid>
          <Grid item xs={12} md={3}>
            <Typography variant="body2" sx={{ textAlign: { xs: 'left', md: 'right' } }}>
              Denumire*
            </Typography>
          </Grid>
          <Grid item xs={12} md={8}>
            <TextField
              required={true}
              disabled={isLoading}
              errorMessage={!!errors.name && 'Denumirea este necesară'}
              defaultValue={initialValues.name}
              {...register('name')}
            />
          </Grid>
          <Grid item xs={12} md={3}>
            <Typography variant="body2" sx={{ textAlign: { xs: 'left', md: 'right' } }}>
              Departament părinte
            </Typography>
          </Grid>
          <Grid item xs={12} md={8}>
            <SelectField
              name="parentId"
              control={control}
              defaultValue={initialValues.parentId}
              options={[
                { name: <i>Departament principal</i>, value: null },
                ...departments.map((department) => ({
                  name: department.name,
                  value: department._id,
                })),
              ]}
            />
          </Grid>
        </Grid>
      </DialogContent>

      {isLoading && <LinearProgress />}
      <DialogActions sx={{ py: 2, justifyContent: 'center', bgcolor: 'background.default' }}>
        <Button onClick={onClose} disabled={isLoading}>
          Anulare
        </Button>
        <Button variant="contained" type="submit" disabled={isLoading}>
          {initialValues?._id ? 'Modifică' : 'Creează'}
        </Button>
      </DialogActions>
    </Dialog>
  );
};

export default DepartmentForm;
