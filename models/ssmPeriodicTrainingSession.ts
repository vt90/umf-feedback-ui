import { IBaseModel } from './base';
import { IUser } from './user';
import { IUserDepartmentAssignment } from './userDepartmentAssignment';
import { ISSMPeriodicTraining } from './ssmPeriodicTraining';
import { IEvaluationEvent } from './evaluation';
import { ISSMPackage } from './ssmPackage';

export interface ISSMPeriodicTrainingSession extends IBaseModel {
  userId: IUser;
  userDepartmentAssignmentId: IUserDepartmentAssignment;
  ssmPeriodicTrainingId: ISSMPeriodicTraining;
  sessionDate: Date;
  userAcknowledged: boolean;
  contraEvaluatorId?: string | IUser;
  ssmEvaluatorId?: string | IUser;
  contraEvaluated: boolean;
  ssmEvaluated: boolean;
  events: IEvaluationEvent[];
  ssmPackages?: ISSMPackage[];
}
