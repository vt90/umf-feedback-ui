export const SELECTED_ACTIONS = {
  VIEW_DETAILS: 'View',
  CREATE_OR_UPDATE: 'CreateUpdate',
  DELETE: 'DELETE',
};

export const IMAGE_EXTENSIONS = [
  '.jpg',
  '.jpeg',
  '.png',
  '.gif',
  '.bmp',
  '.tiff',
  '.webp',
  '.svg',
  '.ico',
  '.jfif',
  '.pjpeg',
  '.pjp',
];

export const isImage = (filename: string) => {
  // Get the file extension from the filename
  const fileExtension = filename.slice(((filename.lastIndexOf('.') - 1) >>> 0) + 2).toLowerCase();

  // Check if the file extension is in the list of known image extensions
  return IMAGE_EXTENSIONS.includes(`.${fileExtension}`);
};
