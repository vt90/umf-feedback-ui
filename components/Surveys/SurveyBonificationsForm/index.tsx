import React, { useEffect } from 'react';
import Box from '@mui/material/Box';
import Button from '@mui/lab/LoadingButton';
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import * as yup from 'yup';
import { ISurveyBonification } from '../../../models/survey';
import TextField, { ITextFieldProps } from '../../Common/FormInputs/TextField';
import Grid from '@mui/material/Grid';
import Typography from '@mui/material/Typography';
import CloseIcon from '@mui/icons-material/Close';
import DeleteIcon from '@mui/icons-material/Delete';
import SaveIcon from '@mui/icons-material/CheckCircle';

interface ISurveyBonificationFormProps {
  bonificationIndex: number;
  initialFocus?: string;
  initialValues: Partial<ISurveyBonification>;
  isLoading: boolean;
  onCancel: () => void;
  onRemoveSurveyBonification?: (data: any) => void;
  onSubmit: (data: any) => void;
}

const schema = yup
  .object({
    name: yup.string().required(),
    value: yup.number().required(),
  })
  .required();

const SurveyBonificationForm = (props: ISurveyBonificationFormProps) => {
  const {
    bonificationIndex,
    initialFocus,
    initialValues,
    isLoading,
    onCancel,
    onRemoveSurveyBonification,
    onSubmit,
  } = props;

  const {
    formState: { errors },
    handleSubmit,
    register,
    setFocus,
  } = useForm({
    defaultValues: initialValues,
    mode: 'onTouched',
    resolver: yupResolver(schema),
    shouldUnregister: true,
  });

  useEffect(() => {
    // @ts-ignore
    initialFocus && setFocus(initialFocus);
  }, [setFocus, initialFocus]);

  const textFieldProps: Partial<ITextFieldProps> = {
    required: true,
    disabled: isLoading,
    size: 'small',
    sx: {
      '& legend': { display: 'none' },
      '& fieldset': {
        top: 0,
        borderColor: 'transparent',
      },
    },
    inputProps: {
      sx: {
        typography: 'body1',
        fontWeight: 'bold',
      },
    },
  };

  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      <Grid container spacing={2} alignItems="center">
        <Grid item xs={1}>
          <Typography variant="body2" sx={{ textAlign: { xs: 'left', md: 'right' } }}>
            {bonificationIndex}
          </Typography>
        </Grid>

        <Grid item xs={11}>
          <Box sx={{ display: 'flex' }}>
            <Box sx={{ flexGrow: 1, mr: 2 }}>
              <input type="hidden" {...register(`_id`)} />
              <TextField
                required
                disabled={isLoading}
                errorMessage={!!errors?.name && 'Denumirea bonificației este necesară'}
                {...register(`name`)}
                size="small"
                sx={{
                  '& legend': { display: 'none' },
                  '& fieldset': {
                    top: 0,
                    borderColor: 'transparent',
                  },
                }}
                inputProps={{
                  sx: {
                    typography: 'body1',
                    fontWeight: 'bold',
                  },
                }}
              />
            </Box>

            <Box
              sx={{
                bgcolor: 'background.default',
                display: 'flex',
                alignItems: 'center',
                pl: 2,
              }}
            >
              <Typography variant="body2">Punctaj:</Typography>
              <Box sx={{ maxWidth: 64, ml: 1 }}>
                <TextField
                  errorMessage={!!errors?.value && 'Camp invalid'}
                  defaultValue={initialValues.value}
                  InputProps={{
                    step: '.01',
                  }}
                  {...register(`value`)}
                  {...textFieldProps}
                />
              </Box>
            </Box>
          </Box>
        </Grid>

        <Grid item xs={12} sx={{ display: 'flex', justifyContent: 'flex-end' }}>
          <Box>
            <Button
              // @ts-ignore
              onClick={onCancel}
              color="inherit"
              endIcon={<CloseIcon fontSize="small" />}
              size="small"
            >
              Anulează
            </Button>
          </Box>

          {initialValues._id && onRemoveSurveyBonification && (
            <Box sx={{ ml: 1 }}>
              <Button
                // @ts-ignore
                onClick={() => onRemoveSurveyBonification(initialValues)}
                variant="contained"
                color="error"
                endIcon={<DeleteIcon fontSize="small" />}
                size="small"
              >
                Șterge bonificație
              </Button>
            </Box>
          )}

          <Box sx={{ ml: 1 }}>
            <Button
              type="submit"
              variant="contained"
              endIcon={<SaveIcon fontSize="small" />}
              size="small"
              loading={isLoading}
            >
              Salvează
            </Button>
          </Box>
        </Grid>
      </Grid>
    </form>
  );
};

export default SurveyBonificationForm;
