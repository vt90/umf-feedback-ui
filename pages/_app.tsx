import React, { Fragment, useMemo } from 'react';
import Head from 'next/head';
import { useRouter } from 'next/router';
import { SessionProvider } from 'next-auth/react';
import { CacheProvider } from '@emotion/react';
import { ThemeProvider } from '@mui/material/styles';
import CssBaseline from '@mui/material/CssBaseline';
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider';
import { AdapterMoment } from '@mui/x-date-pickers/AdapterMoment';
import { theme, createEmotionCache } from '../lib/theme';
import { QueryClient, QueryClientProvider } from '@tanstack/react-query';
import Layout from '../components/Layout';
import type { AppProps } from 'next/app';
import { AuthProvider } from '../context/authContext';
import { NotificationContextProvider } from '../context/notificationContext';
import 'react-quill/dist/quill.snow.css';
import '../styles/globals.css';

// Client-side cache, shared for the whole session of the user in the browser.
const clientSideEmotionCache = createEmotionCache();

// Create a client
const queryClient = new QueryClient();

function MyApp(props: AppProps) {
  const {
    Component,
    // @ts-ignore
    emotionCache = clientSideEmotionCache,
    pageProps: { session, ...pageProps },
  } = props;

  const router = useRouter();

  const LayoutContainer = useMemo(
    () => (router.pathname === '/sign-in' ? Fragment : Layout),
    [router?.pathname],
  );

  return (
    <QueryClientProvider client={queryClient}>
      <SessionProvider session={session}>
        <AuthProvider>
          <CacheProvider value={emotionCache}>
            <LocalizationProvider dateAdapter={AdapterMoment}>
              <Head>
                <meta name="viewport" content="initial-scale=1, width=device-width" />
              </Head>
              <ThemeProvider theme={theme}>
                {/* CssBaseline kickstart an elegant, consistent, and simple baseline to build upon. */}
                <CssBaseline />
                <NotificationContextProvider>
                  <LayoutContainer>
                    <Component {...pageProps} />
                  </LayoutContainer>
                </NotificationContextProvider>
              </ThemeProvider>
            </LocalizationProvider>
          </CacheProvider>
        </AuthProvider>
      </SessionProvider>
    </QueryClientProvider>
  );
}

export default MyApp;
