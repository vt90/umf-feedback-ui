import React, { useEffect } from 'react';
import CircularProgress from '@mui/material/CircularProgress';
import { withPageAuthRequired } from '../guards/authRequired';
import { IUser } from '../models/user';
import { useRouter } from 'next/router';
import { USER_DEP_ASSIGNMENT_ROLES } from '../models/userDepartmentAssignment';

interface IMHomeProps {
  user: IUser;
}

const Home = (props: IMHomeProps) => {
  const { user } = props;
  const router = useRouter();

  useEffect(() => {
    let route = '/my-evaluations';
    const isDepartmentManager = user?.userDepartmentAssignments?.find(
      (dep) => dep.role === USER_DEP_ASSIGNMENT_ROLES.EVALUATOR,
    );
    const isHRManager = user.canViewAllReviews;

    if (isDepartmentManager) route = '/evaluation-sessions';
    if (isHRManager) route = '/reports';

    router.push(route);
  }, [router, user]);

  return (
    <>
      <CircularProgress sx={{ marginTop: -4 }} size={18} />
    </>
  );
};

export const getServerSideProps = withPageAuthRequired();

export default Home;
