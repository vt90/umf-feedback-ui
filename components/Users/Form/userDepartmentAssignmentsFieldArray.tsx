import React, { Fragment, useState } from 'react';
import { useFieldArray, Control, Controller } from 'react-hook-form';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import DeleteIcon from '@mui/icons-material/Block';
import Divider from '@mui/material/Divider';
import Grid from '@mui/material/Grid';
import SelectField, { ISelectFieldOption } from '../../Common/FormInputs/SelectField';
import TextField from '../../Common/FormInputs/TextField';
import Typography from '@mui/material/Typography';
import capitalize from '@mui/utils/capitalize';
import { IDepartment } from '../../../models/department';
import { getFunctionName, IFunctionModel } from '../../../models/function';
import {
  IUserDepartmentAssignment,
  USER_DEP_ASSIGNMENT_ROLES,
  USER_DEP_ASSIGNMENT_STUDY_LEVELS,
  USER_DEP_ASSIGNMENT_STUDY_TYPE,
  USER_DEP_ASSIGNMENT_TYPE,
} from '../../../models/userDepartmentAssignment';
import LoadingButton from '@mui/lab/LoadingButton';
import Dialog from '../../Common/Dialog';
import { useNotificationContext } from '../../../context/notificationContext';
import { useMutation } from '@tanstack/react-query';
import { deleteUserDepartmentAssignment } from '../../../services/userDepartmentAssignment';
import YesNoToggle from '../../Common/YesNoToggle';

interface IUserDepartmentAssignmentFieldArrayProps {
  changeAssignmentOnly?: boolean;
  departments: IDepartment[];
  functions: IFunctionModel[];
  onRemoveAssignment: (data: IUserDepartmentAssignment) => void;
  control: Control;
  errors: any;
  getValues: any;
  register: any;
  setValue: any;
}

interface IRenderFieldSelectProps {
  index: number;
  name: string;
  control: Control;
  options: ISelectFieldOption[];
  label: string;
  errors: any;
  [key: string]: any;
}
interface IRenderFieldTextProps {
  index: number;
  name: string;
  label: string;
  errors: any;
  register: any;
  [key: string]: any;
}

const isUMF = process.env.NEXT_PUBLIC_IS_UMF === 'true';

const RenderFieldSelect = (props: IRenderFieldSelectProps) => {
  const { index, name, control, options, label, errors } = props;

  const fieldErrors = errors?.userDepartmentAssignments?.[index]?.[name];

  return (
    <>
      <Grid item xs={12} md={4}>
        <Typography variant="body1" sx={{ textAlign: { xs: 'left', md: 'right' } }}>
          {label}
        </Typography>
      </Grid>
      <Grid item xs={12} md={7}>
        <SelectField
          name={`userDepartmentAssignments.${index}.${name}`}
          control={control}
          errorMessage={!!fieldErrors && `${capitalize(label)} necesar`}
          options={options}
        />
      </Grid>
    </>
  );
};

const RenderTextField = (props: IRenderFieldTextProps) => {
  const { index, name, label, errors, register } = props;

  const fieldErrors = errors?.userDepartmentAssignments?.[index]?.[name];

  return (
    <>
      <Grid item xs={12} md={4}>
        <Typography variant="body1" sx={{ textAlign: { xs: 'left', md: 'right' } }}>
          {label}
        </Typography>
      </Grid>
      <Grid item xs={12} md={4}>
        <TextField
          errorMessage={!!fieldErrors && `${capitalize(label)} necesar`}
          {...register(`userDepartmentAssignments.${index}.${name}`)}
        />
      </Grid>

      <Grid item xs={0} md={4} />
    </>
  );
};

const RenderToggleField = (props: IRenderFieldTextProps) => {
  const { index, name, label, control } = props;

  return (
    <>
      <Grid item xs={12} md={4}>
        <Typography variant="body1" sx={{ textAlign: { xs: 'left', md: 'right' } }}>
          {label}
        </Typography>
      </Grid>
      <Grid item xs={12} md={4}>
        <Controller
          name={`userDepartmentAssignments.${index}.${name}`}
          control={control}
          render={({ field }) => {
            const { value, onChange } = field;

            return (
              <>
                <YesNoToggle value={!!value} onValueChange={onChange} />
              </>
            );
          }}
        />
      </Grid>
      <Grid item xs={0} md={4} />
    </>
  );
};

const UserDepartmentAssignmentFieldArray = (props: IUserDepartmentAssignmentFieldArrayProps) => {
  const {
    control,
    changeAssignmentOnly,
    departments,
    functions,
    register,
    errors,
    onRemoveAssignment,
    getValues,
  } = props;

  const [removeAssignment, setRemoveAssignment] = useState<IUserDepartmentAssignment | null>(null);
  const [removeAssignmentIndex, setRemoveAssignmentIndex] = useState<number | null>(null);
  const { setError, setMessage } = useNotificationContext();

  const resetRemove = () => {
    setRemoveAssignment(null);
    setRemoveAssignmentIndex(null);
  };

  const { fields, append, remove } = useFieldArray({
    control,
    name: 'userDepartmentAssignments',
  });

  const { isLoading: isDeletingAssignments, mutate: deleteUserDepartmentAssignmentById } =
    useMutation(deleteUserDepartmentAssignment, {
      onSuccess: async () => {
        removeAssignment && onRemoveAssignment(removeAssignment);
        (removeAssignmentIndex || removeAssignmentIndex === 0) && remove(removeAssignmentIndex);
        resetRemove();
        setMessage('Alocare modificata');
      },
      onError: setError,
    });

  return (
    <>
      {fields.map((item, index) => {
        return (
          <Fragment key={item.id || index}>
            <Grid item xs={12} md={4}>
              <Typography variant="body1" sx={{ textAlign: { xs: 'left', md: 'right' } }}>
                <strong>Alocarea #{index + 1}</strong>
              </Typography>
            </Grid>
            <Grid item xs={12} md={8}>
              {changeAssignmentOnly ? null : (
                <Button
                  onClick={() => {
                    const userDepartmentAssignment =
                      getValues()?.userDepartmentAssignments?.[index];

                    if (userDepartmentAssignment?._id) {
                      setRemoveAssignment(userDepartmentAssignment);
                      setRemoveAssignmentIndex(index);
                    } else {
                      remove(index);
                    }
                  }}
                  color="error"
                  startIcon={<DeleteIcon />}
                >
                  Sterge alocarea
                </Button>
              )}
              <input type="hidden" {...register(`userDepartmentAssignments.${index}._id`)} />
            </Grid>
            <RenderFieldSelect
              name="departmentId"
              index={index}
              control={control}
              errors={errors}
              disabled={changeAssignmentOnly}
              label="Departament"
              options={departments.map((department) => ({
                name: `${department.code} - ${department.name}`,
                value: department._id,
                disabled: department.deactivated,
              }))}
            />

            <Box sx={{ display: 'none' }}>
              <RenderFieldSelect
                name="role"
                index={index}
                control={control}
                errors={errors}
                label="Rol"
                options={Object.values(USER_DEP_ASSIGNMENT_ROLES).map((role) => ({
                  name: role,
                  value: role,
                }))}
              />
            </Box>

            <RenderFieldSelect
              name="type"
              index={index}
              control={control}
              errors={errors}
              label="Tip"
              options={Object.values(USER_DEP_ASSIGNMENT_TYPE).map((type) => ({
                name: type,
                value: type,
              }))}
            />

            {isUMF ? (
              <>
                <RenderFieldSelect
                  name="jobFunction"
                  index={index}
                  control={control}
                  errors={errors}
                  label="Funcție"
                  options={functions.map((functionModel) => ({
                    name: getFunctionName(functionModel),
                    value: functionModel._id,
                  }))}
                />

                <RenderTextField
                  name="personalCategory"
                  index={index}
                  control={control}
                  errors={errors}
                  register={register}
                  label="Cod Categorie Personal"
                />

                <RenderTextField
                  name="jobPlace"
                  index={index}
                  control={control}
                  errors={errors}
                  register={register}
                  label="Cod Loc de Munca"
                />

                <RenderFieldSelect
                  name="studyType"
                  index={index}
                  control={control}
                  errors={errors}
                  label="Nivel Studii"
                  options={Object.values(USER_DEP_ASSIGNMENT_STUDY_TYPE).map((type) => ({
                    name: type,
                    value: type,
                  }))}
                />

                <RenderFieldSelect
                  name="studyLevel"
                  index={index}
                  control={control}
                  errors={errors}
                  label="Grad/ treaptă profesională"
                  options={Object.values(USER_DEP_ASSIGNMENT_STUDY_LEVELS).map((type) => ({
                    name: type,
                    value: type,
                  }))}
                />
              </>
            ) : null}

            <RenderToggleField
              name="excludeFromEvaluations"
              index={index}
              control={control}
              label="Exclude din evaluari"
              errors={null}
              register={console.log}
            />

            {fields?.length > 1 ? (
              <Grid item xs={12}>
                <Divider />
              </Grid>
            ) : null}
          </Fragment>
        );
      })}

      <Button
        id="userDepartmentAssignmentAddButton"
        onClick={() =>
          append({
            type: USER_DEP_ASSIGNMENT_TYPE.PROF,
            role: USER_DEP_ASSIGNMENT_ROLES.EVALUATEE,
          })
        }
        type="button"
        sx={{ display: 'none' }}
      >
        add
      </Button>

      <Dialog
        actions={
          <LoadingButton
            variant="contained"
            onClick={() =>
              removeAssignment && deleteUserDepartmentAssignmentById(removeAssignment._id)
            }
            loading={isDeletingAssignments}
          >
            Anulează alocarea
          </LoadingButton>
        }
        content={
          <>
            <Typography color="textSecondary" textAlign="center">
              Sunteți siguri că doriți să anulați alocarea din departamentul:
              <strong>
                {/* @ts-ignore */}
                <i>
                  &quot;
                  {
                    // @ts-ignore
                    removeAssignment &&
                      departments?.find((dep) => dep._id === removeAssignment.departmentId)?.name
                  }
                  &quot;
                </i>
              </strong>
              ?
            </Typography>
          </>
        }
        title="Confirmati anularea alocării?"
        onClose={resetRemove}
        open={!!removeAssignment}
      />
    </>
  );
};

export default UserDepartmentAssignmentFieldArray;
