import React from 'react';
import ButtonBase from '@mui/material/ButtonBase';
import Box from '@mui/material/Box';
import Card from '@mui/material/Card';
import Checkbox from '@mui/material/Checkbox';
import Grid from '@mui/material/Grid';
import Typography from '@mui/material/Typography';
import { IUser } from '../../../models/user';

interface IUserSelectionListProps {
  users: IUser[];
  selectedUsers: IUser[];
  onSelectUser: (org: IUser) => void;
  onRemoveUser: (index: number) => void;
}

const UserSelectionList = (props: IUserSelectionListProps) => {
  const { users, selectedUsers, onSelectUser, onRemoveUser } = props;

  return (
    <Grid container spacing={3} alignItems="stretch">
      {users.map((user, index) => {
        const isSelected = selectedUsers.find((u) => u._id === user._id);
        const onClick = !!isSelected ? () => onRemoveUser(index) : () => onSelectUser(user);

        return (
          <Grid item xs={12} sm={6} md={4} key={user._id}>
            <ButtonBase onClick={onClick} sx={{ width: '100%', height: '100%' }}>
              <Card
                elevation={isSelected ? 8 : 1}
                sx={{
                  flexGrow: 1,
                  p: 2,
                  display: 'flex',
                  alignItems: 'center',
                  textAlign: 'left',
                  height: '100%',
                }}
              >
                <Box sx={{ mr: 2 }}>
                  <Checkbox checked={!!isSelected} />
                </Box>
                <Box>
                  <Typography>{user.fullName}</Typography>
                  <Typography
                    sx={{ mb: -1, display: 'block' }}
                    variant="caption"
                    color="textSecondary"
                  >
                    {user.email}
                  </Typography>
                </Box>
              </Card>
            </ButtonBase>
          </Grid>
        );
      })}
    </Grid>
  );
};

export default UserSelectionList;
