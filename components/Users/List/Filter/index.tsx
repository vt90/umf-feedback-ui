import React from 'react';
import SearchIcon from '@mui/icons-material/Search';
import Box from '@mui/material/Box';
import Checkbox from '@mui/material/Checkbox';
import FormControlLabel from '@mui/material/FormControlLabel';
import InputAdornment from '@mui/material/InputAdornment';
import MenuItem from '@mui/material/MenuItem';
import Select from '@mui/material/Select';
import Typography from '@mui/material/Typography';
import { useDebouncedCallback } from 'use-debounce';
import OrderToggle from '../../../Common/OrderToggle';
import TextField from '../../../Common/FormInputs/TextField';

interface IUserListFilterProps {
  order: number;
  setOrder: (newOrder: number) => void;
  orderBy: string;
  setOrderBy: (newOrderBy: string) => void;
  searchTerm: string;
  setSearchTerm: (newTerm: string) => void;
  showDeactivated: boolean;
  setShowDeactivated: (value: boolean) => void;
}

const ORDER_BY = {
  name: {
    name: 'nume',
    value: 'fullName',
  },
  email: {
    name: 'email',
    value: 'email',
  },
};

const UserListFilter = (props: IUserListFilterProps) => {
  const {
    order,
    orderBy,
    searchTerm,
    setOrder,
    setOrderBy,
    setSearchTerm,
    showDeactivated,
    setShowDeactivated,
  } = props;

  const debouncedChange = useDebouncedCallback(setSearchTerm, 250);

  return (
    <>
      <Box
        sx={{
          display: 'flex',
          alignItems: 'center',
          flexWrap: 'wrap',
          mb: 2,
          bordercolor: 'divider',
        }}
      >
        <Box sx={{ flexGrow: 1 }}>
          <TextField
            size="small"
            fullWidth={false}
            defaultValue={searchTerm}
            onChange={(ev: any) => debouncedChange(ev?.target?.value)}
            InputProps={{
              startAdornment: (
                <InputAdornment position="start">
                  <SearchIcon />
                </InputAdornment>
              ),
              // ...(searchTerm && {
              //   endAdornment: (
              //     <InputAdornment position="start">
              //       <IconButton onClick={() => debouncedChange('')}>
              //         <Close fontSize="small" />
              //       </IconButton>
              //     </InputAdornment>
              //   ),
              // }),
            }}
          />
        </Box>
        <Box>
          <FormControlLabel
            control={
              <Checkbox
                size="small"
                checked={showDeactivated}
                onChange={() => setShowDeactivated(!showDeactivated)}
              />
            }
            label="Include dezactivati"
            labelPlacement="start"
            sx={{ mt: 1 }}
            componentsProps={{
              typography: {
                variant: 'body2',
                color: 'textSecondary',
              },
            }}
          />
        </Box>
        <Box
          sx={{
            width: '1px',
            mx: 2,
            py: 1,
            bgcolor: 'divider',
          }}
        />
        <Box sx={{ display: 'flex', alignItems: 'center' }}>
          <Typography variant="body2" color="textSecondary">
            Ordonează după
          </Typography>

          <Select
            value={orderBy}
            onChange={(ev) => setOrderBy(ev.target.value)}
            size="small"
            sx={{
              boxShadow: 'none',
              '.MuiOutlinedInput-notchedOutline': {
                border: 0,
                outline: 'none',
              },
            }}
          >
            {Object.values(ORDER_BY).map((criteria) => (
              <MenuItem key={criteria.value} value={criteria.value}>
                {criteria.name}
              </MenuItem>
            ))}
          </Select>

          <Box>
            <OrderToggle order={order} setOrder={setOrder} />
          </Box>
        </Box>
      </Box>
    </>
  );
};

export default UserListFilter;
