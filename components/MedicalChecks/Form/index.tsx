import React from 'react';
import * as yup from 'yup';
import { Controller, useForm, useFieldArray } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogTitle from '@mui/material/DialogTitle';
import Grid from '@mui/material/Grid';
import Typography from '@mui/material/Typography';
import { getSSMFileDisplayName } from '../../../models/ssmPackage';
import FileUploadField from '../../Common/FormInputs/FileUploadField';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemAvatar from '@mui/material/ListItemAvatar';
import Avatar from '@mui/material/Avatar';
import File from '@mui/icons-material/InsertDriveFile';
import ListItemText from '@mui/material/ListItemText';
import ListItemSecondaryAction from '@mui/material/ListItemSecondaryAction';
import IconButton from '@mui/material/IconButton';
import Delete from '@mui/icons-material/Delete';
import ToggleField from '../../Common/FormInputs/ToggleField';
import DatePickerField from '../../Common/FormInputs/DatePickerField';
import { IMedicalCheck, MEDICAL_CHECK_RESULT_TYPES } from '../../../models/medicalCheck';
import dynamic from 'next/dynamic';
import { IUser } from '../../../models/user';
import SelectField from '../../Common/FormInputs/SelectField';
import moment from 'moment';
const RichText = dynamic(() => import('../../Common/RichText'), {
  ssr: false,
});

interface IMedicalCheckFormProps {
  initialValues: Partial<IMedicalCheck>;
  users: IUser[];
  isLoading: boolean;
  onClose: () => void;
  onSubmit: (data: any) => void;
  open: boolean;
}
const schema = yup
  .object({
    userId: yup.string().required(),
    startDate: yup.string().required(),
    endDate: yup.string().required(),
  })
  .required();

const MedicalCheckForm = (props: IMedicalCheckFormProps) => {
  const { isLoading, initialValues, users, open, onClose, onSubmit } = props;
  const {
    register,
    handleSubmit,
    formState: { errors },
    control,
    setValue,
    watch,
  } = useForm({
    defaultValues: initialValues,
    mode: 'onTouched',
    resolver: yupResolver(schema),
    shouldUnregister: true,
  });

  // @ts-ignore
  const { fields, remove } = useFieldArray({
    // @ts-ignore
    name: 'documents',
    control,
  });

  // @ts-ignore
  const docs = watch('documents');

  return (
    <Dialog
      open={open}
      fullWidth={true}
      maxWidth="lg"
      keepMounted={false}
      onClose={onClose}
      // @ts-ignore
      PaperComponent="form"
      onSubmit={handleSubmit(onSubmit)}
    >
      <DialogTitle sx={{ typography: 'h5', textAlign: 'center' }}>Fișă de aptitudini</DialogTitle>
      <DialogContent>
        <input type="hidden" {...register(`_id`)} />
        <Grid container spacing={3} alignItems="center">
          <Grid item xs={12} md={3}>
            <Typography variant="body2" sx={{ textAlign: { xs: 'left', md: 'right' } }}>
              Rezultat*
            </Typography>
          </Grid>
          <Grid item xs={12} md={8}>
            <ToggleField
              name="result"
              control={control}
              disabled={!!initialValues._id}
              defaultValue={initialValues.result}
              options={[
                { name: 'Apt', value: MEDICAL_CHECK_RESULT_TYPES.APT },
                { name: 'Apt condiționat', value: MEDICAL_CHECK_RESULT_TYPES.APT_CONDITIONAT },
                { name: 'Inapt condiționat', value: MEDICAL_CHECK_RESULT_TYPES.INAPT_CONDITIONAT },
                { name: 'Inapt', value: MEDICAL_CHECK_RESULT_TYPES.INAPT },
              ]}
            />
          </Grid>
          {initialValues?._id ? (
            <>
              <Grid item xs={12} md={3} key="title">
                <Typography variant="body2" sx={{ textAlign: { xs: 'left', md: 'right' } }}>
                  Realizată de către
                </Typography>
              </Grid>
              <Grid item xs={12} md={8} key="value">
                <Typography>{initialValues?.createdBy?.fullName}</Typography>
              </Grid>
            </>
          ) : null}
          <Grid item xs={12} md={3}>
            <Typography variant="body2" sx={{ textAlign: { xs: 'left', md: 'right' } }}>
              Angajat*
            </Typography>
          </Grid>
          <Grid item xs={12} md={8}>
            {initialValues?._id ? (
              (() => {
                // @ts-ignore
                const user = users?.find((u) => u._id === initialValues?.userId);

                return (
                  <>
                    <input type="hidden" {...register(`userId`)} />
                    <Typography>{user?.fullName}</Typography>
                  </>
                );
              })()
            ) : (
              <SelectField
                name="userId"
                control={control}
                errorMessage={!!errors.userId && 'Angajat necesar'}
                options={users.map((user) => ({
                  name: user.fullName,
                  value: user._id,
                }))}
              />
            )}
          </Grid>
          <Grid item xs={12} md={3}>
            <Typography variant="body2" sx={{ textAlign: { xs: 'left', md: 'right' } }}>
              Valabilă din*
            </Typography>
          </Grid>
          <Grid item xs={12} md={8}>
            {initialValues?._id ? (
              <>
                <input type="hidden" {...register(`startDate`)} />
                <Typography>{moment(initialValues.startDate).format('DD.MM.YYYY')}</Typography>
              </>
            ) : (
              <DatePickerField
                name="startDate"
                control={control}
                defaultValue={initialValues.startDate}
              />
            )}
          </Grid>
          <Grid item xs={12} md={3}>
            <Typography variant="body2" sx={{ textAlign: { xs: 'left', md: 'right' } }}>
              Valabilă până în*
            </Typography>
          </Grid>
          <Grid item xs={12} md={8}>
            {initialValues?._id ? (
              <>
                <input type="hidden" {...register(`endDate`)} />
                <Typography>{moment(initialValues.endDate).format('DD.MM.YYYY')}</Typography>
              </>
            ) : (
              <DatePickerField
                name="endDate"
                control={control}
                defaultValue={initialValues.endDate}
              />
            )}
          </Grid>
          <Grid item xs={12} md={3} alignSelf="flex-start">
            <Typography variant="body2" sx={{ textAlign: { xs: 'left', md: 'right' } }}>
              Descriere*
            </Typography>
          </Grid>
          <Grid item xs={12} md={8}>
            <Controller
              render={({ field }) => {
                const { value, onChange } = field;

                return (
                  <>
                    <RichText
                      disableStickyToolbar={true}
                      defaultValue={value}
                      onChange={(value) => onChange(value)}
                      readOnly={!!initialValues?._id}
                      sx={{
                        // @ts-ignore
                        border: (theme) => `1px solid ${theme.palette.divider}!important`,
                        boxShadow: 0,
                        '& .ql-container': {
                          border: 'none !important',
                          minHeight: '300px',
                          px: 2,
                          pb: 2,
                        },
                      }}
                    />
                  </>
                );
              }}
              name="description"
              control={control}
            />
          </Grid>
          <Grid item xs={12} md={3} alignSelf="flex-start">
            <Typography variant="body2" sx={{ textAlign: { xs: 'left', md: 'right' } }}>
              Atașare Documente
            </Typography>
          </Grid>
          <Grid item xs={12} md={8}>
            <FileUploadField name="files" control={control} setValue={setValue} multiple />

            <List>
              {/* @ts-ignore */}
              {fields?.map((document, index) => (
                <ListItem key={document.id}>
                  {/* @ts-ignore */}
                  <input type="hidden" {...register(`documents.${index}`)} />
                  <ListItemAvatar>
                    <Avatar>
                      <File />
                    </Avatar>
                  </ListItemAvatar>
                  {/* @ts-ignore */}
                  <ListItemText
                    // @ts-ignore
                    primary={getSSMFileDisplayName(docs[index])}
                    primaryTypographyProps={{
                      component: 'a',
                      target: '_blank',
                      // @ts-ignore
                      href: docs[index],
                    }}
                  />
                  <ListItemSecondaryAction>
                    <IconButton onClick={() => remove(index)}>
                      <Delete />
                    </IconButton>
                  </ListItemSecondaryAction>
                </ListItem>
              ))}
            </List>
          </Grid>
        </Grid>
      </DialogContent>
      <DialogActions sx={{ py: 2, justifyContent: 'center', bgcolor: 'background.default' }}>
        <Button color="inherit" disabled={isLoading} onClick={onClose}>
          {initialValues?._id ? 'Inchide' : 'Anulare'}
        </Button>

        <Button disabled={isLoading} variant="contained" type="submit">
          {!initialValues?._id ? 'Crează' : 'Modifică'}
        </Button>
        {/*{initialValues?._id ? null : (*/}
        {/*  <Button disabled={isLoading} variant="contained" type="submit">*/}
        {/*    Crează*/}
        {/*  </Button>*/}
        {/*)}*/}
      </DialogActions>
    </Dialog>
  );
};

export default MedicalCheckForm;
