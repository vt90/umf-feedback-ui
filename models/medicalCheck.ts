import { IBaseModel } from './base';
import { IUser } from './user';

export enum MEDICAL_CHECK_RESULT_TYPES {
  APT,
  APT_CONDITIONAT,
  INAPT_CONDITIONAT,
  INAPT,
}

export const getMedicalCheckResultText = (result: number): string => {
  switch (result) {
    case MEDICAL_CHECK_RESULT_TYPES.INAPT:
      return 'Inapt';
    case MEDICAL_CHECK_RESULT_TYPES.INAPT_CONDITIONAT:
      return 'Inapt conditionat';
    case MEDICAL_CHECK_RESULT_TYPES.APT_CONDITIONAT:
      return 'Apt conditionat';
    default:
      return 'Apt';
  }
};

export interface IMedicalCheck extends IBaseModel {
  userId: IUser;
  createdBy: IUser;
  startDate: string;
  endDate: string;
  description: string;
  documents: [];
  result: MEDICAL_CHECK_RESULT_TYPES;
  isActive: boolean;
  isAccepted: boolean;
  acceptedTimestamp: boolean;
}
