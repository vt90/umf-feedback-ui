import axios from 'axios';
import { ISSMPackage } from '../models/ssmPackage';
import { IPaginationResult } from '../models/base';

const createInstance = (baseURL = '/api/ssm-packages') => {
  const httpInstance = axios.create({
    baseURL,
  });

  httpInstance.interceptors.response.use(
    (response) => response.data,
    (error) => {
      if (error && error.response && error.response.data) {
        return Promise.reject(
          error.response.data.message
            ? `${error.response.data.message || error.response.data.error}`
            : error.response.data?.error || error.response.data,
        );
      } else {
        return Promise.reject({ message: 'Server error' });
      }
    },
  );

  return httpInstance;
};

const httpInstance = createInstance();

const cleanupSSMPackageModelBody = (ssmPackage: ISSMPackage): FormData => {
  // @ts-ignore
  delete ssmPackage._id;
  // @ts-ignore
  delete ssmPackage.id;
  // @ts-ignore
  delete ssmPackage.createdAt;
  // @ts-ignore
  delete ssmPackage.updatedAt;
  // @ts-ignore
  delete ssmPackage['__v'];

  const formData = new FormData();

  for (const [key, value] of Object.entries(ssmPackage)) {
    if (key === 'functions') {
      if (value && !value.length) {
        formData.append('emptyFunctions', 'true');
      }
      value?.forEach((value: string, index: number) => {
        formData.append(`${key}[${index}]`, value);
      });
    } else if (key === 'departments') {
      if (value && !value.length) {
        formData.append('emptyDepartments', 'true');
      }
      value?.forEach((value: string, index: number) => {
        formData.append(`${key}[${index}]`, value);
      });
    } else if (key === 'excludedDepartments') {
      if (value && !value.length) {
        formData.append('emptyExcludedDepartments', 'true');
      }
      value?.forEach((value: string, index: number) => {
        formData.append(`${key}[${index}]`, value);
      });
    } else if (key === 'documents') {
      value?.forEach((value: string, index: number) => {
        formData.append(`${key}[${index}]`, value);
      });
    } else if (key === 'files') {
      value?.forEach((value: string, index: number) => {
        formData.append(key, value);
      });
    } else {
      formData.append(key, value);
    }
  }

  return formData;
};

export const findSSMPackage = async (
  params: Partial<ISSMPackage>,
): Promise<IPaginationResult<ISSMPackage>> => {
  return await httpInstance.get('', { params });
};

export const getAllSSMPackages = async (): Promise<IPaginationResult<ISSMPackage>> => {
  return await httpInstance.get('', {});
};

export const createOrUpdateSSMPackage = async (ssmPackage: ISSMPackage): Promise<ISSMPackage> => {
  const isUpdate = !!ssmPackage._id;
  const url = isUpdate ? `/${ssmPackage._id}` : ``;
  const httpInstance = createInstance(`${process.env.NEXT_PUBLIC_API_URL}/api/ssm-packages`);
  const req = isUpdate ? httpInstance.patch : httpInstance.post;
  const { data } = await axios.get('/api/get-access-token');

  return await req(url, cleanupSSMPackageModelBody(ssmPackage), {
    headers: {
      Authorization: `Bearer ${data?.accessToken}`,
    },
  });
};

export const deleteSSMPackage = async (id: string): Promise<any> => {
  return await httpInstance.delete(`/${id}`);
};
