import React from 'react';
import Box from '@mui/material/Box';
import Divider from '@mui/material/Divider';
import PageHeader from '../Layout/PageHeader';
import { BreadcrumbInfo } from '../Layout/Breadcrumbs';
import Container from '@mui/material/Container';
import { VIEWPORT_MAX_WIDTH } from '../Layout';

interface IAdminPageProps {
  actions?: React.ReactNode;
  children: React.ReactNode;
  pages: BreadcrumbInfo[];
  title: string | React.ReactNode;
}

const AdminPage = (props: IAdminPageProps) => {
  const { actions, children, pages, title } = props;

  const CONTAINER_HEIGHT = 'calc(100vh - 210px)';

  return (
    <>
      <PageHeader pages={pages} title={title} />

      <Box
        sx={{
          pt: 3,
          pb: 7,
          minHeight: CONTAINER_HEIGHT,
        }}
      >
        {children}
      </Box>

      {actions ? (
        <Box
          sx={{
            backdropFilter: 'saturate(110%) blur(3px)',
            backgroundColor: 'hsla(0,0%,100%,.2)!important',
            position: 'fixed',
            bottom: 0,
            left: 0,
            width: '100vw',
          }}
        >
          <Divider />
          <Container maxWidth={VIEWPORT_MAX_WIDTH}>
            <Box sx={{ my: 1, px: 1, display: 'flex', justifyContent: 'flex-end' }}>{actions}</Box>
          </Container>
        </Box>
      ) : null}
    </>
  );
};

export default AdminPage;
