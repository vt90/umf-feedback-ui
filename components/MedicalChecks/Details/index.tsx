import React from 'react';
import moment from 'moment/moment';
import dynamic from 'next/dynamic';
import Grid from '@mui/material/Grid';
import Box from '@mui/material/Box';
import ButtonBase from '@mui/material/ButtonBase';
import Typography from '@mui/material/Typography';
import Avatar from '@mui/material/Avatar';
import File from '@mui/icons-material/InsertDriveFile';
import IconButton from '@mui/material/IconButton';
import ImageListItemBar from '@mui/material/ImageListItemBar';
import InfoIcon from '@mui/icons-material/Download';
import { getMedicalCheckResultText, IMedicalCheck } from '../../../models/medicalCheck';
import { getSSMFileDisplayName } from '../../../models/ssmPackage';
import { isImage } from '../../../lib/constants';

const RichText = dynamic(() => import('../../Common/RichText'), {
  ssr: false,
});

interface IMedicalCheckDetailsProps {
  medicalCheck?: IMedicalCheck;
}

const MedicalCheckDetails = (props: IMedicalCheckDetailsProps) => {
  const { medicalCheck } = props;

  if (!medicalCheck) return null;

  return (
    <Grid container spacing={3}>
      <Grid item xs={12} md={3}>
        <Typography variant="body2" sx={{ textAlign: { xs: 'left', md: 'right' } }}>
          Rezultat*
        </Typography>
      </Grid>
      <Grid item xs={12} md={8}>
        <Typography>{getMedicalCheckResultText(medicalCheck?.result)}</Typography>
      </Grid>
      <Grid item xs={12} md={3} key="title">
        <Typography variant="body2" sx={{ textAlign: { xs: 'left', md: 'right' } }}>
          Realizată de către
        </Typography>
      </Grid>
      <Grid item xs={12} md={8} key="value">
        <Typography>{medicalCheck?.createdBy?.fullName}</Typography>
      </Grid>
      <Grid item xs={12} md={3}>
        <Typography variant="body2" sx={{ textAlign: { xs: 'left', md: 'right' } }}>
          Valabilă din*
        </Typography>
      </Grid>
      <Grid item xs={12} md={8}>
        <Typography>{moment(medicalCheck.startDate).format('DD.MM.YYYY')}</Typography>
      </Grid>
      <Grid item xs={12} md={3}>
        <Typography variant="body2" sx={{ textAlign: { xs: 'left', md: 'right' } }}>
          Valabilă până în*
        </Typography>
      </Grid>
      <Grid item xs={12} md={8}>
        <Typography>{moment(medicalCheck.endDate).format('DD.MM.YYYY')}</Typography>
      </Grid>
      <Grid item xs={12} md={3} alignSelf="flex-start">
        <Typography variant="body2" sx={{ textAlign: { xs: 'left', md: 'right' } }}>
          Documente Atașate
        </Typography>
      </Grid>
      <Grid item xs={12} md={8} container spacing={2}>
        {/* @ts-ignore */}
        {medicalCheck?.documents?.map((document) => {
          const avatarProps = isImage(document) ? { src: document, alt: '' } : {};

          return (
            <Grid item xs={10} sm={5} md={4} key={document}>
              <ButtonBase sx={{ display: 'block' }} component="a" target="_blank" href={document}>
                <Box sx={{ position: 'relative' }}>
                  <Avatar
                    variant="rounded"
                    sx={{
                      height: {
                        xs: 150,
                        sm: 225,
                      },
                      width: '100%',
                      mr: 2,
                      boxShadow: 'rgba(0, 0, 0, 0.12) 0rem 0.3125rem 0.625rem 0rem',
                      bgcolor: 'action.disabled',
                    }}
                    {...avatarProps}
                  >
                    <File sx={{ fontSize: 80 }} />
                  </Avatar>

                  <ImageListItemBar
                    title={getSSMFileDisplayName(document)}
                    actionIcon={
                      <IconButton sx={{ color: 'rgba(255, 255, 255, 0.54)' }}>
                        <InfoIcon />
                      </IconButton>
                    }
                  />
                </Box>
              </ButtonBase>
            </Grid>
          );
        })}
      </Grid>
      <Grid item xs={12} md={3} />
      <Grid item xs={12} md={8}>
        <RichText
          disableStickyToolbar={true}
          defaultValue={medicalCheck?.description}
          readOnly={true}
          sx={{
            // @ts-ignore
            border: (theme) => `1px solid ${theme.palette.divider}!important`,
            boxShadow: 0,
            '& .ql-container': {
              border: 'none !important',
              minHeight: '300px',
              px: 2,
              pb: 2,
            },
          }}
        />
      </Grid>
    </Grid>
  );
};

export default MedicalCheckDetails;
