import React from 'react';
import {
  IEvaluationEvent,
  IEvaluation,
  EVALUATION_ACTIVITY_TYPES,
} from '../../../../models/evaluation';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import Timeline from '@mui/lab/Timeline';
import TimelineItem from '@mui/lab/TimelineItem';
import TimelineSeparator from '@mui/lab/TimelineSeparator';
import TimelineConnector from '@mui/lab/TimelineConnector';
import TimelineContent from '@mui/lab/TimelineContent';
import TimelineDot from '@mui/lab/TimelineDot';
import TimelineOppositeContent, {
  timelineOppositeContentClasses,
} from '@mui/lab/TimelineOppositeContent';
import moment from 'moment';
import Status from '../../../Common/Status';
import isBoolean from 'lodash/isBoolean';

interface IEvaluationEventListProps {
  evaluation: IEvaluation;
}

const EvaluationEventList = (props: IEvaluationEventListProps) => {
  const { evaluation } = props;

  const renderEvaluationEvent = (event: IEvaluationEvent) => {
    let eventStatus;
    let eventStatusColor = 'success';

    switch (event.type) {
      case EVALUATION_ACTIVITY_TYPES.CREATED:
        eventStatus = 'Evaluare creată';
        break;
      case EVALUATION_ACTIVITY_TYPES.MODIFIED:
        eventStatus = 'Evaluare modificată';
        break;
      case EVALUATION_ACTIVITY_TYPES.CONTRA_SIGNED:
        eventStatus = 'Evaluare contra-semnată';
        break;
      case EVALUATION_ACTIVITY_TYPES.USER_ACKNOWLEDGEMENT:
        eventStatus = 'Evaluare luată la cunostinta';
        break;
      case EVALUATION_ACTIVITY_TYPES.SALARY_CHANGE_REFUSED:
        eventStatus = 'Modificare salarială refuzată';
        break;
      case EVALUATION_ACTIVITY_TYPES.USER_APPROVAL:
        // @ts-ignore
        const value = JSON.parse(event.value);
        eventStatus = `Evaluare ${value ? 'acceptată' : 'respinsă'}`;
        if (!value) {
          eventStatusColor = 'error';
        }
        break;
    }

    const renderComment = () => {
      if (!event.comment && event.type === EVALUATION_ACTIVITY_TYPES.MODIFIED) return null;

      if (event.type === EVALUATION_ACTIVITY_TYPES.MODIFIED) {
        const commentData = JSON.parse(event.comment || '{}');
        return (
          <Box>
            {/* @ts-ignore */}
            {commentData.map((change) => {
              const previousValue = isBoolean(change?.previousValue)
                ? change?.previousValue
                  ? 'Da'
                  : 'Nu'
                : change?.previousValue;

              const newValue = isBoolean(change?.newValue)
                ? change?.newValue
                  ? 'Da'
                  : 'Nu'
                : change?.newValue;

              return (
                <Typography key={change?.text}>
                  {change?.text}: {previousValue ? `${previousValue} →` : ''} {newValue}
                </Typography>
              );
            })}
          </Box>
        );
      }

      return <Typography gutterBottom>{event.comment}</Typography>;
    };

    return (
      <TimelineItem key={event._id}>
        <TimelineOppositeContent color="textSecondary">
          {moment(event.createdAt).format('YYYY.MM.DD')}
        </TimelineOppositeContent>
        <TimelineSeparator>
          <TimelineDot />
          <TimelineConnector />
        </TimelineSeparator>
        <TimelineContent>
          <Box>
            {/* @ts-ignore */}
            <Status label={eventStatus} color={eventStatusColor} sx={{ minWidth: 160 }} />

            <Box sx={{ mt: 1.5, pb: 2 }}>
              {renderComment()}
              {/* @ts-ignore */}
              <Typography color="textSecondary">{event?.userId?.fullName}</Typography>
            </Box>
          </Box>
        </TimelineContent>
      </TimelineItem>
    );
  };

  return (
    <>
      {evaluation?.events?.length ? (
        <>
          <Typography gutterBottom>
            <strong>Observații sau comentarii</strong>
          </Typography>
          <Timeline
            sx={{
              pl: 0,
              [`& .${timelineOppositeContentClasses.root}`]: {
                pl: 0,
                maxWidth: 87,
              },
            }}
          >
            {evaluation?.events.map((event) => {
              return renderEvaluationEvent(event);
            })}
          </Timeline>
        </>
      ) : null}
    </>
  );
};

export default EvaluationEventList;
