import React, { useCallback, useMemo } from 'react';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import Link from 'next/link';
import Tooltip from '@mui/material/Tooltip';
import isBoolean from 'lodash/isBoolean';
import EnhancedTable from '../../Common/Table';
import { IEvaluation } from '../../../models/evaluation';
import moment from 'moment';
import NoDataAvailable from '../../Common/NoDataAvailable';

import YesIcon from '@mui/icons-material/CheckBox';
import NoIcon from '@mui/icons-material/DisabledByDefault';
import WaitingIcon from '@mui/icons-material/HourglassBottom';
import { getSurveyRoleName } from '../../../models/survey';
// import Status from '../../Common/Status';

interface IUserUserListProps {
  evaluations: IEvaluation[];
}

const HRUserEvaluationsList = (props: IUserUserListProps) => {
  const { evaluations } = props;

  const headerRows = useMemo(() => {
    return [
      { id: 'name', label: 'Membru Evaluat', align: 'left', colSpan: 2 },
      { id: 'email', label: 'Marca', align: 'left', colSpan: 1 },
      { id: 'department', label: 'Departament', align: 'left', colSpan: 3 },
      { id: 'session', label: 'Sesiune', align: 'left', colSpan: 3 },
      { id: 'role', label: 'Tip', align: 'left', colSpan: 2 },
      { id: 'createdAt', label: 'Data evaluări', align: 'left', colSpan: 2, disableSorting: true },
      { id: 'result', label: 'Rezultat', align: 'left', colSpan: 2 },
      {
        id: 'salaryChangePercentage',
        label: 'Procent Modificare Salarială',
        align: 'left',
        colSpan: 2,
      },
      { id: 'userAgreement', label: 'Opinie evaluat', align: 'center', colSpan: 1 },
      { id: 'contraEvaluated', label: 'Contra-evaluată', align: 'center', colSpan: 1 },
      { id: 'userAcknowledged', label: 'Luată la cunoștință', align: 'center', colSpan: 1 },
    ];
  }, []);

  const renderIcon = useCallback((value: boolean | null | undefined, yesText: string) => {
    const color = isBoolean(value) ? (value ? 'success' : 'error') : 'info';
    const tooltipText = isBoolean(value) ? (value ? yesText : 'Respinsă') : 'În așteptare';

    const iconProps = {
      sx: {
        color: `${color}.light`,
      },
    };

    return (
      <Tooltip title={tooltipText}>
        <Box>
          {isBoolean(value) ? (
            value ? (
              // @ts-ignore
              <YesIcon {...iconProps} />
            ) : (
              // @ts-ignore
              <NoIcon {...iconProps} />
            )
          ) : (
            // @ts-ignore
            <WaitingIcon {...iconProps} />
          )}
        </Box>
      </Tooltip>
    );
  }, []);

  const getHeaderRows = useCallback(() => {
    return evaluations.map((evaluation) => {
      const {
        _id,
        createdAt,
        departmentId,
        evaluationSessionId,
        userDepartmentAssignmentId,
        contraEvaluated,
        role,
        finalResult,
        finalGrade,
        userAcknowledged,
        userAgreement,
        userId,
        salaryChangePercentage,
      } = evaluation;

      // @ts-ignore
      const linkUrl = `/evaluation-sessions/${evaluationSessionId._id}/result/${userDepartmentAssignmentId?._id}?resultId=${_id}`;

      const renderLink = (children: React.ReactNode) => (
        <Link href={linkUrl}>
          <a>{children}</a>
        </Link>
      );

      // @ts-ignore
      const renderName = renderLink(
        <Typography>
          {/* @ts-ignore */}
          {userId.fullName}
        </Typography>,
      );

      const renderRole = (role: string) => {
        // const color = role === SURVEY_ROLE.EVALUATION
        //   ? 'info'
        //   : 'success';
        // return <Status label={getSurveyRoleName(role)} color={color} sx={{ minWidth: 130 }} />;
        return getSurveyRoleName(role);
      };

      return {
        _id,
        smallDeviceHeader: renderName,
        name: renderName,
        // @ts-ignore
        email: userId?.email,
        role: renderRole(`${role}`),
        // @ts-ignore
        session: evaluationSessionId?.name,
        department: (
          // @ts-ignore
          <Tooltip title={userDepartmentAssignmentId?.role}>
            <Typography variant="body2" color="textSecondary">
              {/* @ts-ignore */}
              {departmentId?.name}
            </Typography>
          </Tooltip>
        ),
        // @ts-ignore
        result: finalGrade
          ? `${finalGrade.toFixed(2)}${finalResult ? ` - ${finalResult}` : ''}`
          : '-',
        salaryChangePercentage: salaryChangePercentage ? `${salaryChangePercentage}%` : '-',
        createdAt: moment(createdAt).format('YYYY.MM.DD'),
        contraEvaluated: renderIcon(contraEvaluated ? true : null, 'Contra-evaluată'),
        userAcknowledged: renderIcon(userAcknowledged, 'Luată la cunoștință'),
        userAgreement: renderIcon(userAgreement, 'Acceptată'),
        data: evaluation,
      };
    });
  }, [evaluations, renderIcon]);

  if (!evaluations?.length) {
    return (
      <NoDataAvailable content="În momentul de faţă nu există niciun formular de evaluare disponibil pe platformă" />
    );
  }

  return (
    <>
      <EnhancedTable
        headerRows={headerRows}
        initialOrderByField="createdAt"
        initialOrder="desc"
        orderByFieldCriteriaFunctions={{
          createdAt: (row: any) => row?.data?.createdAt,
          department: (row: any) => row?.data?.departmentId?.name,
          result: (row: any) => row?.data?.finalGrade,
          contraEvaluated: (row: any) => row?.data?.contraEvaluated,
          userAcknowledged: (row: any) => row?.data?.userAcknowledged,
          userAgreement: (row: any) => row?.data?.userAgreement,
          contraEvaluatorAgreement: (row: any) => row?.data?.contraEvaluatorAgreement,
          salaryChangePercentage: (row: any) => row?.data?.salaryChangePercentage,
        }}
        PaperComponent={Box}
        paperComponentStyles={{
          px: 3,
          py: 2,
          border: '0.0625rem solid rgb(222, 226, 230)',
          borderRadius: 2,
          '& h6': {
            mt: 1,
          },
        }}
        rows={getHeaderRows()}
        smallUntilBreakpoint="md"
      />
    </>
  );
};

export default HRUserEvaluationsList;
