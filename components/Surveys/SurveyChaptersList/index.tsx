import React, { useCallback, useEffect, useState } from 'react';
import { useMutation } from '@tanstack/react-query';
import Button from '@mui/material/Button';
import AddIcon from '@mui/icons-material/Add';
import Box from '@mui/material/Box';
import LoadingButton from '@mui/lab/LoadingButton';
import Typography from '@mui/material/Typography';
import SurveyChapterForm from '../SurveyChaptersForm';
import SurveyChapterListItem from './SurveyChapterListItem';
import Dialog from '../../Common/Dialog';
import NoDataAvailable from '../../Common/NoDataAvailable';
import { ISurvey } from '../../../models/survey';
import { ISurveyChapter } from '../../../models/surveyChapter';
import { deleteSurveyChapter } from '../../../services/surveyChapters';

interface ISurveyChaptersListProps {
  isLoading: boolean;
  onSubmit: (data: any) => void;
  onRemoveSurveyChapter: (surveyChapter: ISurveyChapter) => void;
  survey: ISurvey;
}

const SurveyChaptersList = (props: ISurveyChaptersListProps) => {
  const { isLoading, onSubmit, onRemoveSurveyChapter, survey } = props;
  const surveyId = survey._id;

  const [createFormValues, setCreateFormValues] = useState<Partial<ISurveyChapter> | null>(null);
  const [removeSurveyChapterInfo, setRemoveSurveyChapterInfo] =
    useState<Partial<ISurveyChapter> | null>(null);
  const [formFocus, setFormFocus] = useState<string | undefined>(undefined);

  const resetStates = useCallback(() => {
    setRemoveSurveyChapterInfo(null);
    setCreateFormValues(null);
    setFormFocus(undefined);
  }, []);

  const { isLoading: isSurveyChapterDeleting, mutate: deleteSurveyChapterById } = useMutation(
    deleteSurveyChapter,
    {
      onSuccess: () => {
        // @ts-ignore
        removeSurveyChapterInfo && onRemoveSurveyChapter(removeSurveyChapterInfo);
        resetStates();
      },
    },
  );

  useEffect(() => {
    resetStates();
  }, [resetStates, survey]);

  if (!survey) return null;

  const createButton = !createFormValues && !survey.isClone && (
    <Button
      onClick={() =>
        setCreateFormValues({
          // @ts-ignore
          name: `Criteriu de evaluare ${survey?.surveyChapters?.length + 1}`,
          // @ts-ignore
          order: survey?.surveyChapters?.length + 1,
          surveyId,
          weight: 1,
          questions: [{ _id: '', name: 'Competența de evaluare 1', order: 1 }],
        })
      }
      variant="contained"
      size="large"
      endIcon={<AddIcon />}
    >
      Adaugă criteriu&nbsp;&nbsp;&nbsp;&nbsp;
    </Button>
  );

  return (
    <>
      {survey.surveyChapters?.length || createFormValues ? (
        <>
          {survey.surveyChapters?.map((surveyChapter, index) => {
            const isInEditMode = createFormValues && surveyChapter._id === createFormValues._id;
            return (
              <Box key={surveyChapter._id} sx={{ mb: 2 }}>
                {isInEditMode ? (
                  <SurveyChapterForm
                    survey={survey}
                    surveyChapterIndex={index}
                    initialValues={createFormValues}
                    initialFocus={formFocus}
                    isLoading={isLoading}
                    onSubmit={onSubmit}
                    onRemoveSurveyChapter={(surveyChapter) =>
                      setRemoveSurveyChapterInfo(surveyChapter)
                    }
                    onCancel={() => setCreateFormValues(null)}
                  />
                ) : (
                  <>
                    <SurveyChapterListItem
                      onSelect={(id) => {
                        id && setFormFocus(id);
                        setCreateFormValues({ ...surveyChapter });
                      }}
                      survey={survey}
                      surveyChapter={surveyChapter}
                      surveyChapterIndex={index}
                    />
                  </>
                )}
              </Box>
            );
          })}

          {createFormValues && !createFormValues._id && (
            <SurveyChapterForm
              survey={survey}
              surveyChapterIndex={survey.surveyChapters?.length || 0}
              initialValues={createFormValues}
              isLoading={isLoading}
              onSubmit={onSubmit}
              onCancel={() => setCreateFormValues(null)}
            />
          )}

          {createButton}
        </>
      ) : (
        <NoDataAvailable
          content="În momentul de faţă nu există niciun criteriu de evaluare definit"
          actions={createButton}
        />
      )}

      <Dialog
        actions={
          <LoadingButton
            variant="contained"
            onClick={() => {
              if (removeSurveyChapterInfo?._id) {
                deleteSurveyChapterById(removeSurveyChapterInfo._id);
              }
            }}
            loading={isSurveyChapterDeleting}
          >
            Șterge
          </LoadingButton>
        }
        content={
          <>
            <Typography color="textSecondary" textAlign="center">
              Sunteți siguri că doriți să ștergeți criteriul de evaluare
              <strong>
                <i>&quot;{removeSurveyChapterInfo?.name}&quot;</i>
              </strong>
              ?
            </Typography>
          </>
        }
        title="Confirmati ștergerea?"
        onClose={() => setRemoveSurveyChapterInfo(null)}
        open={!!removeSurveyChapterInfo}
      />
    </>
  );
};

export default SurveyChaptersList;
