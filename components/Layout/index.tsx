import React from 'react';
import Box from '@mui/material/Box';
import dynamic from 'next/dynamic';
import { Breakpoint } from '@mui/system';
import Grid from '@mui/material/Grid';

const Sidenav = dynamic(() => import('./Sidenav'), {
  ssr: false,
});

interface ILayoutProps {
  children: React.ReactNode;
}

export const VIEWPORT_MAX_WIDTH: Breakpoint = 'xl';

const Layout = (props: ILayoutProps) => {
  const { children } = props;

  return (
    <Box sx={{ bgcolor: 'transparent', p: 3 }}>
      <Box
        sx={{
          position: 'fixed',
          top: 0,
          left: 0,
          width: '100vw',
          height: '300px',
          bgcolor: 'info.light',
          zIndex: -1,
        }}
      />
      {/*<AppToolbar />*/}

      <Grid container spacing={4} columns={11}>
        <Grid item xs={11} md={2}>
          <Sidenav />
        </Grid>
        <Grid item xs={11} md={9}>
          <Box py={4}>{children}</Box>
          {/*<Container*/}
          {/*  sx={{ position: 'relative', minHeight: 'calc(100vh - 130px)' }}*/}
          {/*  maxWidth={VIEWPORT_MAX_WIDTH}*/}
          {/*></Container>*/}
        </Grid>
      </Grid>
    </Box>
  );
};

export default Layout;
