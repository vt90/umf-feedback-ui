import axios from 'axios';
import { IEvaluationSession } from '../models/evaluationSession';
import { IPaginationResult } from '../models/base';

const httpInstance = axios.create({
  baseURL: '/api/evaluation-sessions',
});

httpInstance.interceptors.response.use(
  (response) => response.data,
  (error) => {
    if (error && error.response && error.response.data) {
      return Promise.reject(
        error.response.data.message
          ? `${error.response.data.message || error.response.data.error}`
          : error.response.data?.error || error.response.data,
      );
    } else {
      return Promise.reject({ message: 'Server error' });
    }
  },
);

const cleanupEvaluationSessionBody = (
  evaluationSession: Partial<IEvaluationSession>,
): Partial<IEvaluationSession> => {
  // @ts-ignore
  delete evaluationSession._id;
  // @ts-ignore
  delete evaluationSession.id;
  // @ts-ignore
  delete evaluationSession['__v'];

  if (evaluationSession.privateUsers) {
    evaluationSession.privateUsers = evaluationSession.privateUsers.map((user) => {
      if (typeof user === 'string') {
        return user;
      }
      // @ts-ignore
      else if (user.value) {
        // @ts-ignore
        return user.value;
      }
    });
  }

  return evaluationSession;
};

export const getEvaluationSessions = async (
  params: Partial<IEvaluationSession>,
): Promise<IPaginationResult<IEvaluationSession>> => {
  return await httpInstance.get('', { params });
};

export const getEvaluationSessionById = async (id: string): Promise<IEvaluationSession> => {
  return await httpInstance.get(`/${id}`);
};

export const getEvaluationSessionUnevaluatedUsers = async (
  id: string,
  params: any,
): Promise<IEvaluationSession> => {
  return await httpInstance.get(`/${id}/un-evaluated`, { params });
};

export const createOrUpdateEvaluationSession = async (
  evaluationSession: Partial<IEvaluationSession>,
): Promise<IEvaluationSession> => {
  const isUpdate = !!evaluationSession._id;
  const url = isUpdate ? `/${evaluationSession._id}` : ``;
  const req = isUpdate ? httpInstance.patch : httpInstance.post;
  return await req(url, cleanupEvaluationSessionBody(evaluationSession));
};

export const deleteEvaluationSession = async (id: string): Promise<any> => {
  return await httpInstance.delete(`/${id}`);
};
