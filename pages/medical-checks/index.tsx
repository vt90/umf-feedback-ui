import React, { useState } from 'react';
import Head from 'next/head';
import moment from 'moment';
import { withPageAuthRequired } from '../../guards/authRequired';
import { createOrUpdateMedicalCheck, findMedicalCheck } from '../../services/medicalCheck';
import { useMutation, useQuery, useQueryClient } from '@tanstack/react-query';
import Card from '@mui/material/Card';
import Button from '@mui/material/Button';
import Paper from '@mui/material/Paper';
import AddIcon from '@mui/icons-material/Add';
import AdminPage from '../../components/AdminPage';
import { IMedicalCheck, MEDICAL_CHECK_RESULT_TYPES } from '../../models/medicalCheck';
import NoDataAvailable from '../../components/Common/NoDataAvailable';
import { SELECTED_ACTIONS } from '../../lib/constants';
import { useNotificationContext } from '../../context/notificationContext';
import MedicalChecksForm from '../../components/MedicalChecks/Form';
import ListLoadingIndicator from '../../components/Common/ListLoadingIndcator';
import { findUsers } from '../../services/users';
import MedicalCheckList from '../../components/MedicalChecks/List';
import HREvaluationsListHeader from '../../components/Evaluation/HRUserEvaluationsList/Header';
import Box from '@mui/material/Box';
import useDebounce from '../../hooks/useDebounce';

const FETCH_QUERY_KEY = ['medical-checks'];
const FETCH_USERS_QUERY_KEY = ['users'];

const MedicalChecks = () => {
  const [selectedMedicalCheck, setSelectedMedicalCheck] = useState<Partial<IMedicalCheck> | null>(
    null,
  );
  const [selectedAction, setSelectedAction] = useState<string | null>(null);
  const [email, setEmail] = useState<string>('');
  const { setError, setMessage } = useNotificationContext();

  const onMedicalCheckAction = (action: string) => (survey: Partial<IMedicalCheck>) => {
    setSelectedMedicalCheck(survey);
    setSelectedAction(action);
  };

  const resetSelectedMedicalCheckState = () => {
    setSelectedMedicalCheck(null);
    setSelectedAction(null);
  };

  const queryClient = useQueryClient();

  const userSearchTerm = useDebounce(`${email}`, 600);

  const MEDICAL_CHECKS_QUERY_KEY = [FETCH_QUERY_KEY, userSearchTerm];

  const { isLoading, data: medicalCheckData } = useQuery(MEDICAL_CHECKS_QUERY_KEY, () =>
    findMedicalCheck({
      // @ts-ignore
      orderBy: 'isActive',
      order: -1,
      userSearchTerm,
    }),
  );

  const fetchParams = {
    limit: 10000000,
    offset: 0,
  };

  const { data: usersData } = useQuery([...FETCH_USERS_QUERY_KEY, fetchParams], () =>
    findUsers(fetchParams),
  );

  const { isLoading: isUpdating, mutate: updateMedicalCheck } = useMutation(
    createOrUpdateMedicalCheck,
    {
      onSuccess: async () => {
        await queryClient.invalidateQueries(MEDICAL_CHECKS_QUERY_KEY);
        resetSelectedMedicalCheckState();
        setMessage('Fișă de aptitudini salvată');
      },
      onError: setError,
    },
  );

  return (
    <>
      <Head>
        <title>Fișe de aptitudini</title>
      </Head>

      <AdminPage
        title="Fișe de aptitudini"
        pages={[{ url: '/medical-checks', name: 'Fișe de aptitudini' }]}
        actions={
          <Button
            onClick={() => {
              onMedicalCheckAction(SELECTED_ACTIONS.CREATE_OR_UPDATE)({
                // @ts-ignore
                startDate: moment().toDate(),
                // @ts-ignore
                endDate: moment().add(1, 'year').toDate(),
                // @ts-ignore
                result: MEDICAL_CHECK_RESULT_TYPES.APT,
              });
            }}
            variant="contained"
            endIcon={<AddIcon />}
          >
            Adaugă fișă de aptitudini
          </Button>
        }
      >
        {medicalCheckData?.results?.length || isLoading ? (
          <>
            {isLoading && <ListLoadingIndicator />}

            <Paper sx={{ p: 3 }}>
              <Box sx={{ mb: 3 }}>
                <HREvaluationsListHeader
                  // @ts-ignore
                  activeView={'s'}
                  order={1}
                  setOrder={console.log}
                  orderBy={''}
                  setOrderBy={console.log}
                  searchTerm={email}
                  // @ts-ignore
                  setSearchTerm={setEmail}
                />
              </Box>

              <MedicalCheckList
                // @ts-ignore
                medicalChecks={medicalCheckData?.results}
                onSelectMedicalCheck={(medicalCheck) =>
                  onMedicalCheckAction(SELECTED_ACTIONS.CREATE_OR_UPDATE)({
                    ...medicalCheck,
                    // @ts-ignore
                    userId: medicalCheck.userId._id,
                  })
                }
              />
            </Paper>
          </>
        ) : (
          <Card sx={{ p: 3 }}>
            <NoDataAvailable content="În momentul de faţă nu există nicio fișă de aptitudini disponibilă pe platformă" />
          </Card>
        )}

        {usersData?.results &&
          selectedMedicalCheck &&
          selectedAction === SELECTED_ACTIONS.CREATE_OR_UPDATE && (
            <MedicalChecksForm
              initialValues={selectedMedicalCheck}
              isLoading={isUpdating}
              open={true}
              onClose={resetSelectedMedicalCheckState}
              onSubmit={updateMedicalCheck}
              users={usersData?.results}
            />
          )}
      </AdminPage>
    </>
  );
};

export const getServerSideProps = withPageAuthRequired();

export default MedicalChecks;
