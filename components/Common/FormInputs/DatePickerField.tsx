import React from 'react';
import { Control, Controller } from 'react-hook-form';
import TextField, { ITextFieldProps } from './TextField';
import { DatePicker, DatePickerProps } from '@mui/x-date-pickers/DatePicker';
import { Moment } from 'moment';

interface IDatePickerFieldProps
  extends Omit<DatePickerProps<any, Moment>, 'value' | 'onChange' | 'renderInput'> {
  name: string;
  control: Control;
  errorMessage?: string | boolean;
  textFieldProps?: ITextFieldProps;
  [key: string]: any;
}

const DatePickerField = (props: IDatePickerFieldProps, ref: any) => {
  const { name, control, errorMessage, textFieldProps, ...rest } = props;

  return (
    <Controller
      name={name}
      control={control}
      render={({ field }) => {
        const { value, onChange, ...others } = field;

        const pickerValue = value || null;

        const handleChange = (newValue: Moment | null) => {
          onChange(newValue);
        };

        return (
          <>
            <DatePicker
              inputFormat="YYYY.MM.DD"
              onChange={handleChange}
              value={pickerValue}
              renderInput={(params) => {
                // @ts-ignore
                return (
                  <TextField
                    errorMessage={errorMessage}
                    {...params}
                    {...textFieldProps}
                    {...others}
                  />
                );
              }}
              {...rest}
            />
          </>
        );
      }}
    />
  );
};

export default React.forwardRef(DatePickerField);
