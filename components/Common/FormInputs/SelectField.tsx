import React, { useState } from 'react';
import { Control, Controller } from 'react-hook-form';
import Autocomplete from '@mui/material/Autocomplete';
import TextField, { ITextFieldProps } from './TextField';
// import MenuItem from '@mui/material/MenuItem';

export interface ISelectFieldOption {
  name: string | React.ReactNode;
  value: string | number | null;
  disabled?: boolean;
}

interface ISelectFieldProps extends ITextFieldProps {
  name: string;
  control: Control;
  options: ISelectFieldOption[];
  hideEmpty?: boolean;
  disableClearable?: boolean;
  [key: string]: any;
}

const SelectField = (props: ISelectFieldProps, ref: any) => {
  const {
    name,
    control,
    options,
    hideEmpty,
    disableClearable,
    errorMessage,
    disabled,
    sx,
    ...rest
  } = props;
  const [inputValue, setInputValue] = useState('');

  return (
    <Controller
      name={name}
      control={control}
      render={({ field }) => {
        const { value, onChange } = field;

        return (
          <Autocomplete
            sx={sx}
            disableClearable={disableClearable}
            disabled={!!disabled}
            value={value}
            onChange={(event: any, newValue: string | null) => {
              onChange(newValue);
            }}
            inputValue={inputValue}
            onInputChange={(event, newInputValue) => {
              setInputValue(newInputValue);
            }}
            options={options.map((o) => o.value)}
            // @ts-ignore
            getOptionLabel={(o) => {
              // @ts-ignore
              const option = options.find((op) => op.value === o);
              // @ts-ignore
              return option?.name || '-';
            }}
            renderInput={(params) => <TextField {...params} errorMessage={errorMessage} />}
          />
        );
        // return (
        //   <TextField
        //     ref={ref}
        //     value={value}
        //     // @ts-ignore
        //     onChange={onChange}
        //     errorMessage={errorMessage}
        //     select
        //     SelectProps={{
        //       displayEmpty: !hideEmpty,
        //     }}
        //     {...rest}
        //   >
        //     {options.map((option) => (
        //       // @ts-ignore
        //       <MenuItem key={option.value} value={option.value} disabled={option.disabled}>
        //         {option.name}
        //       </MenuItem>
        //     ))}
        //   </TextField>
        // );
      }}
    />
  );
};

export default React.forwardRef(SelectField);
