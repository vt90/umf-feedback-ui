import React, { useMemo } from 'react';
import Head from 'next/head';
import { useRouter } from 'next/router';
import { withPageAuthRequired } from '../../../guards/authRequired';
import { useQuery } from '@tanstack/react-query';

import AdminPage from '../../../components/AdminPage';
import ListLoadingIndicator from '../../../components/Common/ListLoadingIndcator';
import { getEvaluationSessionById } from '../../../services/evaluationSessions';
import { getUserSubUsers } from '../../../services/userDepartmentAssignment';
import EvaluationSessionDetails from '../../../components/EvaluationSessions/Details';
import { IUser } from '../../../models/user';
import { getContraEvaluations, getEvaluations } from '../../../services/evaluations';
import { getAllFunctions } from '../../../services/functions';

interface IEvaluationPageProps {
  user: IUser;
}

const FETCH_FUNCTIONS_QUERY_KEY = ['depFunctions'];

const EvaluationSessions = (props: IEvaluationPageProps) => {
  const { user } = props;
  const router = useRouter();
  const { id } = router.query;

  const FETCH_QUERY_KEY = useMemo(() => ['evaluation-session', id], [id]);
  const FETCH_EVALUATOR_QUERY_KEY = useMemo(() => ['my-evaluator-evaluations'], []);
  const FETCH_CONTRA_EVALUATOR_QUERY_KEY = useMemo(() => ['my-contra-evaluator-evaluations'], []);

  const { isLoading, data: evaluationSession } = useQuery(
    FETCH_QUERY_KEY,
    () => getEvaluationSessionById(id as string),
    {
      onError: () => router.push('/404'),
    },
  );

  const { isLoading: isMyEvaluationsLoading, data: evaluatorEvaluations } = useQuery(
    FETCH_EVALUATOR_QUERY_KEY,
    () =>
      getEvaluations({
        // @ts-ignore
        evaluationSessionId: id,
        evaluatorId: user._id,
      }),
    {
      enabled: !!evaluationSession,
    },
  );

  const { isLoading: isMyContraEvaluationsLoading, data: evaluatorContraEvaluateEvaluations } =
    useQuery(
      FETCH_CONTRA_EVALUATOR_QUERY_KEY,
      () =>
        getContraEvaluations({
          // @ts-ignore
          evaluationSessionId: id,
          evaluatorId: user._id,
        }),
      {
        enabled: !!evaluationSession,
      },
    );

  const { isLoading: isSubUsersLoading, data: subUsers } = useQuery(
    ['sub-users'],
    getUserSubUsers,
    {
      onError: () => router.push('/404'),
    },
  );

  const { data: functions, isLoading: isFunctionsLoading } = useQuery(
    FETCH_FUNCTIONS_QUERY_KEY,
    getAllFunctions,
  );

  if (
    isLoading ||
    isMyEvaluationsLoading ||
    isMyContraEvaluationsLoading ||
    isSubUsersLoading ||
    isFunctionsLoading
  )
    return <ListLoadingIndicator />;

  // ToDo implement not found
  if (!evaluationSession) return 'Session unavailable';

  return (
    <>
      <Head>
        <title>{evaluationSession.name}</title>
      </Head>

      <AdminPage
        title={evaluationSession.name}
        pages={[
          { url: '/evaluation-sessions', name: 'Sesiuni de evaluare' },
          { url: `/evaluation-sessions/${id}`, name: evaluationSession.name },
        ]}
      >
        <EvaluationSessionDetails
          evaluationSession={evaluationSession}
          // @ts-ignore
          functions={functions?.results}
          // @ts-ignore
          users={subUsers?.results}
          isUsersLoading={isSubUsersLoading}
          evaluatorEvaluations={evaluatorEvaluations?.results}
          evaluatorContraEvaluateEvaluations={evaluatorContraEvaluateEvaluations}
          currentUserId={user._id}
        />
      </AdminPage>
    </>
  );
};

export const getServerSideProps = withPageAuthRequired();

export default EvaluationSessions;
