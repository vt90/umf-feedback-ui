import React, { useState } from 'react';
import Head from 'next/head';
import { withPageAuthRequired } from '../../guards/authRequired';
import { createOrUpdateSurvey, deleteSurvey, getSurveys } from '../../services/surveys';
import { useQuery, useMutation, useQueryClient } from '@tanstack/react-query';
import Button from '@mui/material/Button';
import SurveysForm from '../../components/Surveys/Form';
import AdminPage from '../../components/AdminPage';
import SurveysList from '../../components/Surveys/List';
import ListLoadingIndicator from '../../components/Common/ListLoadingIndcator';
import { getSurveyRoleName, ISurvey, SURVEY_FORMULA_TYPE, SURVEY_ROLE } from '../../models/survey';
import LoadingButton from '@mui/lab/LoadingButton';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import Dialog from '../../components/Common/Dialog';
import NoDataAvailable from '../../components/Common/NoDataAvailable';
import AddIcon from '@mui/icons-material/Add';
import { USER_DEP_ASSIGNMENT_TYPE } from '../../models/userDepartmentAssignment';
import { SELECTED_ACTIONS } from '../../lib/constants';
import { useNotificationContext } from '../../context/notificationContext';
import Paper from '@mui/material/Paper';

const FETCH_QUERY_KEY = ['surveys'];

const Surveys = () => {
  const [selectedSurvey, setSelectedSurvey] = useState<Partial<ISurvey> | null>(null);
  const [selectedAction, setSelectedAction] = useState<string | null>(null);
  const { setError, setMessage } = useNotificationContext();

  const onSurveyAction = (action: string) => (survey: Partial<ISurvey>) => {
    setSelectedSurvey(survey);
    setSelectedAction(action);
  };

  const resetSelectedSurveyState = () => {
    setSelectedSurvey(null);
    setSelectedAction(null);
  };

  const queryClient = useQueryClient();

  const { isLoading, data: surveysData } = useQuery(FETCH_QUERY_KEY, () =>
    getSurveys({
      isClone: false,
    }).then((data) => {
      return {
        ...data,
        groupedByRole: data.results?.reduce((acc, survey) => {
          // @ts-ignore
          if (!acc[survey.role]) acc[survey.role] = [];

          // @ts-ignore
          acc[survey.role].push(survey);

          return acc;
        }, {}),
      };
    }),
  );

  const { isLoading: isUpdating, mutate: updateSurvey } = useMutation(createOrUpdateSurvey, {
    onSuccess: async () => {
      await queryClient.invalidateQueries(FETCH_QUERY_KEY);
      resetSelectedSurveyState();
      setMessage('Formular salvat');
    },
    onError: setError,
  });

  const { isLoading: isDeleting, mutate: deleteSurveyById } = useMutation(deleteSurvey, {
    onSuccess: async () => {
      await queryClient.invalidateQueries(FETCH_QUERY_KEY);
      resetSelectedSurveyState();
      setMessage('Formular sters');
    },
    onError: setError,
  });

  // @ts-ignore
  return (
    <>
      <Head>
        <title>Formulare</title>
      </Head>

      <AdminPage
        title="Formulare"
        pages={[{ url: '/surveys', name: 'Formulare' }]}
        actions={
          surveysData?.results?.length !==
            Object.keys(USER_DEP_ASSIGNMENT_TYPE).length * Object.keys(SURVEY_ROLE).length && (
            <>
              <Button
                onClick={() => {
                  onSurveyAction(SELECTED_ACTIONS.CREATE_OR_UPDATE)({
                    type: USER_DEP_ASSIGNMENT_TYPE.SUPERIOR,
                    role: SURVEY_ROLE.EVALUATION,
                    calculationFormulaType: SURVEY_FORMULA_TYPE['Medie aritmetică'],
                  });
                }}
                variant="contained"
                endIcon={<AddIcon />}
              >
                Adaugă Formular
              </Button>
            </>
          )
        }
      >
        {surveysData?.results?.length || isLoading ? (
          <>
            {isLoading && <ListLoadingIndicator />}

            <Paper sx={{ p: 3 }}>
              {surveysData?.groupedByRole &&
                Object.keys(surveysData.groupedByRole).map((key) => {
                  // @ts-ignore
                  const surveys = surveysData.groupedByRole[key];

                  return (
                    <Box sx={{ mb: 4 }} key={key}>
                      <Typography variant="subtitle1" sx={{ mb: 2 }}>
                        {getSurveyRoleName(key)}
                      </Typography>
                      <SurveysList
                        baseUrl="/surveys"
                        surveys={surveys}
                        // onSelectSurvey={onSurveyAction(SELECTED_ACTIONS.CREATE_OR_UPDATE)}
                        // onDeleteSurvey={onSurveyAction(SELECTED_ACTIONS.DELETE)}
                      />
                    </Box>
                  );
                })}
            </Paper>
          </>
        ) : (
          <NoDataAvailable content="În momentul de faţă nu există niciun formular de evaluare disponibil pe platformă" />
        )}

        <Dialog
          actions={
            <LoadingButton
              variant="contained"
              onClick={() => selectedSurvey && deleteSurveyById(selectedSurvey._id as string)}
              loading={isDeleting}
            >
              Șterge
            </LoadingButton>
          }
          content={
            <>
              <Typography color="textSecondary" textAlign="center">
                Sunteți siguri că doriți să ștergeți formularul de evaluare
                <strong>
                  <i>&quot;{selectedSurvey?.name}&quot;</i>
                </strong>
                ?
              </Typography>
            </>
          }
          title="Confirmati ștergerea?"
          onClose={resetSelectedSurveyState}
          open={!!(selectedSurvey && selectedAction === SELECTED_ACTIONS.DELETE)}
        />

        {selectedSurvey && selectedAction === SELECTED_ACTIONS.CREATE_OR_UPDATE && (
          <SurveysForm
            initialValues={selectedSurvey}
            isLoading={isUpdating}
            open={true}
            onClose={resetSelectedSurveyState}
            onSubmit={updateSurvey}
          />
        )}
      </AdminPage>
    </>
  );
};

export const getServerSideProps = withPageAuthRequired();

export default Surveys;
