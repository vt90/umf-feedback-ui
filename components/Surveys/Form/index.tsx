import React from 'react';
import * as yup from 'yup';
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogTitle from '@mui/material/DialogTitle';
import Grid from '@mui/material/Grid';
import ToggleField from '../../Common/FormInputs/ToggleField';
import Typography from '@mui/material/Typography';
import { USER_DEP_ASSIGNMENT_TYPE } from '../../../models/userDepartmentAssignment';
import {
  getSurveyRoleName,
  ISurvey,
  SURVEY_FORMULA_TYPE,
  SURVEY_ROLE,
} from '../../../models/survey';
import TextField from '../../Common/FormInputs/TextField';

interface ISurveysFormProps {
  initialValues: Partial<ISurvey>;
  isLoading: boolean;
  onClose: () => void;
  onSubmit: (data: any) => void;
  open: boolean;
}
const schema = yup
  .object({
    name: yup.string().required(),
    type: yup.string().required(),
    calculationFormulaType: yup.string().required(),
  })
  .required();

const SurveysForm = (props: ISurveysFormProps) => {
  const { isLoading, initialValues, open, onClose, onSubmit } = props;
  const {
    register,
    handleSubmit,
    formState: { errors },
    control,
  } = useForm({
    defaultValues: initialValues,
    mode: 'onTouched',
    resolver: yupResolver(schema),
    shouldUnregister: true,
  });

  return (
    <Dialog
      open={open}
      fullWidth={true}
      maxWidth="md"
      keepMounted={false}
      onClose={onClose}
      // @ts-ignore
      PaperComponent="form"
      onSubmit={handleSubmit(onSubmit)}
    >
      <DialogTitle sx={{ typography: 'h5', textAlign: 'center' }}>
        {initialValues?._id ? 'Modifică' : 'Creează'} formular
      </DialogTitle>
      <DialogContent>
        <input type="hidden" {...register(`_id`)} />
        <Grid container spacing={3} alignItems="center">
          <Grid item xs={12} md={3}>
            <Typography variant="body2" sx={{ textAlign: { xs: 'left', md: 'right' } }}>
              Denumire formular*
            </Typography>
          </Grid>
          <Grid item xs={12} md={8}>
            <TextField
              required={true}
              disabled={isLoading}
              errorMessage={!!errors.name && 'Denumire necesară'}
              defaultValue={initialValues.name}
              {...register('name')}
            />
          </Grid>
          <Grid item xs={12} md={3}>
            <Typography variant="body2" sx={{ textAlign: { xs: 'left', md: 'right' } }}>
              Rolul formularului
            </Typography>
          </Grid>
          <Grid item xs={12} md={8}>
            <ToggleField
              name="role"
              control={control}
              defaultValue={initialValues.type}
              options={Object.values(SURVEY_ROLE).map((option) => ({
                name: getSurveyRoleName(option),
                value: option,
              }))}
            />
          </Grid>
          <Grid item xs={12} md={3}>
            <Typography variant="body2" sx={{ textAlign: { xs: 'left', md: 'right' } }}>
              Tip de utilizator
            </Typography>
          </Grid>
          <Grid item xs={12} md={8}>
            <ToggleField
              name="type"
              control={control}
              defaultValue={initialValues.type}
              options={Object.values(USER_DEP_ASSIGNMENT_TYPE).map((option) => ({
                name: option,
                value: option,
              }))}
            />
          </Grid>
          <Grid item xs={12} md={3}>
            <Typography variant="body2" sx={{ textAlign: { xs: 'left', md: 'right' } }}>
              Tip de calcul
            </Typography>
          </Grid>
          <Grid item xs={12} md={8}>
            <ToggleField
              name="calculationFormulaType"
              control={control}
              defaultValue={initialValues.calculationFormulaType}
              options={Object.keys(SURVEY_FORMULA_TYPE).map((option) => ({
                name: option,
                // @ts-ignore
                value: SURVEY_FORMULA_TYPE[option],
              }))}
            />
          </Grid>
        </Grid>
      </DialogContent>
      <DialogActions>
        <Button disabled={isLoading} onClick={onClose}>
          Anulare
        </Button>
        <Button disabled={isLoading} variant="contained" type="submit">
          {initialValues?._id ? 'Modifică' : 'Crează'}
        </Button>
      </DialogActions>
    </Dialog>
  );
};

export default SurveysForm;
