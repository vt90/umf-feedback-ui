import React, { useEffect, useState } from 'react';
import Head from 'next/head';
import dynamic from 'next/dynamic';
import { withPageAuthRequired } from '../../guards/authRequired';
import {
  createOrUpdateSSMPeriodicTraining,
  deleteSSMPeriodicTraining,
  findSSMPeriodicTraining,
} from '../../services/ssmPeriodicTrainings';
import { useQuery, useMutation, useQueryClient } from '@tanstack/react-query';
import Button from '@mui/material/Button';
import Paper from '@mui/material/Paper';
import AdminPage from '../../components/AdminPage';
import { ISSMPeriodicTraining } from '../../models/ssmPeriodicTraining';
import LoadingButton from '@mui/lab/LoadingButton';
import Typography from '@mui/material/Typography';
import Dialog from '../../components/Common/Dialog';
import NoDataAvailable from '../../components/Common/NoDataAvailable';
import AddIcon from '@mui/icons-material/Add';
import { SELECTED_ACTIONS } from '../../lib/constants';
import { useNotificationContext } from '../../context/notificationContext';
import { getAllFunctions } from '../../services/functions';
import SSMPeriodicTrainingsForm from '../../components/SSMPeriodicTraining/Form';
import ListLoadingIndicator from '../../components/Common/ListLoadingIndcator';
import SSMFunctionAllocationMatrix from '../../components/SSM/FunctionAllocationMatrix';
import { useRouter } from 'next/router';
import Tab from '@mui/material/Tab';
import Tabs from '@mui/material/Tabs';
import Card from '@mui/material/Card';
import SSMPeriodicTrainingCalendar from '../../components/SSMPeriodicTraining/Calendar';
import { getAllDepartments } from '../../services/departments';
// @ts-ignore
const SSMPeriodicTrainingList = dynamic(() => import('../../components/SSMPeriodicTraining/List'), {
  ssr: false,
});

const FETCH_QUERY_KEY = ['ssm-periodic-sessions'];
const FETCH_FUNCTIONS_QUERY_KEY = ['depFunctions'];
const FETCH_DEPARTMENTS_QUERY_KEY = ['departments'];

const VIEWS = ['list', 'function-allocations', 'calendar'];
const VIEW_NAMES = ['Listă instruiri periodice', 'Alocări funcţii', 'Calendar'];

const SSMPeriodicTrainings = () => {
  const [selectedSSMPeriodicTraining, setSelectedSSMPeriodicTraining] =
    useState<Partial<ISSMPeriodicTraining> | null>(null);
  const [selectedAction, setSelectedAction] = useState<string | null>(null);
  const { setError, setMessage } = useNotificationContext();
  const router = useRouter();

  const { activeView } = router.query;

  const onViewChange = (view: string) => {
    router.push(`${router.route}?activeView=${view}`);
  };

  useEffect(() => {
    // @ts-ignore
    if (activeView && VIEWS.indexOf(activeView)) {
      return;
    } else {
      onViewChange('list');
    }
  }, [activeView]);

  const onSSMPeriodicTrainingAction =
    (action: string) => (survey: Partial<ISSMPeriodicTraining>) => {
      setSelectedSSMPeriodicTraining(survey);
      setSelectedAction(action);
    };

  const resetSelectedSSMPeriodicTrainingState = () => {
    setSelectedSSMPeriodicTraining(null);
    setSelectedAction(null);
  };

  const queryClient = useQueryClient();

  const { isLoading, data: ssmPeriodicTrainingsData } = useQuery(FETCH_QUERY_KEY, () =>
    findSSMPeriodicTraining({}),
  );

  const { data: functions } = useQuery(FETCH_FUNCTIONS_QUERY_KEY, getAllFunctions);

  const { data: departments } = useQuery(FETCH_DEPARTMENTS_QUERY_KEY, getAllDepartments);

  const { isLoading: isUpdating, mutate: updateSSMPeriodicTraining } = useMutation(
    createOrUpdateSSMPeriodicTraining,
    {
      onSuccess: async () => {
        await queryClient.invalidateQueries(FETCH_QUERY_KEY);
        resetSelectedSSMPeriodicTrainingState();
        setMessage('Instruire periodică salvată');
      },
      onError: setError,
    },
  );

  const { isLoading: isDeleting, mutate: deleteSSMPeriodicTrainingById } = useMutation(
    deleteSSMPeriodicTraining,
    {
      onSuccess: async () => {
        await queryClient.invalidateQueries(FETCH_QUERY_KEY);
        resetSelectedSSMPeriodicTrainingState();
        setMessage('Instruire periodică ștearsă');
      },
      onError: setError,
    },
  );

  const onFunctionAllocationChange = (
    ssmPeriodicTrainingInfo: ISSMPeriodicTraining,
    newFunctionsAllocationValue: string[],
  ) => {
    updateSSMPeriodicTraining({
      ...ssmPeriodicTrainingInfo,
      functions: newFunctionsAllocationValue,
    });
  };

  return (
    <>
      <Head>
        <title>Instruiri periodice SSM</title>
      </Head>

      <AdminPage
        title="Instruiri periodice SSM"
        pages={[{ url: '/ssm-periodic-trainings', name: 'Instruiri periodice SSM' }]}
        actions={
          <Button
            onClick={() => {
              onSSMPeriodicTrainingAction(SELECTED_ACTIONS.CREATE_OR_UPDATE)({
                name: '',
                functions: [],
                departments: [],
                excludedDepartments: [],
                monthsOfYear: [],
              });
            }}
            variant="contained"
            endIcon={<AddIcon />}
          >
            Adaugă Instruire Periodica
          </Button>
        }
      >
        {ssmPeriodicTrainingsData?.results?.length || isLoading ? (
          <>
            {isLoading && <ListLoadingIndicator />}

            {functions?.results?.length && (
              <Paper sx={{ p: 3 }}>
                <Tabs
                  value={activeView}
                  onChange={(ev, newValue) => onViewChange(newValue)}
                  sx={{ mb: 2 }}
                >
                  {VIEWS.map((view, index) => (
                    <Tab label={VIEW_NAMES[index]} value={view} key={view} />
                  ))}
                </Tabs>
                {activeView === VIEWS[0] && (
                  <SSMPeriodicTrainingList
                    // @ts-ignore
                    ssmPeriodicTrainings={ssmPeriodicTrainingsData?.results}
                    // @ts-ignore
                    functions={functions?.results}
                    onEdit={onSSMPeriodicTrainingAction(SELECTED_ACTIONS.CREATE_OR_UPDATE)}
                    onDelete={onSSMPeriodicTrainingAction(SELECTED_ACTIONS.DELETE)}
                  />
                )}
                {activeView === VIEWS[1] && (
                  <SSMFunctionAllocationMatrix
                    functions={functions?.results || []}
                    columns={ssmPeriodicTrainingsData?.results || []}
                    // @ts-ignore
                    onCheckboxCLick={onFunctionAllocationChange}
                    onColumnClick={(ssmPeriodicTraining) =>
                      onSSMPeriodicTrainingAction(SELECTED_ACTIONS.CREATE_OR_UPDATE)(
                        ssmPeriodicTraining,
                      )
                    }
                  />
                )}
                {activeView === VIEWS[2] && (
                  <SSMPeriodicTrainingCalendar
                    // @ts-ignore
                    ssmPeriodicTrainings={ssmPeriodicTrainingsData?.results}
                    onEdit={onSSMPeriodicTrainingAction(SELECTED_ACTIONS.CREATE_OR_UPDATE)}
                  />
                )}
              </Paper>
            )}
          </>
        ) : (
          <Card sx={{ p: 3 }}>
            <NoDataAvailable content="În momentul de faţă nu există nicio instruire periodca disponibilă pe platformă" />
          </Card>
        )}

        <Dialog
          actions={
            <LoadingButton
              variant="contained"
              onClick={() =>
                selectedSSMPeriodicTraining &&
                deleteSSMPeriodicTrainingById(selectedSSMPeriodicTraining._id as string)
              }
              loading={isDeleting}
            >
              Șterge
            </LoadingButton>
          }
          content={
            <>
              <Typography color="textSecondary" textAlign="center">
                Sunteți siguri că doriți să ștergeți pachetul de instruire
                <strong>
                  <i>&quot;{selectedSSMPeriodicTraining?.name}&quot;</i>
                </strong>
                ?
              </Typography>
            </>
          }
          title="Confirmaţi ștergerea?"
          onClose={resetSelectedSSMPeriodicTrainingState}
          open={!!(selectedSSMPeriodicTraining && selectedAction === SELECTED_ACTIONS.DELETE)}
        />

        {functions &&
          departments &&
          selectedSSMPeriodicTraining &&
          selectedAction === SELECTED_ACTIONS.CREATE_OR_UPDATE && (
            <SSMPeriodicTrainingsForm
              initialValues={selectedSSMPeriodicTraining}
              isLoading={isUpdating}
              // @ts-ignore
              functions={functions?.results}
              // @ts-ignore
              departments={departments}
              open={true}
              onClose={resetSelectedSSMPeriodicTrainingState}
              onDelete={() =>
                selectedSSMPeriodicTraining?._id &&
                onSSMPeriodicTrainingAction(SELECTED_ACTIONS.DELETE)(selectedSSMPeriodicTraining)
              }
              onSubmit={updateSSMPeriodicTraining}
            />
          )}
      </AdminPage>
    </>
  );
};

export const getServerSideProps = withPageAuthRequired();

export default SSMPeriodicTrainings;
