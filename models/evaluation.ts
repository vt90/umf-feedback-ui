import { IUser } from './user';
import { IBaseModel, IPagination } from './base';
import { ISurvey } from './survey';
import { IEvaluationSession } from './evaluationSession';
import { IUserDepartmentAssignment } from './userDepartmentAssignment';
import { ISurveyChapter } from './surveyChapter';
import { IDepartment } from './department';

export enum EVALUATION_ACTIVITY_TYPES {
  CREATED,
  MODIFIED,
  USER_APPROVAL,
  USER_ACKNOWLEDGEMENT,
  CONTRA_SIGNED,
  SSM_CONTRA_SIGNED,
  SALARY_CHANGE_REFUSED
}

export interface IEvaluationResult {
  [key: string]: number;
}

export interface ISurveyChapterResult {
  surveyChapterId: string | ISurveyChapter;
  result: number;
}

export interface IEvaluationEvent extends IBaseModel {
  userId: string | IUser;
  type: number;
  value?: string;
  comment?: string;
}

export interface IQuestionResult {
  questionId: string;
  result: number;
}

export interface IObjectiveResult {
  objectiveId: string;
  result: number;
}

export interface IBonificationResult {
  bonificationId: string;
  result: number;
}

export interface IEvaluation extends IBaseModel {
  salaryChangePercentage?: number;
  hasChanges: boolean;
  evaluatorId?: string | IUser;
  departmentId?: string | IDepartment;
  contraEvaluatorId?: string | IUser;
  contraEvaluated: boolean;
  userId: string | IUser;
  userAgreement: boolean | null;
  userAcknowledged: boolean;
  surveyId: string | ISurvey;
  evaluationSessionId: string | IEvaluationSession;
  userDepartmentAssignmentId: string | IUserDepartmentAssignment;
  questionResults: IQuestionResult[];
  objectiveResult: IObjectiveResult[];
  bonifications: IBonificationResult[];
  events: IEvaluationEvent[];
  surveyChapterResults?: ISurveyChapterResult[];
  finalGrade?: number;
  finalResult?: string;
  role?: string;
}

export interface IFindEvaluations extends IEvaluation, IPagination {}
