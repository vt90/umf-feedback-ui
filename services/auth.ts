import axios, { AxiosResponseHeaders } from 'axios';

const httpInstance = axios.create({
  baseURL: `${process.env.NEXT_PUBLIC_API_URL}/api/auth`,
});

httpInstance.interceptors.response.use(
  (response) => response,
  (error) => {
    if (error && error.response && error.response.data) {
      return Promise.reject(
        error.response.data.message
          ? `${error.response.data.message || error.response.data.error}`
          : error.response.data?.error || error.response.data,
      );
    } else {
      return Promise.reject({ message: 'Server error' });
    }
  },
);

export const SESSION_COOKIE_KEY = 'APP_SESSION';

export const extractCookies = (headers: AxiosResponseHeaders) => {
  const cookies = {};

  headers['set-cookie']?.forEach((cookieInfo) => {
    const cookieDetails = cookieInfo.split(';');
    let cookieKey = '';
    cookieDetails.forEach((detail, index) => {
      const [key, value] = detail.split('=');

      let cookieValueKey = key;
      if (index === 0) {
        cookieKey = key;
        cookieValueKey = 'value';

        // @ts-ignore
        cookies[cookieKey] = {};
      }

      // @ts-ignore
      cookies[cookieKey][cookieValueKey] = value;
    });
  });

  return cookies;
};

export const login = async (email: string, password: string): Promise<string> => {
  const { headers } = await httpInstance.post(
    '/login',
    { email, password },
    { withCredentials: true },
  );

  const cookies = extractCookies(headers);
  // @ts-ignore
  const appSessionCookie = cookies[SESSION_COOKIE_KEY];

  return appSessionCookie.value;
};
export const getExtendedUser = async (accessToken: string): Promise<string> => {
  const { data } = await httpInstance.get('/me', {
    headers: {
      Authorization: `Bearer ${accessToken}`,
    },
  });

  return data;
};
