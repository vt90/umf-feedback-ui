import React, { useCallback, useMemo } from 'react';
import Link from 'next/link';
import EditIcon from '@mui/icons-material/Edit';
import DeleteIcon from '@mui/icons-material/Delete';
import IconButton from '@mui/material/IconButton';
import Card from '@mui/material/Card';
import Typography from '@mui/material/Typography';
import EnhancedTable from '../../Common/Table';
import { EVALUATION_SESSION_TYPE, IEvaluationSession } from '../../../models/evaluationSession';
import moment from 'moment';

interface IEvaluationSessionsListProps {
  baseUrl: string;
  evaluationSessions: IEvaluationSession[];
  onSelectEvaluationSession?: (org: IEvaluationSession) => void | null;
  onDeleteEvaluationSession?: (org: IEvaluationSession) => void | null;
}

const EvaluationSessionsList = (props: IEvaluationSessionsListProps) => {
  const { baseUrl, evaluationSessions, onSelectEvaluationSession, onDeleteEvaluationSession } =
    props;

  const headerRows = useMemo(() => {
    return [
      { id: 'name', label: 'Denumire', align: 'left', colSpan: 4, disableSorting: true },
      { id: 'period', label: 'Perioadă', align: 'left', colSpan: 2 },
      { id: 'type', label: 'Tip', align: 'left', colSpan: 2 },
      { id: 'isOpen', label: 'Sesiune activă', align: 'center' },
      { id: 'privateUsersOnly', label: 'Sesiune restricționată', align: 'center' },
      ...(onSelectEvaluationSession || onDeleteEvaluationSession
        ? [{ id: 'actions', label: 'Acțiuni', align: 'right', disableSorting: true }]
        : []),
    ];
  }, [onDeleteEvaluationSession, onSelectEvaluationSession]);

  const getHeaderRows = useCallback(() => {
    return evaluationSessions.map((evaluationSession) => {
      const renderName = (
        <Link href={`${baseUrl}/${evaluationSession._id}`}>
          <a>
            <Typography>{evaluationSession.name}</Typography>
          </a>
        </Link>
      );

      return {
        _id: evaluationSession._id,
        smallDeviceHeader: renderName,
        name: renderName,
        isOpen: evaluationSession.isOpen ? 'Da' : 'Nu',
        privateUsersOnly: evaluationSession.privateUsersOnly ? (
          <Typography color="textSecondary" variant="body2">
            Da
            <br />
            {evaluationSession?.privateUsers?.length} angajați
          </Typography>
        ) : (
          'Nu'
        ),
        period: `${moment(evaluationSession.createdAt).format('YYYY.MM.DD')} - ${
          evaluationSession.endDate ? moment(evaluationSession.endDate).format('YYYY.MM.DD') : ''
        }`,
        sx: {
          ...(evaluationSession.isOpen
            ? {}
            : {
                bgcolor: 'action.hover',
              }),
        },
        type: Object.keys(EVALUATION_SESSION_TYPE).find(
          // @ts-ignore
          (key) => EVALUATION_SESSION_TYPE[key] === evaluationSession.type,
        ),
        actions: (
          <>
            {onSelectEvaluationSession && (
              <IconButton
                onClick={() => onSelectEvaluationSession(evaluationSession)}
                size="small"
                color="info"
              >
                <EditIcon fontSize="inherit" />
              </IconButton>
            )}

            {onDeleteEvaluationSession && (
              <IconButton
                onClick={() => onDeleteEvaluationSession(evaluationSession)}
                size="small"
                color="error"
              >
                <DeleteIcon fontSize="inherit" />
              </IconButton>
            )}
          </>
        ),
        data: evaluationSession,
      };
    });
  }, [baseUrl, onDeleteEvaluationSession, onSelectEvaluationSession, evaluationSessions]);

  return (
    <>
      <EnhancedTable
        headerRows={headerRows}
        orderByFieldCriteriaFunctions={{
          createdAt: (row: any) => row?.data && !!row.data.createdAt,
        }}
        PaperComponent={Card}
        paperComponentStyles={{
          px: 3,
          py: 2,
        }}
        rows={getHeaderRows()}
        smallUntilBreakpoint="md"
      />
    </>
  );
};

export default EvaluationSessionsList;
