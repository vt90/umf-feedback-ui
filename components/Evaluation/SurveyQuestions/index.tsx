import React from 'react';
import { ISurvey, SURVEY_FORMULA_TYPE } from '../../../models/survey';
import { IEvaluationResult } from '../../../models/evaluation';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import Divider from '@mui/material/Divider';
import Grid from '@mui/material/Grid';
import ToggleButtonGroup from '@mui/material/ToggleButtonGroup';
import ToggleButton from '@mui/material/ToggleButton';
import Typography from '@mui/material/Typography';
import InfoIcon from '@mui/icons-material/Info';

interface IEvaluationSurveyQuestionsProps {
  evaluationResult: IEvaluationResult;
  surveyChapterResults?: IEvaluationResult;
  onChangeEvaluationResult?: (questionId: string, value: number) => void;
  survey: ISurvey;
}

const EvaluationSurveyQuestions = (props: IEvaluationSurveyQuestionsProps) => {
  const { evaluationResult, onChangeEvaluationResult, survey, surveyChapterResults } = props;

  const showWeights = survey.calculationFormulaType === SURVEY_FORMULA_TYPE['Medie ponderată'];

  const gridContainerProps = {
    container: true,
    spacing: 2,
    alignItems: 'flex-start',
  };

  return (
    <>
      {survey.surveyChapters.map((chapter, chapterIndex) => {
        const chapterResult = surveyChapterResults && surveyChapterResults[chapter?._id];

        return (
          <Box
            key={chapter._id}
            sx={{
              border: '0.0625rem solid rgb(222, 226, 230)',
              borderRadius: 2,
              p: 3,
              py: 4,
              ...(chapterResult
                ? {
                    mb: 5,
                  }
                : {
                    mb: 3,
                  }),
            }}
          >
            <Grid {...gridContainerProps}>
              <Grid item xs={12} md={9} lg={10}>
                <Box sx={{ display: 'flex', mb: 3 }}>
                  <Box sx={{ minWidth: 32, mr: 2 }}>
                    <Typography sx={{ fontWeight: 900 }}>{chapterIndex + 1}</Typography>
                  </Box>

                  <Box>
                    <Box>
                      <Typography sx={{ fontWeight: 900 }}>{chapter.name}</Typography>
                    </Box>
                  </Box>
                </Box>
              </Grid>
              <Grid item xs={12} md={3} lg={2}>
                <Box>
                  {showWeights ? (
                    <Typography sx={{ textAlign: 'right', fontWeight: 500 }}>
                      Pondere criteriu:&nbsp;
                      <strong style={{ minWidth: '30px', display: 'inline-block' }}>
                        {chapter.weight}
                      </strong>
                    </Typography>
                  ) : null}
                </Box>
              </Grid>
            </Grid>

            {chapter?.questions?.map((question, questionIndex) => {
              const questionValue = evaluationResult[question._id];

              return (
                <Grid key={question._id} {...gridContainerProps} sx={{ mt: 0.5 }}>
                  <Grid item xs={12} md={9} lg={10}>
                    <Box sx={{ display: 'flex' }}>
                      <Box sx={{ minWidth: 32, mr: 2 }}>
                        <Typography>
                          {chapterIndex + 1}.{questionIndex + 1}
                        </Typography>
                      </Box>

                      <Box>
                        <Box>
                          <Typography>{question.name}</Typography>
                        </Box>
                      </Box>
                    </Box>
                  </Grid>
                  <Grid item xs={12} md={3} lg={2}>
                    <Box sx={{ display: 'flex', justifyContent: 'flex-end' }}>
                      <ToggleButtonGroup
                        value={questionValue}
                        disabled={!onChangeEvaluationResult}
                        color="primary"
                        exclusive
                        onChange={(_, value: number) => {
                          value &&
                            onChangeEvaluationResult &&
                            onChangeEvaluationResult(question._id, value);
                        }}
                      >
                        {Array(5)
                          .fill(1)
                          .map((_, index) => {
                            return (
                              <ToggleButton
                                key={index}
                                value={index + 1}
                                sx={{
                                  px: 1.5,
                                  py: 0.75,
                                  '&:hover': {
                                    color: 'text.primary',
                                  },
                                  '&.Mui-selected': {
                                    color: '#FFFFFF',
                                    bgcolor: 'primary.dark',
                                    '&:hover': {
                                      color: '#FFFFFF',
                                      bgcolor: 'primary.dark',
                                      opacity: 0.7,
                                    },
                                  },
                                }}
                              >
                                {index + 1}
                              </ToggleButton>
                            );
                          })}
                      </ToggleButtonGroup>
                    </Box>
                  </Grid>

                  {/* @ts-ignore */}
                  {questionIndex < chapter.questions.length - 1 && (
                    <Grid item xs={12}>
                      <Divider />
                    </Grid>
                  )}
                </Grid>
              );
            })}

            {chapterResult && (
              <Grid
                {...gridContainerProps}
                sx={{
                  mt: 2,
                }}
              >
                <Grid item xs={12}>
                  <Box
                    sx={{
                      borderTop: 1,
                      borderColor: 'text.primary',
                    }}
                  />
                </Grid>
                <Grid item xs={12} md={9} lg={10}>
                  <Typography>Nota criteriu</Typography>
                  {showWeights && (
                    <Box sx={{}}>
                      <Typography variant="body2" color="textSecondary">
                        <InfoIcon fontSize="small" /> ponderea criteriului x media calificativelor
                        acordate
                      </Typography>
                    </Box>
                  )}
                </Grid>
                <Grid item xs={12} md={3} lg={2}>
                  <Box
                    sx={{
                      bgcolor: 'background.default',
                      px: 2,
                      py: 1,
                    }}
                  >
                    <Typography sx={{ textAlign: 'right', fontWeight: 900 }}>
                      {chapterResult}
                    </Typography>
                  </Box>
                </Grid>
              </Grid>
            )}
          </Box>
        );
      })}

      <Button id="evaluationSubmit" type="submit" sx={{ display: 'none' }} />
    </>
  );
};

export default EvaluationSurveyQuestions;
