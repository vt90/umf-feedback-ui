import React, { Fragment } from 'react';
import Box from '@mui/material/Box';
import Grid from '@mui/material/Grid';
import Typography from '@mui/material/Typography';
import { SxProps } from '@mui/material';
import { ISurveyObjective } from '../../../models/surveyObjective';
import { ISurvey } from '../../../models/survey';

interface ISurveyObjectiveListItemProps {
  onSelect: (field?: string) => void;
  survey: ISurvey;
  surveyObjective: ISurveyObjective;
}

interface ITypographyProps {
  id: string;
  onClick: (field?: string) => void;
  readOnly: boolean;
  sx?: SxProps;
  text: string | number;
}

const RenderTypography = (props: ITypographyProps) => {
  const { id, onClick, readOnly, sx, text } = props;

  return (
    <Typography
      onClick={(ev) => {
        ev.preventDefault();
        ev.stopPropagation();
        !readOnly && onClick(id);
      }}
      sx={{
        py: 1,
        pl: 2,
        border: 1,
        borderRadius: 1,
        borderColor: 'transparent',
        '&:hover': {
          borderColor: readOnly ? 'transparent' : '#3e3e3e',
        },
        ...sx,
      }}
    >
      <strong>{text}</strong>
    </Typography>
  );
};

const SurveyObjectiveListItem = (props: ISurveyObjectiveListItemProps) => {
  const { onSelect, survey, surveyObjective } = props;

  const readOnly = !!survey.isClone;

  return (
    <>
      <Box>
        <Box
          sx={{
            border: '0.0625rem solid rgb(222, 226, 230)',
            borderRadius: 2,
          }}
          onClick={() => !readOnly && onSelect()}
        >
          <Box sx={{ p: 3 }}>
            <Grid container spacing={2} alignItems="center">
              <Grid item xs={12}>
                <Box sx={{ display: 'flex' }}>
                  <Box sx={{ flexGrow: 1, mr: 2 }}>
                    <RenderTypography
                      text={surveyObjective.name}
                      id="name"
                      onClick={onSelect}
                      readOnly={readOnly}
                    />
                  </Box>
                </Box>
              </Grid>

              {surveyObjective.questions?.map((question, surveyObjectiveQuestionIndex) => (
                <Fragment key={question._id}>
                  <Grid item xs={1}>
                    <Typography variant="body2" sx={{ textAlign: { xs: 'left', md: 'right' } }}>
                      {surveyObjectiveQuestionIndex + 1}
                    </Typography>
                  </Grid>

                  <Grid item xs={11} sx={{ display: 'flex', alignItems: 'center' }}>
                    <Box
                      sx={{
                        flexGrow: 1,
                        alignItems: 'center',
                      }}
                    >
                      <RenderTypography
                        text={question.name}
                        id={`questions.${surveyObjectiveQuestionIndex}.name`}
                        onClick={onSelect}
                        sx={{ px: '14px', py: '8.5px', minWidth: 64 }}
                        readOnly={readOnly}
                      />
                    </Box>

                    <Box
                      sx={{
                        display: 'flex',
                        alignItems: 'center',
                        pl: 2,
                      }}
                    >
                      <Typography variant="body2">% din timp:</Typography>
                      <RenderTypography
                        text={`${question.timePercentage}%`}
                        id={`questions.${surveyObjectiveQuestionIndex}.timePercentage`}
                        onClick={onSelect}
                        sx={{ px: '14px', py: '8.5px', minWidth: 64 }}
                        readOnly={readOnly}
                      />
                    </Box>

                    <Box
                      sx={{
                        display: 'flex',
                        alignItems: 'center',
                        pl: 2,
                      }}
                    >
                      <Typography variant="body2">% realizat:</Typography>
                      <RenderTypography
                        text={`${question.donePercentage}%`}
                        id={`questions.${surveyObjectiveQuestionIndex}.donePercentage`}
                        onClick={onSelect}
                        sx={{ px: '14px', py: '8.5px', minWidth: 64 }}
                        readOnly={readOnly}
                      />
                    </Box>
                  </Grid>
                </Fragment>
              ))}
            </Grid>
          </Box>
        </Box>
      </Box>
    </>
  );
};

export default SurveyObjectiveListItem;
