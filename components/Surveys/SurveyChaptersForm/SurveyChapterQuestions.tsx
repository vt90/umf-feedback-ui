import React, { Fragment } from 'react';
import { Control, useFieldArray } from 'react-hook-form';
import get from 'lodash/get';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import Grid from '@mui/material/Grid';
import IconButton from '@mui/material/IconButton';
import Typography from '@mui/material/Typography';
import DeleteIcon from '@mui/icons-material/Delete';
import TextField from '../../Common/FormInputs/TextField';

interface ISurveyChapterQuestionsFieldProps {
  control: Control;
  errors: any;
  isLoading: boolean;
  register: any;
  surveyChapterIndex: number;
}

const SurveyChapterQuestionsField = (props: ISurveyChapterQuestionsFieldProps) => {
  const { control, errors, isLoading, register, surveyChapterIndex } = props;

  const {
    fields: surveyChaptersQuestions,
    append: appendSurveyChapterQuestions,
    remove: removeSurveyChapterQuestions,
  } = useFieldArray({
    control,
    name: `questions`,
  });

  return (
    <>
      {surveyChaptersQuestions.map((surveyChapterQuestion, surveyChapterQuestionIndex) => {
        const key = `questions.${surveyChapterQuestionIndex}`;
        const chapterQuestionErrors = get(errors, key);

        return (
          <Fragment key={key}>
            <Grid item xs={1}>
              <input type="hidden" {...register(`${key}._id`)} />
              <Button
                id={`addQuestionButton`}
                onClick={() =>
                  appendSurveyChapterQuestions(
                    {
                      name: `Competența de evaluare ${surveyChaptersQuestions.length + 1}`,
                      order: surveyChaptersQuestions.length + 1,
                    },
                    {
                      shouldFocus: true,
                      focusName: `questions.${surveyChaptersQuestions.length}.name`,
                    },
                  )
                }
                sx={{ display: 'none' }}
              />
              <Typography variant="body2" sx={{ textAlign: { xs: 'left', md: 'right' } }}>
                {surveyChapterIndex + 1}.{surveyChapterQuestionIndex + 1}
              </Typography>
            </Grid>

            <Grid item xs={11}>
              <Box sx={{ display: 'flex', alignItems: 'center' }}>
                <Box sx={{ mr: 1, flexGrow: 1 }}>
                  <TextField
                    multiline
                    required
                    disabled={isLoading}
                    errorMessage={!!chapterQuestionErrors?.name && 'Competența este necesară'}
                    {...register(`${key}.name`)}
                    size="small"
                    sx={{
                      '& legend': { display: 'none' },
                      '& fieldset': {
                        top: 0,
                        borderColor: 'transparent',
                      },
                    }}
                    inputProps={{
                      sx: {
                        typography: 'body1',
                        fontWeight: 'bold',
                      },
                    }}
                  />
                </Box>

                <IconButton
                  onClick={() => removeSurveyChapterQuestions(surveyChapterQuestionIndex)}
                  color="error"
                >
                  <DeleteIcon />
                </IconButton>
              </Box>
            </Grid>
          </Fragment>
        );
      })}
    </>
  );
};

export default SurveyChapterQuestionsField;
