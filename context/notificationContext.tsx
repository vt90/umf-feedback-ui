import React, { createContext, useContext, useState } from 'react';
import Alert from '@mui/material/Alert';
import Snackbar from '@mui/material/Snackbar';

const NotificationContextContext = createContext({
  setError: (error: string | null | undefined) => null,
  setMessage: (message: string | null | undefined) => null,
});

interface INotificationContextWrapperProps {
  children: React.ReactNode;
}

export function NotificationContextProvider(props: INotificationContextWrapperProps) {
  const [message, setMessage] = useState<string | null | undefined>(null);
  const [error, setError] = useState<string | null | undefined>(null);
  const { children } = props;
  const handleClose = () => {
    setError(null);
    setMessage(null);
  };

  const severity = error ? 'error' : 'success';

  return (
    // @ts-ignore
    <NotificationContextContext.Provider value={{ setError, setMessage }}>
      {(error || message) && (
        <Snackbar
          open={!!(error || message)}
          autoHideDuration={4000}
          onClose={handleClose}
          anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
        >
          <Alert
            onClose={handleClose}
            severity={severity}
            sx={{
              boxShadow: 4,
              bgcolor: `${severity}.light`,
              color: '#FFF',
              '& svg': {
                fill: '#FFF',
              },
            }}
          >
            {/* @ts-ignore */}
            {error?.message || error || message}
          </Alert>
        </Snackbar>
      )}

      {children}
    </NotificationContextContext.Provider>
  );
}

export function useNotificationContext() {
  return useContext(NotificationContextContext);
}
