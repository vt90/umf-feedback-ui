import React, { useCallback, useMemo } from 'react';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import Tooltip from '@mui/material/Tooltip';
import EnhancedTable from '../../Common/Table';
import moment from 'moment';
import NoDataAvailable from '../../Common/NoDataAvailable';
import YesIcon from '@mui/icons-material/CheckBox';
import NoIcon from '@mui/icons-material/DisabledByDefault';
import { ISSMPeriodicTrainingSession } from '../../../models/ssmPeriodicTrainingSession';
import { TRAINING_MONTHS } from '../../../models/ssmPeriodicTraining';
import Button from '@mui/material/Button';

interface IUserUserListProps {
  ssmPeriodicTrainingsSessions: ISSMPeriodicTrainingSession[];
  onSelectSession: (session: ISSMPeriodicTrainingSession) => void;
  title?: string;
}

const UserSSMTrainingSessionList = (props: IUserUserListProps) => {
  const { ssmPeriodicTrainingsSessions, onSelectSession, title } = props;

  const headerRows = useMemo(() => {
    return [
      {
        id: 'createdAt',
        label: 'Perioada instruirii',
        align: 'left',
        colSpan: 2,
        disableSorting: true,
      },
      { id: 'training', label: 'Instruire', align: 'left', colSpan: 3 },
      { id: 'department', label: 'Departament', align: 'left', colSpan: 3 },
      { id: 'userAcknowledged', label: 'Semnătură celui instruit', align: 'center', colSpan: 1 },
      {
        id: 'contraEvaluated',
        label: 'Semnătură celui care a instruit',
        align: 'center',
        colSpan: 1,
      },
      { id: 'ssmEvaluated', label: 'Instruire verificată', align: 'center', colSpan: 1 },
      { id: 'actions', label: 'Acțiuni', align: 'right', colSpan: 1 },
    ];
  }, []);

  const renderIcon = useCallback((value: boolean | null | undefined, yesText: string) => {
    const color = value ? 'success' : 'info';
    const tooltipText = value ? yesText : 'În așteptare';

    const iconProps = {
      sx: {
        color: `${color}.light`,
      },
    };

    return (
      <Tooltip title={tooltipText}>
        <Box>
          {value ? (
            // @ts-ignore
            <YesIcon {...iconProps} />
          ) : (
            // @ts-ignore
            <NoIcon {...iconProps} />
          )}
        </Box>
      </Tooltip>
    );
  }, []);

  const getHeaderRows = useCallback(() => {
    return ssmPeriodicTrainingsSessions.map((session) => {
      const {
        _id,
        userDepartmentAssignmentId,
        ssmPeriodicTrainingId,
        sessionDate,
        userAcknowledged,
        contraEvaluated,
        ssmEvaluated,
      } = session;
      const created = moment(sessionDate);

      return {
        _id,
        smallDeviceHeader: ssmPeriodicTrainingId?.name,

        // @ts-ignore
        training: ssmPeriodicTrainingId?.name,
        department: (
          // @ts-ignore
          <Tooltip title={userDepartmentAssignmentId?.role}>
            <Typography
              variant="body2"
              color="textSecondary"
              sx={{
                textDecoration: userDepartmentAssignmentId?.deactivated ? 'line-through' : 'none',
              }}
            >
              {/* @ts-ignore */}
              {userDepartmentAssignmentId?.departmentId?.name}
            </Typography>
          </Tooltip>
        ),
        createdAt: TRAINING_MONTHS[created.month()] + ' ' + created.format('YYYY'),
        contraEvaluated: renderIcon(contraEvaluated, 'Instruire semnată de către instruit'),
        userAcknowledged: renderIcon(
          userAcknowledged,
          'Instruire semnată de către persoana care a instruit',
        ),
        ssmEvaluated: renderIcon(ssmEvaluated, 'Instruire verificată'),
        data: session,
        actions: (
          <>
            <Button onClick={() => onSelectSession(session)} variant="contained" size="small">
              Vizualizează
            </Button>
          </>
        ),
        sx: userDepartmentAssignmentId?.deactivated
          ? {
              bgcolor: 'action.hover',
            }
          : null,
      };
    });
  }, [ssmPeriodicTrainingsSessions, renderIcon]);

  if (!ssmPeriodicTrainingsSessions?.length) {
    return (
      <NoDataAvailable content="În momentul de faţă nu există nicio instruire disponibil pe platformă" />
    );
  }

  return (
    <>
      <EnhancedTable
        headerRows={headerRows}
        initialOrderByField="createdAt"
        initialOrder="desc"
        orderByFieldCriteriaFunctions={{
          createdAt: (row: any) => row?.data?.createdAt,
          // department: (row: any) => row?.data?.departmentId?.name,
          contraEvaluated: (row: any) => row?.data?.contraEvaluated,
          userAcknowledged: (row: any) => row?.data?.userAcknowledged,
          contraEvaluatorAgreement: (row: any) => row?.data?.contraEvaluatorAgreement,
        }}
        PaperComponent={Box}
        paperComponentStyles={{
          px: 3,
          py: 2,
          border: '0.0625rem solid rgb(222, 226, 230)',
          borderRadius: 2,
          '& h6': {
            mt: 1,
          },
        }}
        rows={getHeaderRows()}
        smallUntilBreakpoint="md"
        disableOrder
        title={title}
      />
    </>
  );
};

export default UserSSMTrainingSessionList;
