import React from 'react';
import { AlertColor } from '@mui/material/Alert';
import { SxProps } from '@mui/material';
import Box from '@mui/material/Box';
import Chip from '@mui/material/Chip';
import Tooltip from '@mui/material/Tooltip';

interface IStatusProps {
  label: string;
  tooltipText?: string;
  color: AlertColor;
  sx?: SxProps;
}

const Status = (props: IStatusProps) => {
  const { color, label, tooltipText, sx } = props;

  const containerProps = {
    ...(tooltipText && {
      title: tooltipText,
    }),
  };

  const Container = tooltipText ? Tooltip : Box;

  return (
    // @ts-ignore
    <Container {...containerProps}>
      <Chip
        label={label}
        variant="filled"
        size="small"
        sx={{
          color: `${color}.dark`,
          position: 'relative',
          bgcolor: 'transparent',
          typography: 'caption',
          minWidth: 90,
          '& :after': {
            content: '""',
            position: 'absolute',
            width: '100%',
            height: '100%',
            top: 0,
            left: 0,
            opacity: 0.3,
            bgcolor: `${color}.light`,
            zIndex: 1,
            borderRadius: 1,
          },
          ...sx,
        }}
      />
    </Container>
  );
};

export default Status;
