import axios from 'axios';
import { IPaginationResult } from '../models/base';
import { ISSMPeriodicTrainingSession } from '../models/ssmPeriodicTrainingSession';
import { IEvaluation } from '../models/evaluation';
import { ISSMPackage } from '../models/ssmPackage';

const createInstance = (baseURL = '/api/ssm-periodic-training-session') => {
  const httpInstance = axios.create({
    baseURL,
  });

  httpInstance.interceptors.response.use(
    (response) => response.data,
    (error) => {
      if (error && error.response && error.response.data) {
        return Promise.reject(
          error.response.data.message
            ? `${error.response.data.message || error.response.data.error}`
            : error.response.data?.error || error.response.data,
        );
      } else {
        return Promise.reject({ message: 'Server error' });
      }
    },
  );

  return httpInstance;
};

const httpInstance = createInstance();

export const findSSMPeriodicTrainingSessions = async (
  params: Partial<ISSMPeriodicTrainingSession>,
): Promise<IPaginationResult<ISSMPeriodicTrainingSession>> => {
  return await httpInstance.get('', { params });
};

export const findMySSMPeriodicTrainingSessions = async (
  params: Partial<ISSMPeriodicTrainingSession>,
): Promise<IPaginationResult<ISSMPeriodicTrainingSession>> => {
  return await httpInstance.get('/my-sessions', { params });
};

export const acknowledge = async (id: string, ssmPackages: ISSMPackage[]): Promise<IEvaluation> => {
  return await httpInstance.post(`/acknowledge/${id}`, { ssmPackages });
};

export const contraSign = async (id: string): Promise<IEvaluation> => {
  return await httpInstance.post(`/contra-sign/${id}`);
};

export const ssmSign = async (id: string): Promise<IEvaluation> => {
  return await httpInstance.post(`/ssm-sign/${id}`);
};
