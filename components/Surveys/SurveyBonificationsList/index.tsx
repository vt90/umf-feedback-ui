import React, { useCallback, useEffect, useState } from 'react';
import Button from '@mui/material/Button';
import AddIcon from '@mui/icons-material/Add';
import Box from '@mui/material/Box';
import Card from '@mui/material/Card';
import LoadingButton from '@mui/lab/LoadingButton';
import Typography from '@mui/material/Typography';
import SurveyBonificationForm from '../SurveyBonificationsForm';
import { ISurvey, ISurveyBonification } from '../../../models/survey';
import NoDataAvailable from '../../Common/NoDataAvailable';
import SurveyBonificationListItem from './SurveyChapterListItem';
import Dialog from '../../Common/Dialog';

interface ISurveyBonificationsListProps {
  isLoading: boolean;
  onSubmit: (data: any) => void;
  onRemoveSurveyBonification: (surveyBonification: ISurveyBonification) => void;
  survey: ISurvey;
}

const SurveyBonificationsList = (props: ISurveyBonificationsListProps) => {
  const { isLoading, onSubmit, onRemoveSurveyBonification, survey } = props;

  const [createFormValues, setCreateFormValues] = useState<Partial<ISurveyBonification> | null>(
    null,
  );
  const [selectedBonificationForRemoval, setSelectedBonificationForRemoval] =
    useState<ISurveyBonification | null>(null);

  const [formFocus, setFormFocus] = useState<string | undefined>(undefined);

  const resetStates = useCallback(() => {
    setCreateFormValues(null);
    setSelectedBonificationForRemoval(null);
    setFormFocus(undefined);
  }, []);

  useEffect(() => {
    resetStates();
  }, [resetStates, survey]);

  if (!survey) return null;

  const createButton = !createFormValues && (
    <Button
      onClick={() =>
        setCreateFormValues({
          // @ts-ignore
          name: `Bonificația ${survey?.surveyBonifications?.length + 1}`,
          value: 0,
        })
      }
      variant="contained"
      size="large"
      endIcon={<AddIcon />}
    >
      Adaugă bonificaţii
    </Button>
  );

  return (
    <>
      {survey.surveyBonifications?.length || createFormValues ? (
        <>
          <Card elevation={1}>
            <Box sx={{ p: 3 }}>
              {survey.surveyBonifications?.map((surveyBonification, index) => {
                const isInEditMode =
                  createFormValues && surveyBonification._id === createFormValues._id;
                return (
                  <Box key={surveyBonification._id} sx={{ mb: 2 }}>
                    {isInEditMode ? (
                      <SurveyBonificationForm
                        // @ts-ignore
                        bonificationIndex={index + 1}
                        initialFocus={formFocus}
                        initialValues={createFormValues}
                        isLoading={isLoading}
                        onSubmit={onSubmit}
                        onCancel={() => setCreateFormValues(null)}
                        onRemoveSurveyBonification={setSelectedBonificationForRemoval}
                      />
                    ) : (
                      <>
                        <SurveyBonificationListItem
                          onSelect={(id) => {
                            id && setFormFocus(id);
                            setCreateFormValues({ ...surveyBonification });
                          }}
                          survey={survey}
                          surveyBonification={surveyBonification}
                          surveyBonificationIndex={index}
                        />
                      </>
                    )}
                  </Box>
                );
              })}

              {createFormValues && !createFormValues._id && (
                <SurveyBonificationForm
                  // @ts-ignore
                  bonificationIndex={survey?.surveyBonifications?.length + 1}
                  initialValues={createFormValues}
                  isLoading={isLoading}
                  onSubmit={onSubmit}
                  onCancel={() => setCreateFormValues(null)}
                />
              )}
            </Box>
          </Card>

          <Box sx={{ mt: 3 }}>{createButton}</Box>
        </>
      ) : (
        <NoDataAvailable
          content="În momentul de faţă nu există niciun criteriu de evaluare definit"
          actions={createButton}
        />
      )}

      <Dialog
        actions={
          <LoadingButton
            variant="contained"
            onClick={() => {
              if (selectedBonificationForRemoval?._id) {
                onRemoveSurveyBonification(selectedBonificationForRemoval);
              }
            }}
            loading={isLoading}
          >
            Șterge
          </LoadingButton>
        }
        content={
          <>
            <Typography color="textSecondary" textAlign="center">
              Sunteți siguri că doriți să ștergeți bonificația
              <strong>
                <i>&quot;{selectedBonificationForRemoval?.name}&quot;</i>
              </strong>
              ?
            </Typography>
          </>
        }
        title="Confirmati ștergerea?"
        onClose={() => setSelectedBonificationForRemoval(null)}
        open={!!selectedBonificationForRemoval}
      />
    </>
  );
};

export default SurveyBonificationsList;
