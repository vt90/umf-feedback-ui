import axios from 'axios';
import { IEvaluation, IFindEvaluations } from '../models/evaluation';
import { IPaginationResult } from '../models/base';

const httpInstance = axios.create({
  baseURL: '/api/evaluations',
});

const exportHttpInstance = axios.create({
  baseURL: `${process.env.NEXT_PUBLIC_API_URL}/api/evaluations`,
});

httpInstance.interceptors.response.use(
  (response) => response.data,
  (error) => {
    if (error && error.response && error.response.data) {
      return Promise.reject(
        error.response.data.message
          ? `${error.response.data.message || error.response.data.error}`
          : error.response.data?.error || error.response.data,
      );
    } else {
      return Promise.reject({ message: 'Server error' });
    }
  },
);

const cleanupEvaluationBody = (evaluation: Partial<IEvaluation>): Partial<IEvaluation> => {
  // @ts-ignore
  delete evaluation._id;
  // @ts-ignore
  delete evaluation.id;
  // @ts-ignore
  delete evaluation.evaluatorId;
  // @ts-ignore
  delete evaluation.surveyChapterResults;
  // @ts-ignore
  delete evaluation.finalResult;
  // @ts-ignore
  delete evaluation.finalGrade;
  // @ts-ignore
  delete evaluation.hasChanges;
  // @ts-ignore
  delete evaluation.userAcknowledged;
  // @ts-ignore
  delete evaluation.createdAt;
  // @ts-ignore
  delete evaluation.salaryChangePercentage;
  // @ts-ignore
  delete evaluation.updatedAt;
  // @ts-ignore
  delete evaluation['__v'];

  Object.keys(evaluation).forEach((key) => {
    // @ts-ignore
    if (evaluation?.[key]?._id) {
      // @ts-ignore
      evaluation[key] = evaluation?.[key]?._id;
    }
  });

  return evaluation;
};

export const getEvaluations = async (
  params: Partial<IFindEvaluations>,
): Promise<IPaginationResult<IEvaluation>> => {
  return await httpInstance.get('', { params });
};

export const exportEvaluations = async (params: Partial<IFindEvaluations>): Promise<void> => {
  const { data } = await axios.get('/api/get-access-token');

  const response = await exportHttpInstance.get('/export', {
    params,
    responseType: 'blob',
    headers: {
      Authorization: `Bearer ${data?.accessToken}`,
    },
  });

  // Creating an object URL
  // Creating a Blob object from the response data
  const blob = new Blob([response.data], { type: 'application/zip' });

  const url = window.URL.createObjectURL(blob);

  // Creating a temporary anchor tag to trigger the download
  const a = document.createElement('a');
  a.href = url;
  a.download = 'export-evaluari.zip'; // you can name the file here
  document.body.appendChild(a);
  a.click();

  // Releasing the object URL after the download
  window.URL.revokeObjectURL(url);
};

export const getContraEvaluations = async (
  params: Partial<IEvaluation>,
): Promise<IEvaluation[]> => {
  return await httpInstance.get('/contra-evaluations', { params });
};

export const getEvaluationById = async (id: string): Promise<IEvaluation> => {
  return await httpInstance.get(`/${id}`);
};

export const acknowledge = async (id: string): Promise<IEvaluation> => {
  return await httpInstance.post(`/acknowledge/${id}`);
};

export const refuseSalaryChange = async (id: string): Promise<IEvaluation> => {
  return await httpInstance.post(`/refuse-salary-change/${id}`);
};

export const createOrUpdateEvaluation = async (
  evaluation: Partial<IEvaluation>,
): Promise<IEvaluation> => {
  const isUpdate = !!evaluation._id;
  const url = isUpdate ? `/${evaluation._id}` : ``;
  const req = isUpdate ? httpInstance.patch : httpInstance.post;
  return await req(url, cleanupEvaluationBody(evaluation));
};

export const calculateEvaluationResult = async (
  evaluation: Partial<IEvaluation>,
): Promise<Partial<IEvaluation>> => {
  return await httpInstance.post('/calculate', cleanupEvaluationBody({ ...evaluation }));
};

export const contraSignEvaluation = async (
  data: Partial<IEvaluation>,
): Promise<Partial<IEvaluation>> => {
  return await httpInstance.post(`/contra-sign/${data?._id}`, cleanupEvaluationBody({ ...data }));
};

export const removeEvaluation = async (evaluationId: string): Promise<any> => {
  return await httpInstance.delete(`/${evaluationId}`);
};

export const setUserAgreement = async (
  data: Partial<IEvaluation>,
): Promise<Partial<IEvaluation>> => {
  return await httpInstance.post(
    `/set-user-agreement/${data?._id}`,
    cleanupEvaluationBody({ ...data }),
  );
};
