import { ISSMPeriodicTraining } from '../../../models/ssmPeriodicTraining';
import Box from '@mui/material/Box';
import Grid from '@mui/material/Grid';
import IconButton from '@mui/material/Button';
import EditIcon from '@mui/icons-material/Edit';
import DeleteIcon from '@mui/icons-material/Delete';
import Typography from '@mui/material/Typography';
import React from 'react';
import MonthsInput from '../MonthsInput';
import Status from '../../Common/Status';

interface ISSMPeriodicTrainingList {
  ssmPeriodicTrainings: ISSMPeriodicTraining[];
  functions: ISSMPeriodicTraining[];
  onEdit?: (ssmPeriodicTraining: ISSMPeriodicTraining) => void;
  onDelete?: (ssmPeriodicTraining: ISSMPeriodicTraining) => void;
}

const SSMPeriodicTrainingList = (props: ISSMPeriodicTrainingList) => {
  const { ssmPeriodicTrainings, onEdit, onDelete } = props;

  return (
    <>
      {ssmPeriodicTrainings?.map((ssmPeriodicTraining) => {
        return (
          <Box
            key={ssmPeriodicTraining._id}
            sx={{ p: 2, border: '0.0625rem solid rgb(222, 226, 230)', borderRadius: 2, mb: 3 }}
          >
            <Box sx={{ display: 'flex' }}>
              <Box sx={{ flexGrow: 1 }}>
                <Typography variant="h6">{ssmPeriodicTraining.name}</Typography>
              </Box>

              {onEdit && (
                <IconButton
                  onClick={() => onEdit(ssmPeriodicTraining)}
                  sx={{ color: 'text.primary' }}
                  startIcon={<EditIcon fontSize="inherit" />}
                >
                  Modifică
                </IconButton>
              )}

              {onDelete && (
                <IconButton
                  onClick={() => onDelete(ssmPeriodicTraining)}
                  color="error"
                  startIcon={<DeleteIcon fontSize="inherit" />}
                >
                  Șterge
                </IconButton>
              )}
            </Box>

            <Box sx={{ mt: 3 }}>
              <Grid container spacing={2} alignItems="center">
                {ssmPeriodicTraining.shortDescription && (
                  <Grid item xs={12}>
                    <Typography color="textSecondary">
                      {ssmPeriodicTraining.shortDescription}
                    </Typography>
                  </Grid>
                )}

                <Grid item xs={4} md={3} lg={2}>
                  <Typography color="textSecondary">Perioda de instruire</Typography>
                </Grid>
                <Grid item xs={8} md={9} lg={10}>
                  <Box
                    sx={{ border: '0.0625rem solid rgb(222, 226, 230)', display: 'inline-block' }}
                  >
                    <MonthsInput
                      // @ts-ignore
                      value={ssmPeriodicTraining.monthsOfYear}
                      onChange={console.log}
                      disabled
                      sx={{
                        cursor: 'default !important',
                        '& *': {
                          cursor: 'default !important',
                          py: '0 !important',
                        },
                      }}
                    />
                  </Box>
                </Grid>

                <Grid item xs={4} md={3} lg={2}>
                  <Typography color="textSecondary">Status</Typography>
                </Grid>
                <Grid item xs={8} md={9} lg={10}>
                  <Status
                    label={ssmPeriodicTraining.isActive ? 'Activa' : 'Inactiva'}
                    color={ssmPeriodicTraining.isActive ? 'success' : 'info'}
                    sx={{ py: 1.75 }}
                  />
                </Grid>
              </Grid>
            </Box>
          </Box>
        );
      })}
    </>
  );
};

export default SSMPeriodicTrainingList;
