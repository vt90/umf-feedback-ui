import { NextApiRequest, NextApiResponse } from 'next';
import { withApiAuthRequired } from '../../guards/authRequired';
import { unstable_getServerSession } from 'next-auth/next';
import { authOptions } from './auth/[...nextauth]';

export default withApiAuthRequired(async function (req: NextApiRequest, res: NextApiResponse) {
  const session = await unstable_getServerSession(
    // @ts-ignore
    req,
    res,
    authOptions,
  );

  // @ts-ignore
  res.status(200).json({ accessToken: session?.accessToken });
});
