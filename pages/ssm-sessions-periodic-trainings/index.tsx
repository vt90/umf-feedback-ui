import React, { useState } from 'react';
import Head from 'next/head';
import { withPageAuthRequired } from '../../guards/authRequired';
import {
  contraSign,
  ssmSign,
  findSSMPeriodicTrainingSessions,
} from '../../services/ssmPeriodicTrainingSessions';
import { useMutation, useQuery, useQueryClient } from '@tanstack/react-query';
import AdminPage from '../../components/AdminPage';
import NoDataAvailable from '../../components/Common/NoDataAvailable';
import { getAllFunctions } from '../../services/functions';
import ListLoadingIndicator from '../../components/Common/ListLoadingIndcator';
import Box from '@mui/material/Box';
import Card from '@mui/material/Card';
import Grid from '@mui/material/Grid';
import moment from 'moment';
import Pagination from '../../components/Common/Pagination';
import SSMTrainingSessionList from '../../components/SSMPeriodicTrainingSession/List';
import { getUserSubUsers } from '../../services/userDepartmentAssignment';
import { useRouter } from 'next/router';
import { ISSMPeriodicTrainingSession } from '../../models/ssmPeriodicTrainingSession';
import { useAuthContext } from '../../context/authContext';
import SSMTrainingSessionDetails from '../../components/SSMPeriodicTrainingSession/Details';
import Button from '@mui/lab/LoadingButton';
import Dialog from '../../components/Common/Dialog';
import { findSSMPackage } from '../../services/ssmPackages';
import { useNotificationContext } from '../../context/notificationContext';
import SSMTrainingSessionFilter from '../../components/SSMPeriodicTrainingSession/Filter';
import useDebounce from '../../hooks/useDebounce';
import HREvaluationsListHeader from '../../components/Evaluation/HRUserEvaluationsList/Header';
import { SSM_PACKAGE_TYPE } from '../../models/ssmPackage';
const FETCH_QUERY_KEY = ['ssm-periodic-sessions-trainings'];
const FETCH_PACKAGES_QUERY_KEY = ['training-ssm-packages'];
const FETCH_FUNCTIONS_QUERY_KEY = ['depFunctions'];

const SSMPeriodicTrainingSessions = () => {
  const [pageNumber, setPageNumber] = useState(1);
  const [order, setOrder] = useState(-1);
  const [orderBy, setOrderBy] = useState('userAcknowledged');
  const [email, setEmail] = useState<string | null>('');
  const [sessionDate, setSessionDate] = useState<Date>(moment().utc().startOf('month').toDate());
  const [userAcknowledged, setUserAcknowledged] = useState<string | null>('');
  const [contraEvaluated, setContraEvaluated] = useState<string | null>('');
  const [selectedSession, setSelectedSession] = useState<ISSMPeriodicTrainingSession | null>(null);
  const [pageSize] = useState(50);
  const { setError, setMessage } = useNotificationContext();
  const router = useRouter();
  const { user } = useAuthContext();
  const queryClient = useQueryClient();

  const userSearchTerm = useDebounce(`${email}`, 600);

  const { data: subUsers } = useQuery(['sub-users'], getUserSubUsers, {
    onError: () => router.push('/404'),
  });

  const fetchParams = {
    limit: pageSize,
    offset: (pageNumber - 1) * pageSize,
    order,
    orderBy,
    sessionDate: moment(sessionDate).format('YYYY-MM-DD'),
    userAcknowledged,
    contraEvaluated,
    userSearchTerm,
    // @ts-ignore
    ...(user?.isSSM
      ? {}
      : {
          // @ts-ignore
          userDepartmentAssignmentIds: subUsers?.results?.map((assingment) => assingment._id),
        }),
  };

  const { isLoading, data: ssmPeriodicTrainingsSessions } = useQuery(
    [FETCH_QUERY_KEY, fetchParams],
    // @ts-ignore
    () => findSSMPeriodicTrainingSessions(fetchParams),
    {
      // @ts-ignore
      enabled: user?.isSSM || !!subUsers?.count,
    },
  );

  const { data: functions } = useQuery(FETCH_FUNCTIONS_QUERY_KEY, getAllFunctions);

  const { data: ssmPackages } = useQuery(
    [FETCH_PACKAGES_QUERY_KEY, selectedSession?.userDepartmentAssignmentId?.jobFunction],
    () =>
      findSSMPackage({
        // @ts-ignore
        functions: [selectedSession?.userDepartmentAssignmentId?.jobFunction],
        // @ts-ignore
        userDepartments: [selectedSession?.userDepartmentAssignmentId?.departmentId?._id],
        type: SSM_PACKAGE_TYPE.PERIODIC,
      }),
    {
      enabled: !!selectedSession,
    },
  );

  const { isLoading: isContraSigning, mutate: contraSignInit } = useMutation(
    // @ts-ignore
    () => contraSign(selectedSession._id),
    {
      onSuccess: async () => {
        await queryClient.invalidateQueries([FETCH_QUERY_KEY, fetchParams]);
        setSelectedSession(null);
        setMessage('Instruire periodică semnată');
      },
      onError: setError,
    },
  );

  const { isLoading: isSSMSigning, mutate: ssmSignInit } = useMutation(
    // @ts-ignore
    () => ssmSign(selectedSession._id),
    {
      onSuccess: async () => {
        await queryClient.invalidateQueries([FETCH_QUERY_KEY, fetchParams]);
        setSelectedSession(null);
        setMessage('Instruire periodică validată');
      },
      onError: setError,
    },
  );

  return (
    <>
      <Head>
        <title>Instruiri periodice</title>
      </Head>

      <AdminPage
        title="Instruiri periodice"
        pages={[{ url: '/ssm-sessions-periodic-trainings', name: 'Instruiri periodice' }]}
        actions={<></>}
      >
        <Card sx={{ p: 3 }}>
          <Grid container spacing={2} columns={13}>
            <Grid item xs={12} md={3} lg={2}>
              <SSMTrainingSessionFilter
                order={order}
                setOrder={setOrder}
                orderBy={orderBy}
                setOrderBy={setOrderBy}
                contraEvaluated={contraEvaluated}
                setContraEvaluated={setContraEvaluated}
                userAcknowledged={userAcknowledged}
                setUserAcknowledged={setUserAcknowledged}
                sessionData={sessionDate}
                setSessionData={setSessionDate}
              />
            </Grid>

            <Grid item xs={12} md={10} lg={11}>
              <Box sx={{ mb: 3 }}>
                <HREvaluationsListHeader
                  // @ts-ignore
                  activeView={'s'}
                  order={order}
                  setOrder={setOrder}
                  orderBy={orderBy}
                  setOrderBy={setOrderBy}
                  searchTerm={email}
                  setSearchTerm={setEmail}
                />
              </Box>

              {isLoading || !functions?.results?.length ? (
                <ListLoadingIndicator />
              ) : ssmPeriodicTrainingsSessions?.results?.length ? (
                <>
                  <SSMTrainingSessionList
                    ssmPeriodicTrainingsSessions={ssmPeriodicTrainingsSessions?.results || []}
                    onSelectSession={setSelectedSession}
                  />

                  <Pagination
                    sx={{ mt: 2 }}
                    pageNumber={pageNumber}
                    pageSize={pageSize}
                    totalResults={ssmPeriodicTrainingsSessions?.count || 0}
                    setPageNumber={setPageNumber}
                  />
                </>
              ) : (
                <NoDataAvailable content="În momentul de faţă nu există nicio instruire periodca disponibilă pe platformă" />
              )}
            </Grid>
          </Grid>
        </Card>
      </AdminPage>

      <Dialog
        content={
          <>
            <SSMTrainingSessionDetails
              // @ts-ignore
              functions={functions?.results}
              // @ts-ignore
              session={selectedSession}
              // @ts-ignore
              ssmPackages={
                selectedSession?.ssmPackages?.length
                  ? selectedSession?.ssmPackages
                  : ssmPackages?.results
              }
            />
          </>
        }
        actions={
          <>
            {/* Aici are access doar seful SSM sau managerul direct => no access to se youre session  */}
            {!selectedSession?.contraEvaluated && (
              // @ts-ignore
              <Button
                variant="contained"
                disabled={!selectedSession?.userAcknowledged}
                onClick={contraSignInit}
                loading={isContraSigning}
              >
                Semnează
              </Button>
            )}

            {/* @ts-ignore */}
            {user?.isSSM && !selectedSession?.ssmEvaluated && (
              // @ts-ignore
              <Button
                variant="contained"
                color="success"
                disabled={!selectedSession?.contraEvaluated}
                onClick={ssmSignInit}
                loading={isSSMSigning}
              >
                Validează
              </Button>
            )}
          </>
        }
        // @ts-ignore
        title={selectedSession?.ssmPeriodicTrainingId?.name}
        open={!!selectedSession}
        maxWidth="lg"
        onClose={() => setSelectedSession(null)}
      />
    </>
  );
};

export const getServerSideProps = withPageAuthRequired();

export default SSMPeriodicTrainingSessions;
