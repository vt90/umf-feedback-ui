import React, { useMemo, useState } from 'react';
import Head from 'next/head';
import { useRouter } from 'next/router';
import { withPageAuthRequired } from '../../../../../guards/authRequired';
import { useQuery, useMutation } from '@tanstack/react-query';
import Box from '@mui/material/Box';
import Grid from '@mui/material/Grid';

import AdminPage from '../../../../../components/AdminPage';
import ListLoadingIndicator from '../../../../../components/Common/ListLoadingIndcator';
import { useNotificationContext } from '../../../../../context/notificationContext';
import { getEvaluationSessionById } from '../../../../../services/evaluationSessions';
import { getUserDepartmentAssignmentById } from '../../../../../services/userDepartmentAssignment';
import EvaluationForm from '../../../../../components/Evaluation/Form';
import { IEvaluation, IEvaluationResult } from '../../../../../models/evaluation';
import {
  calculateEvaluationResult,
  createOrUpdateEvaluation,
} from '../../../../../services/evaluations';
import Button from '@mui/lab/LoadingButton';
import SaveIcon from '@mui/icons-material/CheckCircle';
import EvaluationResult from '../../../../../components/Evaluation/Result';
import LoadingButton from '@mui/lab/LoadingButton';
import Dialog from '../../../../../components/Common/Dialog';
import EvaluationHeader from '../../../../../components/Evaluation/Header';
import { SURVEY_ROLE } from '../../../../../models/survey';
import Card from '@mui/material/Card';

const Survey = () => {
  const [result, setResult] = useState<IEvaluationResult>({});
  const [bonifications, setBonifications] = useState<IEvaluationResult>({});
  const [evaluation, setEval] = useState<IEvaluation | null>(null);
  const [evaluationResult, setEvalResult] = useState<Partial<IEvaluation> | null>(null);
  const router = useRouter();
  const { setError, setMessage } = useNotificationContext();
  const { id, userDepartmentAssignmentId } = router.query;

  const FETCH_QUERY_KEY = useMemo(() => ['evaluation-session', id], [id]);
  const FETCH_DEP_QUERY_KEY = useMemo(
    () => ['evaluation-session-dep-ass', userDepartmentAssignmentId],
    [userDepartmentAssignmentId],
  );

  const { isLoading, data: evaluationSession } = useQuery(
    FETCH_QUERY_KEY,
    () => getEvaluationSessionById(id as string),
    {
      onError: () => router.push('/404'),
    },
  );

  const { isLoading: isDepartmentAssignmentLoading, data: userDepartmentAssignment } = useQuery(
    FETCH_DEP_QUERY_KEY,
    () => getUserDepartmentAssignmentById(userDepartmentAssignmentId as string),
    {
      onError: () => router.push('/404'),
    },
  );

  const { isLoading: isEvaluationUpdating, mutate: updateEvaluation } = useMutation(
    createOrUpdateEvaluation,
    {
      onSuccess: async () => {
        setMessage('Evaluare salvată cu success');
        await router.push({
          pathname: '/evaluation-sessions/[id]',
          query: { id },
        });
      },
      onError: setError,
    },
  );
  const { isLoading: isCalculating, mutate: calculate } = useMutation(calculateEvaluationResult, {
    onSuccess: async (data) => {
      setEvalResult(data);
    },
    onError: setError,
  });

  const surveyForm = useMemo(() => {
    return (
      evaluationSession &&
      evaluationSession.surveys &&
      userDepartmentAssignment &&
      evaluationSession.surveys.find((survey) => {
        return (
          survey.type === userDepartmentAssignment.type && survey.role === SURVEY_ROLE.SALARY_CHANGE
        );
      })
    );
  }, [evaluationSession, userDepartmentAssignment]);

  const surveyQuestionsCount = useMemo(() => {
    if (!surveyForm) return 0;

    return surveyForm.surveyChapters.reduce((acc, cur) => {
      // @ts-ignore
      return acc + cur.questions.length;
    }, 0);
  }, [surveyForm]);

  if (isLoading || isDepartmentAssignmentLoading) return <ListLoadingIndicator />;

  // ToDo implement not found
  if (!(evaluationSession && userDepartmentAssignment && surveyForm)) return null;

  const onResultChange = (questionId: string, value: number) => {
    setResult({
      ...result,
      [questionId]: value,
    });
  };

  const onBonificationsChange = (questionId: string, value: number) => {
    setBonifications({
      ...bonifications,
      [questionId]: value,
    });
  };

  const handleSubmit = () => {
    const evaluationInfo: IEvaluation = {
      role: SURVEY_ROLE.SALARY_CHANGE,
      userId: userDepartmentAssignment.userId._id,
      departmentId: userDepartmentAssignment.departmentId._id,
      surveyId: surveyForm._id,
      questionResults: Object.keys(result).map((key) => ({
        questionId: key,
        result: result[key],
      })),
      bonifications: Object.keys(bonifications)
        .filter((key) => !!bonifications[key])
        .map((key) => ({
          bonificationId: key,
          result: bonifications[key],
        })),
      // @ts-ignore
      evaluationSessionId: id,
      // @ts-ignore
      userDepartmentAssignmentId: userDepartmentAssignmentId,
    };

    setEval(evaluationInfo);
    calculate(evaluationInfo);
  };

  return (
    <>
      <Head>
        <title>{evaluationSession.name}</title>
      </Head>

      <AdminPage
        title={evaluationSession.name}
        pages={[
          { url: '/evaluation-sessions', name: 'Sesiuni de evaluare' },
          { url: `/evaluation-sessions/${id}`, name: evaluationSession.name },
          {
            url: `/evaluation-sessions/${id}/evaluate/${userDepartmentAssignmentId}`,
            name: userDepartmentAssignment?.userId?.fullName,
          },
        ]}
        actions={
          <Button
            onClick={() => document.getElementById('evaluationSubmit')?.click()}
            variant="contained"
            endIcon={<SaveIcon />}
            disabled={Object.keys(result).length !== surveyQuestionsCount}
            loading={!!isCalculating}
          >
            Completează Formular
          </Button>
        }
      >
        <Card sx={{ p: 3 }}>
          <EvaluationHeader
            survey={surveyForm}
            userDepartmentAssignment={userDepartmentAssignment}
          />

          <Grid container spacing={2}>
            <Grid item xs={12}>
              <Box sx={{ mt: 4 }}>
                <EvaluationForm
                  // @ts-ignore
                  survey={surveyForm}
                  evaluationResult={result}
                  onChangeEvaluationResult={onResultChange}
                  onSubmit={handleSubmit}
                  bonifications={bonifications}
                  onChangeEvaluationBonifications={onBonificationsChange}
                  objectiveResult={{}}
                  onChangeEvaluationObjectiveResult={console.log}
                />
              </Box>
            </Grid>
          </Grid>
        </Card>

        <Dialog
          actions={
            <LoadingButton
              variant="contained"
              onClick={() => evaluation && updateEvaluation(evaluation)}
              loading={isEvaluationUpdating}
            >
              Confirmă
            </LoadingButton>
          }
          content={
            <>
              {evaluation && (
                <EvaluationResult
                  evaluation={{
                    ...evaluation,
                    ...evaluationResult,
                  }}
                  survey={surveyForm}
                  userDepartmentAssignment={userDepartmentAssignment}
                />
              )}
            </>
          }
          title="Confirmati evaluarea?"
          onClose={() => {
            setEval(null);
            setEvalResult(null);
          }}
          open={!!evaluationResult}
          maxWidth="lg"
        />
      </AdminPage>
    </>
  );
};

export const getServerSideProps = withPageAuthRequired();

export default Survey;
