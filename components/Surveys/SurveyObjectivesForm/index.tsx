import React, { useEffect } from 'react';
import { useForm } from 'react-hook-form';
import * as yup from 'yup';
import { yupResolver } from '@hookform/resolvers/yup';
import Button from '@mui/material/Button';
import Card from '@mui/material/Card';
import Box from '@mui/material/Box';
import Grid from '@mui/material/Grid';
import Collapse from '@mui/material/Collapse';
import AddIcon from '@mui/icons-material/Add';
import CloseIcon from '@mui/icons-material/Close';
import SaveIcon from '@mui/icons-material/CheckCircle';
import SurveyObjectiveQuestionsField from './SurveObjectiveQuestions';
import TextField, { ITextFieldProps } from '../../Common/FormInputs/TextField';
import { ISurvey } from '../../../models/survey';
import { ISurveyObjective } from '../../../models/surveyObjective';

interface ISurveyObjectiveFormProps {
  initialFocus?: string;
  initialValues: Partial<ISurveyObjective>;
  isLoading: boolean;
  onCancel?: () => void;
  onSubmit: (data: any) => void;
  survey: ISurvey;
}

const questionSchema = {
  name: yup.string().required(),
};

const schema = (props: ISurveyObjectiveFormProps) => {
  return yup
    .object({
      name: yup.string().required(),
      questions: yup.array().of(yup.object().shape(questionSchema)),
    })
    .required();
};

const SurveyObjectiveForm = (props: ISurveyObjectiveFormProps) => {
  const { initialFocus, initialValues, isLoading, onSubmit, onCancel, survey } = props;

  const {
    register,
    handleSubmit,
    formState: { errors },
    control,
    setFocus,
  } = useForm({
    defaultValues: initialValues,
    mode: 'onTouched',
    resolver: yupResolver(schema(props)),
    shouldUnregister: true,
  });

  useEffect(() => {
    // @ts-ignore
    initialFocus && setFocus(initialFocus);
  }, [setFocus, initialFocus]);

  const textFieldProps: Partial<ITextFieldProps> = {
    required: true,
    disabled: isLoading,
    size: 'small',
    sx: {
      '& legend': { display: 'none' },
      '& fieldset': {
        top: 0,
        borderColor: 'transparent',
      },
    },
    inputProps: {
      sx: {
        typography: 'body1',
        fontWeight: 'bold',
      },
    },
  };

  return (
    <>
      <form onSubmit={handleSubmit(onSubmit)}>
        <Card elevation={8}>
          <input type="hidden" {...register(`_id`)} />
          <input type="hidden" {...register(`surveyId`)} />
          <Box sx={{ p: 3 }}>
            <Grid container spacing={2} alignItems="center">
              <Grid item xs={12}>
                <Box sx={{ display: 'flex' }}>
                  <Box sx={{ flexGrow: 1, mr: 2 }}>
                    <TextField
                      multiline
                      errorMessage={!!errors?.name && 'Obiectiv necesar'}
                      defaultValue={initialValues.name}
                      {...register(`name`)}
                      {...textFieldProps}
                    />
                  </Box>
                </Box>
              </Grid>

              <SurveyObjectiveQuestionsField
                control={control}
                errors={errors}
                isLoading={isLoading}
                register={register}
              />

              <Grid item xs={12}>
                <Collapse in={true}>
                  <Grid container spacing={2} width="100%">
                    <Grid item xs={1}></Grid>
                    <Grid item xs={11}>
                      <Button
                        onClick={() => document.getElementById(`addQuestionButton`)?.click()}
                        variant="contained"
                        endIcon={<AddIcon />}
                        sx={{ ml: 1.5 }}
                        size="small"
                      >
                        Adaugă obiectiv
                      </Button>
                    </Grid>
                  </Grid>
                </Collapse>
              </Grid>
            </Grid>
          </Box>
        </Card>

        <Box sx={{ py: 1, display: 'flex', justifyContent: 'flex-end' }}>
          <Box>
            <Button
              // @ts-ignore
              onClick={onCancel}
              color="inherit"
              endIcon={<CloseIcon fontSize="small" />}
              size="small"
            >
              Anulează
            </Button>
          </Box>

          <Box sx={{ ml: 1 }}>
            <Button
              type="submit"
              variant="contained"
              endIcon={<SaveIcon fontSize="small" />}
              size="small"
            >
              Salvează
            </Button>
          </Box>
        </Box>
        <Button id="surveyObjectiveFormSubmit" type="submit" sx={{ display: 'none' }} />
      </form>
    </>
  );
};

export default SurveyObjectiveForm;
