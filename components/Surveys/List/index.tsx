import React, { useCallback, useMemo } from 'react';
import Link from 'next/link';
import moment from 'moment';
import EditIcon from '@mui/icons-material/Edit';
import DeleteIcon from '@mui/icons-material/Delete';
import IconButton from '@mui/material/IconButton';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import { ISurvey, SURVEY_FORMULA_TYPE } from '../../../models/survey';
import EnhancedTable from '../../Common/Table';

interface ISurveysListProps {
  baseUrl: string;
  title?: string;
  surveys: ISurvey[];
  onSelectSurvey?: (org: ISurvey) => void;
  onDeleteSurvey?: (org: ISurvey) => void;
  readonly?: boolean;
  PaperComponent?: React.FunctionComponent;
}

const SurveysList = (props: ISurveysListProps) => {
  const {
    baseUrl,
    surveys,
    onSelectSurvey,
    onDeleteSurvey,
    readonly,
    PaperComponent = Box,
    title,
  } = props;

  const headerRows = useMemo(() => {
    const headerItems = [
      { id: 'name', label: 'Chestionar', align: 'left', colSpan: 3, hideOnSmallDevice: true },
      { id: 'type', label: 'Destinat către', colSpan: 1, align: 'v' },
      { id: 'calculationFormulaType', label: 'Tip calcul', colSpan: 1, align: 'left' },
      // { id: 'createdAt', label: 'Ultima modificare' },
    ];

    if (!readonly && (onSelectSurvey || onDeleteSurvey)) {
      // @ts-ignore
      headerItems.push({ id: 'actions', label: 'Acțiuni', align: 'right', disableSorting: true });
    }

    return headerItems;
  }, [onDeleteSurvey, onSelectSurvey, readonly]);

  const getHeaderRows = useCallback(() => {
    return surveys.map((survey) => {
      const renderName = (
        <Link href={`${baseUrl}/${survey._id}`}>
          <a>
            <Typography>{survey.name}</Typography>
          </a>
        </Link>
      );

      return {
        _id: survey._id,
        type: survey.type,
        calculationFormulaType: Object.keys(SURVEY_FORMULA_TYPE).find(
          // @ts-ignore
          (k) => SURVEY_FORMULA_TYPE[k] === survey.calculationFormulaType,
        ),
        name: renderName,
        smallDeviceHeader: renderName,
        createdAt: moment(survey.createdAt).format('YYYY.MM.DD'),
        actions: (
          <>
            {onSelectSurvey && (
              <IconButton onClick={() => onSelectSurvey(survey)} size="small" color="primary">
                <EditIcon fontSize="inherit" />
              </IconButton>
            )}

            {onDeleteSurvey && (
              <IconButton onClick={() => onDeleteSurvey(survey)} size="small" color="error">
                <DeleteIcon fontSize="inherit" />
              </IconButton>
            )}
          </>
        ),
        data: survey,
      };
    });
  }, [baseUrl, onDeleteSurvey, onSelectSurvey, surveys]);

  return (
    <>
      <EnhancedTable
        initialOrder="asc"
        headerRows={headerRows}
        orderByFieldCriteriaFunctions={{}}
        PaperComponent={PaperComponent}
        paperComponentStyles={{
          px: 3,
          py: 2,
          border: '0.0625rem solid rgb(222, 226, 230)',
          borderRadius: 2,
        }}
        rows={getHeaderRows()}
        smallUntilBreakpoint="md"
        title={title}
      />
    </>
  );
};

export default SurveysList;
