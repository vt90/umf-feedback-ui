import React, { useCallback, useMemo } from 'react';
import Typography from '@mui/material/Typography';
import EnhancedTable from '../../../Common/Table';
import Link from 'next/link';
import { IExtendedUserDepartmentAssignment } from '../../../../models/userDepartmentAssignment';
import Tooltip from '@mui/material/Tooltip';
import isBoolean from 'lodash/isBoolean';
import YesIcon from '@mui/icons-material/CheckBox';
import NoIcon from '@mui/icons-material/DisabledByDefault';
import WaitingIcon from '@mui/icons-material/HourglassBottom';
import Box from '@mui/material/Box';

interface IUserUserListProps {
  action?: React.ReactNode;
  secondaryAction?: React.ReactNode;
  baseUrl: string;
  secondaryBaseUrl?: string;
  evaluations?: any;
  initialOrder?: string;
  initialOrderBy?: string;
  title: string;
  users: IExtendedUserDepartmentAssignment[];
}

const EvaluationSessionUserUserList = (props: IUserUserListProps) => {
  const {
    action,
    secondaryAction,
    baseUrl,
    secondaryBaseUrl,
    evaluations,
    initialOrder = 'ASC',
    initialOrderBy = 'department',
    title,
    users,
  } = props;

  const headerRows = useMemo(() => {
    return [
      { id: 'name', label: 'Denumire', align: 'left', colSpan: 3 },
      { id: 'email', label: 'Marcă', align: 'left', colSpan: 1 },
      { id: 'department', label: 'Departament', align: 'left', colSpan: 2 },
      { id: 'result', label: 'Rezultat', align: 'left', colSpan: 1 },
      { id: 'salaryChangePercentage', label: '% Modificare Salariala', align: 'left', colSpan: 1 },
      { id: 'userAgreement', label: 'Opinie evaluat', align: 'center', colSpan: 1 },
      { id: 'contraEvaluated', label: 'Contra-evaluată', align: 'center', colSpan: 1 },
      { id: 'userAcknowledged', label: 'Luată la cunoștință', align: 'center', colSpan: 1 },
      { id: 'actions', label: 'Acțiuni', align: 'right', colSpan: 2 },
    ];
  }, []);

  const renderIcon = useCallback(
    (value: boolean | null | undefined, yesText: string, noText?: string) => {
      const color = isBoolean(value) ? (value ? 'success' : 'error') : 'info';
      const tooltipText = isBoolean(value)
        ? value
          ? yesText
          : noText || 'Respinsă'
        : 'În așteptare';

      const iconProps = {
        sx: {
          color: `${color}.light`,
        },
      };

      return (
        <Tooltip title={tooltipText}>
          <Box>
            {isBoolean(value) ? (
              value ? (
                // @ts-ignore
                <YesIcon {...iconProps} />
              ) : (
                // @ts-ignore
                <NoIcon {...iconProps} />
              )
            ) : (
              // @ts-ignore
              <WaitingIcon {...iconProps} />
            )}
          </Box>
        </Tooltip>
      );
    },
    [],
  );

  const getHeaderRows = useCallback(() => {
    return users.map((user) => {
      const evaluation = evaluations?.[user._id];
      // const result = evaluation?.finalResult;

      const linkUrl = `${baseUrl}/${user._id}?resultId=${evaluation?._id}`;
      const secondaryUrl = `${secondaryBaseUrl}/${user._id}`;

      const renderLink = (children: React.ReactNode) => (
        <Link href={linkUrl}>
          <a>{children}</a>
        </Link>
      );

      const renderSecondaryLink = (children: React.ReactNode) => (
        <Link href={secondaryUrl}>
          <a>{children}</a>
        </Link>
      );

      const renderName = renderLink(<Typography>{user.userId.fullName} </Typography>);

      return {
        _id: user._id,
        smallDeviceHeader: renderName,
        name: renderName,
        email: user?.userId?.email,
        contraEvaluated: renderIcon(
          evaluation?.contraEvaluated ? true : null,
          'Contra-evaluată',
          'Nu',
        ),
        userAcknowledged: renderIcon(evaluation?.userAcknowledged, 'Luată la cunoștință'),
        userAgreement: renderIcon(evaluation?.userAgreement, 'Acceptată'),
        department: (
          <Tooltip title={user.role}>
            <Typography variant="body2" color="textSecondary">
              {user.departmentId.name}
            </Typography>
          </Tooltip>
        ),
        role: user.role,
        salaryChangePercentage: evaluation?.salaryChangePercentage
          ? `${evaluation?.salaryChangePercentage}%`
          : '-',
        result: evaluation?.finalGrade
          ? `${evaluation?.finalGrade.toFixed(2)} ${
              evaluation.finalResult ? ` - ${evaluation.finalResult}` : ''
            }`
          : '-',
        actions: action ? (
          <>
            {renderLink(action)}
            {secondaryAction && renderSecondaryLink(secondaryAction)}
          </>
        ) : (
          '-'
        ),
        data: user,
      };
    });
  }, [action, baseUrl, evaluations, users, renderIcon]);

  return (
    <>
      <EnhancedTable
        headerRows={headerRows}
        initialOrderByField={initialOrderBy}
        initialOrder={initialOrder}
        orderByFieldCriteriaFunctions={{
          name: (row: any) => row?.data && row.data.userId.fullName,
          department: (row: any) => row?.data && row.data.departmentId.name,
          result: (row: any) => evaluations?.[row._id]?.finalGrade,
          salaryChangePercentage: (row: any) => evaluations?.[row._id]?.salaryChangePercentage,
          userAgreement: (row: any) => evaluations?.[row._id]?.userAgreement,
          contraEvaluatorAgreement: (row: any) => evaluations?.[row._id]?.contraEvaluatorAgreement,
        }}
        PaperComponent={Box}
        paperComponentStyles={{
          px: 3,
          py: 2,
          border: '0.0625rem solid rgb(222, 226, 230)',
          borderRadius: 2,
          '& h6': {
            mt: 1,
          },
        }}
        rows={getHeaderRows()}
        smallUntilBreakpoint="md"
        title={title}
      />
    </>
  );
};

export default EvaluationSessionUserUserList;
