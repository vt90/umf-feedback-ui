import React from 'react';
import { SxProps } from '@mui/material';
import Box from '@mui/material/Box';
import MuiPagination from '@mui/material/Pagination';

interface IPaginationProps {
  pageNumber: number;
  pageSize: number;
  totalResults: number;
  setPageNumber: (order: number) => void;
  sx?: SxProps;
}

const Pagination = (props: IPaginationProps) => {
  const { pageNumber, setPageNumber, totalResults, pageSize, sx } = props;

  return (
    <Box sx={{ display: 'flex', justifyContent: 'flex-end' }}>
      <MuiPagination
        sx={sx}
        shape="rounded"
        count={Math.ceil(totalResults / pageSize)}
        page={pageNumber}
        onChange={(_, newValue) => setPageNumber(newValue)}
        showFirstButton
        showLastButton
      />
    </Box>
  );
};

export default Pagination;
