import { IBaseModel } from './base';

export interface IFunctionModel extends IBaseModel {
  code: string;
  name: string;
  isLead: boolean;
}

export const getFunctionName = (functionModel: IFunctionModel): string => {
  return `${functionModel.code} - ${functionModel.name}`;
};
