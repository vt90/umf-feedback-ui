import React, { Fragment, useState } from 'react';
import { useFieldArray, Control } from 'react-hook-form';
import Box from '@mui/material/Box';
import DepartmentAddUsersForm from '../../Departments/Details/AddUsersForm';
import { IUser } from '../../../models/user';
import UserSelectionList from '../../Users/SelectionList';

interface IPrivateUsersFieldArrayProps {
  initialValues: any;
  control: Control;
  errors: any;
  register: any;
}

const PrivateUsersFieldArray = (props: IPrivateUsersFieldArrayProps) => {
  const { control, initialValues, register } = props;

  const [usersInfoList, setUsersInfoList] = useState(initialValues.privateUsersInfo || []);

  const { fields, append, remove } = useFieldArray({
    control,
    name: 'privateUsers',
  });

  const onAddUsers = (users: IUser[]) => {
    append(users.map((user) => ({ value: user._id })));
    setUsersInfoList((prevValues: IUser[]) => [...prevValues, ...users]);
  };

  const onRemoveUser = (userIndex: number) => {
    const newUsersInfoList = [...usersInfoList];
    newUsersInfoList.splice(userIndex, 1);
    setUsersInfoList(newUsersInfoList);
    remove(userIndex);
  };

  return (
    <>
      <DepartmentAddUsersForm
        departmentId={''}
        onUsersAdded={console.log}
        onBulkAssignUsers={onAddUsers}
      />

      {fields.map((item, index) => {
        return (
          <Fragment key={item.id || index}>
            <input type="hidden" {...register(`privateUsers.${index}.value`)} />
          </Fragment>
        );
      })}

      <Box sx={{ mt: 2 }}>
        <UserSelectionList
          users={usersInfoList}
          selectedUsers={usersInfoList}
          onRemoveUser={onRemoveUser}
          onSelectUser={console.log}
        />
      </Box>
    </>
  );
};

export default PrivateUsersFieldArray;
