import * as React from 'react';
import { Control, Controller } from 'react-hook-form';
import Autocomplete from '@mui/material/Autocomplete';
import TextField, { ITextFieldProps } from '../FormInputs/TextField';
import { useState } from 'react';
import { getFunctionName, IFunctionModel } from '../../../models/function';

interface IMultipleSelectFieldProps extends ITextFieldProps {
  name: string;
  control: Control;
  functions: IFunctionModel[];
  [key: string]: any;
}

function FunctionsSelect(props: IMultipleSelectFieldProps) {
  const { name, control, functions } = props;
  const [inputValue, setInputValue] = useState('');

  return (
    <Controller
      name={name}
      control={control}
      render={({ field }) => {
        const { value, onChange } = field;

        // @ts-ignore
        const handleChange = (event) => {
          const {
            target: { value },
          } = event;
          onChange(
            // On autofill, we get a stringified value.
            typeof value === 'string' ? value.split(',') : value,
          );
        };

        return (
          <div>
            <Autocomplete
              multiple
              value={value}
              // @ts-ignore
              onChange={(event: any, newValue: string | null) => {
                handleChange({ target: { value: newValue } });
              }}
              inputValue={inputValue}
              onInputChange={(event, newInputValue) => {
                setInputValue(newInputValue);
              }}
              options={functions.map((o) => o._id)}
              // @ts-ignore
              getOptionLabel={(o) => {
                // @ts-ignore
                const functionInfo = functions.find((op) => op._id === o);
                // @ts-ignore
                return (functionInfo && getFunctionName(functionInfo)) || '-';
              }}
              renderInput={(params) => <TextField {...params} />}
            />
          </div>
        );
      }}
    />
  );
}

export default React.forwardRef(FunctionsSelect);
