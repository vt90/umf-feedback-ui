import axios from 'axios';
import { IMedicalCheck } from '../models/medicalCheck';
import { IPaginationResult } from '../models/base';
import { ISSMPeriodicTrainingSession } from '../models/ssmPeriodicTrainingSession';
import { IEvaluation } from '../models/evaluation';

const createInstance = (baseURL = '/api/medical-check') => {
  const httpInstance = axios.create({
    baseURL,
  });

  httpInstance.interceptors.response.use(
    (response) => response.data,
    (error) => {
      if (error && error.response && error.response.data) {
        return Promise.reject(
          error.response.data.message
            ? `${error.response.data.message || error.response.data.error}`
            : error.response.data?.error || error.response.data,
        );
      } else {
        return Promise.reject({ message: 'Server error' });
      }
    },
  );

  return httpInstance;
};

const httpInstance = createInstance();

const cleanupMedicalCheckModelBody = (medicalCheck: IMedicalCheck): FormData => {
  // @ts-ignore
  delete medicalCheck._id;
  // @ts-ignore
  delete medicalCheck.id;
  // @ts-ignore
  delete medicalCheck.createdAt;
  // @ts-ignore
  delete medicalCheck.updatedAt;
  // @ts-ignore
  delete medicalCheck['__v'];

  const formData = new FormData();

  for (const [key, value] of Object.entries(medicalCheck)) {
    if (key === 'documents') {
      value?.forEach((value: string, index: number) => {
        formData.append(`${key}[${index}]`, value);
      });
    } else if (key === 'files') {
      value?.forEach((value: string, index: number) => {
        formData.append(key, value);
      });
    } else {
      formData.append(key, value);
    }
  }

  return formData;
};

export const findMedicalCheck = async (
  params: Partial<IMedicalCheck>,
): Promise<IPaginationResult<IMedicalCheck>> => {
  return await httpInstance.get('', { params });
};

export const createOrUpdateMedicalCheck = async (
  medicalCheck: IMedicalCheck,
): Promise<IMedicalCheck> => {
  const isUpdate = !!medicalCheck._id;
  const url = isUpdate ? `/${medicalCheck._id}` : ``;
  const httpInstance = createInstance(`${process.env.NEXT_PUBLIC_API_URL}/api/medical-check`);
  const req = isUpdate ? httpInstance.patch : httpInstance.post;
  const { data } = await axios.get('/api/get-access-token');

  return await req(url, cleanupMedicalCheckModelBody(medicalCheck), {
    headers: {
      Authorization: `Bearer ${data?.accessToken}`,
    },
  });
};

export const findMyMedicalCheck = async (
  params: Partial<ISSMPeriodicTrainingSession>,
): Promise<IPaginationResult<ISSMPeriodicTrainingSession>> => {
  return await httpInstance.get('/my-medical-checks', { params });
};

export const accept = async (id: string): Promise<IEvaluation> => {
  return await httpInstance.post(`/accept/${id}`);
};
