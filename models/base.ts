export interface IBaseModel {
  _id: string;
  createdAt?: string;
  updatedAt?: string;
  deactivated?: boolean;
  deactivatedTimestamp?: string;
}
export interface IPagination {
  limit?: number;
  offset?: number;
}

export interface IPaginationResult<Type> {
  count: number;
  results?: Type[];
}
