import React, { useCallback, useMemo } from 'react';
import Box from '@mui/material/Box';
import Tooltip from '@mui/material/Tooltip';
import EnhancedTable from '../../Common/Table';
import NoDataAvailable from '../../Common/NoDataAvailable';
import YesIcon from '@mui/icons-material/CheckBox';
import WaitingIcon from '@mui/icons-material/HourglassBottom';
import { ISSMInitialTrainingSession } from '../../../models/ssmInitialTrainingSession';
import Button from '@mui/material/Button';
import { TRAINING_MONTHS } from '../../../models/ssmPeriodicTraining';
import moment from 'moment';
// import Status from '../../Common/Status';

interface IUserUserListProps {
  ssmInitialTrainingsSessions: ISSMInitialTrainingSession[];
  onSelectSession?: (session: ISSMInitialTrainingSession) => void;
}

const SSMInitialTrainingSessionList = (props: IUserUserListProps) => {
  const { ssmInitialTrainingsSessions, onSelectSession } = props;

  const headerRows = useMemo(() => {
    const columns = [
      { id: 'name', label: 'Membru Evaluat', align: 'left', colSpan: 2 },
      { id: 'email', label: 'Marca', align: 'left', colSpan: 1 },
      {
        id: 'type',
        label: 'Tip instruire',
        align: 'left',
        colSpan: 2,
        disableSorting: true,
      },
      {
        id: 'createdAt',
        label: 'Data instruirii',
        align: 'center',
        colSpan: 1,
        disableSorting: true,
      },
      {
        id: 'userAcknowledged',
        label: 'Semnătură celui instruit',
        align: 'center',
        colSpan: 2,
      },
      {
        id: 'contraEvaluated',
        label: 'Semnătură celui care a efectuat instruirea',
        align: 'center',
        colSpan: 2,
      },
      {
        id: 'ssmEvaluated',
        label: 'Semnătură celui care a verificat',
        align: 'center',
        colSpan: 2,
      },
    ];

    if (onSelectSession) {
      columns.push({ id: 'actions', label: 'Acțiuni', align: 'right', colSpan: 1 });
    }

    return columns;
  }, [onSelectSession]);

  const renderIcon = useCallback((value: boolean | null | undefined, yesText: string) => {
    const color = value ? 'success' : 'info';
    const tooltipText = value ? yesText : 'În așteptare';

    const iconProps = {
      sx: {
        color: `${color}.light`,
      },
    };

    return (
      <Tooltip title={tooltipText}>
        <Box>
          {value ? (
            // @ts-ignore
            <YesIcon {...iconProps} />
          ) : (
            // @ts-ignore
            <WaitingIcon {...iconProps} />
          )}
        </Box>
      </Tooltip>
    );
  }, []);

  const getHeaderRows = useCallback(() => {
    return ssmInitialTrainingsSessions.map((session) => {
      const {
        _id,
        userDepartmentAssignmentId,
        userId,
        isAccepted,
        createdAt,
        // @ts-ignore
        action_type_flag,
        // @ts-ignore
        userAcknowledged,
        // @ts-ignore
        contraEvaluated,
        // @ts-ignore
        ssmEvaluated,
      } = session;

      const created = moment(createdAt);

      return {
        _id,
        smallDeviceHeader: userId.fullName,
        type:
          action_type_flag === 'Work'
            ? 'Instruire la locul de muncă'
            : action_type_flag === 'Accept_At_Work'
            ? 'Admis la lucru'
            : 'Instruire introductiv generală',
        name: userId.fullName,
        // @ts-ignore
        email: userId?.email,
        createdAt: TRAINING_MONTHS[created.month()] + ' ' + created.format('YYYY'),
        userAcknowledged:
          action_type_flag === 'Accept_At_Work'
            ? '-'
            : renderIcon(userAcknowledged, 'Instruire semnată de către persoana care a instruit'),
        contraEvaluated:
          action_type_flag === 'Accept_At_Work'
            ? '-'
            : renderIcon(
                contraEvaluated,
                'Instruire semnată de către persoana care a efectuat instruirea',
              ),
        ssmEvaluated:
          action_type_flag === 'Accept_At_Work'
            ? renderIcon(isAccepted, 'Admis la lucru')
            : renderIcon(
                ssmEvaluated,
                'Instruire semnată de către persoana care a verificat instruirea',
              ),
        data: session,
        actions: onSelectSession ? (
          <>
            <Button onClick={() => onSelectSession(session)} variant="contained" size="small">
              Vizualizează
            </Button>
          </>
        ) : null,
        sx:
          action_type_flag === 'Accept_At_Work'
            ? {
                '& td': {
                  borderBottomWidth: 2,
                  borderBottomColor: 'grey.A400',
                  // borderBottom: '2px solid red !important',
                },
              }
            : null,
      };
    });
  }, [ssmInitialTrainingsSessions, renderIcon, onSelectSession]);

  if (!ssmInitialTrainingsSessions?.length) {
    return (
      <NoDataAvailable content="În momentul de faţă nu există nicio instruire disponibila pe platformă" />
    );
  }

  return (
    <>
      <EnhancedTable
        headerRows={headerRows}
        initialOrderByField="createdAt"
        initialOrder="desc"
        orderByFieldCriteriaFunctions={{
          createdAt: (row: any) => row?.data?.createdAt,
          // department: (row: any) => row?.data?.departmentId?.name,
          contraEvaluated: (row: any) => row?.data?.contraEvaluated,
          userAcknowledged: (row: any) => row?.data?.userAcknowledged,
          userAgreement: (row: any) => row?.data?.userAgreement,
          ssmEvaluated: (row: any) => row?.data?.ssmEvaluated,
          contraEvaluatorAgreement: (row: any) => row?.data?.contraEvaluatorAgreement,
        }}
        PaperComponent={Box}
        paperComponentStyles={{
          px: 3,
          py: 2,
          border: '0.0625rem solid rgb(222, 226, 230)',
          borderRadius: 2,
          '& h6': {
            mt: 1,
          },
        }}
        rows={getHeaderRows()}
        smallUntilBreakpoint="md"
        disableOrder
      />
    </>
  );
};

export default SSMInitialTrainingSessionList;
