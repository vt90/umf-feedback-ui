import { IBaseModel } from './base';

export interface ISSMPackage extends IBaseModel {
  name: string;
  type: string;
  functions: string[];
  documents: string[];
  departments: string[];
  excludedDepartments: string[];
  specificForDepartments: boolean;
}

export const getSSMFileDisplayName = (file: string): string => {
  const fileInfo = file.split('-PREFIX-');

  return fileInfo[1] || file;
};

export const SSM_PACKAGE_TYPE = {
  PERIODIC: 'per',
  AT_HIRE: 'hire',
};
