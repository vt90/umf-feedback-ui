import React, { useEffect } from 'react';
import { useRouter } from 'next/router';
import { withPageAuthRequired } from '../../guards/authRequired';
import ListLoadingIndicator from '../../components/Common/ListLoadingIndcator';

const Organigram = () => {
  const router = useRouter();

  useEffect(() => {
    router.push('/organigram/departments');
  }, [router]);

  return <ListLoadingIndicator />;
};

export const getServerSideProps = withPageAuthRequired();

export default Organigram;
