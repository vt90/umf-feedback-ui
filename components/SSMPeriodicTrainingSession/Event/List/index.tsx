import React from 'react';
import { IEvaluationEvent, EVALUATION_ACTIVITY_TYPES } from '../../../../models/evaluation';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import Timeline from '@mui/lab/Timeline';
import TimelineItem from '@mui/lab/TimelineItem';
import TimelineSeparator from '@mui/lab/TimelineSeparator';
import TimelineConnector from '@mui/lab/TimelineConnector';
import TimelineContent from '@mui/lab/TimelineContent';
import TimelineDot from '@mui/lab/TimelineDot';
import TimelineOppositeContent, {
  timelineOppositeContentClasses,
} from '@mui/lab/TimelineOppositeContent';
import moment from 'moment';
import Status from '../../../Common/Status';
import { ISSMPeriodicTrainingSession } from '../../../../models/ssmPeriodicTrainingSession';
import { SSM_INITIAL_TRAINING_TYPES } from '../../../../models/ssmInitialTrainingSession';

interface ISSMTrainingSessionEventListProps {
  session: ISSMPeriodicTrainingSession;
  isInitial?: boolean;
}

const SSMTrainingSessionEventList = (props: ISSMTrainingSessionEventListProps) => {
  const { session, isInitial } = props;

  const renderSSMTrainingSessionEvent = (event: IEvaluationEvent) => {
    // @ts-ignore
    const isUser = event.userId === session?.userId?._id && session?.userId;
    const isContraEvaluator =
      // @ts-ignore
      event.userId === session?.contraEvaluatorId?._id && session?.contraEvaluatorId;
    const isSSMEvaluatorGeneral =
      // @ts-ignore
      event.userId === session?.ssmEvaluatorIdGeneral?._id && session?.ssmEvaluatorIdGeneral;
    const isSSMEvaluatorWork =
      // @ts-ignore
      event.userId === session?.ssmEvaluatorIdWork?._id && session?.ssmEvaluatorIdWork;

    const user = isUser || isContraEvaluator || isSSMEvaluatorGeneral || isSSMEvaluatorWork;

    let eventStatus;
    const eventStatusColor = 'success';

    if (isInitial) {
      switch (event.type) {
        case SSM_INITIAL_TRAINING_TYPES.USER_ACKNOWLEDGEMENT_GENERAL:
          eventStatus = 'Instruire introductiv generală semnată de către instruit';
          break;
        case SSM_INITIAL_TRAINING_TYPES.USER_ACKNOWLEDGEMENT_WORK:
          eventStatus = 'Instruire la locul de muncă semnată de către instruit';
          break;
        case SSM_INITIAL_TRAINING_TYPES.CONTRA_EVALUATED_GENERAL:
          eventStatus = 'Instruire introductiv generală semnată de către persoana care a instruit';
          break;
        case SSM_INITIAL_TRAINING_TYPES.CONTRA_EVALUATED_WORK:
          eventStatus = 'Instruire la locul de muncă semnată de către persoana care a instruit';
          break;
        case SSM_INITIAL_TRAINING_TYPES.SSM_EVALUATED_GENERAL:
          eventStatus = 'Instruire introductiv generală verificată';
          break;
        case SSM_INITIAL_TRAINING_TYPES.SSM_EVALUATED_WORK:
          eventStatus = 'Instruire la locul de muncă verificată';
          break;
        case SSM_INITIAL_TRAINING_TYPES.ACCEPTED:
          eventStatus = 'Admins la lucru';
          break;
      }
    } else {
      switch (event.type) {
        case EVALUATION_ACTIVITY_TYPES.USER_ACKNOWLEDGEMENT:
          eventStatus = 'Instruire semnată de către instruit';
          break;
        case EVALUATION_ACTIVITY_TYPES.CONTRA_SIGNED:
          eventStatus = 'Instruire semnată de către persoana care a instruit';
          break;
        case EVALUATION_ACTIVITY_TYPES.SSM_CONTRA_SIGNED:
          eventStatus = 'Instruire verificată';
          break;
      }
    }

    const renderComment = () => {
      if (!event.comment) return null;

      return <Typography gutterBottom>{event.comment}</Typography>;
    };

    return (
      <TimelineItem key={event._id}>
        <TimelineOppositeContent color="textSecondary">
          {moment(event.createdAt).format('YYYY.MM.DD')}
        </TimelineOppositeContent>
        <TimelineSeparator>
          <TimelineDot />
          <TimelineConnector />
        </TimelineSeparator>
        <TimelineContent>
          <Box>
            {/* @ts-ignore */}
            <Status label={eventStatus} color={eventStatusColor} sx={{ minWidth: 160 }} />

            <Box sx={{ mt: 1.5, pb: 2 }}>
              {renderComment()}
              {/* @ts-ignore */}
              <Typography color="textSecondary">{user?.fullName}</Typography>
            </Box>
          </Box>
        </TimelineContent>
      </TimelineItem>
    );
  };

  return (
    <>
      {session?.events?.length ? (
        <>
          <Typography gutterBottom>
            <strong>Istoric</strong>
          </Typography>
          <Timeline
            sx={{
              pl: 0,
              [`& .${timelineOppositeContentClasses.root}`]: {
                pl: 0,
                maxWidth: 87,
              },
            }}
          >
            {session?.events.map((event) => {
              return renderSSMTrainingSessionEvent(event);
            })}
          </Timeline>
        </>
      ) : null}
    </>
  );
};

export default SSMTrainingSessionEventList;
