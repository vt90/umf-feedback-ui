import React, { useCallback, useMemo } from 'react';
import Box from '@mui/material/Box';
import Tooltip from '@mui/material/Tooltip';
import EnhancedTable from '../../Common/Table';
import NoDataAvailable from '../../Common/NoDataAvailable';
import YesIcon from '@mui/icons-material/CheckBox';
import WaitingIcon from '@mui/icons-material/HourglassBottom';
import Button from '@mui/material/Button';
import moment from 'moment';
import {
  getMedicalCheckResultText,
  IMedicalCheck,
  MEDICAL_CHECK_RESULT_TYPES,
} from '../../../models/medicalCheck';
import Status from '../../Common/Status';

interface IMedicalCheckListProps {
  medicalChecks: IMedicalCheck[];
  onSelectMedicalCheck?: (medicalCheck: IMedicalCheck) => void;
}

const MedicalCheckList = (props: IMedicalCheckListProps) => {
  const { medicalChecks, onSelectMedicalCheck } = props;

  const headerRows = useMemo(() => {
    const columns = [
      { id: 'isActive', label: 'Status', align: 'center', colSpan: 1 },
      { id: 'name', label: 'Angajat', align: 'left', colSpan: 2 },
      { id: 'email', label: 'Marca', align: 'left', colSpan: 1 },
      { id: 'result', label: 'Rezultat', align: 'left', colSpan: 1 },
      { id: 'isAccepted', label: 'Rezultat luat la cunostintă', align: 'center', colSpan: 1 },
      { id: 'startDate', label: 'Valabilă din*', align: 'center', colSpan: 1 },
      { id: 'endDate', label: 'Valabilă până în*', align: 'center', colSpan: 1 },
    ];

    if (onSelectMedicalCheck) {
      columns.push({ id: 'actions', label: 'Acțiuni', align: 'right', colSpan: 1 });
    }

    return columns;
  }, [onSelectMedicalCheck]);

  const renderIcon = useCallback((value: boolean | null | undefined, yesText: string) => {
    const color = value ? 'success' : 'info';
    const tooltipText = value ? yesText : 'În așteptare';

    const iconProps = {
      sx: {
        color: `${color}.light`,
      },
    };

    return (
      <Tooltip title={tooltipText}>
        <Box>
          {value ? (
            // @ts-ignore
            <YesIcon {...iconProps} />
          ) : (
            // @ts-ignore
            <WaitingIcon {...iconProps} />
          )}
        </Box>
      </Tooltip>
    );
  }, []);

  const getHeaderRows = useCallback(() => {
    return medicalChecks.map((medicalCheck) => {
      const { _id, userId, startDate, endDate, result, isAccepted, isActive } = medicalCheck;

      return {
        _id,
        smallDeviceHeader: userId.fullName,
        name: userId.fullName,
        // @ts-ignore
        email: userId?.email,
        isActive: (
          <Status
            color={isActive ? 'success' : 'warning'}
            label={isActive ? 'Activă' : 'Inactivă'}
          />
        ),

        // @ts-ignore
        startDate: moment(startDate).format('DD.MM.YYYY'),
        endDate: moment(endDate).format('DD.MM.YYYY'),
        result: getMedicalCheckResultText(result),
        isAccepted: renderIcon(
          result === MEDICAL_CHECK_RESULT_TYPES.APT ? true : isAccepted,
          'Rezultat luat la cunostintă',
        ),
        data: medicalCheck,
        actions: onSelectMedicalCheck ? (
          <>
            <Button
              onClick={() => onSelectMedicalCheck(medicalCheck)}
              variant="contained"
              size="small"
            >
              Vizualizează
            </Button>
          </>
        ) : null,
      };
    });
  }, [onSelectMedicalCheck, renderIcon, medicalChecks]);

  if (!medicalChecks?.length) {
    return (
      <NoDataAvailable content="În momentul de faţă nu există nicio fisa de aptitudini disponibila pe platformă" />
    );
  }

  return (
    <>
      <EnhancedTable
        headerRows={headerRows}
        initialOrderByField="createdAt"
        initialOrder="desc"
        orderByFieldCriteriaFunctions={{
          createdAt: (row: any) => row?.data?.createdAt,
          // department: (row: any) => row?.data?.departmentId?.name,
          contraEvaluated: (row: any) => row?.data?.contraEvaluated,
          userAcknowledged: (row: any) => row?.data?.userAcknowledged,
          userAgreement: (row: any) => row?.data?.userAgreement,
          ssmEvaluated: (row: any) => row?.data?.ssmEvaluated,
          contraEvaluatorAgreement: (row: any) => row?.data?.contraEvaluatorAgreement,
        }}
        PaperComponent={Box}
        paperComponentStyles={{
          px: 3,
          py: 2,
          border: '0.0625rem solid rgb(222, 226, 230)',
          borderRadius: 2,
          '& h6': {
            mt: 1,
          },
        }}
        rows={getHeaderRows()}
        smallUntilBreakpoint="md"
        disableOrder
      />
    </>
  );
};

export default MedicalCheckList;
