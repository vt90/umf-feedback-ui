import React, { useMemo } from 'react';
import { ISurvey } from '../../../models/survey';
import { IEvaluation } from '../../../models/evaluation';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import EvaluationSurveyQuestions from '../SurveyQuestions';
import { IExtendedUserDepartmentAssignment } from '../../../models/userDepartmentAssignment';
import EvaluationHeader from '../Header';
import CommentList from '../Comment/List';
import EvaluationSurveyBonifications from '../SurveyBonification';
import EvaluationSurveyObjectives from '../SurveyObjectives';

interface IEvaluationResultProps {
  canEdit?: boolean;
  evaluation: IEvaluation;
  hasChanges?: boolean;
  onChangeBonifications?: (bonificationId: string, value: number) => void;
  onChangeEvaluationResult?: (questionId: string, value: number) => void;
  onChangeObjectiveResult?: (questionId: string, value: number) => void;
  survey: ISurvey;
  userDepartmentAssignment: IExtendedUserDepartmentAssignment;
}

const EvaluationResult = (props: IEvaluationResultProps) => {
  const {
    canEdit,
    evaluation,
    hasChanges,
    onChangeBonifications,
    onChangeEvaluationResult,
    onChangeObjectiveResult,
    survey,
    userDepartmentAssignment,
  } = props;

  const surveyChapterResults = useMemo(() => {
    return evaluation?.surveyChapterResults?.reduce((acc, cur) => {
      return {
        ...acc,
        // @ts-ignore
        [cur.surveyChapterId]: cur.result,
      };
    }, {});
  }, [evaluation]);

  const evaluationResult = useMemo(() => {
    return (
      evaluation &&
      evaluation.questionResults &&
      evaluation.questionResults.reduce((acc, result) => {
        // @ts-ignore
        acc[result.questionId] = result.result;

        return acc;
      }, {})
    );
  }, [evaluation]);

  const objectiveResult = useMemo(() => {
    return (
      evaluation &&
      evaluation.objectiveResult &&
      evaluation.objectiveResult.reduce((acc, result) => {
        // @ts-ignore
        acc[result.objectiveId] = result.result;

        return acc;
      }, {})
    );
  }, [evaluation]);

  const bonifications = useMemo(() => {
    return (
      evaluation &&
      evaluation.bonifications &&
      evaluation.bonifications.reduce((acc, result) => {
        // @ts-ignore
        acc[result.bonificationId] = result.result;

        return acc;
      }, {})
    );
  }, [evaluation]);

  return (
    <>
      <Box mb={3}>
        <EvaluationHeader
          evaluation={evaluation}
          hasChanges={hasChanges}
          survey={survey}
          userDepartmentAssignment={userDepartmentAssignment}
        />
      </Box>

      {evaluationResult && (
        <EvaluationSurveyObjectives
          showResults={true}
          objectiveResult={objectiveResult}
          survey={survey}
          onChangeEvaluationObjectiveResult={canEdit ? onChangeObjectiveResult : undefined}
        />
      )}

      {evaluationResult && (
        <EvaluationSurveyQuestions
          evaluationResult={evaluationResult}
          survey={survey}
          surveyChapterResults={surveyChapterResults}
          onChangeEvaluationResult={canEdit ? onChangeEvaluationResult : undefined}
        />
      )}

      {bonifications && (
        <EvaluationSurveyBonifications
          bonifications={bonifications}
          survey={survey}
          onChangeBonifications={canEdit ? onChangeBonifications : undefined}
          showSum={true}
        />
      )}

      <CommentList evaluation={evaluation} />

      <Button id="evaluationSubmit" type="submit" sx={{ display: 'none' }} />
    </>
  );
};

export default EvaluationResult;
