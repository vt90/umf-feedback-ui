import React from 'react';
import dynamic from 'next/dynamic';
const ReactJson = dynamic(() => import('react-json-view'), {
  ssr: false,
});

interface IJSONViewerProps {
  data: any;
}

const JSONViewer = (props: IJSONViewerProps) => {
  const { data } = props;

  return <ReactJson src={data} />;
};

export default JSONViewer;
