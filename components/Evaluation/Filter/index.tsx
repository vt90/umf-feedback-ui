import React from 'react';
import Box from '@mui/material/Box';
import Grid from '@mui/material/Grid';
import MenuItem from '@mui/material/MenuItem';
import Select from '@mui/material/Select';
import Typography from '@mui/material/Typography';
import { IDepartment } from '../../../models/department';
import { IEvaluationSession } from '../../../models/evaluationSession';
import { getSurveyRoleName, SURVEY_ROLE } from '../../../models/survey';
// import { USER_DEP_ASSIGNMENT_TYPE } from '../../../models/userDepartmentAssignment';

interface IEvaluationsFilterProps {
  activeView: string | null;
  order: number;
  setOrder: (newOrder: number) => void;
  orderBy: string;
  setOrderBy: (newOrderBy: string) => void;
  department: string | null;
  departments: IDepartment[] | undefined;
  setDepartment: (newOrderBy: string | null) => void;
  evaluationSessionId: string | null;
  evaluationSessions: IEvaluationSession[] | undefined;
  setEvaluationSessionId: (newOrderBy: string | null) => void;
  role: string | null;
  setRole: (newRole: string | null) => void;
  userAgreement: string | null;
  setUserAgreement: (newRole: string | null) => void;
  contraEvaluated: string | null;
  setContraEvaluated: (newRole: string | null) => void;
  userAcknowledged: string | null;
  setUserAcknowledged: (newRole: string | null) => void;
  email: string | null;
  setEmail: (newRole: string | null) => void;
  minGrade: string | null;
  setMinGrade: (newRole: string | null) => void;
  finalResult: string | null;
  setFinalResult: (newRole: string | null) => void;
  userDepartmentAssignmentType: string | null;
  setUserDepartmentAssignmentType: (newRole: string | null) => void;
}

interface SelectOption {
  name: string;
  value: string;
}

interface SelectProps {
  label: string;
  value: string | null;
  disabled?: boolean;
  onChange: (value: string | null) => void;
  options?: SelectOption[];
  hideAll?: boolean;
}
export const RenderSelectField = (props: SelectProps) => {
  const { hideAll, label, value, onChange, options, disabled } = props;

  return (
    <>
      <Typography variant="body2" color="textSecondary">
        {label}
      </Typography>
      <Select
        value={value}
        onChange={(ev) => onChange(ev.target.value)}
        size="small"
        displayEmpty
        disabled={!!disabled}
        sx={{
          boxShadow: 'none',
          maxWidth: '100%',
          '.MuiOutlinedInput-notchedOutline': {
            border: 0,
            outline: 'none',
          },
        }}
      >
        {hideAll ? null : (
          <MenuItem value="">
            <i>Toate</i>
          </MenuItem>
        )}

        {options?.map((option) => (
          <MenuItem key={option.value} value={option.value}>
            {option.name}
          </MenuItem>
        ))}
      </Select>
    </>
  );
};

const EvaluationsFilter = (props: IEvaluationsFilterProps) => {
  const {
    activeView,
    departments,
    department,
    setDepartment,
    // userDepartmentAssignmentType,
    // setUserDepartmentAssignmentType,
    evaluationSessions,
    evaluationSessionId,
    setEvaluationSessionId,
    role,
    setRole,
    userAgreement,
    setUserAgreement,
    contraEvaluated,
    setContraEvaluated,
    userAcknowledged,
    setUserAcknowledged,
    minGrade,
    setMinGrade,
    finalResult,
    setFinalResult,
  } = props;

  const isEvalReports = activeView === 'reports';

  return (
    <>
      <Box
        sx={{
          display: 'flex',
          alignItems: 'center',
          flexWrap: 'wrap',
          mb: 2,
          borderColor: 'divider',
          borderRightStyle: 'solid',
          borderRightWidth: 1,
        }}
      >
        <Grid container spacing={2}>
          <Grid item xs={12}>
            <Typography variant="body1">
              <strong>Filtrează</strong>
            </Typography>
          </Grid>
          <Grid item xs={12}>
            <RenderSelectField
              hideAll={true}
              label="Sesiune de evaluare"
              value={evaluationSessionId}
              onChange={setEvaluationSessionId}
              options={evaluationSessions?.map((evaluation) => ({
                value: evaluation._id,
                name: evaluation.name,
              }))}
            />
          </Grid>

          <Grid item xs={12}>
            <RenderSelectField
              label="Departament"
              value={department}
              onChange={setDepartment}
              options={departments?.map((department) => ({
                value: department._id,
                name: department.name,
              }))}
            />
          </Grid>

          {/*<Grid item xs={12}>*/}
          {/*  <RenderSelectField*/}
          {/*    label="Rol in departament"*/}
          {/*    value={userDepartmentAssignmentType}*/}
          {/*    onChange={setUserDepartmentAssignmentType}*/}
          {/*    disabled={!isEvalReports}*/}
          {/*    options={Object.values(USER_DEP_ASSIGNMENT_TYPE).map((role) => ({*/}
          {/*      value: role,*/}
          {/*      name: role,*/}
          {/*    }))}*/}
          {/*  />*/}
          {/*</Grid>*/}

          <Grid item xs={12}>
            <RenderSelectField
              label="Tipul Evaluării"
              value={role}
              onChange={setRole}
              disabled={!isEvalReports}
              options={Object.values(SURVEY_ROLE).map((role) => ({
                value: role,
                name: getSurveyRoleName(role),
              }))}
            />
          </Grid>

          <Grid item xs={12}>
            <RenderSelectField
              label="Opinie evaluat"
              value={userAgreement}
              onChange={setUserAgreement}
              disabled={!isEvalReports}
              options={[
                { name: 'In așteptare', value: 'pending' },
                { name: 'Acceptată', value: 'true' },
                { name: 'Respinsă', value: 'false' },
              ]}
            />
          </Grid>

          <Grid item xs={12}>
            <RenderSelectField
              label="Contra-semntată"
              value={contraEvaluated}
              disabled={!isEvalReports}
              onChange={setContraEvaluated}
              options={[
                { name: 'Da', value: 'true' },
                { name: 'Nu', value: 'false' },
              ]}
            />
          </Grid>

          <Grid item xs={12}>
            <RenderSelectField
              label="Luată la cunoștință"
              value={userAcknowledged}
              disabled={!isEvalReports}
              onChange={setUserAcknowledged}
              options={[
                { name: 'Da', value: 'true' },
                { name: 'Nu', value: 'false' },
              ]}
            />
          </Grid>

          <Grid item xs={12}>
            <RenderSelectField
              label="Nota minimă"
              value={minGrade}
              onChange={setMinGrade}
              disabled={!isEvalReports}
              options={[
                { name: '1', value: '1' },
                { name: '2', value: '2' },
                { name: '3', value: '3' },
                { name: '4', value: '4' },
                { name: '5', value: '5' },
              ]}
            />
          </Grid>

          <Grid item xs={12}>
            <RenderSelectField
              label="Rezultat final"
              value={finalResult}
              onChange={setFinalResult}
              disabled={!isEvalReports}
              options={[
                'Nesatisfăcător',
                'Satisfăcător',
                'Bun',
                'Foarte bun',
                'Cuantum 15%',
                'Cuantum 30%',
              ].map((value) => ({
                name: value,
                value,
              }))}
            />
          </Grid>
        </Grid>
      </Box>
    </>
  );
};

export default EvaluationsFilter;
