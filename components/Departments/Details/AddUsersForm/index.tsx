import React, { FormEvent, useState } from 'react';
import { useMutation } from '@tanstack/react-query';
import Button from '@mui/lab/LoadingButton';
import Box from '@mui/material/Box';
import Search from '@mui/icons-material/Search';
import { IUser } from '../../../../models/user';
import Dialog from '../../../Common/Dialog';
import TextField from '../../../Common/FormInputs/TextField';
import NoDataAvailable from '../../../Common/NoDataAvailable';
import UserSelectionList from '../../../Users/SelectionList';
import { bulkAssignUsersToDepartment } from '../../../../services/userDepartmentAssignment';
import { findUsers } from '../../../../services/users';

interface IDepartmentAddUsersFormProps {
  departmentId: string;
  onUsersAdded: () => void;
  onBulkAssignUsers?: (users: IUser[]) => void;
}

const DepartmentAddUsersForm = (props: IDepartmentAddUsersFormProps) => {
  const [searchUsersTerm, setSearchUsersTerm] = useState('');
  const [showUsersDialog, setShowUsersDialog] = useState(false);
  const [users, setUsers] = useState<IUser[]>([]);
  const [selectedUsers, setSelectedUsers] = useState<IUser[]>([]);
  const { departmentId, onUsersAdded, onBulkAssignUsers } = props;

  const { isLoading, mutate: searchUsers } = useMutation(findUsers, {
    onSuccess: (data) => {
      if (data?.results) {
        const possibleUsersForAssignment = data.results.filter((result) => {
          const isAlreadyAssigned = result?.userDepartmentAssignments?.find(
            (assignment) => assignment.departmentId === departmentId,
          );

          return !isAlreadyAssigned;
        });

        setUsers(possibleUsersForAssignment);
      }
      setShowUsersDialog(true);
    },
  });

  const handleBulkAssignUsers = () => {
    onReset();
    onUsersAdded();

    if (onBulkAssignUsers) {
      onBulkAssignUsers(selectedUsers);
    }
  };

  const { isLoading: isAdding, mutate: bulkAssignUsers } = useMutation(
    bulkAssignUsersToDepartment,
    {
      onSuccess: handleBulkAssignUsers,
    },
  );

  const onSearchUsers = async () => {
    searchUsers({
      searchTerm: searchUsersTerm,
      limit: 1000,
    });
  };

  const onReset = () => {
    setSearchUsersTerm('');
    setUsers([]);
    setSelectedUsers([]);
    setShowUsersDialog(false);
  };

  return (
    <>
      <Box
        sx={{
          bgcolor: 'background.default',
          p: 3,
          display: 'flex',
          alignItems: 'center',
        }}
      >
        <Box sx={{ flexGrow: 1, mr: 2, maxWidth: { xs: '100%', md: '50%' } }}>
          <TextField
            placeholder="Adaugă membru"
            value={searchUsersTerm}
            size="small"
            // @ts-ignore
            onChange={(ev) => setSearchUsersTerm(ev.target.value)}
          />
        </Box>

        <Box>
          <Button
            variant="contained"
            onClick={onSearchUsers}
            size="large"
            endIcon={<Search />}
            loading={isLoading}
          >
            Caută
          </Button>
        </Box>
      </Box>

      {showUsersDialog ? (
        <Dialog
          maxWidth="md"
          content={
            <div>
              {users?.length ? (
                <UserSelectionList
                  users={users}
                  selectedUsers={selectedUsers}
                  onSelectUser={(user) => setSelectedUsers([...selectedUsers, user])}
                  onRemoveUser={(index) => {
                    const newSelectedUsers = [...selectedUsers];
                    newSelectedUsers.splice(index, 1);

                    setSelectedUsers(newSelectedUsers);
                  }}
                />
              ) : (
                <NoDataAvailable content="Nu s-a găsit niciun utilizator comform căutării" />
              )}
            </div>
          }
          title="Utilizatori"
          onClose={onReset}
          open={true}
          actions={
            <Button
              onClick={() => {
                if (onBulkAssignUsers) {
                  handleBulkAssignUsers();
                } else {
                  bulkAssignUsers({
                    userIDs: selectedUsers.map((user) => user._id),
                    departmentId,
                  });
                }
              }}
              loading={isAdding}
              variant="contained"
              disabled={!selectedUsers?.length}
            >
              Adaugă
            </Button>
          }
        />
      ) : null}
    </>
  );
};

export default DepartmentAddUsersForm;
