import { unstable_getServerSession } from 'next-auth/next';
import { authOptions } from '../pages/api/auth/[...nextauth]';
import { NextApiRequest, NextApiResponse, NextPageContext } from 'next';

export const withPageAuthRequired = () => async (context: NextPageContext) => {
  const { req, res } = context;

  const session = await unstable_getServerSession(
    // @ts-ignore
    req,
    res,
    authOptions,
  );

  const validSession = !!session; // && session.expires && moment().diff(moment(session?.expires)) > 0;

  if (!validSession) {
    return {
      redirect: {
        destination: `/sign-in`,
        permanent: false,
      },
    };
  }

  return {
    props: {
      user: session?.user,
    },
  };
};

export const withApiAuthRequired =
  (apiRoute: any) =>
  async (req: NextApiRequest, res: NextApiResponse): Promise<void> => {
    const session = await unstable_getServerSession(
      // @ts-ignore
      req,
      res,
      authOptions,
    );

    if (!(session && session.user)) {
      res.status(401).json({
        error: 'not_authenticated',
        description: 'The user does not have an active session or is not authenticated',
      });
      return;
    }

    await apiRoute(req, res);
  };
