import { IBaseModel, IPagination } from './base';
import { IUserDepartmentAssignment } from './userDepartmentAssignment';
import { IDepartment } from './department';

export enum UserType {
  User = 'USER',
  Admin = 'ADMIN',
}

export interface IUser extends IBaseModel {
  email: string;
  fullName: string;
  userType: UserType;
  canViewAllReviews: boolean;
  canEditAllReviews: boolean;
  canManageUsers: boolean;
  canManageDepartments: boolean;
  canManageSessions: boolean;
  allowSalaryChange: boolean;
  isSSM: boolean;
  isExternal: boolean;
  password?: string;
  userDepartmentAssignments?: IUserDepartmentAssignment[];
  departmentsInManagement?: IDepartment[];
  searchTerm?: string;
}

export interface IFindUsers extends IUser, IPagination {}

export const isAdmin = (user: IUser): boolean => user.userType === UserType.Admin;
export const isUser = (user: IUser): boolean => user.userType === UserType.User;
