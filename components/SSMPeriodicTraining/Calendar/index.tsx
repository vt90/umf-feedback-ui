import { ISSMPeriodicTraining } from '../../../models/ssmPeriodicTraining';
import Box from '@mui/material/Box';
import ButtonBase from '@mui/material/ButtonBase';
import Checkbox from '@mui/material/Checkbox';
import FormControlLabel from '@mui/material/FormControlLabel';
import Grid from '@mui/material/Grid';
import Typography from '@mui/material/Typography';
import React, { useState, useMemo } from 'react';

import { TRAINING_MONTHS } from '../../../models/ssmPeriodicTraining';
import { COLORS } from '../../../lib/theme';

interface ISSMPeriodicTrainingCalendar {
  ssmPeriodicTrainings: ISSMPeriodicTraining[];
  onEdit?: (ssmPeriodicTraining: ISSMPeriodicTraining) => void;
}

const SSMPeriodicTrainingCalendar = (props: ISSMPeriodicTrainingCalendar) => {
  const { ssmPeriodicTrainings, onEdit } = props;
  const [onlyActive, setOnlyActive] = useState(true);

  const groupedByMonth = useMemo(() => {
    return ssmPeriodicTrainings.reduce((acc, cur, currentIndex) => {
      if (onlyActive && !cur.isActive) return acc;

      const color = COLORS[currentIndex % COLORS.length];

      for (const month of cur.monthsOfYear) {
        // @ts-ignore
        if (!acc[month]) acc[month] = [];

        // @ts-ignore
        acc[month].push({ ...cur, color });
      }

      return acc;
    }, {});
  }, [ssmPeriodicTrainings, onlyActive]);

  return (
    <Grid container spacing={3} alignItems="stretch">
      <Grid item xs={12}>
        <FormControlLabel
          control={<Checkbox checked={onlyActive} onChange={() => setOnlyActive(!onlyActive)} />}
          label="Doar instruiri active"
        />
      </Grid>
      {TRAINING_MONTHS.map((month) => {
        // @ts-ignore
        const monthTrainings = groupedByMonth?.[month];

        return (
          <Grid key={month} item xs={2} sm={4} md={2}>
            <Box
              sx={{
                border: '0.0625rem solid rgb(222, 226, 230)',
                borderRadius: 2,
                pt: 1,
                height: '100%',
                overflow: 'hidden',
                minHeight: 100,
              }}
            >
              <Typography sx={{ mb: 1, px: 1, fontWeight: 100 }}>{month}</Typography>

              {monthTrainings?.map((ssmTraining: ISSMPeriodicTraining) => (
                // @ts-ignore
                <ButtonBase
                  key={ssmTraining._id}
                  sx={{
                    // @ts-ignore
                    color: `${ssmTraining.color}`,
                    position: 'relative',
                    bgcolor: 'transparent',
                    width: '100%',
                    typography: 'caption',
                    textAlign: 'left',
                    justifyContent: 'flex-start',
                    my: 0,
                    px: 1,
                    py: 0.75,
                    // opacity: ssmTraining.isActive ? 1 : 0.6,
                    ...(ssmTraining.isActive && {
                      textShadow: '0.1px 0.1px 0.1px black',
                    }),
                    '& :after': {
                      content: '""',
                      position: 'absolute',
                      width: '100%',
                      height: '100%',
                      top: 0,
                      left: 0,
                      opacity: 0.3,
                      // @ts-ignore
                      bgcolor: `${ssmTraining.color}`,
                      zIndex: 1,
                    },
                  }}
                  onClick={() => onEdit && onEdit(ssmTraining)}
                >
                  {ssmTraining.name}
                </ButtonBase>
              ))}
            </Box>
          </Grid>
        );
      })}
    </Grid>
  );
};

export default SSMPeriodicTrainingCalendar;
