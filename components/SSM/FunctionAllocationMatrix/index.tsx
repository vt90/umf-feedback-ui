import React, { useState, useEffect, useMemo, useCallback } from 'react';
import Box from '@mui/material/Box';
import Checkbox from '@mui/material/Checkbox';
import FormControlLabel from '@mui/material/FormControlLabel';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Typography from '@mui/material/Typography';
import { IFunctionModel } from '../../../models/function';

interface IFunctionAllocationColumnInfo {
  name: string;
  functions: string[];
  _id: string;
}

interface ISSMFunctionAllocationMatrixProps {
  functions: IFunctionModel[];
  columns: IFunctionAllocationColumnInfo[];
  onColumnClick: (column: IFunctionAllocationColumnInfo) => void;
  onCheckboxCLick: (
    column: IFunctionAllocationColumnInfo,
    newFunctionAllocationValue: string[],
  ) => void;
}

import Button from '@mui/material/Button';
import Menu from '@mui/material/Menu';
import MenuItem from '@mui/material/MenuItem';

interface ISelectMenuProps {
  column: IFunctionAllocationColumnInfo;
  [key: string]: any;
}

function SelectMenu(props: ISelectMenuProps) {
  const {
    column,
    isLoading,
    setIsLoading,
    hasAllFunctionsSelected,
    isAllLeadFunctions,
    isAllNotLeadFunctions,
    onSelectAllClick,
    onSelectFunctionTypeClick,
  } = props;
  const [anchorEl, setAnchorEl] = useState<null | HTMLElement>(null);
  const open = Boolean(anchorEl);

  const handleClick = (event: React.MouseEvent<HTMLButtonElement>) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };

  return (
    <>
      <Button onClick={handleClick} disabled={isLoading === column._id} color="inherit">
        Selecteaza
      </Button>
      <Menu anchorEl={anchorEl} open={open} onClose={handleClose}>
        <MenuItem>
          <FormControlLabel
            control={
              <Checkbox
                checked={hasAllFunctionsSelected}
                onChange={(ev) => {
                  setIsLoading(column._id);
                  onSelectAllClick(column, ev.target.checked);
                  handleClose();
                }}
              />
            }
            label="Selecteaza toate"
            sx={{
              my: 0,
              mr: 0,
              width: 'calc(100% + 11px)',
            }}
          />
        </MenuItem>
        <MenuItem>
          <FormControlLabel
            control={
              <Checkbox
                checked={!!isAllLeadFunctions}
                onChange={(ev) => {
                  setIsLoading(column._id);
                  onSelectFunctionTypeClick(column, true, ev.target.checked);
                  handleClose();
                }}
              />
            }
            label="Doar functii de conducere"
            sx={{
              my: 0,
              mr: 0,
              width: 'calc(100% + 11px)',
            }}
          />
        </MenuItem>
        <MenuItem>
          <FormControlLabel
            control={
              <Checkbox
                checked={!!isAllNotLeadFunctions}
                onChange={(ev) => {
                  setIsLoading(column._id);
                  onSelectFunctionTypeClick(column, false, ev.target.checked);
                  handleClose();
                }}
              />
            }
            label="Doar functii de executie"
            sx={{
              my: 0,
              mr: 0,
              width: 'calc(100% + 11px)',
            }}
          />
        </MenuItem>
      </Menu>
    </>
  );
}

const SSMFunctionAllocationMatrix = (props: ISSMFunctionAllocationMatrixProps) => {
  const { functions, columns, onColumnClick, onCheckboxCLick } = props;
  const [isLoading, setIsLoading] = useState<string | null>(null);

  useEffect(() => {
    setIsLoading(null);
  }, [columns]);

  // const isLeadGroup = useMemo(() => {
  //   return functions.reduce(
  //     (acc, cur) => {
  //       // @ts-ignore
  //       acc[`${cur.isLead}`].push(cur);
  //
  //       return acc;
  //     },
  //     {
  //       true: [],
  //       false: [],
  //     },
  //   );
  // }, [functions]);

  const isLeadGroup = {
    true: functions,
  };

  // @ts-ignore
  const [allIsLeadFunctions, allNotIsLeadFunctions] = useMemo(() => {
    return functions.reduce((acc, cur) => {
      if (!acc.length) {
        // @ts-ignore
        acc.push([]);

        // @ts-ignore
        acc.push([]);
      }

      if (cur.isLead) {
        // @ts-ignore
        acc[0].push(cur._id);
      } else {
        // @ts-ignore
        acc[1].push(cur._id);
      }

      return acc;
    }, []);
  }, [functions]);

  const onIndividualFunctionClick = useCallback(
    (
      column: IFunctionAllocationColumnInfo,
      functionModel: IFunctionModel,
      newCheckboxValue: boolean,
    ) => {
      if (newCheckboxValue) {
        column.functions.push(functionModel._id);
      } else {
        const functionIndex = column.functions.findIndex((f) => f === functionModel._id);
        column.functions.splice(functionIndex, 1);
      }

      onCheckboxCLick(column, column.functions);
    },
    [onCheckboxCLick],
  );

  const onSelectAllClick = useCallback(
    (column: IFunctionAllocationColumnInfo, newCheckboxValue: boolean) => {
      const newFunctionsValue = newCheckboxValue ? functions.map((f) => f._id) : [];

      onCheckboxCLick(column, newFunctionsValue);
    },
    [functions, onCheckboxCLick],
  );

  const onSelectFunctionTypeClick = useCallback(
    (column: IFunctionAllocationColumnInfo, isLeadType: boolean, newCheckboxValue: boolean) => {
      const selectedLeadTypeFunctions = functions.filter((f) => f.isLead === isLeadType);
      const selectedLeadTypeFunctionsIds = selectedLeadTypeFunctions.map((f) => f._id);

      const newFunctionsValue = newCheckboxValue
        ? selectedLeadTypeFunctionsIds
        : column.functions.filter((f) => !selectedLeadTypeFunctionsIds.includes(f));

      onCheckboxCLick(column, newFunctionsValue);
    },
    [functions, onCheckboxCLick],
  );

  return (
    <Box>
      <TableContainer sx={{ maxHeight: 'calc(100vh - 100px)' }}>
        <Table stickyHeader>
          <TableHead>
            <TableRow sx={{ verticalAlign: 'bottom' }}>
              <TableCell sx={{ bgcolor: 'background.paper' }} />

              {columns.map((column) => {
                const hasAllFunctionsSelected = column?.functions?.length === functions?.length;
                // @ts-ignore;
                const leadDiff = column.functions.filter((f) => !allIsLeadFunctions.includes(f));
                // @ts-ignore;
                const notLeadDiff = column.functions.filter(
                  // @ts-ignore;
                  (f) => !allNotIsLeadFunctions.includes(f),
                );
                const isAllLeadFunctions =
                  column.functions.length &&
                  // @ts-ignore;
                  column.functions.length === allIsLeadFunctions?.length &&
                  !leadDiff.length;
                const isAllNotLeadFunctions =
                  column.functions.length &&
                  // @ts-ignore;
                  column.functions.length === allNotIsLeadFunctions?.length &&
                  !notLeadDiff.length;

                return (
                  <TableCell
                    key={column.name}
                    sx={{ minWidth: 200, maxWidth: 200, bgcolor: 'background.paper' }}
                    align="center"
                  >
                    <Typography
                      onClick={() => {
                        const columnData = { ...column };
                        // @ts-ignore
                        delete columnData.displayName;

                        onColumnClick(columnData);
                      }}
                      sx={{ color: 'primary.main', textDecoration: 'underline', cursor: 'pointer' }}
                    >
                      {/* @ts-ignore */}
                      {column.displayName || column.name}
                    </Typography>

                    <SelectMenu
                      column={column}
                      isLoading={isLoading}
                      setIsLoading={setIsLoading}
                      hasAllFunctionsSelected={hasAllFunctionsSelected}
                      isAllLeadFunctions={isAllLeadFunctions}
                      isAllNotLeadFunctions={isAllNotLeadFunctions}
                      onSelectAllClick={onSelectAllClick}
                      onSelectFunctionTypeClick={onSelectFunctionTypeClick}
                    />
                  </TableCell>
                );
              })}
            </TableRow>
          </TableHead>
          <TableBody>
            {/* {Object.entries(isLeadGroup).map(([_, functions]) => {*/}
            {Object.values(isLeadGroup).map((functions) => {
              return [
                // <TableRow key={key}>
                //   <TableCell
                //     colSpan={columns?.length + 1}
                //     sx={{ borderColor: 'transparent' }}
                //   >
                //     <Typography color="textSecondary">
                //       {key === 'true' ? 'Functii de conducere' : 'Functii de executie'}
                //     </Typography>
                //   </TableCell>
                // </TableRow>,
                ...functions.map((functionModel: IFunctionModel) => {
                  return (
                    <TableRow hover role="checkbox" tabIndex={-1} key={functionModel._id}>
                      <TableCell>
                        <Typography>
                          {functionModel.code} - {functionModel.name}
                        </Typography>
                      </TableCell>

                      {columns.map((column, index) => {
                        const checked = column.functions.find((f) => f === functionModel._id);
                        const loadingLabel = `${functionModel._id}${column._id}`;
                        return (
                          <TableCell key={index} align="center">
                            <Checkbox
                              checked={!!checked}
                              disabled={isLoading === loadingLabel}
                              onChange={(ev) => {
                                setIsLoading(loadingLabel);
                                onIndividualFunctionClick(column, functionModel, ev.target.checked);
                              }}
                            />
                          </TableCell>
                        );
                      })}
                    </TableRow>
                  );
                }),
              ];
            })}
          </TableBody>
        </Table>
      </TableContainer>
    </Box>
  );
};

export default React.memo(SSMFunctionAllocationMatrix);
