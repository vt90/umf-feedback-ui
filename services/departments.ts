import axios from 'axios';
import { IDepartment, IHierarchyDepartment, IHierarchyTreeItem } from '../models/department';
import get from 'lodash/get';
import { IPaginationResult } from '../models/base';

export const getDepartmentTree = (departments: IDepartment[]) => {
  const hierarchyTree: IHierarchyTreeItem[] = [];
  const ID_DELIMITATION = '-';

  departments
    .sort((a, b) => {
      return a._id.localeCompare(b._id);
    })
    .forEach((department) => {
      const existingDepartment = hierarchyTree.find(
        (item) => department.parentId && item._id.includes(department.parentId),
      );

      const itemId = `${existingDepartment ? `${existingDepartment._id}${ID_DELIMITATION}` : ''}${
        department._id
      }`;

      hierarchyTree.push({
        _id: itemId,
        name: department.name,
        department,
      });
    });

  // @ts-ignore
  const hierarchy: IHierarchyDepartment = {};

  hierarchyTree.forEach((item) => {
    const nodes = item._id.split(ID_DELIMITATION);
    const itemId = nodes.pop(); // remaining nodes will provide the elements path in the tree

    let nodeRoot = hierarchy;

    if (nodes?.length) {
      nodeRoot = get(hierarchy, nodes.map((node) => `${node}.children`).join('.'));
    }

    // @ts-ignore
    nodeRoot[itemId] = {
      children: {},
      ...item.department,
      id: itemId,
    };
  });

  return hierarchy;
};

const httpInstance = axios.create({
  baseURL: '/api/departments',
});

httpInstance.interceptors.response.use(
  (response) => response.data,
  (error) => {
    if (error && error.response && error.response.data) {
      return Promise.reject(
        error.response.data.message
          ? `${error.response.data.message || error.response.data.error}`
          : error.response.data?.error || error.response.data,
      );
    } else {
      return Promise.reject({ message: 'Server error' });
    }
  },
);

const cleanupDepartmentBody = (department: IDepartment): IDepartment => {
  // @ts-ignore
  delete department._id;
  // @ts-ignore
  delete department.id;
  // @ts-ignore
  delete department['__v'];

  if (!department.parentId) {
    // @ts-ignore
    delete department.parentId;
  }

  return department;
};

export const findDepartments = async (
  params: Partial<IDepartment>,
): Promise<IPaginationResult<IDepartment>> => {
  return await httpInstance.get('', { params });
};

export const getAllDepartments = async (): Promise<IDepartment[]> => {
  return await httpInstance.get('/all');
};

export const getDepartmentById = async (id: string): Promise<IDepartment> => {
  return await httpInstance.get(`/${id}`);
};

export const createOrUpdateDepartment = async (department: IDepartment): Promise<IDepartment> => {
  const isUpdate = !!department._id;
  const url = isUpdate ? `/${department._id}` : ``;
  const req = isUpdate ? httpInstance.patch : httpInstance.post;
  return await req(url, cleanupDepartmentBody(department));
};

export const deleteDepartment = async (id: string): Promise<any> => {
  return await httpInstance.delete(`/${id}`);
};
