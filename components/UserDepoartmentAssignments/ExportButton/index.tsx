import React from 'react';
import DownloadIcon from '@mui/icons-material/Download';
import { useMutation } from '@tanstack/react-query';
import { exportAllUserDepartmentAssignments } from '../../../services/userDepartmentAssignment';
import LoadingButton from '@mui/lab/LoadingButton';
import { getFunctionName } from '../../../models/function';
import moment from 'moment/moment';
import XLSX from 'sheetjs-style';
import * as FileSaver from 'file-saver';
import { IExtendedUserDepartmentAssignment } from '../../../models/userDepartmentAssignment';
import orderBy from 'lodash/orderBy';

const UserDepartmentAssignmentExportButton = () => {
  const onExport = async (userDepartmentAssignments: IExtendedUserDepartmentAssignment[]) => {
    try {
      const namePrefix = 'Raport general angajati UMF';

      // @ts-ignore
      const excelData = orderBy(userDepartmentAssignments || [], ['userId.fullName'])?.map(
        (data, index) => {
          return {
            '#': index + 1,
            Membru: data.userId.fullName,
            Marca: data.userId.email,
            Departament: data.departmentId.name.replaceAll(',', '‚'),
            Rol: data.role,
            Tip: data.type,
            Funcția: data.jobFunctionInfo ? getFunctionName(data.jobFunctionInfo) : '-',
            'Tip Funcție': data.jobFunctionInfo
              ? data.jobFunctionInfo.isLead
                ? 'Funcție de conducere'
                : 'Funcție de execuție'
              : '-',
            'Cod Loc de Munca': data.jobPlace,
            'Cod Categorie Personal': data.personalCategory,
            'Grad / Treapta': data.studyLevel || '-',
            Studii: data.studyType || '-',
            'Exclude din evaluari': data.excludeFromEvaluations ? 'Da' : '-',
            'Permite managementul sesiunilor de evaluare': data.userId.canManageSessions
              ? 'Da'
              : '-',
            'Permite managementul utilizatorilor': data.userId.canManageUsers ? 'Da' : '-',
            'Access full': data.userId.canEditAllReviews ? 'Da' : '-',
            'Access full read-only (HR Manager)': data.userId.canViewAllReviews ? 'Da' : '-',
            'Permite managementul departamentelor': data.userId.canManageDepartments ? 'Da' : '-',
            'Permite managementul SSM': data.userId.isSSM ? 'Da' : '-',
            'Rol extern': data.userId.isExternal ? 'Da' : '-',
            // 'Angajat dezactivat': data.userId.deactivated ? 'Da' : '-',
          };
        },
      );

      console.log('generating XLSX');
      const fileType =
        'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8';
      const ws = XLSX.utils.json_to_sheet(excelData);
      const wb = { Sheets: { data: ws }, SheetNames: ['data'] };
      const excelBuffer = XLSX.write(wb, { bookType: 'xlsx', type: 'array' });
      const blob = new Blob([excelBuffer], { type: fileType });
      // @ts-ignore
      const fileName = `${namePrefix} ${moment().format('YYYY.MM.DD HH:mm')}.xlsx`;
      FileSaver.saveAs(blob, fileName);
    } catch (e) {
      console.error(e);
    }
  };

  const { isLoading, mutate: exportAll } = useMutation(exportAllUserDepartmentAssignments, {
    onSuccess: async (data) => {
      await onExport(data);
    },
  });

  return (
    <LoadingButton
      onClick={() => exportAll()}
      color="secondary"
      variant="contained"
      endIcon={<DownloadIcon />}
      loading={isLoading}
    >
      Raport General Utilizatori
    </LoadingButton>
  );
};

export default UserDepartmentAssignmentExportButton;
