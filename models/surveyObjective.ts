import { IBaseModel } from './base';

export interface IObjectiveQuestion extends IBaseModel {
  name: string;
  order: number;
  timePercentage: number;
  donePercentage: number;
}

export interface ISurveyObjective extends IBaseModel {
  surveyId: string;
  name: string;
  order: number;
  weight?: number;
  questions?: IObjectiveQuestion[];
}
