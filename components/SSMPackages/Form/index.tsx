import React from 'react';
import * as yup from 'yup';
import { useForm, useFieldArray } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogTitle from '@mui/material/DialogTitle';
import Grid from '@mui/material/Grid';
import Typography from '@mui/material/Typography';
import { getSSMFileDisplayName, ISSMPackage, SSM_PACKAGE_TYPE } from '../../../models/ssmPackage';
import { IFunctionModel } from '../../../models/function';
import TextField from '../../Common/FormInputs/TextField';
import FunctionsSelect from '../../Common/FunctionsSelect';
import FileUploadField from '../../Common/FormInputs/FileUploadField';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemAvatar from '@mui/material/ListItemAvatar';
import Avatar from '@mui/material/Avatar';
import File from '@mui/icons-material/InsertDriveFile';
import ListItemText from '@mui/material/ListItemText';
import ListItemSecondaryAction from '@mui/material/ListItemSecondaryAction';
import IconButton from '@mui/material/IconButton';
import Delete from '@mui/icons-material/Delete';
import { IDepartment } from '../../../models/department';
import DepartmentsSelect from '../../Common/DepartmentsSelect';
import Divider from '@mui/material/Divider';
import InfoIcon from '@mui/icons-material/Info';
import ToggleField from '../../Common/FormInputs/ToggleField';

interface ISSMPackagesFormProps {
  initialValues: Partial<ISSMPackage>;
  functions: IFunctionModel[];
  departments: IDepartment[];
  isLoading: boolean;
  onClose: () => void;
  onDelete: () => void;
  onSubmit: (data: any) => void;
  open: boolean;
}
const schema = yup
  .object({
    name: yup.string().required(),
  })
  .required();

const SSMPackagesForm = (props: ISSMPackagesFormProps) => {
  const { isLoading, departments, functions, initialValues, open, onClose, onDelete, onSubmit } =
    props;
  const {
    register,
    handleSubmit,
    formState: { errors },
    control,
    setValue,
    watch,
  } = useForm({
    defaultValues: initialValues,
    mode: 'onTouched',
    resolver: yupResolver(schema),
    shouldUnregister: true,
  });

  const { fields, remove } = useFieldArray({
    // @ts-ignore
    name: 'documents',
    control,
  });

  // @ts-ignore
  const docs = watch('documents');

  return (
    <Dialog
      open={open}
      fullWidth={true}
      maxWidth="lg"
      keepMounted={false}
      onClose={onClose}
      // @ts-ignore
      PaperComponent="form"
      onSubmit={handleSubmit(onSubmit)}
    >
      <DialogTitle sx={{ typography: 'h5', textAlign: 'center' }}>
        {initialValues?._id ? 'Modifică' : 'Creează'} pachet de instruire
      </DialogTitle>
      <DialogContent>
        <input type="hidden" {...register(`_id`)} />
        <Grid container spacing={3} alignItems="center">
          <Grid item xs={12} md={3}>
            <Typography variant="body2" sx={{ textAlign: { xs: 'left', md: 'right' } }}>
              Rolul pachetului*
            </Typography>
          </Grid>
          <Grid item xs={12} md={8}>
            <ToggleField
              name="type"
              control={control}
              defaultValue={initialValues.type}
              options={[
                { name: 'Instruire periodică', value: SSM_PACKAGE_TYPE.PERIODIC },
                { name: 'Instruire la angajare', value: SSM_PACKAGE_TYPE.AT_HIRE },
              ]}
            />
          </Grid>
          <Grid item xs={12} md={3}>
            <Typography variant="body2" sx={{ textAlign: { xs: 'left', md: 'right' } }}>
              Denumire pachet*
            </Typography>
          </Grid>
          <Grid item xs={12} md={8}>
            <TextField
              required={true}
              disabled={isLoading}
              errorMessage={!!errors.name && 'Denumire necesară'}
              defaultValue={initialValues.name}
              {...register('name')}
            />
          </Grid>
          <Grid item xs={12} md={3}>
            <Typography variant="body2" sx={{ textAlign: { xs: 'left', md: 'right' } }}>
              Functii asociate
            </Typography>
          </Grid>
          <Grid item xs={12} md={8}>
            <FunctionsSelect name="functions" control={control} functions={functions} />
          </Grid>

          <Grid item xs={12}>
            <Divider />
          </Grid>
          <Grid item xs={12} md={3}>
            <Typography
              variant="body2"
              sx={{ textAlign: { xs: 'left', md: 'right' } }}
              gutterBottom
            >
              Pachet specific doar anumitor departamente
            </Typography>
            <Typography
              variant="caption"
              component="p"
              color="textSecondary"
              sx={{ textAlign: { xs: 'left', md: 'right' } }}
            >
              <InfoIcon fontSize="small" /> în cazul în care sunt specificate departamente, pachetul
              se va aplica doar acelor departamente
            </Typography>
          </Grid>
          <Grid item xs={12} md={8}>
            <DepartmentsSelect name="departments" control={control} departments={departments} />
          </Grid>
          <Grid item xs={12} md={3}>
            <Typography
              variant="body2"
              sx={{ textAlign: { xs: 'left', md: 'right' } }}
              gutterBottom
            >
              Pachet exclus anumitor departamente
            </Typography>
            <Typography
              variant="caption"
              component="p"
              color="textSecondary"
              sx={{ textAlign: { xs: 'left', md: 'right' } }}
            >
              <InfoIcon fontSize="small" /> în cazul în care sunt specificate departamente, pachetul
              nu se va aplica persoanelor din acele departamente
            </Typography>
          </Grid>
          <Grid item xs={12} md={8}>
            <DepartmentsSelect
              name="excludedDepartments"
              control={control}
              departments={departments}
            />
          </Grid>

          <Grid item xs={12} md={3} alignSelf="flex-start">
            <Typography variant="body2" sx={{ textAlign: { xs: 'left', md: 'right' } }}>
              Atasare Documente
            </Typography>
          </Grid>
          <Grid item xs={12} md={8}>
            <FileUploadField name="files" control={control} setValue={setValue} multiple />

            {fields ? (
              <List>
                {/* @ts-ignore */}
                {fields?.map((document, index) => (
                  <ListItem key={document.id}>
                    {/* @ts-ignore */}
                    <input type="hidden" {...register(`documents.${index}`)} />
                    <ListItemAvatar>
                      <Avatar>
                        <File />
                      </Avatar>
                    </ListItemAvatar>
                    {/* @ts-ignore */}
                    <ListItemText primary={getSSMFileDisplayName(docs[index])} />
                    <ListItemSecondaryAction>
                      <IconButton onClick={() => remove(index)}>
                        <Delete />
                      </IconButton>
                    </ListItemSecondaryAction>
                  </ListItem>
                ))}
              </List>
            ) : null}
          </Grid>
        </Grid>
      </DialogContent>
      <DialogActions sx={{ py: 2, justifyContent: 'center', bgcolor: 'background.default' }}>
        <Button color="inherit" disabled={isLoading} onClick={onClose}>
          Anulare
        </Button>
        {initialValues?._id ? (
          <Button variant="contained" color="error" onClick={onDelete}>
            Șterge
          </Button>
        ) : null}
        <Button disabled={isLoading} variant="contained" type="submit">
          {initialValues?._id ? 'Modifică' : 'Crează'}
        </Button>
      </DialogActions>
    </Dialog>
  );
};

export default SSMPackagesForm;
