import React from 'react';
import Box from '@mui/material/Box';
import Container from '@mui/material/Container';
import { VIEWPORT_MAX_WIDTH } from '../../../lib/theme';

const AppFooter = () => {
  return (
    <>
      <Box component="footer" sx={{ pt: 6, pb: 8, bgcolor: 'text.primary' }}>
        <Container maxWidth={VIEWPORT_MAX_WIDTH}>
          {/*<Typography variant="caption" textAlign="center" sx={{ color: '#FFFFFF' }}>*/}
          {/*  Copyright © 2022 {process.env.NEXT_PUBLIC_TENANT_NAME}*/}
          {/*</Typography>*/}
        </Container>
      </Box>
    </>
  );
};

export default AppFooter;
