import axios from 'axios';
import {
  IExtendedUserDepartmentAssignment,
  IUserDepartmentAssignment,
  USER_DEP_ASSIGNMENT_ROLES,
  USER_DEP_ASSIGNMENT_TYPE,
} from '../models/userDepartmentAssignment';
import { IPaginationResult } from '../models/base';

const httpInstance = axios.create({
  baseURL: '/api/user-department-assignments',
});

httpInstance.interceptors.response.use(
  (response) => response.data,
  (error) => {
    if (error && error.response && error.response.data) {
      return Promise.reject(
        error.response.data.message
          ? `${error.response.data.message || error.response.data.error}`
          : error.response.data?.error || error.response.data,
      );
    } else {
      return Promise.reject({ message: 'Server error' });
    }
  },
);

const cleanupUserDepartmentAssignmentBody = (
  userDepartmentAssignment: Partial<IUserDepartmentAssignment>,
): Partial<IUserDepartmentAssignment> => {
  // @ts-ignore
  delete userDepartmentAssignment._id;
  // @ts-ignore
  delete userDepartmentAssignment.id;
  // @ts-ignore
  delete userDepartmentAssignment['__v'];

  return userDepartmentAssignment;
};

export const findUserDepartmentAssignments = async (
  params: Partial<IUserDepartmentAssignment>,
): Promise<IPaginationResult<IUserDepartmentAssignment>> => {
  return await httpInstance.get('', { params });
};

export const getAllUserDepartmentAssignments = async (): Promise<IUserDepartmentAssignment[]> => {
  return await httpInstance.get('/all');
};

export const getUserSubUsers = async (): Promise<
  IPaginationResult<IExtendedUserDepartmentAssignment[]>
> => {
  return await httpInstance.get('/sub-users');
};

export const getUserDepartmentAssignmentById = async (
  id: string,
): Promise<IExtendedUserDepartmentAssignment> => {
  return await httpInstance.get(`/${id}`);
};

export const createOrUpdateUserDepartmentAssignment = async (
  userDepartmentAssignment: Partial<IUserDepartmentAssignment>,
): Promise<IUserDepartmentAssignment> => {
  const isUpdate = !!userDepartmentAssignment._id;
  const url = isUpdate ? `/${userDepartmentAssignment._id}` : ``;
  const req = isUpdate ? httpInstance.patch : httpInstance.post;
  return await req(url, cleanupUserDepartmentAssignmentBody(userDepartmentAssignment));
};

interface IBulkAssign {
  userIDs: string[];
  departmentId: string;
  role?: string;
  type?: string;
}

export const bulkAssignUsersToDepartment = async (data: IBulkAssign): Promise<any> => {
  const { userIDs, departmentId, role, type } = data;
  for (const id of userIDs) {
    await createOrUpdateUserDepartmentAssignment({
      userId: id,
      departmentId,
      role: role || USER_DEP_ASSIGNMENT_ROLES.EVALUATEE,
      type: type || USER_DEP_ASSIGNMENT_TYPE.PROF,
    });
  }
};

export const changeDepartmentManagers = async ({
  previousManagerAssignmentId,
  newManagerAssignmentId,
}: {
  previousManagerAssignmentId: string | null;
  newManagerAssignmentId: string;
}): Promise<any> => {
  previousManagerAssignmentId &&
    (await createOrUpdateUserDepartmentAssignment({
      _id: previousManagerAssignmentId,
      role: USER_DEP_ASSIGNMENT_ROLES.EVALUATEE,
    }));

  await createOrUpdateUserDepartmentAssignment({
    _id: newManagerAssignmentId,
    role: USER_DEP_ASSIGNMENT_ROLES.EVALUATOR,
  });
};

export const deleteUserDepartmentAssignment = async (id: string): Promise<any> => {
  return await httpInstance.delete(`/${id}`);
};

export const exportAllUserDepartmentAssignments = async (): Promise<any> => {
  return await httpInstance.get(`/general-report`);
};
