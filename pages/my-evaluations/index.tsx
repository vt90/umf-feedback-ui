import React from 'react';
import Head from 'next/head';
import { withPageAuthRequired } from '../../guards/authRequired';
import { useQuery } from '@tanstack/react-query';
import AdminPage from '../../components/AdminPage';
import { getEvaluations } from '../../services/evaluations';
import { IUser } from '../../models/user';
import ListLoadingIndicator from '../../components/Common/ListLoadingIndcator';
import UserEvaluationsList from '../../components/Evaluation/UserEvaluationsList';

const FETCH_QUERY_KEY = ['my-evaluations'];

interface IMyEvaluationsProps {
  user: IUser;
}

const MyEvaluations = (props: IMyEvaluationsProps) => {
  const { user } = props;

  const { isLoading, data } = useQuery(
    FETCH_QUERY_KEY,
    () => getEvaluations({ userId: user._id }),
    {
      enabled: !!user,
    },
  );

  if (isLoading) return <ListLoadingIndicator />;

  if (!data?.results) return null;

  return (
    <>
      <Head>
        <title>Evaluări</title>
      </Head>

      <AdminPage title="Evaluări primite" pages={[{ url: '/surveys', name: 'Evaluări primite' }]}>
        {/* @ts-ignore */}
        <UserEvaluationsList evaluations={data?.results} />
      </AdminPage>
    </>
  );
};

export const getServerSideProps = withPageAuthRequired();

export default MyEvaluations;
