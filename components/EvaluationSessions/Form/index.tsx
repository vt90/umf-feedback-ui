import React, { Fragment, useEffect } from 'react';
import Button from '@mui/material/Button';
import Grid from '@mui/material/Grid';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogTitle from '@mui/material/DialogTitle';
import Typography from '@mui/material/Typography';
import { Controller, useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import * as yup from 'yup';
import LinearProgress from '@mui/material/LinearProgress';
import { EVALUATION_SESSION_TYPE, IEvaluationSession } from '../../../models/evaluationSession';
import TextField from '../../Common/FormInputs/TextField';
import SelectField from '../../Common/FormInputs/SelectField';
import DatePickerField from '../../Common/FormInputs/DatePickerField';
import YesNoToggle from '../../Common/YesNoToggle';
import InfoIcon from '@mui/icons-material/Info';
import PrivateUsersFieldArray from './privateUsersFieldArray';
import Divider from '@mui/material/Divider';
import { USER_DEP_ASSIGNMENT_TYPE } from '../../../models/userDepartmentAssignment';
import Box from '@mui/material/Box';
import { getFunctionName, IFunctionModel } from '../../../models/function';
import Checkbox from '@mui/material/Checkbox';

// import dynamic from 'next/dynamic';
// const RichText = dynamic(() => import('../../Common/RichText'), {
//   ssr: false,
// });

interface IEvaluationSessionFormProps {
  initialValues: Partial<IEvaluationSession>;
  functions: IFunctionModel[];
  isLoading: boolean;
  onClose: () => void;
  onSubmit: (data: any) => void;
  open: boolean;
}
const schema = yup
  .object({
    type: yup.number().required(),
    name: yup.string().required(),
    // description: yup.string().required(),
    endDate: yup.date().nullable(true),
  })
  .required();

const EvaluationSessionForm = (props: IEvaluationSessionFormProps) => {
  const { functions, initialValues, isLoading, onClose, onSubmit, open } = props;
  const {
    control,
    getValues,
    formState: { errors },
    handleSubmit,
    register,
    watch,
    setValue,
  } = useForm({
    defaultValues: initialValues,
    mode: 'onTouched',
    resolver: yupResolver(schema),
    shouldUnregister: true,
  });

  const allowSalaryChanges = watch('allowSalaryChanges') || getValues('allowSalaryChanges');
  const hasMinimumEvaluationValueForSalaryChange =
    watch('hasMinimumEvaluationValueForSalaryChange') ||
    getValues('hasMinimumEvaluationValueForSalaryChange');
  const privateUsersOnly = watch('privateUsersOnly') || getValues('privateUsersOnly');
  const sessionConfigurationType =
    watch('sessionConfigurationType') || getValues('sessionConfigurationType');

  const configurationFields =
    sessionConfigurationType === 'type'
      ? Object.values(USER_DEP_ASSIGNMENT_TYPE).reduce(
          (acc, cur) => ({
            ...acc,
            [cur]: cur,
          }),
          {},
        )
      : functions.reduce((acc, functionModel) => {
          return {
            ...acc,
            [functionModel._id]: getFunctionName(functionModel),
          };
        }, {});

  useEffect(() => {
    if (sessionConfigurationType) {
      // @ts-ignore
      if (initialValues.sessionConfigurationType === sessionConfigurationType) {
        setValue('configurations', initialValues.configurations);
      } else {
        setValue(
          'configurations',
          Object.keys(configurationFields).reduce(
            (acc, key) => ({
              ...acc,
              [key]: {
                allowEvaluations: true,
                allowSalaryChanges: true,
              },
            }),
            {},
          ),
        );
      }
    }
  }, [sessionConfigurationType, initialValues]);

  return (
    <Dialog
      open={open}
      keepMounted={false}
      onClose={onClose}
      // @ts-ignore
      PaperComponent="form"
      maxWidth="lg"
      fullWidth
      onSubmit={handleSubmit(onSubmit)}
      PaperProps={{
        // @ts-ignore
        noValidate: true,
      }}
    >
      <DialogTitle sx={{ typography: 'h5', textAlign: 'center' }}>
        {initialValues?._id ? 'Modifică' : 'Creează'} sesiune de evaluare
      </DialogTitle>
      <DialogContent sx={{ my: 3 }}>
        <input type="hidden" {...register(`_id`)} />
        <Grid container spacing={2} alignItems="center">
          <Grid item xs={12} md={3}>
            <Typography variant="body2" sx={{ textAlign: { xs: 'left', md: 'right' } }}>
              Sesiune activă
            </Typography>

            <Typography
              variant="caption"
              component="p"
              color="textSecondary"
              sx={{ textAlign: { xs: 'left', md: 'right' } }}
            >
              <InfoIcon fontSize="small" /> O sesiune inactivă nu mai permite evaluări noi
            </Typography>
          </Grid>
          <Grid item xs={12} md={8}>
            <Controller
              name="isOpen"
              control={control}
              defaultValue={initialValues.isOpen}
              render={({ field }) => {
                const { value, onChange } = field;

                return (
                  <>
                    <YesNoToggle
                      value={!!value}
                      onValueChange={onChange}
                      sx={{
                        minHeight: 0,
                        '& button': {
                          py: 0.75,
                        },
                      }}
                    />
                  </>
                );
              }}
            />
          </Grid>
          <Grid item xs={12} md={3}>
            <Typography variant="body2" sx={{ textAlign: { xs: 'left', md: 'right' } }}>
              Denumire*
            </Typography>
          </Grid>
          <Grid item xs={12} md={8}>
            <TextField
              required={true}
              disabled={isLoading}
              size="small"
              defaultValue={initialValues.name}
              errorMessage={!!errors.name && 'Denumirea este necesară'}
              {...register('name')}
            />
          </Grid>
          {/*<Grid item xs={12} md={3} alignSelf="flex-start">*/}
          {/*  <Typography variant="body2" sx={{ textAlign: { xs: 'left', md: 'right' } }}>*/}
          {/*    Descriere**/}
          {/*  </Typography>*/}
          {/*</Grid>*/}
          {/*<Grid item xs={12} md={8}>*/}
          {/*  <Controller*/}
          {/*    render={({ field }) => {*/}
          {/*      const { value, onChange } = field;*/}

          {/*      return (*/}
          {/*        <>*/}
          {/*          <RichText*/}
          {/*            disableStickyToolbar={true}*/}
          {/*            defaultValue={value}*/}
          {/*            onChange={(value) => onChange(value)}*/}
          {/*            sx={{*/}
          {/*              // @ts-ignore*/}
          {/*              border: (theme) => `1px solid ${theme.palette.divider}!important`,*/}
          {/*              boxShadow: 0,*/}
          {/*              '& .ql-container': {*/}
          {/*                border: 'none !important',*/}
          {/*                minHeight: '300px',*/}
          {/*                p: 2,*/}
          {/*              },*/}
          {/*            }}*/}
          {/*          />*/}
          {/*        </>*/}
          {/*      );*/}
          {/*    }}*/}
          {/*    name="description"*/}
          {/*    control={control}*/}
          {/*  />*/}
          {/*</Grid>*/}
          <Grid item xs={12} md={3}>
            <Typography variant="body2" sx={{ textAlign: { xs: 'left', md: 'right' } }}>
              Tipul sesiunii
            </Typography>
          </Grid>
          <Grid item xs={12} md={4}>
            <SelectField
              name="type"
              size="small"
              control={control}
              sx={{
                '& .MuiInputBase-root': {
                  py: 0,
                },
              }}
              errorMessage={!!errors['type'] && `Tipul sesiunnii necesar`}
              options={Object.keys(EVALUATION_SESSION_TYPE).map((option) => ({
                name: option,
                // @ts-ignore
                value: EVALUATION_SESSION_TYPE[option],
              }))}
            />
          </Grid>

          <Grid item xs={12} md={4} />

          {/*<Grid item xs={12} md={3}>*/}
          {/*  <Typography variant="body2" sx={{ textAlign: { xs: 'left', md: 'right' } }}>*/}
          {/*    Se vor complete formulare standard de performanță?*/}
          {/*  </Typography>*/}
          {/*</Grid>*/}
          {/*<Grid item xs={12} md={8}>*/}
          {/*  <Controller*/}
          {/*    name="allowEvaluations"*/}
          {/*    control={control}*/}
          {/*    defaultValue={!!initialValues.allowEvaluations}*/}
          {/*    render={({ field }) => {*/}
          {/*      const { value, onChange } = field;*/}

          {/*      return (*/}
          {/*        <>*/}
          {/*          <YesNoToggle*/}
          {/*            value={!!value}*/}
          {/*            onValueChange={onChange}*/}
          {/*            disabled={!!initialValues?._id}*/}
          {/*          />*/}
          {/*        </>*/}
          {/*      );*/}
          {/*    }}*/}
          {/*  />*/}
          {/*</Grid>*/}
          {/*<Grid item xs={12} md={3}>*/}
          {/*  <Typography variant="body2" sx={{ textAlign: { xs: 'left', md: 'right' } }}>*/}
          {/*    Se vor completa formulare de modificări salariale?*/}
          {/*  </Typography>*/}
          {/*</Grid>*/}
          {/*<Grid item xs={12} md={8}>*/}
          {/*  <Controller*/}
          {/*    name="allowSalaryChanges"*/}
          {/*    control={control}*/}
          {/*    defaultValue={!!initialValues.allowSalaryChanges}*/}
          {/*    render={({ field }) => {*/}
          {/*      const { value, onChange } = field;*/}

          {/*      return (*/}
          {/*        <>*/}
          {/*          <YesNoToggle*/}
          {/*            value={!!value}*/}
          {/*            onValueChange={onChange}*/}
          {/*            disabled={!!initialValues?._id}*/}
          {/*          />*/}
          {/*        </>*/}
          {/*      );*/}
          {/*    }}*/}
          {/*  />*/}
          {/*</Grid>*/}

          <Grid item xs={12} md={3}>
            <Typography variant="body2" sx={{ textAlign: { xs: 'left', md: 'right' } }}>
              Termen limita de finalizare (opţional)
            </Typography>
          </Grid>
          <Grid item xs={12} md={4}>
            <DatePickerField
              name="endDate"
              control={control}
              defaultValue={initialValues.startDate}
              errorMessage={!!errors['endDate'] && `Data invalidă`}
              disablePast={!initialValues._id}
              textFieldProps={{
                size: 'small',
              }}
            />
          </Grid>
          <Grid item xs={12} md={4} />

          <Grid item xs={12}>
            <Divider />
          </Grid>

          <Grid item xs={12} md={3}>
            <Typography variant="body1" sx={{ textAlign: { xs: 'left', md: 'right' } }}>
              <strong>Configurare proces de evaluare</strong>
            </Typography>
          </Grid>

          <Grid item xs={12} md={9} />

          {!!allowSalaryChanges && (
            <>
              <Grid item xs={12} md={3}>
                <Typography variant="body2" sx={{ textAlign: { xs: 'left', md: 'right' } }}>
                  Este modificarea salarială influențată de o notă minimă obtinută la evaluarea de
                  performanță?
                </Typography>
              </Grid>
              <Grid item xs={12} md={9}>
                <Controller
                  name="hasMinimumEvaluationValueForSalaryChange"
                  control={control}
                  defaultValue={!!initialValues.hasMinimumEvaluationValueForSalaryChange}
                  render={({ field }) => {
                    const { value, onChange } = field;

                    return (
                      <>
                        <YesNoToggle
                          value={!!value}
                          onValueChange={onChange}
                          sx={{
                            minHeight: 0,
                            '& button': {
                              py: 0.75,
                            },
                          }}
                        />
                      </>
                    );
                  }}
                />
              </Grid>
            </>
          )}

          {!!(allowSalaryChanges && hasMinimumEvaluationValueForSalaryChange) && (
            <>
              <Grid item xs={12} md={3}>
                <Typography variant="body2" sx={{ textAlign: { xs: 'left', md: 'right' } }}>
                  Nota minimă
                </Typography>
              </Grid>
              <Grid item xs={12} md={4}>
                <TextField
                  required={true}
                  disabled={isLoading}
                  size="small"
                  errorMessage={
                    !!errors.minimumEvaluationValueForSalaryChange && 'Nota minimă necesară'
                  }
                  defaultValue={initialValues.minimumEvaluationValueForSalaryChange}
                  inputProps={{
                    type: 'number',
                  }}
                  {...register('minimumEvaluationValueForSalaryChange')}
                />
              </Grid>

              <Grid item xs={12} md={5} />
            </>
          )}

          <Grid item xs={12} md={3}>
            <Typography variant="body2" sx={{ textAlign: { xs: 'left', md: 'right' } }}>
              Model de configurare
            </Typography>
          </Grid>

          <Grid item xs={12} md={4}>
            <SelectField
              name="sessionConfigurationType"
              control={control}
              disableClearable={true}
              errorMessage={!!errors['type'] && `Tipul sesiunnii necesar`}
              sx={{
                '& .MuiInputBase-root': {
                  py: 0,
                },
              }}
              options={[
                { name: 'Tip Alocare in Departament', value: 'type' },
                { name: 'Funcție in Departament', value: 'jobFunction' },
              ]}
            />
          </Grid>

          <Grid item xs={12} md={5} />

          <Grid item xs={12}>
            <br />
          </Grid>

          <Grid item xs={4} md={3}>
            <Typography variant="body2" sx={{ textAlign: { xs: 'left', md: 'right' } }}>
              {sessionConfigurationType === 'type' ? 'Tip Alocare' : 'Funcție'}
            </Typography>
          </Grid>
          <Grid item xs={4} md={3}>
            <Typography variant="body2" sx={{ textAlign: { xs: 'left', md: 'right' } }}>
              Se va realiza evaluarea de performanță
            </Typography>
          </Grid>
          <Grid item xs={4} md={3}>
            <Typography variant="body2" sx={{ textAlign: { xs: 'left', md: 'right' } }}>
              Va fi posibilă modificare salarială
            </Typography>
          </Grid>
          <Grid item xs={4} md={3} />

          {Object.entries(configurationFields).map(([type, label]) => (
            <Fragment key={type}>
              <Grid item xs={12} md={3} alignSelf="flex-start">
                <Typography
                  variant="body2"
                  sx={{ mt: 1.5, textAlign: { xs: 'left', md: 'right' } }}
                >
                  {label as string}
                </Typography>
              </Grid>

              <Grid item xs={4} md={3}>
                <Box sx={{ display: 'flex', justifyContent: { xs: 'flex-start', md: 'flex-end' } }}>
                  <Controller
                    name={`configurations.${type}.allowEvaluations`}
                    control={control}
                    defaultValue={!!initialValues?.configurations?.[type]?.allowEvaluations}
                    render={({ field }) => {
                      const { value, onChange } = field;

                      return (
                        <Checkbox
                          checked={!!value}
                          onChange={(ev) => {
                            onChange(ev.target.checked);
                          }}
                        />
                      );
                    }}
                  />
                </Box>
              </Grid>
              <Grid item xs={4} md={3}>
                <Box sx={{ display: 'flex', justifyContent: { xs: 'flex-start', md: 'flex-end' } }}>
                  <Controller
                    name={`configurations.${type}.allowSalaryChanges`}
                    control={control}
                    defaultValue={!!initialValues?.configurations?.[type]?.allowSalaryChanges}
                    render={({ field }) => {
                      const { value, onChange } = field;

                      return (
                        <Checkbox
                          checked={!!value}
                          onChange={(ev) => {
                            onChange(ev.target.checked);
                          }}
                        />
                      );
                    }}
                  />
                </Box>
              </Grid>
              <Grid item xs={4} md={3} />

              <Grid item xs={12}>
                <Divider />
              </Grid>
            </Fragment>
          ))}

          <Grid item xs={12} md={3}>
            <Typography variant="body2" sx={{ textAlign: { xs: 'left', md: 'right' } }}>
              Este sesiunea de evaluare destinată tuturor angajaților?
            </Typography>
          </Grid>
          <Grid item xs={12} md={9}>
            <Controller
              name="privateUsersOnly"
              control={control}
              defaultValue={!!initialValues.privateUsersOnly}
              render={({ field }) => {
                const { value, onChange } = field;

                // we need to reverse the boolean UX value
                const displayValue = !value;

                return (
                  <>
                    <YesNoToggle
                      value={displayValue}
                      onValueChange={(newValue) => onChange(!newValue)}
                      sx={{
                        minHeight: 0,
                        '& button': {
                          py: 0.75,
                        },
                      }}
                    />
                  </>
                );
              }}
            />
          </Grid>

          {privateUsersOnly && (
            <>
              <Grid item xs={12} md={3} alignSelf="flex-start">
                <Typography variant="body2" sx={{ mt: 1, textAlign: { xs: 'left', md: 'right' } }}>
                  Condiție
                </Typography>
              </Grid>

              <Grid item xs={12} md={9}>
                <Controller
                  name="privateUsersOperation"
                  control={control}
                  defaultValue={initialValues.privateUsersOperation}
                  render={({ field }) => {
                    const { value, onChange } = field;

                    const isInclude = value === 'INCLUDE';

                    return (
                      <>
                        <YesNoToggle
                          value={isInclude}
                          yesLabel="Include doar angajații specificați"
                          noLabel="Exclude angajații specificați"
                          onValueChange={(newValue) => onChange(newValue ? 'INCLUDE' : 'EXCLUDE')}
                          sx={{
                            minHeight: 0,
                            '& button': {
                              py: 0.75,
                            },
                          }}
                        />
                      </>
                    );
                  }}
                />
              </Grid>

              <Grid item xs={12} md={3} alignSelf="flex-start">
                <Typography variant="body2" sx={{ textAlign: { xs: 'left', md: 'right' } }}>
                  Angajați
                </Typography>
              </Grid>

              <Grid item xs={12} md={9}>
                <PrivateUsersFieldArray
                  control={control}
                  errors={errors}
                  register={register}
                  initialValues={initialValues}
                />
              </Grid>
            </>
          )}
        </Grid>
      </DialogContent>

      {isLoading && <LinearProgress />}
      <DialogActions sx={{ py: 2, justifyContent: 'center', bgcolor: 'background.default' }}>
        <Button onClick={onClose} disabled={isLoading}>
          Anulare
        </Button>
        <Button variant="contained" type="submit" disabled={isLoading}>
          {initialValues?._id ? 'Modifică' : 'Creează'}
        </Button>
      </DialogActions>
    </Dialog>
  );
};

export default EvaluationSessionForm;
